#!/bin/bash
# ///////////////////////////////////////////////////////////////////////
# Aplicacion : TIRAS AUDITORAS
# Macroaplicacion : TIRAS AUDITORAS
# Nombre del Shell : Zipea_ArchsDiarios.sh
# Tipo de Proceso: En linea. Es un Shell que NO requiere planificacion en Control-M, debido a que puede ser
#                  llemado desde la aplicaci�n en cualquier momento.
# Periocidad : Eventual
# Equipo de Produccion : MXMCVLTIAPRO001
# Dependencias : N/A
# Tiempo de Ejecucion : 1 Min
# Descripcin de funcionalidad general del proceso : Agrupa los archivos de Movimientos y Saldos del Dia y
#                 posteriormente comprime el archivo.
# Base de Datos : N/A
# Tablas que Accesa:  N/A
# Uso de BD en Forma Exclusiva : N/A
# Reprocesable : S
# Accion a Seguir en caso de Reproceso : Ejecutar nuevamente el shell
# Srvr Tuxedo : N/A
# Variables : $1 (fecha en formato ddmmyyyy), $1 (fecha ddmmyyyy) $dia (dia), $mes (mes), $year (a�o)
# Archivos de Entrada: N/A
# Archivo de Salida  : Movimientosddmmyyyy.tar.gz  Saldosddmmyyyy.tar.gz
# Fecha de Creacion :  09 Marzo 2006
# Autor : Juan Carlos P�rez Ramirez
# Fecha de Ultima Modificacion : 08-06-2006
# Autor Ultima Modificacion : Juan Carlos P�rez Ramirez
# Motivo de Ultima Modificacion :
# Fecha de Ultima Modificacion  : 03 Marzo 2014
# Autor Ultima Modificacion     : Alejandro Montes Zarate (FSW LACERTUS)
# Motivo de Ultima Modificacion : Migracion por riesgo tecnologico
#
# /////////////////////////////////////////////////////////////////////////////

export USR_SFTP_BD=gctrtau1
export HOST_BD=DBTATIVLMX17.pre.mx.corp
export USR_TRANSFER_BD_KEY=/home/gctrtau1/.ssh/id_dsa_gcuptaud

export DIR_PROG=/plantiras/procesos/tiras_audit
export DIR_INTERFACES="${DIR_PROG}"/interfaces

export ARCH1=${DIR_PROG}/ListaMovCon.txt
export ARCH2=${DIR_PROG}/ListaSdoCon.txt
export ARCH3=${DIR_PROG}/ControlCon.txt
export COMANDOS=${DIR_PROG}/comand.dats

export SALIDASFTP=${DIR_PROG}/valida.log
export ERRORSFTP=${DIR_PROG}/error.log
dia=$1
d=`echo $dia | cut -c1-2`
m=`echo $dia | cut -c3-4`
y=`echo $dia | cut -c5-8`

#EnviaMail() {

#echo "from:juaperez$DOMINIO_ALTEC" > envia_correo.txt
#echo "to:juaperez$DOMINIO_ALTEC " >> envia_correo.txt
#echo "subject:${HOSTNAME} Proceso $0" >> envia_correo.txt
#echo "msg: No se genero archivo de Movimientos y/o Saldos Diarios" >> envia_correo.txt
#/opt/mailSS/bin/correo envia_correo.txt
#}

ValidaMovimientos(){
if [ -s MovimientosDiarios$d$m$y.tar.gz ] ; then
      echo "Si existe el archivo: " MovimientosDiarios$d$m$y.tar.gz
      echo "TAUD" > $ARCH3
      cp MovimientosDiarios$d$m$y.tar.gz $DIR_PROG/MovimientosDiarios$d$m$y.tar.gz
      RESP=$?
      if [ $RESP -ne 0 ]; then
       echo "NO FUE POSIBLE COPIAR EL ARCHIVO MOVIMIENTOS"
      else
       echo "ARCHIVO MOVIMIENTOS COPIADO EXITOSAMENTE"
      fi
   else
      echo "No se genero el archivo Comprimido GZ de Movimientos Diarios"
      #EnviaMail "No se genero el archivo de Movimientos Contingentes"
   fi
}

ValidaSaldos(){
if [ -s SaldosDiarios$d$m$y.tar.gz ] ; then
      echo "Si existe el archivo: " SaldosDiarios$d$m$y.tar.gz
      echo "TAUD" > $ARCH3
      cp SaldosDiarios$d$m$y.tar.gz $DIR_PROG/SaldosDiarios$d$m$y.tar.gz
      RESP=$?
      if [ $RESP -ne 0 ]; then
       echo "NO FUE POSIBLE COPIAR EL ARCHIVO SALDOS"
      else
       echo "ARCHIVO SALDOS COPIADO EXITOSAMENTE"
      fi
   else
      echo "No se genero el archivo Comprimido GZ de Saldos Diarios"
      #EnviaMail "No se genero el archivo de Saldos Contingentes"
   fi
}

ComprimeMovimientos(){
#  Archivos de Movimientos
export ARCHMOV="????MOV$d$m$y.txt"
echo "cd /plantiras/procesos/tiras_audit/interfaces" > $COMANDOS
echo "mget ${ARCHMOV} ${DIR_PROG}" >> $COMANDOS
echo "exit" >> $COMANDOS
sftp -oIdentityFile=$USR_TRANSFER_BD_KEY -oBatchMode=yes -b $COMANDOS $USR_SFTP_BD@$HOST_BD 1>$SALIDASFTP 2>$ERRORSFTP
RESP=$?
if [ $RESP -ne 0 ]; then
 echo "NO FUE POSIBLE TRAER EL ARCHIVO $ARCHMOV"
fi
find ${DIR_PROG} -name ????MOV$d$m$y.txt > $ARCH1
if [ -s $ARCH1 ] ; then
   # Agrupa todos los archivos de movimientos las sucursales
   tar -cvf MovimientosDiarios$d$m$y.tar $DIR_PROG/????MOV$d$m$y.txt
   rm -f ${DIR_PROG}/????MOV$d$m$y.txt
   # Comprime el archivo con la informacin agrupada
   gzip MovimientosDiarios$d$m$y.tar
else
   echo "No existen archivos TXT de Movimientos Diarios"
fi
}

ComprimeSaldos(){
#  Archivos de Saldos
export ARCHSDO="????SDO$d$m$y.txt"
echo "cd /plantiras/procesos/tiras_audit/interfaces" > $COMANDOS
echo "mget ${ARCHSDO} ${DIR_PROG}" >> $COMANDOS
echo "exit" >> $COMANDOS

sftp -oIdentityFile=$USR_TRANSFER_BD_KEY -oBatchMode=yes -b $COMANDOS $USR_SFTP_BD@$HOST_BD 1>$SALIDASFTP 2>$ERRORSFTP
RESP=$?
if [ $RESP -ne 0 ]; then
 echo "NO FUE POSIBLE TRAER EL ARCHIVO $ARCHSDO"
fi
find ${DIR_PROG} -name ????SDO$d$m$y.txt > $ARCH2
if [ -s $ARCH2 ] ; then
   # Agrupa todos los archivos de movimientos las sucursales
   tar -cvf SaldosDiarios$d$m$y.tar $DIR_PROG/????SDO$d$m$y.txt 2>/dev/null
   rm -f ${DIR_PROG}/????SDO$d$m$y.txt

   # Comprime el archivo con la informacin agrupada
   gzip SaldosDiarios$d$m$y.tar
else
   echo "No existen archivos TXT de Saldos Diarios"
   fi
}

ComprimeMovimientos > ${DIR_PROG}/ComprimeMovimientos.out
ValidaMovimientos > ${DIR_PROG}/ValidaMovimientos.out
ComprimeSaldos > ${DIR_PROG}/ComprimeSaldos.out
ValidaSaldos > ${DIR_PROG}/ValidaSaldos.out

rm $ARCH1
rm $ARCH2

if [ -s $ARCH3 ] ; then
      rm $ARCH3
      echo "TAUD"
   else
      echo "EMPTY"
fi