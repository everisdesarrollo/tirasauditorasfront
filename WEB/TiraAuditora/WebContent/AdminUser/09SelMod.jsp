<!-- AUTOR : Alfredo Resendiz Vargas    -->
<!-- CREACION :     28/03/2006   -->
<!-- DESCRIPCION : Pantalla para seleccionar altas y bajas de facultades   -->
<!--               de perfil de usuarios    -->
<!-- MODIFICACION :     			    -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%			
	String usuario = null;		
	String perfil = null;		
	String grupos = null;		
	usuario = request.getHeader("iv-user");		
	grupos = request.getHeader("iv-groups");		
			
	if (usuario == null || grupos == null) {		
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}		
%>	
<HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="../theme/Master.css" rel="stylesheet"
	type="text/css">
<TITLE>Asignar y Quitar Facultades del Perfil de Usuario</TITLE>
<SCRIPT>
	//Funci�n para dar de alta a los usuarios

	function fnAlta ()
	{
		document.frmSelMod.accion.value = 'buscar';
		document.frmSelMod.action = '../servlet/AdminUserServlet';
		document.frmSelMod.submit();
	}

	//Funci�n para dar de baja a los usuarios

	function fnBaja ()
	{
		document.frmSelMod.accion.value = 'buscar';
		document.frmSelMod.action = '../servlet/AdminUserServlet';
		document.frmSelMod.submit();
	}

	//Funci�n para realizar el regreso de la pagina

	function fnRegresa()
	{
		document.frmSelMod.action = '09SelPer.jsp';
		document.frmSelMod.submit();
	}
</SCRIPT>
</HEAD>
<BODY>
<FORM id="frmSelMod" name="frmSelMod" method="post">
	<TABLE width="100%" border="0">
	  <TR>
	    <TD>
	      <P class="ATitulocolor">Asignar y Quitar Facultades del Perfil de Usuario</P>
	      <TABLE width="80%" border="0" cellspacing="0" align="center">
	        <TR>
	          <TD class="Atexencabezado">&nbsp;</TD>
	        </TR>
	        <TR>
	          <TD colspan="1">&nbsp;</TD>
	        </TR>
	        <TR>
	          <TD colspan="2" class="Atittabcenazu">&nbsp;Seleccione opci�n</TD>
	        </TR>
	        <TR class="AEtiquetaDentro">
	          <TD align="right">
	          	<INPUT type="radio" name="seleccion" value="2" onclick="javascript:fnAlta();"></TD>
	          <TD colspan="1" class="Atexencabezado">Asignar Facultades a un Perfil</TD>
	        </TR>
	        <TR class="AEtiquetaDentro">
			  <TD align="right">
			  	<INPUT type="radio" name="seleccion" value="3" onclick="javascript:fnBaja();">
			  </TD>
			  <TD class="Atexencabezado">&nbsp;Quitar Facultades a un Perfil</TD>
			</TR>
			<TR>
			  <TD colspan="2" height="10" class="Atittabcenazu">&nbsp;</TD>
			</TR>
			<TR>
			  <TD colspan="2" height="14"><p>&nbsp;</p></TD>
			</TR>
			<TR>
				<TD>
					&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
				</TD>
				<TD align="center"><A href="javascript:fnRegresa();"><IMG border="0"
					name="continuar" src="../images/b_regresar.gif" align="left">
				</A>
			</TR>

		</TABLE>
        <P></P>
	    <P>&nbsp;</P>
	    </TD>
	  </TR>
	</TABLE>
	<INPUT type="hidden" name="accion" value="">
</FORM>
</BODY>
</HTML>