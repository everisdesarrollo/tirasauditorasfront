<!-- AUTOR : Alfredo Resendiz Vargas    -->
<!-- CREACION :     11/04/2006   -->
<!-- DESCRIPCION : Pantalla para cambiar la contrase�a de usuario    -->
<!-- MODIFICACION :     			    -->

<%@ page import="java.util.*"%>
<%@ page import="com.santander.usuarios.*"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%			
	String usuario = null;		
	String perfil = null;		
	String grupos = null;		
	usuario = request.getHeader("iv-user");		
	grupos = request.getHeader("iv-groups");		
			
	if (usuario == null || grupos == null) {		
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}		
%>	
<HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<SCRIPT language="JavaScript" src="../theme/utiljs.js"></SCRIPT>
<LINK href="../theme/Master.css" rel="stylesheet"
	type="text/css">
<TITLE>Tira Auditora</TITLE>
<SCRIPT src="../theme/validaciones.js"></SCRIPT>
<SCRIPT language="Javascript">

	var valida_password = new Array('password','Contrase�a','A',true,0,8);

// Funci�n para limpiar el contenido de los campos

	function fnLimpia()
	{
		document.frmlogini.usuario.value = '';
		document.frmlogini.contrasena.value = '';
		document.frmlogini.contrasena2.value = '';
	}

// Funci�n que valida la contrase�a y envia para que se actualice

	function enviar()
	{
		var opcion = 1;
			if((document.frmlogini.usuario.value == ""))
			{
				alert("Debe ingresar el usuario");
				opcion = '0';
				document.frmlogini.usuario.focus();
			}
		else
		{
			if((document.frmlogini.contrasena.value == ""))
			{
				alert("Debe ingresar su contrase�a nueva");
				opcion = '0';
			}
			else
				if((document.frmlogini.contrasena2.value == ""))
				{
					alert("Debe confirmar su contrase�a nueva");
					opcion = '0';
				}
				else
					if((document.frmlogini.contrasena.value != document.frmlogini.contrasena2.value))
					{
						alert("Contrase�a no coincide");
						opcion = '0';
						document.frmlogini.contrasena2.focus();
					}
				}

		if(opcion=='1')
		{
			document.frmlogini.accion.value = 'actpass';
			document.frmlogini.submit();
		}
	}

// Funci�n que direcciona el foco hacia el campo de usuario

	function fnFoco()
	{
		document.frmlogini.usuario.focus();
	}

//Funci�n para redireccionar a la seleccion altas y bajas

	function fnRegresa()
	{
		document.frmlogini.action = '09SelAdm.jsp';
		document.frmlogini.submit();
	}


</SCRIPT>
</HEAD>
<BODY onLoad="javascript:fnFoco();">
<FORM id="frmlogini" name="frmlogini" method="post" action="../servlet/AdminUserServlet" >
<TABLE width="100%" border="0" cellspacing="0" align="center">
	<TR>
		<TD width="100%" align="left">
		    <P class="ATitulocolor">Cambio de Contrase�a</P><BR>
		</TD>
		<TD>
			<BR><BR>
		</TD>
	</TR>
</TABLE>
<TABLE width="80%" border="0" cellspacing="0" align="center">
	<TR>
		<TD colspan="3" class="Atittabcenazu">&nbsp; Escribir su contrase�a nueva</TD>
	</TR>
	<TR class="AEtiquetaDentro">
		<TD align="right" class="AEtiquetaDentro">Usuario:</TD>
		<TD>
          	<INPUT  type="text" name="usuario" size="10" class="Atextittab" maxlength="8">
        </TD>
    </TR>
	<TR class="AEtiquetaDentro">
	<TD align="right" class="AEtiquetaDentro">Contrase�a:</td>
		<TD>
          	<INPUT type="password" name="contrasena" size="10" class="Atextittab" maxlength="8">
          </TD>
    </TR>
    <TR class="AEtiquetaDentro">
	<TD align="right" class="AEtiquetaDentro">Confirmar Contrase�a:</td>
		<TD>
          	<INPUT type="password" name="contrasena2" size="10" class="Atextittab" maxlength="8" >
          </TD>
    </TR>

		<TD colspan="3" class="Atittabcenazu">&nbsp;</TD>
	<TR>
		<TD colspan="2">&#160;</TD>
	</TR>
	<TR>
		<TD colspan="2"></TD>
	</TR>
<TABLE width="30%" border="0" cellspacing="0" align="center">
	<TR>
		<TD align="center"><A href="javascript:fnRegresa();"><IMG border="0"
		name="regresar" src="../images/b_regresar.gif" align="right"></A></TD>
		<TD align="center" width="60%"><A href="#"></A></TD>
		<TD align="center"><A href="javascript:fnLimpia();"><IMG border="0"
			name="limpiar" src="../images/b_limpiar.gif" align="left"></A></TD>
		<TD align="center"><A href="javascript:enviar();"><IMG border="0"
			name="continuar" src="../images/b_enviar.gif" align="left"></A></TD>
	</TR>
</TABLE>
</TABLE>
	<INPUT type="hidden" name="accion" value="">
</FORM>
</BODY>
</HTML>