<!-- AUTOR : Alfredo Resendiz Vargas    -->
<!-- CREACION :     28/03/2006   -->
<!-- DESCRIPCION : Pantalla para dar aviso de error en la baja del perfil del usuario -->
<!-- MODIFICACION :     			    -->

<%@ page import="java.util.*"%>
<%@ page import="com.santander.contingente.*"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%			
	String usuario = null;		
	String perfil = null;		
	String grupos = null;		
	usuario = request.getHeader("iv-user");		
	grupos = request.getHeader("iv-groups");		
			
	if (usuario == null || grupos == null) {		
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}		
%>	
<HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="../theme/Master.css" rel="stylesheet" type="text/css">
<TITLE>Error Alta de Perfil de Usuario</TITLE>
</HEAD>
<BODY>
<FORM id="frmConfi2" name="frmconfi2" method="post"	action="09SelAdm.jsp">
<TABLE width="100%" align="center" border="0">
	<TR>
		<TD colspan="2" align="left" width="100%">
			<P class="ATitulocolor">Error Baja de Perfil de Usuario</P><BR>
		</TD>
	</TR>
	<TR>
		<TD>
		<TABLE width="70%" align="center" border="0" cellspacing="0">
			<TR class="Atittabcenazu">
			    <TD colspan="2">&nbsp;Error Baja de Perfil de Usuario</TD>
			</TR>
			<TR>
				<TD colspan="2" class="AEtiquetaDentro" align="center">&#160;</TD>
			</TR>

			<TR class="AEtiquetaDentro">
				<TD colspan="2" align="center" class="Atexencabezado">Error baja de perfil de usuario, perfil est� asignado a un usuario</TD>
			</TR>
			<TR class="AEtiquetaDentro">
				<TD colspan="2">&#160;</TD>
			</tr>
			<TR class="Atittabcenazu">
				<TD colspan="2">&#160;</TD>
			</TR>
		</TABLE>
		<TABLE align="center" width="25%">
			<TR>
				<TD align="center"><A href='09SelPer.jsp'><IMG border="0"
					name="si" src="../images/b_regresar.gif"></A>
				</TD>
			</TR>
		</TABLE>
		</TD>
	</TR>
</TABLE>
</FORM>
<P><BR></P>
</BODY>
</HTML>