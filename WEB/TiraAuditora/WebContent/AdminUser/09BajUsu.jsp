<!-- AUTOR : Alfredo Resendiz Vargas    -->
<!-- CREACION :     28/03/2006   -->
<!-- DESCRIPCION : Baja de Cuentas de Usuario    -->
<!-- MODIFICACION :     			    -->
<%			
	String usuario = null;		
	String perfil = null;		
	String grupos = null;		
	usuario = request.getHeader("iv-user");		
	grupos = request.getHeader("iv-groups");		
			
	if (usuario == null || grupos == null) {		
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}		
%>	
<HTML>
<HEAD>
<%@ page import="javax.servlet.http.*"%>
<%@ page import="java.util.ArrayList.*"%>
<%@ page
language="java"
contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"
%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<SCRIPT language="JavaScript" src="../theme/utiljs.js"></SCRIPT>
<LINK href="../theme/Master.css" rel="stylesheet"
	type="text/css">

<TITLE>Alta de Usuarios</TITLE>
<SCRIPT>

//Funci�n que valida los campos y env�a a guardar el registro en la BD

	function fnValida()
	{

		var usuario = document.frmAltUsu.usuario.value;


		if(usuario=="")
		{
			alert("Debe capturar el campo de usuario");

			if(usuario == "")
			{
				document.frmAltUsu.usuario.focus();
			}
	    }
	    else
		{
			document.frmAltUsu.accion.value = 'baja';
			document.frmAltUsu.action = '../servlet/AdminUserServlet';
			document.frmAltUsu.submit();
		}
	}

//Funci�n para Limpiar los campos

function fnLimpia()
{
	document.frmAltUsu.usuario.value = ''
}

//Funci�n para redireccionar a la seleccion altas y bajas

function fnRegresa()
{
	document.frmAltUsu.submit();
}

//Funci�n que coloca el foco en el primer campo

function fnFoco()
{
	document.frmAltUsu.usuario.focus();
}
</SCRIPT>
</HEAD>
<BODY onLoad="javascript:fnFoco();">
<FORM name="frmAltUsu" method="post" action="09SelAdm.jsp">
<INPUT type="hidden" name="oculto" value="5"/>
<TABLE width="100%" align="center" border="0">
<TR>
	<TD>
	<P class="ATitulocolor">Baja Usuarios para la Aplicaci�n de Tiras Auditoras</P><BR>
	</TD>
</TR>
<TR>
	<TD>
	<TABLE width="80%" align="center" border="0" cellspacing="0">
	<TR class="Atittabcenazu">
		<TD colspan="3">&#160;Baja usuarios</TD>
	</TR>
	<TR class="AEtiquetaDentro">
		<TD colspan="3">&#160;</TD>
	</TR>
	<TR class="AEtiquetaDentro">
		<TD align="right" class="Atexencabezado">Nombre de usuario:&#160;&#160;</TD>
		<TD><INPUT Type="text" name="usuario" class="Atextittab" maxlength="8"></TD>
		<TD><IMG border="0" src="../images/invisible.gif" width="250"></TD>
	</TR>
	<TR class="AEtiquetaDentro">
		<TD colspan="3">&#160;</TD>
	</TR>
	<TR class="ATittabcenazu">
		<TD colspan="3">&#160;</TD>
	</TR>
	<TR>
		<TD colspan="3">&#160;<BR></TD>
	</TR>
	<TABLE width="55%" align="center" border="0" cellspacing="0">
		<TR>
			<TD align="center"><A href="javascript:fnRegresa();"><IMG border="0"
				name="regresar" src="../images/b_regresar.gif" align="right"></A></TD>
			<TD align="center"><A href="javascript:fnLimpia();"><IMG border="0"
				name="limpiar" src="../images/b_limpiar.gif" align="right"></A></TD>
			<TD align="center"><A href="javascript:fnValida();"><IMG border="0"
				name="continuar" src="../images/b_enviar.gif" align="right"></A></TD>
		</TR>
	</TABLE>
</TABLE>
</TD>
</TR>
</TABLE>
<INPUT type="hidden" name="accion" value="">
</FORM>
</BODY>
</HTML>