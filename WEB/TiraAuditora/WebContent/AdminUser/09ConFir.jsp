<!-- AUTOR : Alfredo Resendiz Vargas    -->
<!-- CREACION :     28/03/2006   -->
<!-- DESCRIPCION : Pantalla para dar aviso del �xito de la operaci�n    -->
<!-- MODIFICACION :     			    -->

<%@ page import="java.util.*"%>
<%@ page import="com.santander.contingente.*"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%			
	String usuario = null;		
	String perfil = null;		
	String grupos = null;		
	usuario = request.getHeader("iv-user");		
	grupos = request.getHeader("iv-groups");		
			
	if (usuario == null || grupos == null) {		
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}		
%>	
<HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="../theme/Master.css" rel="stylesheet" type="text/css">
<TITLE>Tira Auditora</TITLE>
</HEAD>
<BODY>
<FORM id="frmConFir" name="frmConFir" method="post"	action="09AltUsu.jsp">
<SCRIPT>
		function fnRegreso()
		{
			document.frmConFir.action = '09SelAdm.jsp';
			document.frmConFir.submit();
		}
</SCRIPT>
<TABLE width="100%" border="0">
	<TR>
		<TD>
		<%
		  HttpSession sesion = request.getSession();
		  String res = (String)sesion.getAttribute("consuluser");
		  String res_desb = (String)sesion.getAttribute("consuldesb");

		  if(res == "0" || res_desb == "0")
		  {
		%>
			<SCRIPT>
				alert ('Actualizaci�n correcta');
					fnRegreso();

			</SCRIPT>
	   <%
		  }
		  else
		  {

		 %>
			<SCRIPT>
				alert ('Error en la Transacci�n');
					fnRegreso();
			</SCRIPT>

		 <%
		}
		%>
</TABLE>
</FORM>
</BODY>
</HTML>