<!-- AUTOR : Alfredo Resendiz Vargas    -->
<!-- CREACION :     28/03/2006   -->
<!-- DESCRIPCION : Pantalla para seleccionar altas y bajas de perfil de usuarios    -->
<!-- MODIFICACION :     			    -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%			
	String usuario = null;		
	String perfil = null;		
	String grupos = null;		
	usuario = request.getHeader("iv-user");		
	grupos = request.getHeader("iv-groups");		
			
	if (usuario == null || grupos == null) {		
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}		
%>	
<HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="../theme/Master.css" rel="stylesheet"
	type="text/css">
<TITLE>Altas y Bajas de Perfil de Usuario</TITLE>
<SCRIPT>
	//Funci�n para dar de alta a los usuarios

	function fnAlta ()
	{
		document.frmSelPer.action = '09AltPer.jsp';
		document.frmSelPer.submit();
	}

	//Funci�n para dar de baja a los usuarios

	function fnBaja ()
	{
		document.frmSelPer.accion.value = 'buscar';
		document.frmSelPer.action = '../servlet/AdminUserServlet';
		document.frmSelPer.submit();
	}

	//Funci�n para modificar la contrase�a de los usuarios

	function fnModifica ()
	{
		document.frmSelPer.action = '09SelMod.jsp';
		document.frmSelPer.submit();
	}

	//Funci�n para realizar el regreso de la pagina

	function fnRegresa()
	{
		document.frmSelPer.action = '../Menu/01Tira.jsp';
		document.frmSelPer.submit();
	}
</SCRIPT>
</HEAD>
<BODY>
<FORM id="frmSelPer" name="frmSelPer" method="post">
	<TABLE width="100%" border="0">
	  <TR>
	    <TD>
	      <P class="ATitulocolor">Altas y Bajas de Perfil de Usuario</P>
	      <TABLE width="80%" border="0" cellspacing="0" align="center">
	        <TR>
	          <TD class="Atexencabezado">&nbsp;</TD>
	        </TR>
	        <TR>
	          <TD colspan="1">&nbsp;</TD>
	        </TR>
	        <TR>
	          <TD colspan="2" class="Atittabcenazu">&nbsp;Seleccione opci�n</TD>
	        </TR>
	        <TR class="AEtiquetaDentro">
	          <TD align="right">
	          	<INPUT type="radio" name="seleccion" value="1" onclick="javascript:fnAlta();"></TD>
	          <TD colspan="1" class="Atexencabezado">Alta de Perfil</TD>
	        </TR>
	        <TR class="AEtiquetaDentro">
			  <TD align="right">
			  	<INPUT type="radio" name="seleccion" value="0" onclick="javascript:fnBaja();">
			  </TD>
			  <TD class="Atexencabezado">&nbsp;Baja de Perfil</TD>
			</TR>
			<TR class="AEtiquetaDentro">
			  <TD align="right">
			  	<INPUT type="radio" name="seleccion" value="2" onclick="javascript:fnModifica();">
			  </TD>
			  <TD class="Atexencabezado">&nbsp;Modificaci�n de Perfil</TD>
			</TR>
			<TR>
			  <TD colspan="2" height="10" class="Atittabcenazu">&nbsp;</TD>
			</TR>
			<TR>
			  <TD colspan="2" height="14"><p>&nbsp;</p></TD>
			</TR>
			<TR>
				<TD>
					&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
				</TD>
				<TD align="center"><A href="javascript:fnRegresa();"><IMG border="0"
					name="continuar" src="../images/b_regresar.gif" align="left">
				</A>
			</TR>

		</TABLE>
        <P></P>
	    <P>&nbsp;</P>
	    </TD>
	  </TR>
	</TABLE>
	<INPUT type="hidden" name="accion" value="">
</FORM>
</BODY>
</HTML>