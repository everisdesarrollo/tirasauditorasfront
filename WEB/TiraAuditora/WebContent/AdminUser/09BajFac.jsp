<!-- AUTOR : Alfredo Resendiz Vargas    -->
<!-- CREACION :     28/03/2006   -->
<!-- DESCRIPCION : Modificaci�n de Facultades del Perfil de Usuario    -->
<!-- MODIFICACION :     			    -->
<%			
	String usuario = null;		
	String perfil = null;		
	String grupos = null;		
	usuario = request.getHeader("iv-user");		
	grupos = request.getHeader("iv-groups");		
			
	if (usuario == null || grupos == null) {		
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}		
%>			

<HTML>
<HEAD>
<%@ page import="javax.servlet.http.*"%>
<%@ page import="java.util.ArrayList.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.santander.usuarios.*"%>

<%@ page
language="java"
contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"
%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<SCRIPT language="JavaScript" src="../theme/utiljs.js"></SCRIPT>
<LINK href="../theme/Master.css" rel="stylesheet"
	type="text/css">

<TITLE>Quitar Facultades al Perfil de Usuario</TITLE>
<SCRIPT>

//Funci�n que valida los campos y env�a a guardar el registro en la BD

	function fnValida()
	{
		var facultad = document.frmAltPer.quitaFacultad.value;
		var perfil = document.frmAltPer.quitaPerfil.value;

		if(facultad==""||perfil=="-1")
		{
			alert("Debe capturar todos los campos");

			if(perfil == "")
			{
				document.frmAltPer.quitaPerfil.focus();
			}
			if(facultad == "")
			{
				document.frmAltPer.quitaFacultad.focus();
			}
	    }
	    else
	    {
			document.frmAltPer.accion.value = 'quitaPerfil';
			document.frmAltPer.action = '../servlet/AdminUserServlet';
			document.frmAltPer.submit();
		}
	}

//Funci�n para Limpiar los campos

function fnLimpia()
{
	document.frmAltPer.quitaPerfil.value = '-1'
	document.frmAltPer.quitaFacultad.value = '-1'
}

//Funci�n para redireccionar a la seleccion altas y bajas

function fnRegresa()
{
	document.frmAltPer.submit();
}

//Funci�n que coloca el foco en el primer campo

function fnFoco()
{
	document.frmAltPer.quitaFacultad.focus();
}
</SCRIPT>
</HEAD>
<BODY onLoad="javascript:fnFoco();">
<FORM name="frmAltPer" method="post" action="09SelPer.jsp">
<INPUT type="hidden" name="oculto" value="5"/>
<TABLE width="100%" align="center" border="0">
<%
  HttpSession sesion = request.getSession();
  ArrayList total = (ArrayList)sesion.getAttribute("buscaperfil");
%>
<TR>
	<TD>
	<P class="ATitulocolor">Quita Facultades a un Perfil</P><BR>
	</TD>
</TR>
<TR>
	<TD>
	<TABLE width="80%" align="center" border="0" cellspacing="0">
	<TR class="Atittabcenazu">
		<TD colspan="3">&#160;Quita Facultades a un Perfil</TD>
	</TR>
	<TR class="AEtiquetaDentro">
		<TD colspan="3">&#160;</TD>
	</TR>
	<TR class="AEtiquetaDentro">
		<TD align="right">&#160;&#160;&#160;&#160;Facultad:&#160;&#160;</TD>
		<TD><SELECT size="1" name="quitaFacultad" class="Atextittab">
				<OPTION selected value="-1">Elegir...</OPTION>
				<OPTION value="F01">F01 "Consulta de Tira Auditora"</OPTION>
				<OPTION value="F02">F02 "Consulta de Cat�logo de Sucursales"</OPTION>
				<OPTION value="F03">F03 "Mantto. al catalogo de Sucursales"</OPTION>
				<OPTION value="F04">F04 "Consulta de Sucursales Contingentes"</OPTION>
				<OPTION value="F05">F05 "Mantto. de Sucursales Contingentes"</OPTION>
				<OPTION value="F06">F06 "Carga Manual de la Tira Auditora"</OPTION>
				<OPTION value="F07">F07 "Generaci�n de Archs. de Ings./Egrs."</OPTION>
				<OPTION value="F08">F08 "Solicitud de la Inf. de la BD Hist."</OPTION>
				<OPTION value="F09">F09 "Hist�rico de Sucursales Contingentes"</OPTION>
				<OPTION value="F10">F10 "Cambio de Password del Telnet"</OPTION>
				<OPTION value="F11">F11 "Mantenimiento Comu_operaciones"</OPTION>
				<OPTION value="F12">F12 "Mantto. Usr del Aplicativo"</OPTION>
				<OPTION value="F13">F13 "Mantenimiento P�rfil"</OPTION>
				<OPTION value="F14">F14 "Mantenimiento b�sico de Sucursal"</OPTION>
				<OPTION value="F15">F15 "Monitoreo Scheduler Jobs"</OPTION>
				<OPTION value="F16">F16 "Bit�cora de Carga Diaria"</OPTION>
			</SELECT>
		</TD>
		<TD><IMG border="0" src="../images/invisible.gif" width="250"></TD>
	</TR>
	<TR class="AEtiquetaDentro">
		<TD align="right">Perfil:&#160;&#160;</TD>
		<TD><SELECT size="1" name="quitaPerfil" class="Atextittab">
				<OPTION selected value="-1">Elegir...</OPTION>
				<%
				for (int i=0;i<total.size();i++)
				{
					usuariosValue regusuarios = (usuariosValue)total.get(i);
				%>
					<OPTION value="<%=regusuarios.getperfil()%>"><%=regusuarios.getperfil()%></OPTION>
				<%
				}
				%>
			</SELECT>
		</TD>
		<TD><IMG border="0" src="../images/invisible.gif" width="250"></TD>
	</TR>
	<TR class="AEtiquetaDentro">
		<TD colspan="3">&#160;</TD>
	</TR>
	<TR class="ATittabcenazu">
		<TD colspan="3">&#160;</TD>
	</TR>
	<TR>
		<TD colspan="4">&#160;<BR></TD>
	</TR>
	<TABLE width="55%" align="center" border="0" cellspacing="0">
		<TR>
			<TD align="center"><A href="javascript:fnRegresa();"><IMG border="0"
				name="regresar" src="../images/b_regresar.gif" align="right"></A></TD>
			<TD align="center"><A href="javascript:fnLimpia();"><IMG border="0"
				name="limpiar" src="../images/b_limpiar.gif" align="right"></A></TD>
			<TD align="center"><A href="javascript:fnValida();"><IMG border="0"
				name="continuar" src="../images/b_enviar.gif" align="right"></A></TD>
		</TR>
	</TABLE>
</TABLE>
</TD>
</TR>
</TABLE>
<INPUT type="hidden" name="accion" value="">
</FORM>
</BODY>
</HTML>