<!-- AUTOR : Alfredo Resendiz Vargas    -->
<!-- CREACION :     26/02/2006   -->
<!-- DESCRIPCION : Consulta del Mantto. de Sucursales Contingentes    -->
<!-- MODIFICACION :     			    -->

<%@ page import="java.util.*"%>
<%@ page import="com.santander.contingente.*"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%			
	String usuario_rto = null;		
	String perfil_rto = null;		
	String grps_rto = null;		
	usuario_rto = request.getHeader("iv-user");		
	grps_rto = request.getHeader("iv-groups");		
			
	if (usuario_rto == null || grps_rto == null) {		
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}		
%>			


<HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="../theme/Master.css" rel="stylesheet" type="text/css">
<TITLE>Tira Auditora</TITLE>
<SCRIPT src="../theme/validaciones.js"></SCRIPT>
<SCRIPT language="JavaScript" src="../theme/utiljs.js"></SCRIPT>
<script src="../theme/generando.js" type="text/javascript"></script>
<SCRIPT language="Javascript">

		var Loc_valida_descuser = new Array('descuser','Descripcion usuario','A',true,0,50);

	//Funci�n forza a capturar la descripci�n de usuario
		function valida(campo,propiedades)
		{
		    var Loc_Mensaje="";
            var lTempMssg=assertField(campo, propiedades);

		    if (Loc_Mensaje.length>0)
		    {
		        alert(lMensaje);
		        return false;
		    }
		    else
		    {
		        return true;
		    }
		}

	//Funci�n que permite la paginaci�n
		function Lco_mueve_pag(pag,pagant)
		{
				document.frmImpCon.pagina.value = pag;
				document.frmImpCon.pagant.value = pagant;
				document.frmImpCon.submit();
		}

	//Funci�n que permite volver a realizar la b�squeda de Sucursales Contingentes
		function fnLimpia()
		{
			document.frmImpCon.action = '04ManCtg.jsp';
			document.frmImpCon.submit();
		}

	//Funci�n que permite eliminar registros de la BD de Sucursales Contingentes
		function fnEliminar()
		{
			document.frmImpCon.accion.value = 'eliminar';
			document.frmImpCon.action = '../servlet/ManttoCtgServlet';
			document.frmImpCon.submit();
		}

	//Funci�n que realiza la reactivaci�n manual y guarda la descripci�n del usuario
	//para busqueda por Pto. de Vta. y Fecha
		function fn_enviar_uno()
		{
			//var	Loc_descuser_valor_s = document.frmImpCon.descuser.value;
			//if(Loc_descuser_valor_s== "" )
			//{
			//	alert("El campo Descripci�n Usuario no debe estar vac�o" );
	   		//}
			//else
		   	//{
				document.frmImpCon.accion.value = 'guardar';
				document.frmImpCon.action = '../servlet/ManttoCtgServlet';
				document.frmImpCon.submit();
			//}
		}

	//Funci�n que confirma la eliminaci�n de registro
		function valida_eliminar()
		{
		 	var respuesta=confirm("Confirma eliminar los registros ?")
		    if(respuesta==true)
		       fnEliminar();
		}
	//Funci�n que deshabilita la funci�n eliminar cuando se selecciona el campo
	//reactivaci�n manual

		function desahabilita_eliminar()
		{
		  if(document.frmImpCon.verifica.checked=true)
		  {
		  	alert ("Se ha seleccionado el campo de reactivaci�n manual, no es posible elimiar");
		  	document.frmImpCon.elimina.disabled=true;
		  }
		}

	//VCM 23/07/12 Extraer informaci�n en l�nea
		function getOnLine(sucLine, fecLine){
			var respuesta=confirm("�Desea obtener la informaci�n en l�nea de la sucursal " + sucLine + " con fecha del " + fecLine + "?");
			    if(respuesta==true){
			    	document.frmImpCon.sucursalLinea.value = sucLine;
			    	document.frmImpCon.fechaLinea.value = fecLine;
				    document.frmImpCon.accion.value = 'linea';
					document.frmImpCon.action = '../servlet/ManttoCtgServlet';
					muestraGenerando();
					document.frmImpCon.submit();
			    }
		}
</SCRIPT>
</HEAD>
<jsp:include page="../cargando.jsp"/>
<BODY>
<FORM id="frmImpCon" name="frmImpCon" method="post"	action="04ImpCtg.jsp">
<TABLE width="100%" border="0" >
	<TR>
		<TD>
		<P class="ATitulocolor">Mantenimiento de Sucursales Contingentes</P>
</TABLE>
		<%
		/** Rutina que realiza la Consulta de sucursales Contingentes en el
		 *  Mantto. de Sucursales Contingentes Total, por Fecha y por pto.de vta./Fecha
		 */
		try
		{
		  int nopagina = Integer.parseInt(request.getParameter("pagina")!=null?request.getParameter("pagina"):"0");
		  HttpSession sesion = request.getSession();
		  int opcion=0;
		  String grupos = null; 
		  if(request.getParameter("pventa").equals("") && request.getParameter("fecha").equals(""))
		  {
			ArrayList total = (ArrayList)sesion.getAttribute("consulTotManCtg");
			if(request.getParameter("pagant")!=null)
			{
	  			int pag = Integer.parseInt(request.getParameter("pagant"));
				ArrayList pagant = (ArrayList)total.get(pag);

			  	pagant = null;
		  	}
		    ArrayList pagina = (ArrayList)total.get(nopagina);
		%>
			<TABLE width="90%" border="0" align="center" cellpadding="0" cellspacing="1">
			<TR>
				<TD colspan="5">
				<P>&nbsp;</P>
				</TD>
			</TR>
		<%
			int seleccion = Integer.parseInt(request.getParameter("seleccion"));
			if(seleccion==0)
			{
		%>
				<TR>
					<TD colspan="3" class="ALigaonce">
						<%if (nopagina != 0){%>
							<A href="javascript:Lco_mueve_pag('<%=nopagina-1%>','<%=nopagina%>');" class="txt">&lt;&lt;</A>
						<%}%>
						 <%=(100*nopagina)+1%> a <%=(100*nopagina)+pagina.size()%>
						<%if (nopagina != total.size()-1){%>
							<A href="javascript:Lco_mueve_pag('<%=nopagina+1%>','<%=nopagina%>');" class="txt">&gt;&gt;</A>
						<%}%>
					</TD>
					<TD colspan="5" class="Atexnegro">Rastreo general</TD>
				</TR>
				<TR>
					<TD width="2%" class="ATituloTabla">></TD> <!-- VCM 23/07/12 Extraer informaci�n en l�nea -->
					<TD width="4%"  class="ATituloTabla">CdeC ALTAIR</TD>
					<TD width="18%" class="ATituloTabla">Fecha</TD>
					<TD width="20%" class="ATituloTabla">Nombre Sucursal</TD>
					<TD width="27%" class="ATituloTabla">Descripci�n Sistema</TD>
					<TD width="25%" class="ATituloTabla">Descripci�n Usuario</TD>
					<TD width="2%" class="ATituloTabla">Reactivaci�n Manual</TD>
					<TD width="2%" class="ATituloTabla">Eliminar</TD>
				</TR>
				<%
				for (int i=0;i<pagina.size();i++)
				{
					ManCtgValue regManCtg = (ManCtgValue)pagina.get(i);
				%>
					<SCRIPT>
						//Funci�n que realiza la reactivaci�n manual y guarda la descripci�n del usuario
						//para busqueda Total

						//function fnEnviar()
						//{
							//var	Loc_descuser_val_s = document.frmImpCon.descuser<%=i%>.value;
							//if(Loc_descuser_val_s == "" )
							//{
								//alert("El campo Descripci�n Usuario no debe estar vac�o" );
						   	//}
							//else
						   	//{
							//	document.frmImpCon.accion.value = 'guardar';
							//	document.frmImpCon.action = '../servlet/ManttoCtgServlet';
							//	document.frmImpCon.submit();
							//}
						//}
						</SCRIPT>
					<INPUT type="hidden" name="bandera" value="2">
				<%
					if(i%2==0)
					{
				%>
					<TR  >
				<%
					}
					else
					{
				%>
					<TR  class="fondo">
				<%
					}
				%>
					<!-- VCM 23/07/12 Extraer informaci�n en l�nea -->
					<TD align="center"><a href="javascript:getOnLine('<%=regManCtg.get_suc_altair()%>','<%=regManCtg.get_fecha()%>');" ><img src="../images/online.gif" title="Descargar en linea"></a></TD>
					<TD align="center"><%=regManCtg.get_suc_altair()%></TD>
					<TD align="center"><%=regManCtg.get_fecha()%></TD>
					<TD align="center"><%=regManCtg.get_nom_sucursal()%></TD>
					<TD align="center"><%=regManCtg.get_desc_sistema()%></TD>
					<%if(regManCtg.get_desc_usuario() != null)
					  {
					  %>
						<TD align="center"><INPUT type="text" class="Atextittab" name="descuser<%=i%>" size="30"  maxlength="50" value="<%=regManCtg.get_desc_usuario()%>"></TD>
					<%}
					  else
					  {%>
						<TD align="center"><INPUT type="text" class="Atextittab" name="descuser<%=i%>" size="30"  maxlength="50"></TD>
					<%}

					if(Integer.parseInt(regManCtg.get_verificacion_sis())==0)
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="veri<%=i%>" value="1" <%if(regManCtg.get_num_intentos()<5){%>disabled<%}%> onclick="javascript:document.frmImpCon.sele<%=i%>.disabled = this.checked;"></TD>
				<%
					}
					else
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="veri<%=i%>" value="1" checked <%if(regManCtg.get_num_intentos()<5){%>disabled<%}%> onclick="javascript:document.frmImpCon.sele<%=i%>.disabled = this.checked;"></TD>
				<%
					}
					if(regManCtg.getElimina()==0)
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="sele<%=i%>" value="1" onclick="javascript:document.frmImpCon.veri<%=i%>.disabled = this.checked;"></TD>
				<%
					}
					else
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="sele<%=i%>" value="1" checked onclick="javascript:document.frmImpCon.veri<%=i%>.disabled = this.checked;"></TD>
				<%
					}
				}
			  }
		%>
		</TABLE>
		<%
	      }
	      else
	      {
	      	if(request.getParameter("pventa").equals("") && request.getParameter("fecha") != null)
		  	{
				ArrayList total = (ArrayList)sesion.getAttribute("consultafecCtg");
				if(request.getParameter("pagant")!=null)
				{
	  				int pag = Integer.parseInt(request.getParameter("pagant"));
					ArrayList pagant = (ArrayList)total.get(pag);

		  			pagant = null;
		  		}
			    ArrayList pagina = (ArrayList)total.get(nopagina);
		%>
			<TABLE width="90%" border="0" align="center" cellpadding="0" cellspacing="1">
			<TR>
				<TD colspan="5">
				<P>&nbsp;</P>
				</TD>
			</TR>
		<%
			int seleccion = Integer.parseInt(request.getParameter("seleccion"));
			if(seleccion==0)
			{
		%>
				<TR>
					<TD colspan="3" class="ALigaonce">
					<%if (nopagina != 0){%>
						<A href="javascript:Lco_mueve_pag('<%=nopagina-1%>','<%=nopagina%>');" class="txt">&lt;&lt;</A>
					<%}%>
					 <%=(100*nopagina)+1%> a <%=(100*nopagina)+pagina.size()%>
					<%if (nopagina != total.size()-1){%>
						<A href="javascript:Lco_mueve_pag('<%=nopagina+1%>','<%=nopagina%>');"	class="txt">&gt;&gt;</A>
					<%}%>
					</TD>
					<TD colspan="5" class="Atexnegro">Rastreo por Fecha <%=request.getParameter("fecha")%>"</TD>
					</TR>
					<TR>
						<TD width="2%" class="ATituloTabla">></TD> <!-- VCM 23/07/12 Extraer informaci�n en l�nea -->
						<TD width="4%"  class="ATituloTabla">CdeC ALTAIR</TD>
						<TD width="18%" class="ATituloTabla">Fecha</TD>
						<TD width="20%" class="ATituloTabla">Nombre Sucursal</TD>
						<TD width="27%" class="ATituloTabla">Descripci�n Sistema</TD>
						<TD width="25%" class="ATituloTabla">Descripci�n Usuario</TD>
						<TD width="2%" class="ATituloTabla">Reactivaci�n Manual</TD>
						<TD width="2%" class="ATituloTabla">Eliminar</TD>
					</TR>
				<%
					for (int i=0;i<pagina.size();i++)
					{
						ManCtgValue regManCtg = (ManCtgValue)pagina.get(i);
				%>
						<SCRIPT>
						//Funci�n que realiza la reactivaci�n manual y guarda la descripci�n del usuario
						//para busqueda por Fecha

						//function fnEnviar()
						//{
							//var	Loc_descuser_val_s = document.frmImpCon.descuser<%=i%>.value;
							//if(Loc_descuser_val_s=="" )
							//{
								//alert("El campo Descripci�n Usuario no debe estar vac�o" );
						   	//}
							//else
						   	//{
								//document.frmImpCon.accion.value = 'guardar';
								//document.frmImpCon.action = '../servlet/ManttoCtgServlet';
								//document.frmImpCon.submit();
							//}
						//}
						</SCRIPT>
						<INPUT type="hidden" name="bandera" value="1">
				<%
						if(i%2==0)
						{
				%>
							<TR  >
				<%
						}
						else
						{
				%>
							<TR  class="fondo">
				<%
						}
				%>
						<!-- VCM 23/07/12 Extraer informaci�n en l�nea -->
						<TD align="center"><a href="javascript:getOnLine('<%=regManCtg.get_suc_altair()%>','<%=regManCtg.get_fecha()%>');" ><img src="../images/online.gif" title="Descargar en linea"></a></TD>
						<TD align="center"><%=regManCtg.get_suc_altair()%></TD>
						<TD align="center"><%=regManCtg.get_fecha()%></TD>
						<TD align="center"><%=regManCtg.get_nom_sucursal()%></TD>
						<TD align="center"><%=regManCtg.get_desc_sistema()%></TD>
					  <%if(regManCtg.get_desc_usuario() != null){%>
							<TD align="center"><INPUT type="text" class="Atextittab" name="descuser<%=i%>" size="30"  maxlength="50"  value="<%=regManCtg.get_desc_usuario()%>"></TD>
					  <%}
					    else
					    {%>
							<TD align="center"><INPUT type="text" class="Atextittab" name="descuser<%=i%>" size="30"  maxlength="50"></TD>
					  <%}

						if(Integer.parseInt(regManCtg.get_verificacion_sis())==0)
						{
				%>
							<TD align="center"><INPUT type="checkbox" name="veri<%=i%>" value="1" <%if(regManCtg.get_num_intentos()<5){%>disabled<%}%> onclick="javascript:document.frmImpCon.sele<%=i%>.disabled = this.checked;"></TD>
				<%
						}
						else
						{
				%>
							<TD align="center"><INPUT type="checkbox" name="veri<%=i%>" value="1" <%if(regManCtg.get_num_intentos()<5){%>disabled<%}%> checked onclick="javascript:document.frmImpCon.sele<%=i%>.disabled = this.checked;"></TD>
				<%
						}

						if(regManCtg.getElimina()==0)
						{
				%>
							<TD align="center"><INPUT type="checkbox" name="sele<%=i%>" value="1" onclick="javascript:document.frmImpCon.veri<%=i%>.disabled = this.checked;"></TD>
				<%
						}
						else
						{
				%>
							<TD align="center"><INPUT type="checkbox" name="sele<%=i%>" value="1" checked onclick="javascript:document.frmImpCon.veri<%=i%>.disabled = this.checked;"></TD>
				<%
						}
					}
			  	}
				%>
		</TABLE>
	    <%
		}
		else
		{
			opcion=1;
			ArrayList total = (ArrayList)sesion.getAttribute("consultaManCtg");
		    if(request.getParameter("pagant")!=null)
		    {
	  			int pag = Integer.parseInt(request.getParameter("pagant"));
				ArrayList pagant = (ArrayList)total.get(pag);

		  		pagant = null;
		  	}
		  	ArrayList pagina = (ArrayList)total.get(nopagina);
		%>
			<TABLE width="90%" border="0" align="center" cellpadding="0" cellspacing="1">
			<TR>
				<TD colspan="5">
				<P>&nbsp;</P>
				</TD>
			</TR>
		<%
			int seleccion = Integer.parseInt(request.getParameter("seleccion"));
			if(seleccion==0)
			{
		%>
				<TR>
					<TD colspan="3" class="ALigaonce">
					<%if (nopagina != 0){%>
						<A href="javascript:Lco_mueve_pag('<%=nopagina-1%>','<%=nopagina%>');" class="txt">&lt;&lt;</A>
					<%}%>
					 <%=(100*nopagina)+1%> a <%=(100*nopagina)+pagina.size()%>
					<%if (nopagina != total.size()-1){%>
						<A href="javascript:Lco_mueve_pag('<%=nopagina+1%>','<%=nopagina%>');"	class="txt">&gt;&gt;</A>
					<%}%>
					</TD>
					<TD colspan="5" class="Atexnegro">
						Rastreo por Punto de Venta "<%=request.getParameter("pventa")%>" en la Fecha "<%=request.getParameter("fecha")%>"
					</TD>
				</TR>
				<TR>
					<TD width="2%" class="ATituloTabla">></TD> <!-- VCM 23/07/12 Extraer informaci�n en l�nea -->
					<TD width="4%" class="ATituloTabla">CdeC ALTAIR</TD>
					<TD width="18%" class="ATituloTabla">Fecha</TD>
					<TD width="20%" class="ATituloTabla">Nombre Sucursal</TD>
					<TD width="27%" class="ATituloTabla">Descripci�n Sistema</TD>
					<TD width="27%" class="ATituloTabla">Descripci�n Usuario</TD>
					<TD width="2%" class="ATituloTabla">Reactivaci�n Manual</TD>
					<TD width="2%" class="ATituloTabla">Eliminar</TD>
				</TR>
				<%
				for (int i=0;i<pagina.size();i++)
				{
					ManCtgValue regManCtg = (ManCtgValue)pagina.get(i);
				%>
					<INPUT type="hidden" name="bandera" value="0">
				<%
					if(i%2==0){
				%>
						<TR  >
				<%
					}
					else
					{
				%>
						<TR  class="fondo">
				<%
					}
				%>
					<!-- VCM 23/07/12 Extraer informaci�n en l�nea -->
					<TD align="center"><a href="javascript:getOnLine('<%=regManCtg.get_suc_altair()%>','<%=regManCtg.get_fecha()%>');" ><img src="../images/online.gif" title="Descargar en linea"></a></TD>
					<TD align="center"><%=regManCtg.get_suc_altair()%></TD>
					<TD align="center"><%=regManCtg.get_fecha()%></TD>
					<TD align="center"><%=regManCtg.get_nom_sucursal()%></TD>
					<TD align="center"><%=regManCtg.get_desc_sistema()%></TD>
				<%	if(regManCtg.get_desc_usuario() != null)
					{
				%>
						<TD align="left"><INPUT type="text" class="Atextittab" name="descuser" size="30"  maxlength="50" value="<%=regManCtg.get_desc_usuario()%>"></TD>
				<%	}
					else
					{
				%>
						<TD align="left"><INPUT type="text" class="Atextittab" name="descuser" size="30"  maxlength="50"></TD>
				 <% }

					if(Integer.parseInt(regManCtg.get_verificacion_sis())==0)
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="verifica" value="1" <%if(regManCtg.get_num_intentos()<5){%>disabled<%}%> onclick="javascript:document.frmImpCon.elimina.disabled = this.checked"></TD>
				<%
					}
					else
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="verifica" value="1" <%if(regManCtg.get_num_intentos()<5){%>disabled<%}%> checked onclick="javascript:document.frmImpCon.elimina.disabled = this.checked"></TD>
				<%
					 }

					if(regManCtg.getElimina()==0)
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="elimina" value="1" onclick="javascript:document.frmImpCon.verifica.disabled = this.checked"></TD>
				<%
					}
					else
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="elimina" value="1" checked onclick="javascript:document.frmImpCon.verifica.disabled = this.checked"></TD>
				<%
					}
				}
			}
			%>
			  </TABLE>
			<%
			}
		}
	%>

<TABLE align="center" width="55%">
		<TR>
			<TD>
			<P>&nbsp;</P>
			<P>&nbsp;</P>
			</TD>
			<TD align="center"><A href="javascript:fnLimpia();"><IMG border="0"
				name="continuar" src="../images/b_regresar.gif" ></A>
				&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
			<TD align="center"><a href="javascript:fn_enviar_uno();"><IMG border="0"
				name="guardar" src="../images/b_enviar.gif"></A>
				&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
			<TD align="center"><a href="javascript:valida_eliminar();"><IMG border="0"
				name="eliminar" src="../images/b_eliminar.gif"></A></TD>
		</TR>
	<%
		}
		catch (Exception ex)
		{
			System.out.println("Error en Mantto. de Sucursal Contingente/04ImpCtg.jsp: " + ex.getMessage());
		}
	%>
</TABLE>

<INPUT type="hidden" name="pagina" value="<%=request.getParameter("pagina")!=null?request.getParameter("pagina"):"0"%>">
<INPUT type="hidden" name="pagant" value="<%=request.getParameter("pagant")%>">
<INPUT type="hidden" name="fecha" value="<%=request.getParameter("fecha")%>">
<INPUT type="hidden" name="pventa" value="<%=request.getParameter("pventa")%>">
<INPUT type="hidden" name="seleccion" value="<%=request.getParameter("seleccion")%>">
<INPUT type="hidden" name="accion" value="">
<!-- VCM 23/07/12 Extraer informaci�n en l�nea -->
<INPUT type="hidden" name="sucursalLinea" value="">
<INPUT type="hidden" name="fechaLinea" value="">

</FORM>
</BODY>
</HTML>