<!-- AUTOR : Alfredo Resendiz Vargas    -->
<!-- CREACION :     26/02/2006   -->
<!-- DESCRIPCION : Consulta del Histórico de Sucursales Contingentes    -->
<!-- MODIFICACION :     			    -->

<%@ page import="java.util.*"%>
<%@ page import="com.santander.contingente.*"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%			
	String usuario = null;		
	String perfil = null;		
	String grupos = null;		
	usuario = request.getHeader("iv-user");		
	grupos = request.getHeader("iv-groups");		
			
	if (usuario == null || grupos == null) {		
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}		
%>			

<HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="../theme/Master.css" rel="stylesheet" type="text/css">
<TITLE>Tira Auditora</TITLE>
<SCRIPT language="Javascript">
//Función que realiza la paginación

	function muevePag(pag,pagant)
	{
		document.frmHisImp.pagina.value = pag;
		document.frmHisImp.pagant.value = pagant;
		document.frmHisImp.submit();
	}

//Función que limpia los campos de la pantalla

	function fnLimpia()
	{
		document.frmHisImp.action = '04HisCtg.jsp';
		document.frmHisImp.submit();
	}

//Función que envía a imprimir la consulta

	function imprime()
	{
		document.frmHisImp.action = '../servlet/ManCtgServlet';
		document.frmHisImp.submit();
	}
</SCRIPT>
</HEAD>
<BODY>
<FORM id="frmHisImp" name="frmHisImp" method="post"	action="04ImpHis.jsp">
<TABLE width="100%" border="0">
	<TR>
		<TD>
		<P class="ATitulocolor">Histórico de Sucursales Contingentes</P>
		<%
		/** Rutina que realiza la Consulta del Histórico de Sucursales Contingentes
		 *  Contingentes por Fecha y por Pto.de Vta./Fecha
		 */
		try
		{
		  String mensaje = "";
          ArrayList pagina = null;
          int seleccion = Integer.parseInt(request.getParameter("seleccion"));
          String tipo = request.getParameter("tipo");
          int nopagina = Integer.parseInt(request.getParameter("pagina")!=null?request.getParameter("pagina"):"0");
		  HttpSession sesion = request.getSession();
		  if(request.getParameter("pventa").equals("0000") && request.getParameter("fecha")!="")
		  {
			ArrayList total = (ArrayList)sesion.getAttribute("histfecCtg");
			if(total.size()>0 && total.get(0).getClass().equals(ArrayList.class))
		  	{
		  		if(request.getParameter("pagant")!=null)
		  		{
		  			int pag = Integer.parseInt(request.getParameter("pagant"));
		  			ArrayList pagant = (ArrayList)total.get(pag);
		  			for(int j=0;j<pagant.size();j++)
		  			{
		  				ManCtgValue ManCtg = (ManCtgValue)pagant.get(j);
		  			}
			  		pagant = null;
				}
				pagina = (ArrayList)total.get(nopagina);
		 	}
		  	else
		  	{
		 		if(total.size()>0 && total.get(0).getClass().equals(String.class))
		  		{
					System.out.println("El texto es:"+(String)total.get(0));
					seleccion = 2;
					mensaje = (String)total.get(0);
				}
		  	else
		  	{
				System.out.println("No hay Datos");
				seleccion = 2;
				mensaje = "No hay Datos.";
		 	 }
		  }
		%>
		<TABLE width="90%" border="0" align="center" cellpadding="0" cellspacing="1">
			<TR>
				<TD colspan="5">
				<P>&nbsp;</P>
				</TD>
			</TR>
				<%
				switch(seleccion)
				{
					case 1:
				%>
					<TR>
						<TD colspan="2" class="ALigaonce">
							<%if (nopagina != 0){%>
								<A href="javascript:muevePag('<%=nopagina-1%>','<%=nopagina%>');" class="txt">&lt;&lt;</A>
							<%}%>
							 <%=(100*nopagina)+1%> a <%=(100*nopagina)+pagina.size()%>
							<%if (nopagina != total.size()-1){%>
								<A href="javascript:muevePag('<%=nopagina+1%>','<%=nopagina%>');"	class="txt">&gt;&gt;</A>
							<%}%>
						</TD>
						<TD colspan="5" class="Atexnegro">
							Rastreo en la Fecha "<%=request.getParameter("fecha")%>"
						</TD>
					</TR>
					<TR>
						<TD width="4%" class="ATituloTabla">CdeC ALTAIR</TD>
						<TD width="15%" class="ATituloTabla">Fecha</TD>
						<TD width="15%" class="ATituloTabla">Nombre Sucursal</TD>
						<TD width="30%" class="ATituloTabla">Descripción Sistema</TD>
						<TD width="30%" class="ATituloTabla">Descripción Usuario</TD>
						<TD width="10%" class="ATituloTabla">Reactivación Manual</TD>
					</TR>
				<%
					for (int i=0;i<pagina.size();i++){
						ManCtgValue regManCtg = (ManCtgValue)pagina.get(i);
						if(i%2==0)
						{
				%>
							<TR  >
				<%
						}
						else
						{
				%>
							<TR  class="fondo">
				<%
						}
				%>
						<TD align="center"><%=regManCtg.get_suc_altair()%></TD>
						<TD align="center"><%=regManCtg.get_fecha()%></TD>
						<TD align="center"><%=regManCtg.get_nom_sucursal()%></TD>
						<TD align="center"><%=regManCtg.get_desc_sistema()%></TD>
				<%
						if(regManCtg.get_desc_usuario() != null)
						{
				%>
							<TD align="center"><%=regManCtg.get_desc_usuario()%></TD>
				<%
						}
						else
						{
							String valor = "";
				%>
							<TD align="center"><%=valor%></TD>
				<%
						}
						if(regManCtg.get_verificacion_sis() != null)
						{
						  String valor_reac = "";
						  if(regManCtg.get_verificacion_sis().equals("0"))
						  {
						    valor_reac = "Contingente";
				%>
							<TD align="center"><%=valor_reac%></TD>
				<%
						  }else{
     						  if(regManCtg.get_verificacion_sis().equals("1"))
     						  {
	   	   				        System.out.println("Reactivación Manual(1): "+regManCtg.get_verificacion_sis());
						        valor_reac = "Reactivación";
			    %>
		    					<TD align="center"><%=valor_reac%></TD>
		    	<%
						      }else{
       	   				        System.out.println("Reactivación Manual(2): "+regManCtg.get_verificacion_sis());
						          valor_reac = "Recuperación";
				%>
								  		<TD align="center"><%=valor_reac%></TD>
				<%
						      }
						      }
						}else{
						    String valor_reac = "";
   	   				        System.out.println("Reactivación Manual(nada): "+regManCtg.get_verificacion_sis());
				%>
						<TD align="center"><%=valor_reac%></TD>
				<%
				      }
					}
					break;
				case 2:
				%>
					<SCRIPT>
						fnVacio();

						function fnVacio()
						{
							document.frmHisImp.action = '../Contingentes/04ConFir.jsp';
							document.frmHisImp.submit();
						}
					</SCRIPT>
				<%
					break;
				}
				%>
	</TABLE>
		<%
		  }
		  else
		  {
		    ArrayList total = (ArrayList)sesion.getAttribute("historicoCtg");

  		  if(total.size()>0 && total.get(0).getClass().equals(ArrayList.class))
		  {
		  	if(request.getParameter("pagant")!=null)
		  	{
		  		int pag = Integer.parseInt(request.getParameter("pagant"));
		  		ArrayList pagant = (ArrayList)total.get(pag);
		  		for(int j=0;j<pagant.size();j++)
		  		{
		  			ManCtgValue ManCtg = (ManCtgValue)pagant.get(j);
		  		}
			  	pagant = null;
			}
			pagina = (ArrayList)total.get(nopagina);
		  }
		  else
		  {
		 	if(total.size()>0 && total.get(0).getClass().equals(String.class))
		  	{
				System.out.println("El texto es:"+(String)total.get(0));
				seleccion = 2;
				mensaje = (String)total.get(0);
			}
		  else
		  {
			System.out.println("No hay Datos");
			seleccion = 2;
			mensaje = "No hay Datos.";
		  }
		  }
		%>
		<TABLE width="90%" border="0" align="center" cellpadding="0" cellspacing="1">
			<TR>
				<TD colspan="5">
				<P>&nbsp;</P>
				</TD>
			</TR>
				<%
				switch(seleccion)
				{
					case 1:
				%>
					<TR>
						<TD colspan="2" class="ALigaonce">
							<%if (nopagina != 0){%>
								<A href="javascript:muevePag('<%=nopagina-1%>','<%=nopagina%>');" class="txt">&lt;&lt;</A>
							<%}%>
							 <%=(100*nopagina)+1%> a <%=(100*nopagina)+pagina.size()%>
							<%if (nopagina != total.size()-1){%>
								<A href="javascript:muevePag('<%=nopagina+1%>','<%=nopagina%>');"	class="txt">&gt;&gt;</A>
							<%}%>
						</TD>
						<TD colspan="5" class="Atexnegro">
							Rastreo por Punto de Venta "<%=request.getParameter("pventa")%>" en la Fecha "<%=request.getParameter("fecha")%>"
						</TD>
					</TR>
					<TR>
						<TD width="4%" class="ATituloTabla">CdeC ALTAIR</TD>
						<TD width="15%" class="ATituloTabla">Fecha</TD>
						<TD width="15%" class="ATituloTabla">Nombre Sucursal</TD>
						<TD width="30%" class="ATituloTabla">Descripción Sistema</TD>
						<TD width="30%" class="ATituloTabla">Descripción Usuario</TD>
						<TD width="10%" class="ATituloTabla">Reactivación Manual</TD>
					</TR>
				<%
					for (int i=0;i<pagina.size();i++)
					{
						ManCtgValue regManCtg = (ManCtgValue)pagina.get(i);
						if(i%2==0)
						{
				%>
							<TR  >
				<%
						}
						else
						{
				%>
							<TR  class="fondo">
				<%
						}
				%>
						<TD align="center"><%=regManCtg.get_suc_altair()%></TD>
						<TD align="center"><%=regManCtg.get_fecha()%></TD>
						<TD align="center"><%=regManCtg.get_nom_sucursal()%></TD>
						<TD align="center"><%=regManCtg.get_desc_sistema()%></TD>
				<%
						if(regManCtg.get_desc_usuario() != null)
						{
				%>
						<TD align="center"><%=regManCtg.get_desc_usuario()%></TD>
				<%
						}
						else
						{
							String valor = "";
				%>
							<TD align="center"><%=valor%></TD>
				<%
						}
						if(regManCtg.get_verificacion_sis() != null)
						{
						  String valor_reac = "";
						  if(regManCtg.get_verificacion_sis() == "0")
						  {
						    valor_reac = "Contingente";
						  }else{
     						  if(regManCtg.get_verificacion_sis() == "1")
						        valor_reac = "Reactivación";
						      else
						        valor_reac = "Recuperación";
						      }
				%>
						<TD align="center"><%=valor_reac%></TD>
				<%
						}else{
						  String valor_reac = "";
				%>
						<TD align="center"><%=valor_reac%></TD>
				<%
				        }
					}
					break;
				case 2:
				%>
					<SCRIPT>
						fnVacio();

						function fnVacio()
						{
							document.frmHisImp.action = '../Contingentes/04ConFir.jsp';
							document.frmHisImp.submit();
						}
					</SCRIPT>
				<%
					break;
				}
			}
				%>
</TABLE>
<TABLE align="center" width="20%">
	<TR>
		<TD>
			<P>&nbsp;</P>
			<P>&nbsp;</P>
		</TD>
		<TD align="center"><A href="javascript:fnLimpia();"><IMG border="0"
			name="continuar" src="../images/b_regresar.gif" align="left"></A></TD>
	</TR>
</TABLE>
	<%
		}
		catch (Exception ex)
		{
			System.out.println("Error en Histórico de Sucursal Contingente/04ImpHis.jsp: " + ex.getMessage());
		}
	%>

</TABLE>
<INPUT type="hidden" name="pagina" value="<%=request.getParameter("pagina")!=null?request.getParameter("pagina"):"0"%>">
<INPUT type="hidden" name="pagant" value="<%=request.getParameter("pagant")%>">
<INPUT type="hidden" name="fecha" value="<%=request.getParameter("fecha")%>">
<INPUT type="hidden" name="pventa" value="<%=request.getParameter("pventa")%>">
<INPUT type="hidden" name="seleccion" value="<%=request.getParameter("seleccion")%>">
<INPUT type="hidden" name="accion" value="imprimir">
</FORM>
</BODY>
</HTML>