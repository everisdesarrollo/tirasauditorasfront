<!-- AUTOR : Alfredo Resendiz Vargas    -->
<!-- CREACION :     26/02/2006   -->
<!-- DESCRIPCION : Consulta de de Sucursales Contingentes    -->
<!-- MODIFICACION :     			    -->

<%@ page import="java.util.*"%>
<%@ page import="com.santander.contingente.*"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%			
	String usuario = null;		
	String perfil = null;		
	String grupos = null;		
	usuario = request.getHeader("iv-user");		
	grupos = request.getHeader("iv-groups");		
			
	if (usuario == null || grupos == null) {		
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}		
%>			

<HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="../theme/Master.css" rel="stylesheet" type="text/css">
<SCRIPT src="../theme/calendario.js"></SCRIPT>
<SCRIPT language="JavaScript" src="../theme/utiljs.js"></SCRIPT>
<TITLE>Tira Auditora</TITLE>
<SCRIPT language="Javascript">

	//Funci�n para realizar la paginaci�n

  		function muevePag(pag,pagant)
		{
			document.frmConCtg.pagina.value = pag;
			document.frmConCtg.pagant.value = pagant;
			document.frmConCtg.submit();
		}

	//Funci�n para realizar el regreso de la pagina

		function fnRegresa()
		{
			document.frmConCtg.action = '../Menu/01Tira.jsp';
			document.frmConCtg.submit();
		}

	//Funci�n para realizar la impresion de la consulta

		function imprime()
		{
				document.frmConCtg.action = '../servlet/ContingenteServlet';
				document.frmConCtg.submit();
		}
</SCRIPT>
</HEAD>
<BODY>
<FORM id="frmConCtg" name="frmConCtg" method="post"	action="04ConCtg.jsp">
<TABLE width="100%" border="0">
	<TR>
		<TD>
		<P class="ATitulocolor">Consulta de Sucursales Contingentes</P>
		<%
		/** Rutina que realiza la Consulta de Sucursales Contingentes
		 */
	  java.text.SimpleDateFormat formato = new java.text.SimpleDateFormat("MM/dd/yyyy");
	  ManCtgClase manCtgClase = new ManCtgClase();
	  List resul = null;
	  resul = manCtgClase.consulDescSistema();
	  try
	  {
		int nopagina = Integer.parseInt(request.getParameter("pagina")!=null?request.getParameter("pagina"):"0");
        String mensaje = "";
        ArrayList pagina = new ArrayList();
		HttpSession sesion = request.getSession();
		ArrayList total = (ArrayList)sesion.getAttribute("consulctg");
		int seleccion =0;
		if(total.size()>0 && total.get(0).getClass().equals(ArrayList.class))
		{
			if(request.getParameter("pagant")!=null)
			{
		  		int pag = Integer.parseInt(request.getParameter("pagant"));
		  		ArrayList pagant = (ArrayList)total.get(pag);
			  	for(int j=0;j<pagant.size();j++)
			  	{
			  		ContingenteValue contingente = (ContingenteValue)pagant.get(j);
			  		if(request.getParameter("sele"+j)!=null)
			  			contingente.set_impresion(1);
			  		else
			  			contingente.set_impresion(0);
		  		}
		  		pagant = null;
			}
			pagina = (ArrayList)total.get(nopagina);
			seleccion = 0;
			System.out.println("seleccion: "+seleccion);
		}
		else
		{
		 	if(total.size()>0 && total.get(0).getClass().equals(String.class))
		  	{
				System.out.println("El texto es:"+(String)total.get(0));
				seleccion = 1;
				mensaje = (String)total.get(0);
			}
			else
			{
				System.out.println("No hay Datos");
				seleccion = 1;
				mensaje = "No hay Datos.";
			}
		}
		%>
		<TABLE width="90%" border="0" align="center" cellpadding="0" cellspacing="1">
			<TR>
				<TD colspan="6">
					<P><font color="gray">&nbsp;<%=sesion.getAttribute("message")%></font></P>
				</TD>
			</TR>
				<TR>
					<TD colspan="2" class="ALigaonce">
						<%if (nopagina != 0)
						  {%>
							<A href="javascript:muevePag('<%=nopagina-1%>','<%=nopagina%>');" class="txt">&lt;&lt;</A>
						<%}%>
						<%=(100*nopagina)+1%> a <%=(100*nopagina)+pagina.size()+seleccion%>
						<%if (seleccion == 0 && nopagina != total.size()-1)
						  {%>
							<A href="javascript:muevePag('<%=nopagina+1%>','<%=nopagina%>');" class="txt">&gt;&gt;</A>
						<%}%>
					</TD>
					<TD colspan="5" class="Atexnegro">
						Fecha:
						<INPUT type="text" name="fecha" tabindex="1" size="11" maxlength="10" class="Atextittab" readonly>
						<A href="javascript:cal1.popup()"> <IMG name="Calendario" border="0" src="../images/calendario.gif"></A>
						&nbsp;&nbsp;&nbsp;&nbsp;
						Tipo de Error:
						<select name="descSistema" class="Atextittab">
								<option value="%"> </option>
							<%
							Iterator itResul = resul.iterator();
							while(itResul.hasNext()){
								String tipoError = (String) itResul.next();
							%>
								<option value='<%=tipoError%>'><%=tipoError%> </option>
							<%
							}
							 %>
						</select>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="submit" value="Buscar" onclick="javascript:setFecha();">
					</TD>
				</TR>
				<TR>
					<TD width="8%" class="ATituloTabla">CdeC ALTAIR</TD>
					<TD width="18%" class="ATituloTabla">Fecha</TD>
					<TD width="17%" class="ATituloTabla">Nombre Sucursal</TD>
					<TD width="27%" class="ATituloTabla">Descripci�n Sistema</TD>
					<TD width="27%" class="ATituloTabla">Descripci�n Usuario</TD>
					<TD width="1%" class="ATituloTabla">Reactivaci�n Manual</TD>
					<TD width="1%" class="ATituloTabla">Imprimir</TD>
				</TR>
				<%
			if(seleccion==0){
				for (int i=0;i<pagina.size();i++)
				{
					ContingenteValue regCtg = (ContingenteValue)pagina.get(i);
					if(i%2==0)
					{
				%>
					<TR>
				<%
					}
					else
					{
				%>
					<TR  class="fondo">
				<%
					}
				%>
						<TD width="8%" align="center"><%=regCtg.get_suc_altair()%></TD>
						<TD width="18%" align="center"><%=regCtg.get_fecha()%></TD>
						<TD width="17%" align="center"><%=regCtg.get_nom_sucursal()%></TD>
						<TD width="27%" align="center"><%=regCtg.get_desc_sistema()%></TD>
				<%
						if(regCtg.get_desc_usuario() != null)
						{
				%>
							<TD width="27%" align="center"><%=regCtg.get_desc_usuario()%></TD>
				<%
						}
						else
						{
							String valor = "";
				%>
							<TD width="27%" align="center"><%=valor%></TD>
				<%
						}
				%>
						<TD width="1%" align="center"><%=regCtg.get_verificacion()%></TD>
					<%
					if(regCtg.get_impresion()==0)
					{
					%>
						<TD width="1%" align="center"><INPUT type="checkbox" name="sele<%=i%>" value="1"></TD>
					<%
					}
					else
					{
					%>
						<TD align="center"><INPUT type="checkbox" name="sele<%=i%>" value="1" checked></TD>
					<%
					}
					}
					%>
				</TR>
				<%
				}
				else
				{
				%>
				<TR>
					<TD colspan="6" align="center"><BR><BR><b>No hay datos</b></TD>
				</TR>
				<%
				}
				%>
				<TR>
					<TD colspan="5">
						<P>&nbsp;</P>
					</TD>
				</TR>
	</TABLE>
		<TABLE>
			<TR>
				<TD align="center" width="53%"><A href="#"></A></TD>
				<%if(seleccion != 1)
				{%>
				<TD align="center"><A href="javascript:fnRegresa();"><IMG border="0"
					name="continuar" src="../images/b_regresar.gif" align="left">
				</A>
				&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
				<TD align="center" colspan="10">
					<A href="javascript:imprime();">
						<IMG name="btnimprimir" border="0" src="../images/b_imprimir.gif">
					</A>
				</TD>
			</TR>
	<%			}
		}
		catch (Exception ex)
		{
			System.out.println("Error en Consulta de Sucursales Contingente/04ConCtg.jsp: " + ex.getMessage());
		}
	%>
		</TABLE>
	</TD>
	</TR>
</TABLE>
<INPUT type="hidden" name="seleccion" value="<%=request.getParameter("seleccion")%>">
<INPUT type="hidden" name="pagina" value="<%=request.getParameter("pagina")!=null?request.getParameter("pagina"):"0"%>">
<INPUT type="hidden" name="pagant" value="<%=request.getParameter("pagant")%>">
<INPUT type="hidden" name="accion" value="imprimir">
<INPUT type="hidden" name="afecha" value="<%=formato.format(new java.util.Date())%>">
</FORM>
<SCRIPT language="JavaScript">
	var cal1 = new calendario(document.frmConCtg.fecha,document.frmConCtg.afecha);
	function setFecha(){
		if(document.frmConCtg.fecha.value.length != 10){
			document.frmConCtg.afecha.value = "ALL";
		} else {
			document.frmConCtg.afecha.value = document.frmConCtg.fecha.value;
		}
		document.frmConCtg.action = '../servlet/ContingenteServlet';
		document.frmConCtg.accion.value = "read";
	}
</SCRIPT>
</BODY>
</HTML>