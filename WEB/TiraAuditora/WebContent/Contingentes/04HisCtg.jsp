<!-- AUTOR : Alfredo Resendiz Vargas    -->
<!-- CREACION :     26/02/2006   -->
<!-- DESCRIPCION : Consulta del Hist�rico de Sucursales Contingentes    -->
<!-- MODIFICACION :     			    -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%			
	String usuario = null;		
	String perfil = null;		
	String grupos = null;		
	usuario = request.getHeader("iv-user");		
	grupos = request.getHeader("iv-groups");		
			
	if (usuario == null || grupos == null) {		
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}		
%>			

<HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<SCRIPT language="JavaScript" src="../theme/utiljs.js"></SCRIPT>
<LINK href="../theme/Master.css" rel="stylesheet"
	type="text/css">
<TITLE>Historico de Sucursales Contingentes.jsp</TITLE>
<SCRIPT src="../theme/calendario.js"></SCRIPT>
<SCRIPT src="../theme/validaciones.js"></SCRIPT>
<SCRIPT language="Javascript">
	var valida_fecha = new Array('fecha','Fecha','D',false,0,0);
	var valida_pventa = new Array('pventa','Punto de Venta','N',true,4,4);

//	Funci�n la validaci�n de los campos de entrada

	function valida(campo,propiedades)
	{

	   var lMensaje="";
	   var lTempMssg=assertField(campo, propiedades);
	   if (lTempMssg.length>1)
      	lMensaje+=assertField(campo, propiedades)+"\n";

		if (lMensaje.length>0)
    	{
	      alert(lMensaje);
    	  return false;
	    }
    	else
	    {
    	  return true;
	    }
    }

//Funci�n para limpiar el contenido de los campos de b�squeda

	function fnLimpia()
	{
		document.frmhisCtg.fecha.value = '';
		document.frmhisCtg.pventa.value = '';
	}

//Funci�n que realiza las validaciones y env�a a busqueda la consulta

	function busca()
	{
		var opcion = 1;
		if((document.frmhisCtg.fecha.value == ""))
		{
			opcion = '0';
			alert("Debe ingresar la Fecha");
		}
		else
		{
			if(!valida(document.frmhisCtg.pventa,valida_pventa))
			{
				document.frmhisCtg.pventa.focus();
				document.frmhisCtg.pventa.select();
				opcion = '0';
			}
		}

		if(opcion=='1')
		{
			document.frmhisCtg.accion.value = 'buscar';
			document.frmhisCtg.submit();
		}
	}

</SCRIPT>
</HEAD>
<BODY>
<FORM id="frmhisCtg" name="frmhisCtg" method="post" action="../servlet/ManttoCtgServlet">
<TABLE width="100%" border="0">
	<TR>
		<TD align="left" width="100%">
		<P class="ATitulocolor">Hist�rico de Sucursales Contingentes</P><BR><BR>
		</TD>
	</TR>
</TABLE>

<TABLE width="80%" border="0" cellspacing="0" align="center">
	<TR>
		<TD colspan="2" align="center"></TD>
	</TR>
	<TR>
		<TD colspan="2" align="center">&#160;</TD>
	</TR>
	<TR>
		<TD colspan="5" class="Atittabcenazu">&nbsp; Criterios de B�squeda</TD>
	</TR>
	<TR class="AEtiquetaDentro">
		<TD align="right" class="AEtiquetaDentro">Fecha:</TD>
		<TD>
		<INPUT type="text" name="fecha" tabindex="1" size="11" maxlength="10" class="Atextittab" readonly><A href="javascript:cal1.popup()">
					<IMG name="Calendario" border="0" src="../images/calendario.gif"></A>
		</TD>
	</TR>
		<TR class="AEtiquetaDentro">
		<TD align="right" class="AEtiquetaDentro">Centro de Costos ALTAIR:</TD>
			<TD>
	          	<INPUT type="text" name="pventa" size="11" class="Atextittab" maxlength="4" onBlur="javascript:valida(this,valida_pventa);">
	          </TD>
	    </TR>
	<TR>
		<TD colspan="5" class="Atittabcenazu">&nbsp;</TD>
	</TR>
	<TR>
		<TD colspan="2">&#160;</TD>
	</TR>
	<TR>
		<TD colspan="2"></TD>
	</TR>
</TABLE>
<TABLE>
	<TR>
		<TD align="center" width="53%"><A href="#"></A></TD>
		<TD align="center"><a href="javascript:fnLimpia();"><IMG border="0"
			name="continuar" src="../images/b_limpiar.gif" align="left"></A>
		&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
		<TD align="center"><a href="javascript:busca();"><IMG border="0"
			name="continuar" src="../images/b_buscar.gif" align="left"></A>
		</TD>
	</TR>
</TABLE>
	<INPUT type="hidden" name="accion" value="">
	<%
	java.text.SimpleDateFormat formato = new java.text.SimpleDateFormat("MM'/'dd'/'yyyy");
	%>
	<INPUT type="hidden" name="afecha" value="<%=formato.format(new java.util.Date())%>">
	<INPUT type="hidden"  name="seleccion" value="1">
</FORM>
<SCRIPT language="JavaScript">
	var cal1 = new calendario(document.frmhisCtg.fecha,document.frmhisCtg.afecha);
</SCRIPT>
</BODY>
</HTML>