<!-- AUTOR : Alfredo Resendiz Vargas    -->
<!-- CREACION :     16/02/2006   -->
<!-- DESCRIPCION : Consulta de b�squeda del Mantto. de Sucursales Contingentes    -->
<!-- MODIFICACION :     			    -->

<%@ page import="java.util.*"%>
<%@ page import="com.santander.contingente.*"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%			
	String usuario = null;		
	String perfil = null;		
	String grupos = null;		
	usuario = request.getHeader("iv-user");		
	grupos = request.getHeader("iv-groups");		
			
	if (usuario == null || grupos == null) {		
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}		
%>			

<HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<SCRIPT language="JavaScript" src="../theme/utiljs.js"></SCRIPT>
<LINK href="../theme/Master.css" rel="stylesheet" type="text/css">
<TITLE>Mantenimiento de Sucursales Contingentes.jsp</TITLE>
<SCRIPT src="../theme/calendario.js"></SCRIPT>
<SCRIPT src="../theme/validaciones.js"></SCRIPT>
<SCRIPT language="Javascript">

	var valida_fecha = new Array('fecha','Fecha','D',false,0,0);
	var valida_pventa = new Array('pventa','Punto de Venta','N',true,4,4);

//	Funci�n la validaci�n de los campos de entrada

	function valida(campo,propiedades)
	{
   	   var lMensaje="";
	   var lTempMssg=assertField(campo, propiedades);

	    if (lTempMssg.length>1)
      	lMensaje+=assertField(campo, propiedades)+"\n";

		if (lMensaje.length>0)
    	{
	      alert(lMensaje);
    	  return false;
	    }
    	else
	    {
    	  return true;
	    }
	}

	function fnLimpia()
	{
		document.frmmanCtg.fecha.value = '';
		document.frmmanCtg.pventa.value = '';
	}

	function busca()
	{
		if(document.frmmanCtg.fecha.value == "" && document.frmmanCtg.pventa.value != "")
		{
		   alert("Favor de capturar la fecha");
		}
		else
		{
			if(!valida(document.frmmanCtg.pventa,valida_pventa))
			{
				document.frmmanCtg.pventa.focus();
			}
			else
			{
				if(document.frmmanCtg.pventa.value=="0000")
				{
					document.frmmanCtg.pventa.value=""
					document.frmmanCtg.accion.value = 'buscar';
					document.frmmanCtg.submit();
				}
				else
				{
					document.frmmanCtg.accion.value = 'buscar';
					document.frmmanCtg.submit();
				}
			}
		}
	}
</SCRIPT>
</HEAD>
<BODY>
<FORM id="frmmanCtg" name="frmmanCtg" method="post" action="../servlet/ManttoCtgServlet">
<%
	/**
	 *  VCM 23/07/12 - Obtiene lista de tipos de error
	 */
	ManCtgClase manCtgClase = new ManCtgClase();
	List resul = manCtgClase.consulDescSistema();

%>
<TABLE width="90%" border="0">
	<TR>
		<TD colspan="2" align="left" width="100%">
		<P class="ATitulocolor">Mantenimiento de Sucursales Contingentes</P>
		<BR>
		<BR>
		</TD>
	</TR>
</TABLE>
<TABLE width="80%" border="0" cellspacing="0" align="center">
	<TR>
		<TD colspan="3" align="center"></TD>
	</TR>
	<TR>
		<TD colspan="2" align="center">&#160;</TD>
	</TR>
	<TR>
		<TD colspan="5" class="Atittabcenazu">&nbsp;Criterios de B�squeda</TD>
	</TR>
	<TR class="AEtiquetaDentro">
		<td align="right" class="AEtiquetaDentro">Fecha:</td>
		<TD><INPUT type="text" name="fecha" tabindex="1" size="11"
			maxlength="10" class="Atextittab" readonly> <A
			href="javascript:cal1.popup()"> <IMG name="Calendario" border="0"
			src="../images/calendario.gif"></A></TD>
	</TR>
	<TR class="AEtiquetaDentro">
		<TD align="right" class="AEtiquetaDentro">Centro de Costos
		ALTAIR:</TD>
		<TD><INPUT type="text" name="pventa" size="11" class="Atextittab"
			maxlength="4" value=""
			onBlur="javascript:valida(this,valida_pventa);"></TD>
	</TR>
	<!-- VCM 23/07/12 Filtro por tipo de error -->
	<TR class="AEtiquetaDentro">
		<TD align="right" class="AEtiquetaDentro">Tipo de error:</TD>
		<TD>
			<select name="descSistema" class="Atextittab">
					<option value=""> </option>
				<%
				Iterator itResul = resul.iterator();
				while(itResul.hasNext()){
					String tipoError = (String) itResul.next();
				%>
					<option value='<%=tipoError%>'><%=tipoError%> </option>
				<%
				}
				 %>
			</select>
		</TD>
	</TR>
	<TR>
		<TD colspan="5" class="Atittabcenazu">&nbsp;</TD>
	</TR>
	<TR>
		<TD colspan="2">&#160;</TD>
	</TR>
</TABLE>
<TABLE>
	<TR>
		<TD align="center" width="53%"><A href="#"></A></TD>
		<TD align="center"><A href="javascript:fnLimpia();"><IMG
			border="0" name="continuar" src="../images/b_limpiar.gif"
			align="left"></A>
		&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;

		<TD align="center"><A href="javascript:busca();"><IMG
			border="0" name="continuar" src="../images/b_buscar.gif" align="left"></A></TD>
	</TR>
</TABLE>
<INPUT type="hidden" name="accion" value=""> <%
 	java.text.SimpleDateFormat formato = new java.text.SimpleDateFormat(
 			"MM'/'dd'/'yyyy");
 %> <INPUT type="hidden" name="afecha"
	value="<%=formato.format(new java.util.Date())%>"> <INPUT
	type="hidden" name="seleccion" value="0"></FORM>
<SCRIPT language="JavaScript">
	var cal1 = new calendario(document.frmmanCtg.fecha,document.frmmanCtg.afecha);
</SCRIPT>
</BODY>
</HTML>