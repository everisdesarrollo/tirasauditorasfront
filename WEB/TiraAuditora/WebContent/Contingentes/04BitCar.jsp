
<%@ page import="java.util.*"%>
<%@ page import="com.santander.contingente.*"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%			
	String usuario = null;		
	String perfil = null;		
	String grupos = null;		
	usuario = request.getHeader("iv-user");		
	grupos = request.getHeader("iv-groups");		
			
	if (usuario == null || grupos == null) {		
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}		
%>			

<HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="../theme/Master.css" rel="stylesheet" type="text/css">
<SCRIPT language="JavaScript" src="../theme/utiljs.js"></SCRIPT>
<TITLE>Tira Auditora</TITLE>
<SCRIPT language="Javascript">
	//Función para realizar la paginación
 	function muevePag(pag,pagant)
	{
		window.location.href="04BitCar.jsp?pagina="+pag+"&pagant="+pagant;
	}

</SCRIPT>
</HEAD>
<BODY>
<FORM id="frmMonJob" name="frmMonJob" method="post"	action="../servlet/ContingenteServlet">
<TABLE width="100%" border="0">
	<TR>
		<TD>
		<P class="ATitulocolor">Bitácora de Carga Diaria</P>
<%
		/** Rutina que realiza la Consulta de la Bitacora
		 */
		CargaDiariaClase cargaDiaria = new CargaDiariaClase();
		List resul = cargaDiaria.consulDescSistema();

		try {
			int nopagina = Integer.parseInt(request.getParameter("pagina") != null ?
							request.getParameter("pagina")
							: "0");
			int contador = 0;
			HttpSession sesion = request.getSession();
			ArrayList total = (ArrayList) sesion.getAttribute("bitacoraResult");
			ArrayList paginaResult = new ArrayList();
			int opcion = 0;
			if (total != null && total.size() > 0 && total.get(0).getClass().equals(ArrayList.class) && ((ArrayList)total.get(0)).size() > 0) {
				paginaResult = (ArrayList) total.get(nopagina);
				contador = (total.size() * 100) - 100 + ((ArrayList)total.get(total.size()-1)).size();
				System.out.println("TAUD Lista de resultados de Bitacora de Carga Diaria (" + paginaResult.size() + ")");
			} else {
				opcion = 1;
			}
%>
			<TABLE width="90%" border="0" align="center" cellpadding="0" cellspacing="1">
				<TR>
					<TD colspan="6">
					<P><font color="gray">&nbsp;<%=sesion.getAttribute("message")%></font></P>
					</TD>
				</TR>
				<TR>
					<TD class="ALigaonce">
<%
			if (nopagina != 0) {
%>
					<A href="javascript:muevePag('<%=nopagina - 1%>','<%=nopagina%>');" class="txt">&lt;&lt;</A>
<%
			}
%>
					<%=(100 * nopagina) + 1%> a <%=(100 * nopagina) + paginaResult.size() + opcion%> de <%=contador%>
<%
			if (opcion == 0 && nopagina != total.size() - 1) {
%>
					 <A href="javascript:muevePag('<%=nopagina + 1%>','<%=nopagina%>');" class="txt">&gt;&gt;</A> <%
	 		}
%>
					</TD>

					<TD colspan="6" class="Atexnegro" >
						&nbsp;&nbsp;&nbsp;&nbsp;
						Estatus:
						<select name="descSistema" class="Atextittab">
								<option value="%"> </option>
							<%
							Iterator itResul = resul.iterator();
							while(itResul.hasNext()){
								String tipoError = (String) itResul.next();
							%>
								<option value='<%=tipoError%>'><%=tipoError%> </option>
							<%
							}
							 %>
						</select>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input value="Buscar" type="submit" >
					</TD>
				</TR>

<%
			if (opcion == 0) {
%>
				<TR>
					<TD class="ATituloTabla">SUCURSAL</TD>
					<TD class="ATituloTabla">ESTATUS</TD>
					<TD class="ATituloTabla">HORA_DE_CARGA</TD>
					<TD class="ATituloTabla">FIN_DE_CARGA</TD>
				</TR>
<%
				for (int i = 0; i < paginaResult.size(); i++) {
					CargaDiariaValue regBit = (CargaDiariaValue) paginaResult.get(i);
%>
				<TR<%if (i % 2 != 0){ %> class="fondo"<%} %>>

					<TD align="center"><%=regBit.getSucursal()%></TD>
					<TD align="center"><%=regBit.getEstatus()%></TD>
					<TD align="center"><%=regBit.getHoraDeCarga()%></TD>
					<TD align="center"><%=regBit.getFinDeCarga()%></TD>
<%
				}
%>
				</TR>
<%
			} else if (opcion == 1){
%>
				<TR>
					<TD colspan="6" align="center"><BR><BR><b>No hay datos</b></TD>
				</TR>
<%
			}
%>
				<TR>
					<TD colspan="5">
					<P>&nbsp;</P>
					</TD>
				</TR>
<%
		} catch (Exception ex) {
			System.out.println("Error en Consulta de Sucursales Schedulers/14ConSch.jsp: " + ex.getMessage());
		}
%>
			</TABLE>
		</TD>
	</TR>
</TABLE>

<INPUT type="hidden" name="accion" value="bitacora">
<INPUT type="hidden" name="pagina" value="<%=request.getParameter("pagina")!=null?request.getParameter("pagina"):"0"%>">
<INPUT type="hidden" name="pagant" value="<%=request.getParameter("pagant")%>">

</FORM>
</BODY>
</HTML>