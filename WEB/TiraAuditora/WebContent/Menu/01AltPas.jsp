<!-- AUTOR : Alfredo Resendiz Vargas    -->
<!-- CREACION :     26/02/2006   -->
<!-- DESCRIPCION : Pantalla para cambiar la contrase�a de usuario    -->
<!-- MODIFICACION :     			    -->

<%@ page import="java.util.*"%>
<%@ page import="com.santander.autentificacion.*"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<SCRIPT language="JavaScript" src="../theme/utiljs.js"></SCRIPT>
<LINK href="../theme/Master.css" rel="stylesheet"
	type="text/css">
<TITLE>Tira Auditora</TITLE>
<SCRIPT src="../theme/validaciones.js"></SCRIPT>
<SCRIPT language="Javascript">

	var valida_password = new Array('password','Contrase�a','A',true,0,8);

// Funci�n para limpiar el contenido de los campos

	function fnLimpia()
	{
		document.frmlogini.contrasena.value = '';
		document.frmlogini.contrasena2.value = '';
	}

// Funci�n que valida la contrase�a y envia para que se actualice

	function enviar()
	{
		var opcion = 1;
			if((document.frmlogini.contrasena.length > 8))
			{
				alert("Contrase�a no debe ser mayor a 8 caracteres");
				opcion = '0';
				document.frmlogini.contrasena.focus();
			}
		else
		{
			if((document.frmlogini.contrasena.value == ""))
			{
				alert("Debe ingresar su contrase�a nueva");
				opcion = '0';
			}
			else
				if((document.frmlogini.contrasena2.value == ""))
				{
					alert("Debe confirmar su contrase�a nueva");
					opcion = '0';
				}
				else
					if((document.frmlogini.contrasena.value != document.frmlogini.contrasena2.value))
					{
						alert("Contrase�a no coincide");
						opcion = '0';
						document.frmlogini.contrasena2.focus();
					}
				}

		if(opcion=='1')
		{
			document.frmlogini.accion.value = 'actualizar';
			document.frmlogini.submit();
		}
	}

// Funci�n que direcciona el foco hacia el campo de contrase�a

	function fnFoco()
	{
		document.frmlogini.contrasena.focus();
	}
</SCRIPT>
</HEAD>
<BODY onLoad="javascript:fnFoco();">
<FORM id="frmlogini" name="frmlogini" method="post" action="../servlet/LogueoServlet" >
<TABLE width="60%" border="0" cellspacing="0" align="center">
<%	HttpSession sesion = request.getSession();
  	ArrayList total = (ArrayList)sesion.getAttribute("consulogueo");
	LogueoValue regLogueo = (LogueoValue)total.get(0);
%>
	<TR>
		<TD width="100%" align="center"><BR><BR>
			<P class="ATitulocolor">Cambio de Contrase�a</P><BR>
		</TD>
		<TD colspan="2" align="left" width="100%">
			<BR><BR><BR><BR><BR><BR>
		</TD>
</TABLE>
<TABLE width="40%" border="0" cellspacing="0" align="center">
	<TR>
		<TD colspan="3" align="center"></TD>
	</TR>
	<TR>
		<TD colspan="2" align="center">&#160;</TD>
	</TR>
	<TR>
		<TD colspan="6" class="Atittabcenazu">&nbsp; Escribir su contrase�a nueva</TD>
	</TR>
	<TR class="AEtiquetaDentro">
	<TD align="right" class="AEtiquetaDentro">Contrase�a:</td>
		<TD>
          	<INPUT type="password" name="contrasena" size="10" class="Atextittab" maxlength="8">
          </TD>
    </TR>
    <TR class="AEtiquetaDentro">
	<TD align="right" class="AEtiquetaDentro">Confirmar Contrase�a:</td>
		<TD>
          	<INPUT type="password" name="contrasena2" size="10" class="Atextittab" maxlength="8" >
          </TD>
    </TR>

		<TD colspan="6" class="Atittabcenazu">&nbsp;</TD>
	<TR>
		<TD colspan="2">&#160;</TD>
	</TR>
	<TR>
		<TD colspan="2"></TD>
	</TR>
<TABLE width="30%" border="0" cellspacing="0" align="center">
	<TR>
		<TD align="center" width="60%"><A href="#"></A></TD>
		<TD align="center"><A href="javascript:fnLimpia();"><IMG border="0"
			name="limpiar" src="../images/b_limpiar.gif" align="left"></A></TD>
		<TD align="center"><A href="javascript:enviar();"><IMG border="0"
			name="continuar" src="../images/b_enviar.gif" align="left"></A></TD>
	</TR>
</TABLE>
</TABLE>
	<INPUT type="hidden" name="accion" value="">
</FORM>
</BODY>
</HTML>