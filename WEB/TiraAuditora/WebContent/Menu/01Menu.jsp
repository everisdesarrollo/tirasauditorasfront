<!-- AUTOR : Alfredo Resendiz Vargas    -->
<!-- CREACION :     26/02/2006   -->
<!-- DESCRIPCION : Menu de la aplicaci�n    -->
<!-- MODIFICACION :     			    -->

<%@ page import="java.util.*"%>
<%@ page import="com.santander.autentificacion.*"%>

<HTML>
<HEAD>
<%@ page
language="java"
contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"
session="true"
%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<STYLE>
.ATitulocolor {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #FF3333;
	font-weight: bold;
	text-decoration: none;
	font-style: normal;
}
A:link {text-decoration: none}
A:visited {text-decoration: none}
A:active {text-decoration: none}
A:hover {text-decoration: underline}
</STYLE>
<TITLE>Applicacion Tiras Auditoras</TITLE>
</HEAD>

<jsp:include page="../cargando.jsp"/>
<BODY bgcolor="#f2f2f2" text="#ffffff" link="#000000" alink="#000000" vlink="#000000">
<FORM id="formenu" name="formenu" method="post" action="../servlet/LogueoServlet" target="_top">
<TABLE width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
	<TR bgcolor="#686868">
		<TD>
			<A href="http://altec.mx.bsch/intranet.htm" target="_blank"><IMG border="0" src="../images/intranet.gif"></A>
		</TD>
	</TR>
	<TR height="22">
		<TD background="../images/menu_slide.jpg">
			<FONT size="4" color="white"><B>&nbsp;&nbsp;Men�</B></FONT>
		</TD>
	</TR>
	<TR bgcolor="#f2f2f2">
		<TD>&nbsp;</TD>
	</TR>
<%
  try
  {
 	HttpSession sesion = request.getSession();
  	ArrayList total = (ArrayList)sesion.getAttribute("consulogueo");
	int bandera=0;

	for (int i=0;i<total.size();i++)
	{
		LogueoValue regLogueo = (LogueoValue)total.get(i);
		if(regLogueo.getfac01()!=null || regLogueo.getfac02()!=null || regLogueo.getfac04()!=null || regLogueo.getfac16()!=null)
		{
			bandera=1;
		}
	}
	if(bandera ==1)
	{
		bandera=0;
%>
		<TR height="22" valign="middle">
			<TD background="../images/menu_slide.jpg">
				<FONT size="3"><b>&nbsp;&nbsp;Consulta</b></FONT>
			</TD>
		</TR>
<%
	}
	for (int i=0;i<total.size();i++)
	{
		LogueoValue regLogueo = (LogueoValue)total.get(i);
		if(regLogueo.getfac01()!=null)
		{
%>
		<TR>
			<TD>
				<FONT size="2">&#160;&#160;&#160;&#160;&#160;<a href="../Tira/02ConTir.jsp" target="centro" >Tira Auditora</a></font>
			</TD>
		</TR>
<%
		}
		if(regLogueo.getfac02()!=null)
		{
%>
		<TR>
			<TD>
				<FONT size="2">&#160;&#160;&#160;&#160;&#160;<a href="../Sucursales/03ConSuc.jsp" target="centro" >Sucursal</a></font>
			</TD>
		</TR>
<%
		}

		if(regLogueo.getfac04()!=null)
		{
%>
		<TR>
			<TD>
				<FONT size="2">&#160;&#160;&#160;&#160;&#160;<a href="../servlet/ContingenteServlet" target="centro" >Sucursal Contingente</a></font>
			</TD>
		</TR>
<%
		}

		if(regLogueo.getfac16()!=null)
		{
%>
		<TR>
			<TD>
				<FONT size="2">&#160;&#160;&#160;&#160;&#160;<a href="../servlet/ContingenteServlet?accion=bitacora" target="centro" >Bit�cora de Carga</a></font>
			</TD>
		</TR>
<%
		}
    }
	for (int i=0;i<total.size();i++)
	{
		LogueoValue regLogueo = (LogueoValue)total.get(i);
		if(regLogueo.getfac03()!=null || regLogueo.getfac05()!=null || regLogueo.getfac06()!=null || regLogueo.getfac07()!=null || regLogueo.getfac10()!=null ||
		   regLogueo.getfac11()!=null || regLogueo.getfac12()!=null || regLogueo.getfac13()!=null || regLogueo.getfac14()!=null || regLogueo.getfac15()!=null)
		{
			bandera=1;
		}
	}
	if(bandera ==1)
	{
		bandera=0;
%>
	<TR bgcolor="#f2f2f2">
		<TD>&nbsp;</TD>
	</TR>
	<TR height="22" valign="middle">
		<TD background="../images/menu_slide.jpg">
			<FONT size="3"><b>&nbsp;&nbsp;Mantenimiento</b></FONT>
		</TD>
	</TR>
<%
	}
	for (int i=0;i<total.size();i++)
	{
		LogueoValue regLogueo = (LogueoValue)total.get(i);

		if(regLogueo.getfac06()!=null)
		{
%>
		<TR>
			<TD>
				<FONT size="2">&#160;&#160;&#160;&#160;&#160;<a href="../Tira/07CarMan.jsp" target="centro" >Carga Tira Auditora</a></font>
			</TD>
		</TR>
<%
		}
		if(regLogueo.getfac03()!=null)
		{
%>
		<TR>
			<TD>
				<FONT size="2">&#160;&#160;&#160;&#160;&#160;<a href="../Sucursales/03MtoSuc.jsp" target="centro" >Sucursal</a></font>
			</TD>
		</TR>
<%
		}
		if(regLogueo.getfac05()!=null)
		{
%>
		<TR>
			<TD>
				<FONT size="2">&#160;&#160;&#160;&#160;&#160;<a href="../Contingentes/04ManCtg.jsp" target="centro" >Sucursal Contingente</a></font>
			</TD>
		</TR>
<%
		}
		if(regLogueo.getfac07()!=null)
		{
%>
		<TR>
			<TD>
				<FONT size="2">&#160;&#160;&#160;&#160;&#160;<a href="../IngresosEgresos/05GenArc.jsp" target="centro" >Ingresos/Egresos</a></font>
			</TD>
		</TR>
<%
		}

		if(regLogueo.getfac11()!=null)
		{
%>
		<TR>
			<TD>
				<FONT size="2">&#160;&#160;&#160;&#160;&#160;<A href="../CatOperaciones/08MtoOpe.jsp" target="centro" >Operaciones</A></FONT>
			</TD>
		</TR>
<%
	     }
	     if(regLogueo.getfac13()!=null)
		 {
%>
		<TR>
			<TD>
				<FONT size="2">&#160;&#160;&#160;&#160;&#160;<A href="../AdminUser/09SelPer.jsp" target="centro" >Admon. Perfil</A></FONT>
			</TD>
		</TR>
<%
		}
		if(regLogueo.getfac14()!=null)
		{
%>
		<TR>
			<TD>
				<FONT size="2">&#160;&#160;&#160;&#160;&#160;<a href="../Sucursales/03MtoSucBas.jsp" target="centro" >Estatus Sucursal</a></font>
			</TD>
		</TR>
<%
		}
		if(regLogueo.getfac15()!=null)
		{
%>
		<TR>
			<TD>
				<FONT size="2">&#160;&#160;&#160;&#160;&#160;<a href="../servlet/SchedulerServlet" target="centro" >Schedulers Jobs</a></font>
			</TD>
		</TR>

<%
	     }
	  }
	for (int i=0;i<total.size();i++)
	{
		LogueoValue regLogueo = (LogueoValue)total.get(i);
		if(regLogueo.getfac09()!=null)
		{
			bandera=1;
		}
	}
	if(bandera ==1)
	{
		bandera=0;
%>
		<TR bgcolor="#f2f2f2">
			<TD>&nbsp;</TD>
		</TR>
		<TR height="22" valign="middle">
			<TD background="../images/menu_slide.jpg">
				<FONT size="3"><b>&nbsp;&nbsp;Hist�rico</b></FONT>
			</TD>
		</TR>
<%
	}
	for (int i=0;i<total.size();i++)
	{
		LogueoValue regLogueo = (LogueoValue)total.get(i);

		if(regLogueo.getfac09()!=null)
		{
%>
		<TR>
			<TD>
				<FONT size="2">&#160;&#160;&#160;&#160;&#160;<A href="../Contingentes/04HisCtg.jsp" target="centro" >Sucursal Contingente</A></FONT>
			</TD>
		</TR>
<%
		}
	}
  }
  catch (Exception ex)
  {
	System.out.println("Error en construcci�n de menu/01Menu.jsp: " + ex.getMessage());
  }
%>
	<TR bgcolor="#f2f2f2">
		<TD>&nbsp;</TD>
	</TR>

	<TR>
	<TD bgcolor="white">
		<INPUT type="hidden" name="accion" value="">
		<FONT size="2" color="FFFFFF">&#160;&#160;<A href="javascript:top.location.replace('../servlet/LogOutSAM');" ><b>Cerrar Sesi�n</b></A></FONT>
	</TD>
	</TR>
</TABLE>
</FORM>
</BODY>
</HTML>