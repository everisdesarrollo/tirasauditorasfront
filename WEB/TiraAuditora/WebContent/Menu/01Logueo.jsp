<!-- AUTOR : Alfredo Resendiz Vargas    -->
<!-- CREACION :     26/02/2006   -->
<!-- DESCRIPCION : Pantalla para validar acceso a la aplicación    -->
<!-- MODIFICACION :     			    -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<SCRIPT language="JavaScript" src="../theme/utiljs.js"></SCRIPT>
<LINK href="../theme/Master.css" rel="stylesheet"
	type="text/css">
<TITLE>Tira Auditora</TITLE>
<STYLE type="text/css">
.iTable
{
	background-image: url("../images/l_logoagua2.gif");
	background-repeat: no-repeat;
	background-position: center center;
}
</STYLE>
<SCRIPT src="../theme/validaciones.js"></SCRIPT>
<SCRIPT language="Javascript">

	function fnLimpia()
	{
		document.frmlogini.usuario.value = '';
		document.frmlogini.contrasena.value = '';
		document.frmlogini.usuario.focus();
	}

	function enviar()
	{
		var opcion = 1;
		if((document.frmlogini.usuario.length > 8))
		{
			opcion = '0';
			alert("Nombre de usuario no debe ser mayor a 8 caracteres");
		}
		else
		{
			if((document.frmlogini.usuario.value == ""))
			{
				alert("Debe ingresar su usuario");
				opcion = '0';
				document.frmlogini.usuario.focus();
			}
			else
			{
				if((document.frmlogini.contrasena.length > 8))
				{
					alert("Contraseña no debe ser mayor a 8 caracteres");
					opcion = '0';
					document.frmlogini.contrasena.focus();
				}
				else
				{
					if((document.frmlogini.contrasena.value == ""))
					{
						alert("Debe ingresar su contraseña");
						opcion = '0';
					}
				}
			}
		}

		if(opcion=='1')
		{
			document.frmlogini.accion.value = 'enviar';
			document.frmlogini.submit();
		}
	}

	function fnCambioPass()
	{
		document.frmlogini.action = '01AltPas.jsp';
		document.frmlogini.submit();
	}


	function fnFoco()
	{
		document.frmlogini.usuario.focus();
	}

	function fnEnter()
	{
      if (event.keyCode == 13)
	   	enviar();
	}
</SCRIPT>
</HEAD>
<BODY onLoad="javascript:fnFoco();" onKeyDown="javascript:fnEnter();">
<FORM id="frmlogini" name="frmlogini" method="post" action="../servlet/LogueoServlet">
<TABLE width="100%" border="0" cellspacing="0">
	<TR>
		<TD><BR><BR>
			<P class="ATitulocolor">&nbsp;Acceso Tiras Auditoras</P><BR>
		</TD>
		<TD><A><IMG border="0"name="logo" src="../images/l_logo.gif" align="right"></A></TD>
	</TR>
</TABLE>
<TABLE width="50%" border="0" cellspacing="0" align="center"  class ="iTable">
	<TR>
		<TD>
			<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>
		</TD>
	</TR>
	<TR>
		<TD colspan="6" class="Atittabcenazu">&nbsp;Escribir su usuario y contraseña </TD>
	</TR>
	<TR class="AEtiquetaDentro">
		<TD align="right" class="AEtiquetaDentro">Usuario:</TD>
		<TD>
		<INPUT type="text" name="usuario" tabindex="1" size="10" maxlength="8" class="Atextittab">
		</TD>

	<TR class="AEtiquetaDentro">
		<TD align="right" class="AEtiquetaDentro">Contraseña:</td>
		<TD>
	      	<INPUT type="password" name="contrasena" size="10" tabindex="2" class="Atextittab" maxlength="8" >
	    </TD>
	<TR>
		<TD colspan="6" class="Atittabcenazu">&nbsp;</TD>
	</TR>
	<TR>
		 <TD><BR><BR><BR></TD>
	</TR>
</TABLE>
<TABLE width="40%" border="0" align="center">
	<TR>
		<TD align="center"><A href="javascript:fnLimpia();" ><IMG border="0"
			src="../images/b_limpiar.gif" align="right"></A></TD>
		<TD align="center"><A href="javascript:enviar();"><IMG border="0"
			src="../images/b_enviar.gif"  align="right"></A></TD>
</TABLE>
	<INPUT type="hidden" name="accion" value="">
</FORM>
</BODY>
</HTML>