<%@ page import="java.io.*" %>
<%@ page import="com.santander.utilerias.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.util.ArrayList.*"%>
<%@ page import="javax.servlet.http.*"%>
<%@ page import="com.santander.sucursal.*"%>
<%@ page import ="com.santander.utilerias.*"%>
<%
	String usuario = null;
	String perfil = null;
	String grupos = null;
	usuario = request.getHeader("iv-user");
	grupos = request.getHeader("iv-groups");
	
	if (usuario == null || grupos == null) {
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}	
%>
<HTML>
<HEAD>
<%@ page
language="java"
contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"
%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="../theme/Master.css" rel="stylesheet" type="text/css">
<LINK rel="stylesheet" href="../theme/Master.css" type="text/css">
<SCRIPT language="JavaScript" src="../theme/utiljs.js"></SCRIPT>

<%
	String sBandera = request.getParameter("bandera");
	int sPag = Integer.parseInt(request.getParameter("pag"));
	String sXml2 = request.getParameter("dat");
	System.out.println("antes de crear el objeto conecta");
	AccesoServlet conecta =  new AccesoServlet();
	System.out.println("Despues de conecta");
	Vector vCadena = new Vector();
	Vector vDatos  = new Vector();
	ArrayList arrDatos = new ArrayList();
	LoadProperties rb=LoadProperties.getInstance(); 
	String lsXmlServlet = "";
	String reEnvioDatos ="";
	String sTitulo = "";
	int sOculto =0;
	String uno ="";
	String dos ="";
	String tres = "";
	int campos = 13;
	int cuantos = 0;
	int paginas = 0;
	int sueltos = 0;
	int regxpag = Integer.parseInt(rb.getValue("ta.noregistros"));
	int inicio = 0;
	int fcont = 0;
	if(sBandera.equals("0")){
	sOculto = Integer.parseInt(request.getParameter("oculto"));
	uno = request.getParameter("txtNumSuc");
	dos = request.getParameter("txtServidor");
	tres = request.getParameter("txtNomSuc");
		switch(sOculto){
		      		case 1: // Consulta Sucursales dadas de alta en la tira auditora
							try{
								System.out.println("Case que consulta sucursales por Numero de Sucursal");
								sTitulo = "Consulta de Sucursales por Centro de Costos Altair";
								lsXmlServlet = sOculto+"|"+uno;
								vCadena.addElement(lsXmlServlet);
								lsXmlServlet = conecta.LlamaServlet("SucursalServlet",vCadena).toString();
								String limit = "|";
								StringTokenizer strToken_value= new StringTokenizer(lsXmlServlet, limit );
								while(strToken_value.hasMoreElements())
								{
									String tmpVar= (strToken_value.nextToken()).trim();
									vDatos.add(tmpVar);
									//System.out.println(tmpVar);
								}
								/***Si no trae datos redirecciona a pagina de error******/
									if(vDatos.size()<=1)
										response.sendRedirect("03SinDat.jsp");
								/*********************************************************/
								}catch (Exception e8){
										System.out.println ("Error en el servlet Sucursal case 1: " + e8.getMessage());
										throw new ServletException (e8.getMessage());
								}
					break;
					case 2: // Consulta Sucursales dadas de alta en la tira auditora
							try{
									System.out.println("Case que consulta sucursales por Servidor ");
									sTitulo = "Consulta de Sucursales por Nombre de Servidor";
									lsXmlServlet = sOculto+"|"+dos;
									vCadena.addElement(lsXmlServlet);
									lsXmlServlet = conecta.LlamaServlet("SucursalServlet",vCadena).toString();
									String limit = "|";
									StringTokenizer strToken_value= new StringTokenizer(lsXmlServlet, limit );
								while(strToken_value.hasMoreElements())
								{
										String tmpVar= (strToken_value.nextToken()).trim();
										vDatos.add(tmpVar);
										//System.out.println(tmpVar);
								}
								/***Si no trae datos redirecciona a pagina de error******/
									if(vDatos.size()<=1)
										response.sendRedirect("03SinDat.jsp");
								/*********************************************************/
								}catch (Exception e8){
										System.out.println ("Error en el servlet Sucursal case 2: " + e8.getMessage());
										throw new ServletException (e8.getMessage());
								}
					break;
					case 3: // Consulta Sucursales dadas de alta en la tira auditora
								try{
									System.out.println("Case que consulta sucursales por Nombre de Sucursal");
									sTitulo = "Consulta de Sucursales por Nombre de Sucursal";
									lsXmlServlet = sOculto+"|"+tres;
									vCadena.addElement(lsXmlServlet);
									lsXmlServlet = conecta.LlamaServlet("SucursalServlet",vCadena).toString();
									String limit = "|";
									StringTokenizer strToken_value= new StringTokenizer(lsXmlServlet, limit );
									while(strToken_value.hasMoreElements())
									{
										String tmpVar= (strToken_value.nextToken()).trim();
										vDatos.add(tmpVar);
										//System.out.println(tmpVar);
									}
									/***Si no trae datos redirecciona a pagina de error******/
									if(vDatos.size()<=1)
										response.sendRedirect("03SinDat.jsp");
									/*********************************************************/
								}catch (Exception e8){
										System.out.println ("Error en el servlet Sucursal case 3: " + e8.getMessage());
										throw new ServletException (e8.getMessage());
								}
					break;
					case 4: // Consulta Sucursales dadas de alta en la tira auditora
								try{
									System.out.println("Case que consulta todas las sucursales");
									sTitulo = "Consulta de todas las Sucursales";
									lsXmlServlet = " "+sOculto;
									vCadena.addElement(lsXmlServlet);
									lsXmlServlet = conecta.LlamaServlet("SucursalServlet",vCadena).toString();
									String limit = "|";
									StringTokenizer strToken_value= new StringTokenizer(lsXmlServlet, limit );
									while(strToken_value.hasMoreElements())
									{
										String tmpVar= (strToken_value.nextToken()).trim();
										vDatos.add(tmpVar);
										//System.out.println(tmpVar);
									}
									/***Si no trae datos redirecciona a pagina de error******/
									if(vDatos.size()<=1)
										response.sendRedirect("03SinDat.jsp");
									/*********************************************************/

								}catch (Exception e8){
										System.out.println ("Error en el servlet Sucursal case 4: " + e8.getMessage());
										throw new ServletException (e8.getMessage());
								}
					break;

					}//Cierra el Switch
	}
	cuantos = vDatos.size()/campos;
	System.out.println("cuantos: "+cuantos);
	paginas = cuantos/regxpag;
	System.out.println("paginas: "+paginas);
	sueltos = (cuantos)%regxpag;
	if(sueltos == 0)
		System.out.println("Registros exactos para la paginaci�n");
	else
		paginas = paginas + 1;

	sPag = 1;
	reEnvioDatos = lsXmlServlet;
	System.out.println("paginas: "+paginas);

	/****************************/
	if(cuantos > 1){
		if(sPag == 1){
			inicio = 0;
		}
		else
			{
			  inicio = (sPag - 1)*regxpag;
			  System.out.println("inicio: "+inicio);
			}
	}
	else
	{
		if(sBandera.equals("1")){
			String limit = "|";
			StringTokenizer strToken_value= new StringTokenizer(sXml2, limit );
				while(strToken_value.hasMoreElements())
				{
					String tmpVar= (strToken_value.nextToken()).trim();
					vDatos.add(tmpVar);
					//System.out.println(tmpVar);
				}
		reEnvioDatos = sXml2;
		cuantos = vDatos.size()/campos;
		System.out.println("cuantos: "+cuantos);
		paginas = cuantos/regxpag;
		System.out.println("paginas: "+paginas);
		sueltos = (cuantos)%regxpag;
			if(sueltos == 0)
					System.out.println("Registros exactos para la paginaci�n");
			else
				paginas = paginas + 1;

		if(sPag == 1){
			inicio = 0;
		}
		else
			{
			  inicio = (sPag - 1)*regxpag;
			  System.out.println("inicio: "+inicio);
			}
	}
	}

%>

<TITLE>Sucursales</TITLE>
<script>
/*******************************************************/
var arrDatos = new Array();
var arrContenedor = new Array();
var paginas = 0;
var cont = 0;

/********Funcion Inical de datos cuando se buscan todos*****************************/
function fnStart(){
	var bandera = <%=cuantos%>;
	if(bandera > 1){
		var camp = <%=campos%>;

		<%  for(int r=0; r<cuantos; r++){  %>

		var val0 = 	'<%=vDatos.elementAt(r * campos)%>';
		var val1 = 	'<%=vDatos.elementAt(r * campos + 1)%>';
		var val2 = 	'<%=vDatos.get(r * campos + 2)%>';
		var val3 = 	'<%=vDatos.get(r * campos + 3)%>';
		var val4 = 	'<%=vDatos.get(r * campos + 4)%>';
		var val5 = 	'<%=vDatos.get(r * campos + 5)%>';
		var val6 = 	'<%=vDatos.get(r * campos + 6)%>';
		var val7 = 	'<%=vDatos.get(r * campos + 7)%>';
		var val8 = 	'<%=vDatos.get(r * campos + 8)%>';
		var val9 = 	'<%=vDatos.get(r * campos + 9)%>';
		var val10 =	'<%=vDatos.get(r * campos + 10)%>';
		var val11 =	'<%=vDatos.get(r * campos + 11)%>';
		var val12 =	'<%=vDatos.get(r * campos + 12)%>';

		arrDatos[cont++] = new Array(val0,val1,val2,val3,val4,val5,val6,val7,val8,val9,val10,val11,val12);
		<% }  %>
		var regxpag = 6;

		var arrConPaginas = new Array();
		var completas = parseInt(arrDatos.length / regxpag);
		var sueltos = arrDatos.length % regxpag;
		if(sueltos == 0)
			paginas = completas;
		else
			paginas = completas + 1;
		var cont2=0;
		var cont3=0;

		for(l=0; l<arrDatos.length; l++){
			arrConPaginas[cont2++] = new Array(arrDatos[l]);
			if(arrConPaginas.length==regxpag){
				arrContenedor[cont3++] = new Array(arrConPaginas);
				cont2=0;
				arrConPaginas = new Array();
			}
		}
		if(arrConPaginas.length != 0){
				arrContenedor[cont3++] = new Array(arrConPaginas);
			}

	}
}
/***************************************/
function fnImprime(){
	accion = "imprimir";
	lsSeleccion = "0";
	lsDatos = '<%=lsXmlServlet%>';

	document.frm2.accion.value = accion;
	document.frm2.seleccion.value = lsSeleccion;
	document.frm2.lsDatos.value = lsDatos;
	document.frm2.submit();
}


/***************************************/

var arrDelete = new Array();
var arrNew = new Array();
var cont = 0;

/**************************/
function fnAdelante(actual){
	if(actual != '<%=paginas%>'){
		actual = parseInt(actual) + 1;
		document.forma.pag.value = actual;
		//alert("pag: "+document.forma.pag.value);
		document.forma.dat.value = '<%=reEnvioDatos%>';
		//alert("dat: "+document.forma.dat.value);
		document.forma.submit();
	}
	else
		alert("Fin de los Registros");
}

function fnAtras(actual){
	if(actual != 1){
		actual = parseInt(actual) - 1;
		document.forma.pag.value = actual;
		//alert("pag: "+document.forma.pag.value);
		document.forma.dat.value = '<%=reEnvioDatos%>';
		//alert("dat: "+document.forma.dat.value);
		document.forma.submit();
	}
	else
		alert("Inicio de los Registros");
}
/**************************/
function fnRegresar(){
			document.location.replace("03ConSuc.jsp");
	}
/***************************************************************************/
	function fnAgrega(indice){


		//Si aqui voy a agregar codigo, pero todavia no lo termino.

		}
/*******COMIENZA FUNCION BORRAR FILAS DEL ARREGLO*********************************************/
	function fnBorra(indice){

					var iIndice=indice;
					var iEle=0;
					arrNew = new Array();
					for (var x=0;x<arrDelete.length;x++){
						if (iIndice!=arrDelete[x][0]){
							arrNew[iEle++] = new Array(arrDelete[x][0],arrDelete[x][1],arrDelete[x][2],arrDelete[x][3],arrDelete[x][4],arrDelete[x][5],arrDelete[x][6],arrDelete[x][7],arrDelete[x][8],arrDelete[x][9],arrDelete[x][10]);
						}
					}

					iEle=0;
					arrDelete = new Array();
					conQ=0;

					for (var x=0;x<arrNew.length;x++){
						conQ++;

						arrDelete[iEle++] = new Array(arrNew[x][0],arrNew[x][1],arrNew[x][2],arrNew[x][3],arrNew[x][4],arrNew[x][5],arrNew[x][6],arrNew[x][7],arrNew[x][8],arrNew[x][9],arrNew[x][10]);
					}
					if(arrNew.length==0){
						arrDelete = new Array();
						conQ=0;
					}
	}


/************************TERMINA**********FUNCION BORRAR*******************************/
/****************************************************************************/
	function fnCheca(indice){
			var exp = 'document.forma.rad'+indice+'.checked == false;';
			var estado = eval(exp);
			if(estado == false){
					fnAgrega(indice);
				}else
					fnBorra(indice);

		}
/*****************************************************************************/
	function code2Status(status){
		if(status == "0" || status == "1"){
			status = "Abierta";
		} else if (status == "2"){
			status = "Cerrada";
		}
		return status;
	}
</script>
</HEAD>
<%
try{
    String radio = "rad";
    int sDe = 0;
    int sA = 0;
    if(sPag == 1){
    	sDe = 1;
    	if(paginas > 1)
    		sA = 100;
    	else
    		sA = cuantos;
    }else{
    	sDe = (sPag-1) * 100;
    	if(paginas > sPag){
    		sA = sPag * 100;
    	}else{
    		sA = cuantos;
    	}
    }
%>

<BODY>
<form name="frm2" method="post" action="../servlet/SucImprimeServlet" >
<input type="hidden" name="accion" />
<input type="hidden" name="seleccion" />
<input type="hidden" name="lsDatos" />
</form>
<form name="forma" method="post" action="03ResSuc.jsp">
<input type="hidden" name="Parametro" />
<input type="hidden" name="arrDelete" />
<input type="hidden" name="bandera" value="1"/>
<input type="hidden" name="pag" value="0"/>
<input type="hidden" name="dat" value="0"/>


<h1 class="ATitulocolor">Cat�logo de Sucursales</h1>

<TABLE align="center" border="0" width="99%">
	<TR  class="ALigaonce">
		<TD colspan="3" class="ALigaonce">
		<% if(cuantos > 1){%>
			<% if(sPag != 1){%><A href="javascript:fnAtras('<%=sPag%>');" class="txt">&lt;&lt;</A><%}%>&#160;<%=sDe%> a <%=sA%>
			<% if(sPag != paginas){%><A href="javascript:fnAdelante('<%=sPag%>');" class="txt">&gt;&gt;</A><%}%>
		<%}%>
		</TD>
		<TD colspan="9" width="80%" class = "Atexnegro" align="right">&#160;<%=sTitulo%></TD>
	</TR>
	<tr class="Atittabcenazu">
		<TD align="center" width="5%" >C de C ALTAIR</TD>
		<TD align="center" width="10%" >Trabaja Sabado</TD>
		<TD align="center" width="10%" >Nombre Servidor</TD>
		<TD align="center" width="10%" >Direcci�n IP</TD>
		<TD align="center" width="10%" >Link</TD>
		<TD align="center" width="10%" >Nombre de Sucursal</TD>
		<TD align="center" colspan="5">Direcci�n</TD>
		<TD align="center" width="5%">Abierta/Cerrada</TD>
	</TR>

<%
	if(cuantos > 1){

	for(int i = inicio; i<cuantos; i++){
	 if(!(i%2!=0)){
%>
	<TR>
		<TD align="center" width="5%"><%= vDatos.get(i * campos) %> </TD>
		<TD align="center" width="10%"><%= vDatos.get(i * campos + 1) %></TD>
		<TD align="center" width="10%"><%= vDatos.get(i * campos + 2) %></TD>
		<TD align="center" width="10%"><%= vDatos.get(i * campos + 3) %></TD>
		<TD align="center" width="10%"><%= vDatos.get(i * campos + 4) %></TD>
		<TD align="center" width="10%"><%= vDatos.get(i * campos + 5) %></TD>
		<TD align="center" width="10%" colspan = 5><%= vDatos.get(i * campos + 6) %>,
		<%= vDatos.get(i * campos + 7) %>, <%= vDatos.get(i * campos + 8) %>,
		<%= vDatos.get(i * campos + 9) %>, <%= vDatos.get(i * campos + 10) %>,
		<%= vDatos.get(i * campos + 11) %></TD>
		<TD align="center" width="5%"><script>document.write(code2Status('<%=vDatos.get(i * campos + 12)%>'));</script></TD>
	</TR>
<%}else{%>
		<TR>
		<TD align="center" width="5%" class="fondo"><%= vDatos.get(i * campos)%></TD>
		<TD align="center" width="10%" class="fondo"><%= vDatos.get(i * campos + 1) %></TD>
		<TD align="center" width="10%" class="fondo"><%= vDatos.get(i * campos + 2) %></TD>
		<TD align="center" width="10%" class="fondo"><%= vDatos.get(i * campos + 3) %></TD>
		<TD align="center" width="10%" class="fondo"><%= vDatos.get(i * campos + 4) %></TD>
		<TD align="center" width="10%" class="fondo"><%= vDatos.get(i * campos + 5) %></TD>
		<TD align="center" width="10%" class="fondo" colspan = 5><%= vDatos.get(i * campos + 6) %>,
		<%= vDatos.get(i * campos + 7) %>, <%= vDatos.get(i * campos + 8) %>,
		<%= vDatos.get(i * campos + 9) %>, <%= vDatos.get(i * campos + 10) %>,
		<%= vDatos.get(i * campos + 11) %></TD>
		<TD align="center" width="5%" class="fondo"><script>document.write(code2Status('<%=vDatos.get(i * campos + 12)%>'));</script></TD>
	</TR>
<% }
	++fcont;
	if(fcont == regxpag)
		break;
} }%>


	<%
	if(cuantos == 1){
	for(int i = 0; i<cuantos; i++){
	   if(!(i%2!=0)){
	%>
	<TR>
		<TD align="center" width="5%"><%= vDatos.get(i * campos) %> </TD>
		<TD align="center" width="10%"><%= vDatos.get(i * campos + 1) %></TD>
		<TD align="center" width="10%"><%= vDatos.get(i * campos + 2) %></TD>
		<TD align="center" width="10%"><%= vDatos.get(i * campos + 3) %></TD>
		<TD align="center" width="10%"><%= vDatos.get(i * campos + 4) %></TD>
		<TD align="center" width="10%"><%= vDatos.get(i * campos + 5) %></TD>
		<TD align="center" width="10%" colspan = 5><%= vDatos.get(i * campos + 6) %>,
		<%= vDatos.get(i * campos + 7) %>, <%= vDatos.get(i * campos + 8) %>,
		<%= vDatos.get(i * campos + 9) %>, <%= vDatos.get(i * campos + 10) %>,
		<%= vDatos.get(i * campos + 11) %></TD>
		<TD align="center" width="5%"><script>document.write(code2Status('<%=vDatos.get(i * campos + 12)%>'));</script></TD>
	</TR>
	<%}
	else{%>
		<TR>
		<TD align="center" width="5%" class="fondo"><%= vDatos.get(i * campos)%></TD>
		<TD align="center" width="10%" class="fondo"><%= vDatos.get(i * campos + 1) %></TD>
		<TD align="center" width="10%" class="fondo"><%= vDatos.get(i * campos + 2) %></TD>
		<TD align="center" width="10%" class="fondo"><%= vDatos.get(i * campos + 3) %></TD>
		<TD align="center" width="10%" class="fondo"><%= vDatos.get(i * campos + 4) %></TD>
		<TD align="center" width="10%" class="fondo"><%= vDatos.get(i * campos + 5) %></TD>
		<TD align="center" width="10%" class="fondo" colspan = 5><%= vDatos.get(i * campos + 6) %>,
		<%= vDatos.get(i * campos + 7) %>, <%= vDatos.get(i * campos + 8) %>,
		<%= vDatos.get(i * campos + 9) %>, <%= vDatos.get(i * campos + 10) %>,
		<%= vDatos.get(i * campos + 11) %></TD>
		<TD align="center" width="5%" class="fondo"><script>document.write(code2Status('<%=vDatos.get(i * campos + 12)%>'));</script></TD>

	</TR>
   <% } } }%>
	<TR>
		<TD colspan="9" align="center">&#160;</TD>
	</TR>


	<TR>
		<TD colspan="4" align="right">
			<A href="javascript:fnRegresar();">
					<IMG name="btnRegresar" border="0" src="../images/b_regresar.gif">
			</A>
		</TD>
		<td>&#160;</td>
		<td>&#160;</td>
		<TD colspan="6" >
			<A href="javascript:fnImprime();">
					<IMG name="btnimprimir" border="0" src="../images/b_imprimir.gif">
			</A>
		</TD>
	</TR>







</TABLE>
</form>
<CENTER><TABLE>
<TBODY><TR>
			<TD align="center">&#160;</TD>
		</TR>
</TBODY></TABLE>

</CENTER>
<%}catch (Exception e) {
		System.out.println("problem "+e);
		System.out.println("error:" + e.getMessage());
	}
%>
</BODY>

</HTML>