<%@ page import="javax.naming.*"%>
<%@ page import="com.santander.utilerias.*"%>
<%@ page import="java.util.*"%>
<%@ page import="javax.servlet.http.*"%>
<%
	//   Banco Santander Mexicano
	//   jsp 03ResMto  Presenta los datos de las sucursales en base a criterio
	//   @author Angel Gabriel Ramirez Alva
	//   @version 1.0
	//   fecha de creacion : 1 de Febrero del 2006
	//   responsable : Eloisa Hernandez H
	//   descripcion : Resuelve la presentaci�n de la informaci�n ha ser modificada
%>
<%
	String usuario = null;
	String perfil = null;
	String grupos = null;
	usuario = request.getHeader("iv-user");
	grupos = request.getHeader("iv-groups");
	
	if (usuario == null || grupos == null) {
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}	
%>
<HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<%
	String sOculto = request.getParameter("oculto");
	String sNumsuc = request.getParameter("txtNumSuc");
%>
<LINK rel="stylesheet" href="../theme/Master.css" type="text/css">
<SCRIPT language="JavaScript" src="../theme/utiljs.js"></SCRIPT>
<TITLE>Mantenimiento basico a Sucursales</TITLE>

<%
	try {
		String lsXmlServlet = null;
		int valor = 6;
		Vector vDatos = new Vector();
		String arrayElement = "";
		String parametro = "";
		String radio = "rad";
		/**************************************************/
		String pto_venta = "pto_venta";
		String sabado = "sabado";
		String nom_maq = "nom_maq";
		String dir_ip = "dir_ip";
		String sTitulo = "Mantenimiento b�sico de Sucursal por Centro de Costo Altair";

		/***************************************************/

		int campos = 12;
		String isEmpty = "false";
		AccesoServlet Conecta = new AccesoServlet();
		Vector vCadena = new Vector();

		/******************************************************************/
		parametro = sOculto + "|" + sNumsuc;
		/*****************************************************************/
		vCadena.addElement(parametro);
		lsXmlServlet = Conecta.LlamaServlet("SucursalServlet", vCadena)
				.toString();

		String limit = "|";
		StringTokenizer strToken_value = new StringTokenizer(
				lsXmlServlet, limit);
		while (strToken_value.hasMoreElements()) {
			String tmpVar = (strToken_value.nextToken()).trim();
			vDatos.add(tmpVar);
			System.out.println(tmpVar);
		}

		arrayElement = (String) vDatos.elementAt(0);
		if (arrayElement.equals("NO EXISTEN DATOS")) {
			System.out.println("nO Hay dAtos: " + lsXmlServlet);
			isEmpty = "true";
		}
		vCadena.clear();
%>

<script>

/********************************************************/
	var empty = new Boolean();
	empty = <%=isEmpty%>;

	if(empty){
		alert("No hubo coincidencias en la b�squeda: <%=sNumsuc%>" );
		window.location.href="03MtoSucBas.jsp";
	}

	function fnValida(ccaltair, saturdays, status, funcion){
		var valida = new Boolean();
		var dato = "";
		valida = false;
		var respuesta=confirm("�Desea modificar este estatus?");
		if(respuesta == true){
			if(funcion == 0){
				dato = saturdays;
				if(saturdays == "N"){
					saturdays = "S";
				} else if (saturdays == "S"){
					saturdays = "N";
				} else {
					valida = true;
				}
			} else if (funcion == 1){
				dato = status;
				if(status == "2"){
					status = "0";
				} else if (status == "0" || status == "1"){
					status = "2";
				} else {
					valida = true;
				}
			}
			if(valida){
				alert("Favor de validar ya que el dato " + dato + " es incorrecto");
			} else {
				var arrUpdate = ccaltair + "|" + saturdays + "|" + status;
				document.forma.arrSend.value = arrUpdate;
				document.forma.submit();
			}
		}
	}
	function code2Status(status){
		if(status == "0" || status == "1"){
			status = "Abierta";
		} else if (status == "2"){
			status = "Cerrada";
		}
		return status;
	}
/******************************************************************************/
</script>

</HEAD>
<BODY>

<form name="forma" method="post" action="03MtoModBas.jsp">
<input type="hidden" name="oculto" value="8" />
<input type="hidden" name="arrSend"	value="" />


<h1>Mantenimiento b�sico de Sucursales</h1>


<TABLE align="center" border="0" width="90%">
	<TR class="ALigaonce">
		<TD colspan="3" align="left" class="ALigaonce">&#160;&#160;1 de	1</TD>
		<TD colspan="10" width="80%" class="Atexnegro" align="right">&#160;<%=sTitulo%></TD>
	</TR>
	<tr class="Atittabcenazu">
		<TD align="center" width="10%">C de C ALTAIR</TD>
		<TD align="center" width="10%">Trabaja Sabado</TD>
		<TD align="center" width="10%">Servidor</TD>
		<TD align="center" width="10%">Direcci�n IP</TD>
		<TD align="center" width="10%">Link</TD>
		<TD align="center" width="10%">Sucursal</TD>
		<TD align="center" colspan="6">Direcci�n</TD>
		<TD align="center" width="10%">Abierta/Cerrada</TD>
	</tr>

	<%
		for (int i = 0; i < vDatos.size() / campos; i++) {
				if (!(i % 2 != 0)) {
	%>
	<TR>
		<TD align="center" width="10%"><%=vDatos.get(i * campos)%></TD>
		<TD align="center" width="10%"><%=vDatos.get(i * campos + 1)%>
			<a href="javascript:fnValida('<%= vDatos.get(i * campos) %>','<%=vDatos.get(i * campos + 1)%>','<%=vDatos.get(i * campos + 12)%>',0);"><img src="../images/change.png" title="Cambiar estatus"></a>
		</TD>
		<TD align="center" width="10%"><%=vDatos.get(i * campos + 2)%></TD>
		<TD align="center" width="10%"><%=vDatos.get(i * campos + 3)%></TD>
		<TD align="center" width="10%"><%=vDatos.get(i * campos + 4)%></TD>
		<TD align="center" width="10%"><%=vDatos.get(i * campos + 5)%></TD>
		<TD align="center" colspan="6"><%=vDatos.get(i * campos + 6)%>,<%=vDatos.get(i * campos + 7)%>,
		<%=vDatos.get(i * campos + 8)%>,<%=vDatos.get(i * campos + 9)%>,<%=vDatos.get(i * campos + 10)%>,
		<%=vDatos.get(i * campos + 11)%></TD>
		<TD align="center" width="10%"><script>document.write(code2Status('<%=vDatos.get(i * campos + 12)%>'));</script>
			<a href="javascript:fnValida('<%= vDatos.get(i * campos) %>','<%=vDatos.get(i * campos + 1)%>','<%=vDatos.get(i * campos + 12)%>',1);"><img src="../images/change.png" title="Cambiar estatus"></a>
		</TD>
	</TR>

	<%
		} else {
	%>
	<TR>
		<TD align="center" class="fondo" width="10%"><%=vDatos.get(i * campos)%></TD>
		<TD align="center" class="fondo" width="10%"><%=vDatos.get(i * campos + 1)%>
			<a href="javascript:fnValida('<%= vDatos.get(i * campos) %>','<%=vDatos.get(i * campos + 1)%>','<%=vDatos.get(i * campos + 12)%>',0);"><img src="../images/change.png" title="Cambiar estatus"></a>
		</TD>
		<TD align="center" class="fondo" width="10%"><%=vDatos.get(i * campos + 2)%></TD>
		<TD align="center" class="fondo" width="10%"><%=vDatos.get(i * campos + 3)%></TD>
		<TD align="center" class="fondo" width="10%"><%=vDatos.get(i * campos + 4)%></TD>
		<TD align="center" class="fondo" width="10%"><%=vDatos.get(i * campos + 5)%></TD>
		<TD align="center" class="fondo" colspan="6"><%=vDatos.get(i * campos + 6)%>,
		<%=vDatos.get(i * campos + 7)%>,<%=vDatos.get(i * campos + 8)%>, <%=vDatos.get(i * campos + 9)%>,<%=vDatos.get(i * campos + 10)%>,<%=vDatos.get(i * campos + 11)%></TD>
		<TD align="center" width="10%"><script>document.write(code2Status('<%=vDatos.get(i * campos + 12)%>'));</script>
			<a href="javascript:fnValida('<%= vDatos.get(i * campos) %>','<%=vDatos.get(i * campos + 1)%>','<%=vDatos.get(i * campos + 12)%>',1);"><img src="../images/change.png" title="Cambiar estatus"></a>
		</TD>
	</TR>
	<%
		}
			}
	%>
	<TR>
		<TD colspan="9" align="center">&#160;</TD>
	</TR>

</TABLE>
</form>

<%
	} catch (Exception e) {
		System.out.println("problem " + e);
		System.out.println("error:" + e.getMessage());
	}
%>
</BODY>

</HTML>