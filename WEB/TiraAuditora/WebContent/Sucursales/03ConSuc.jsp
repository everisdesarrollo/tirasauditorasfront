
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%
	String usuario = null;
	String perfil = null;
	String grupos = null;
	usuario = request.getHeader("iv-user");
	grupos = request.getHeader("iv-groups");
	
	if (usuario == null || grupos == null) {
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}	
%>
<HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<SCRIPT language="JavaScript" src="../theme/utiljs.js"></SCRIPT>
<SCRIPT src="../theme/validaciones.js"></SCRIPT>

<LINK href="../theme/Master.css" rel="stylesheet"
	type="text/css">
<TITLE>ConsultaSucursal.jsp</TITLE>
<script>
function fnColoca(){
	document.forma.txtNumSuc.focus();
}
function fnValida(){
	document.forma.submit();
}
function fnValida1(){
var numSuc = document.forma.txtNumSuc.value;
if(numSuc == ""){
		document.forma.txtServidor.disabled = false;
		document.forma.txtNomSuc.disabled = false;
	}else{
			document.forma.txtServidor.disabled = true;
			document.forma.txtNomSuc.disabled = true;
			document.forma.oculto.value=1;
		 }
}
function fnValida2(){
var sServidor = document.forma.txtServidor.value;
if(sServidor == ""){
		document.forma.txtNumSuc.disabled = false;
		document.forma.txtNomSuc.disabled = false;
	}else{
			document.forma.txtNumSuc.disabled = true;
			document.forma.txtNomSuc.disabled = true;
			document.forma.oculto.value=2;
		 }
}
function fnValida3(){
var sSucur = document.forma.txtNomSuc.value;
if(sSucur == ""){
		document.forma.txtNumSuc.disabled = false;
		document.forma.txtServidor.disabled = false;
	}else{
			document.forma.txtNumSuc.disabled = true;
			document.forma.txtServidor.disabled = true;
			document.forma.oculto.value=3;
		 }
}
/**********************/
function fnLimpiar()
{
	if(document.forma.txtNumSuc.value != "")
	{
		document.forma.txtNumSuc.value = "";
		document.forma.txtNumSuc.focus();
	}
	if(document.forma.txtServidor.value != ""){
		document.forma.txtServidor.value = "";
		document.forma.txtServidor.focus();
	}
	if(document.forma.txtNomSuc.value != ""){
		document.forma.txtNomSuc.value = "";
		document.forma.txtNomSuc.focus();
	}

}
/***************************/
function valida(campo,propiedades){
		   var lTempMssg=assertField(campo, propiedades);
		}
var valida_pventa = new Array('txtNumSuc','Numero Sucursal','N',false,4,4);
/********************************/
</script>
</HEAD>
<BODY onLoad = fnColoca();>
<FORM name="forma" method="post" action="03ResSuc.jsp">
<input type="hidden" name="oculto" value="4"/>
<input type="hidden" name="bandera" value="0"/>
<input type="hidden" name="pag" value="0"/>
<input type="hidden" name="dat" value="0"/>
<table width="100%" align="center" border="0">
<tr>
	<td colspan="2" align="left" width="100%">
		<p class="ATitulocolor">Consulta C�talogo de Sucursales</p>
		<BR>
	</td>
</tr>
<tr>
	<td>
		<table width="80%" align="center" border="0" cellspacing="0">

			<TR>
			    <TD colspan="3" class="Atittabcenazu">&nbsp;Criterios de B�squeda</TD>
			</TR>
			<tr>
				<td class="AEtiquetaDentro" colspan="3" align="center">&#160;</td>
			</tr>
			<tr class="AEtiquetaDentro">
				<td align="right" class="Atexencabezado" width="45%">Centro de Costos ALTAIR:&#160;&#160;&#160;&#160;</td>
				<td><input Type="text" name="txtNumSuc" value="" class="Atextittab" maxlength="4" onkeypress="return Valida_Texto(this,event,'e');" onBlur="javascript:fnValida1(),valida(this,valida_pventa);"/></td>
				<td><IMG border="0" src="../images/invisible.gif"></td>
			</tr>
			<tr class="AEtiquetaDentro">
				<td align="right" class="Atexencabezado" >Nombre Servidor:&#160;&#160;&#160;&#160;</td>
				<td><input Type="text" name="txtServidor" value="" maxlength="20" class="Atextittab" onBlur="javascript:fnValida2();" /></td>
				<td><IMG border="0" src="../images/invisible.gif"></td>
			</tr>
			<tr class="AEtiquetaDentro">
				<td align="right" class="Atexencabezado">Nombre de Sucursal:&#160;&#160;&#160;&#160;</td>
				<TD><input Type="text" name="txtNomSuc" value="" maxlength="50" class="Atextittab" onBlur="javascript:fnValida3();"/></TD>
				<td><IMG border="0" src="../images/invisible.gif"></td>
			</tr>
			<tr class="AEtiquetaDentro">
				<td colspan="3">&#160;</td>
			</tr>
			<tr class="Atittabcenazu">
				<td colspan="3">&#160;</td>
			</tr>
			<tr>
				<td colspan="3">&#160;<br></td>
			</tr>
			<tr>

				<td align="center" colspan="3"><a href="javascript:fnLimpiar();"><IMG border="0"
					name="continuar" src="../images/b_limpiar.gif" ></a>
				&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					<a href="javascript:fnValida();"><IMG border="0"
					name="continuar" src="../images/b_buscar.gif" ></a>

				</td>

			</tr>
		</table>
		</td>
	</tr>
</table>
</FORM>

</BODY>
</HTML>