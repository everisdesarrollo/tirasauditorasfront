<%
	String usuario = null;
	String perfil = null;
	String grupos = null;
	usuario = request.getHeader("iv-user");
	grupos = request.getHeader("iv-groups");
	
	if (usuario == null || grupos == null) {
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}	
%>
<HTML>
<HEAD>
<%@ page
language="java"
contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"
%>
<%@ page import="java.util.*"%>
<%
Vector vArr = new Vector();
Vector vArrPart = new Vector();
String arreglo = request.getParameter("arrSend");
System.out.println(arreglo);
String limit = "|";
StringTokenizer strToken_value= new StringTokenizer(arreglo, limit);
while(strToken_value.hasMoreElements())
	{
	String tmpVar= (strToken_value.nextToken()).trim();
	vArr.add(tmpVar);
	System.out.println(tmpVar);
}
String ipPart = (String)vArr.elementAt(3);

strToken_value= new StringTokenizer(ipPart, ".");
while(strToken_value.hasMoreElements())
	{
	String tmpVar= (strToken_value.nextToken()).trim();
	vArrPart.add(tmpVar);
	System.out.println(tmpVar);
}



int cuantos = vArr.size();
System.out.println("cuantos: "+cuantos);
%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="../theme/Master.css" rel="stylesheet"
	type="text/css">
<SCRIPT language="JavaScript" src="../theme/utiljs.js"></SCRIPT>
<TITLE>Modificar SUCURSAL</TITLE>
<script>
/************VALIDACIONES***********************************************/
function fnValIP(){
		var ipTemp = document.forma.txtDirIp1.value;
		if(ipTemp == "")
			return false;
		else
			ipTemp=ipTemp + ".";

		var ipTemp2= document.forma.txtDirIp2.value;
		if(ipTemp2 == "")
			return false;
		else
			ipTemp+=ipTemp2 + ".";

		var ipTemp3 = document.forma.txtDirIp3.value;
		if(ipTemp3 == "")
			return false;
		else
			ipTemp+=ipTemp3 + ".";

		var ipTemp4 = document.forma.txtDirIp4.value;
		if(ipTemp4 == "")
			return false;
		else{
			ipTemp+=ipTemp4;
			//alert("listo: "+ipTemp);
			document.forma.txtDirIp.value = ipTemp;
			return true;
			}
	}


	function fnValida(){
		var ns = document.forma.txtNumSuc.value;
		var tb = document.forma.txtTraSab.value.toUpperCase();
		var nm = document.forma.txtNomMaq.value;
		fnValIP();
		var ip = document.forma.txtDirIp.value;
		var lk = document.forma.txtLink.value;
		var des = document.forma.txtDesc.value;
		var cn = document.forma.txtCaNu.value;
		var cp = document.forma.txtCp.value;
		var cl = document.forma.txtCol.value;
		var cpo = document.forma.txtCidPob.value;
		var dm = document.forma.txtDelMpio.value;
		var ce = document.forma.comboEstado.value;
		var open = document.forma.comboEstatus.value;

		if(ns==""||ns=="0000"||tb==""||nm==""||ip==""||lk==""||des==""||cn==""||cp==""||cl==""||cpo==""||dm==""||ce=="-1"||open=="-1"){
			alert("Debe llenar todos los campos");

			if(open == "-1"){
				document.forma.comboEstatus.focus();
			}
			if(ce == "-1"){
				document.forma.comboEstado.focus();
			}
			if(dm == ""){
				document.forma.txtDelMpio.focus();
			}
			if(cpo == ""){
				document.forma.txtCidPob.focus();
			}
			if(cl == ""){
				document.forma.txtCol.focus();
			}
			if(cp == ""){
				document.forma.txtCp.focus();
			}
			if(cn == ""){
				document.forma.txtCaNu.focus();
			}
			if(des == ""){
				document.forma.txtDesc.focus();
			}
			if(lk == ""){
				document.forma.txtLink.focus();
			}
			if(ip == ""){
				document.forma.txtDirIp.focus();
			}
			if(nm == ""){
				document.forma.txtNomMaq.focus();
			}
			if(tb == ""){
				document.forma.txtTraSab.focus();
			}
			if(ns == ""){
				document.forma.txtNumSuc.focus();
			}
			if(ns == "0000"){
				alert("Debe ser diferente de 0000");
				document.forma.txtNumSuc.focus();
			}
		}else{
				var arrDelete = ns+"|"+tb+"|"+nm+"|"+ip+"|"+lk+"|"+des+"|"+cn+"|"+cp+"|"+cl+"|"+cpo+"|"+dm+"|"+ce+"|"+open;
				document.forma.arrSend.value=arrDelete;
				document.forma.submit();
				//alert("Los datos son incorrectos");
			 }
	}
/****************************************************/
function fnRegresar(){
			document.location.replace("03MtoSuc.jsp");
	}
/***************************************************************************/

</script>
</HEAD>
<BODY onLoad="javascript:document.forma.txtNumSuc.focus();">
<form name="forma" method="post" action="03MtoEli.jsp">
<input type="hidden" name="oculto" value="7"/>
<input type="hidden" name="parametro" value="M"/>
<input type="hidden" name="arrSend" value=""/>
<input type="hidden" name="txtDirIp" />
<table width="100%" align="center" border="0">
<tr>
	<td>
	<p class="ATitulocolor">Alta de Sucursales al Sistema Tira Auditora</p><BR>
	</td>
</tr>
<tr>
	<td>
<TABLE width="85%" border="0" cellpadding="0" cellspacing="0" align="center">
	<TBODY>
	<TR class="Atittabcenazu">
		<td colspan="3">&#160;Modificaci�n de Sucursal</td>
	</TR>
	<tr class="AEtiquetaDentro">
		<td colspan="3">&#160;</td>
	</tr>
		<TR class="AEtiquetaDentro">
			<TD width="40%" align="right" class="Atexencabezado">Centro de Costos ALTAIR:&#160;&#160;</TD>
			<TD width="30%" ><INPUT type="text" name="txtNumSuc" maxlength="4" onBlur="javascript:fnValNumSuc(this.value, this.name)"
				class="Atextittab" value="<%= vArr.elementAt(0)%>" size="30"></TD>
			<TD><IMG border="0" src="../images/invisible.gif" width="100"></TD>
		</TR>
		<TR class="AEtiquetaDentro">
			<TD align="right">Trabaja Sabado:&#160;&#160;</TD>
			<TD><INPUT type="text" name="txtTraSab" maxlength="1" style="TEXT-TRANSFORM: uppercase";
				class="Atextittab" value="<%= vArr.elementAt(1)%>" size="30" onBlur="javascript:fnValTsab(this.value, this.name)" onkeypress="return Valida_Texto(this,event,'z');"></TD>
			<TD></TD>
		</TR>
		<TR class="AEtiquetaDentro">
			<TD align="right">Nombre Maquina:&#160;&#160; </TD>
			<TD><INPUT type="text" name="txtNomMaq" maxlength="20"
				class="Atextittab" value="<%= vArr.elementAt(2)%>" size="30" onkeypress="return Valida_Texto(this,event,'a2');"></TD>
			<TD></TD>
		</TR>

		<tr class="AEtiquetaDentro">
		<td align="right">Direcci�n IP:&#160;&#160;</td>
		<td><input Type="text" value="<%= vArrPart.elementAt(0)%>" name="txtDirIp1" class="Atextittab" maxlength="3" size="1" onBlur="javascript:fnValIPsola(this.value,this.name);" onkeypress="return Valida_Texto(this,event,'r');" />.
			<input Type="text" value="<%= vArrPart.elementAt(1)%>" name="txtDirIp2" class="Atextittab" maxlength="3" size="1" onBlur="javascript:fnValIPsola(this.value,this.name);" onkeypress="return Valida_Texto(this,event,'r');" />.
			<input Type="text" value="<%= vArrPart.elementAt(2)%>" name="txtDirIp3" class="Atextittab" maxlength="3" size="1" onBlur="javascript:fnValIPsola(this.value,this.name);" onkeypress="return Valida_Texto(this,event,'r');" />.
			<input Type="text" value="<%= vArrPart.elementAt(3)%>" name="txtDirIp4" class="Atextittab" maxlength="3" size="1" onBlur="javascript:fnValIPsola(this.value,this.name);" onkeypress="return Valida_Texto(this,event,'r');" />
		</td>
		<td></td>
	   </tr>

		<TR class="AEtiquetaDentro">
			<TD align="right">Nombre Link: &#160;&#160;</TD>
			<TD><INPUT type="text" name="txtLink" maxlength="25"
				class="Atextittab" value="<%= vArr.elementAt(4)%>" size="30" readOnly onkeypress="return Valida_Texto(this,event,'a2');"></TD>
			<TD></TD>
		</TR>
		<TR class="AEtiquetaDentro">
			<TD align="right">Descripci�n:&#160;&#160; </TD>
			<TD ><INPUT type="text" name="txtDesc" maxlength="50"
				class="Atextittab" value="<%= vArr.elementAt(5)%>" size="30" onkeypress="return Valida_Texto(this,event,'a2');"></TD>
			<TD></TD>
		</TR>
		<TR class="AEtiquetaDentro">
			<TD align="right">Calle, N�mero:&#160;&#160; </TD>
			<TD><INPUT type="text" name="txtCaNu" maxlength="60"
				class="Atextittab" value="<%= vArr.elementAt(6)%>" size="30" onkeypress="return Valida_Texto(this,event,'t1');"></TD>
			<TD></TD>
		</TR>
		<TR class="AEtiquetaDentro">
			<TD align="right">Codigo Postal:&#160;&#160; </TD>
			<TD><INPUT type="text" name="txtCp" maxlength="5"
				value="<%= vArr.elementAt(7)%>" class="Atextittab" size="30" size="30" onBlur="javascript:fnValNums(this.value, this.name);" onkeypress="return Valida_Texto(this,event,'e');"></TD>
			<TD></TD>
		</TR>
		<TR class="AEtiquetaDentro">
			<TD align="right">Colonia: &#160;&#160;</TD>
			<TD><INPUT type="text" name="txtCol" maxlength="30"
				class="Atextittab" value="<%= vArr.elementAt(8)%>" size="30" onkeypress="return Valida_Texto(this,event,'a2');"></TD>
			<TD></TD>
		</TR>
		<TR class="AEtiquetaDentro">
			<TD align="right">Ciudad / Poblaci�n:&#160;&#160; </TD>
			<TD><INPUT type="text" name="txtCidPob" maxlength="35"
				class="Atextittab" value="<%= vArr.elementAt(9)%>" size="30" onkeypress="return Valida_Texto(this,event,'a2');"></TD>
			<TD></TD>
		</TR>
		<TR class="AEtiquetaDentro">
			<TD align="right">Delegaci�n / Municipio:&#160;&#160;</TD>
			<TD><INPUT type="text" name="txtDelMpio" maxlength="35"
				class="Atextittab" value="<%= vArr.elementAt(10)%>" size="30" onkeypress="return Valida_Texto(this,event,'a2');"></TD>
			<TD></TD>
		</TR>
		<tr class="AEtiquetaDentro">
		<td align="right">Estado:&#160;&#160;</td>
		<td ><select size="1" name="comboEstado" class="Atextittab" style="width=180px">
				<option value="Aguascalientes" <% if (vArr.elementAt(11).equals("Aguascalientes")) {%>selected<%}%>>Aguascalientes</option>
				<option value="Baja California Norte" <% if (vArr.elementAt(11).equals("Baja California Norte")) {%>selected<%}%>>Baja California Norte</option>
				<option value="Baja California Sur" <% if (vArr.elementAt(11).equals("Baja California Sur")) {%>selected<%}%>>Baja California Sur</option>
				<option value="Campeche" <% if (vArr.elementAt(11).equals("Campeche")) {%>selected<%}%>>Campeche</option>
				<option value="Chiapas" <% if (vArr.elementAt(11).equals("Chiapas")) {%>selected<%}%>>Chiapas</option>
				<option value="Chihuahua" <% if (vArr.elementAt(11).equals("Chihuahua")) {%>selected<%}%>>Chihuahua</option>
				<option value="Coahuila" <% if (vArr.elementAt(11).equals("Coahuila")) {%>selected<%}%>>Coahuila</option>
				<option value="Colima" <% if (vArr.elementAt(11).equals("Colima")) {%>selected<%}%>>Colima</option>
				<option value="Distrito Federal" <% if (vArr.elementAt(11).equals("Distrito Federal")) {%>selected<%}%>>Distrito Federal</option>
				<option value="Durango" <% if (vArr.elementAt(11).equals("Durango")) {%>selected<%}%>>Durango</option>
				<option value="Estado de M�xico" <% if (vArr.elementAt(11).equals("Estado de M�xico")) {%>selected<%}%>>Estado de M�xico</option>
				<option value="Guanajuato" <% if (vArr.elementAt(11).equals("Guanajuato")) {%>selected<%}%>>Guanajuato</option>
				<option value="Guerrero" <% if (vArr.elementAt(11).equals("Guerrero")) {%>selected<%}%>>Guerrero</option>
				<option value="Hidalgo" <% if (vArr.elementAt(11).equals("Hidalgo")) {%>selected<%}%>>Hidalgo</option>
				<option value="Jalisco" <% if (vArr.elementAt(11).equals("Jalisco")) {%>selected<%}%>>Jalisco</option>
				<option value="Michoac�n" <% if (vArr.elementAt(11).equals("Michoac�n")) {%>selected<%}%>>Michoac�n</option>
				<option value="Morelos" <% if (vArr.elementAt(11).equals("Morelos")) {%>selected<%}%>>Morelos</option>
				<option value="Nuevo Le�n" <% if (vArr.elementAt(11).equals("Nuevo Le�n")) {%>selected<%}%>>Nuevo Le�n</option>
				<option value="Nayarit" <% if (vArr.elementAt(11).equals("Nayarit")) {%>selected<%}%>>Nayarit</option>
				<option value="Oaxaca" <% if (vArr.elementAt(11).equals("Oaxaca")) {%>selected<%}%>>Oaxaca</option>
				<option value="Puebla" <% if (vArr.elementAt(11).equals("Puebla")) {%>selected<%}%>>Puebla</option>
				<option value="Quer�taro" <% if (vArr.elementAt(11).equals("Quer�taro")) {%>selected<%}%>>Quer�taro</option>
				<option value="Quintana Roo" <% if (vArr.elementAt(11).equals("Quintana Roo")) {%>selected<%}%>>Quintana Roo</option>
				<option value="San Luis Potos�" <% if (vArr.elementAt(11).equals("San Luis Potos�")) {%>selected<%}%>>San Luis Potos�</option>
				<option value="Sinaloa" <% if (vArr.elementAt(11).equals("Sinaloa")) {%>selected<%}%>>Sinaloa</option>
				<option value="Sonora" <% if (vArr.elementAt(11).equals("Sonora")) {%>selected<%}%>>Sonora</option>
				<option value="Tabasco" <% if (vArr.elementAt(11).equals("Tabasco")) {%>selected<%}%>>Tabasco</option>
				<option value="Tamaulipas" <% if (vArr.elementAt(11).equals("Tamaulipas")) {%>selected<%}%>>Tamaulipas</option>
				<option value="Tlaxcala" <% if (vArr.elementAt(11).equals("Tlaxcala")) {%>selected<%}%>>Tlaxcala</option>
				<option value="Veracruz" <% if (vArr.elementAt(11).equals("Veracruz")) {%>selected<%}%>>Veracruz</option>
				<option value="Yucat�n" <% if (vArr.elementAt(11).equals("Yucat�n")) {%>selected<%}%>>Yucat�n</option>
				<option value="Zacatecas" <% if (vArr.elementAt(11).equals("Zacatecas")) {%>selected<%}%>>Zacatecas</option>
			</select>
		</td>
		<td><IMG border="0" src="../images/invisible.gif" width="250"></td>
	</tr>
		<tr class="AEtiquetaDentro">
		<td align="right">Estatus:&#160;&#160;</td>
		<td ><select size="1" name="comboEstatus" class="Atextittab" style="width=180px">
				<option value="0" <% if (vArr.elementAt(12).equals("0")) {%>selected<%}%>>Abierta</option>
				<option value="2" <% if (vArr.elementAt(12).equals("2")) {%>selected<%}%>>Cerrada</option>
			</select>
		</td>
		<td><IMG border="0" src="../images/invisible.gif" width="250"></td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td colspan="3">&#160;</td>
	</tr>
	<tr class="ATittabcenazu">
		<td colspan="3">&#160;</td>
	</tr>
	<tr>
		<td colspan="3">&#160;<br></td>
	</tr>
	<tr>
		<TD colspan ="3" align="center">
			<A href="javascript:fnRegresar();">
					<IMG name="btnRegresar" border="0" src="../images/b_regresar.gif"></A>
		&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
			<a href="javascript:fnValida();"><IMG border="0"
			name="continuar" src="../images/b_guardar.gif"></a>
		</td>
	</tr>


	</TBODY>
</TABLE>

</td>
</tr>
</table>
</form>
</BODY>
</HTML>