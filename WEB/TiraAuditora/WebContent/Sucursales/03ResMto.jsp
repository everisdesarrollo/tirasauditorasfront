<%@ page import="javax.naming.*"%>
<%@ page import="com.santander.utilerias.*"%>
<%@ page import="java.util.*"%>
<%@ page import="javax.servlet.http.*"%>
<%
//   Banco Santander Mexicano
//   jsp 03ResMto  Presenta los datos de las sucursales en base a criterio
//   @author Angel Gabriel Ramirez Alva
//   @version 1.0
//   fecha de creacion : 1 de Febrero del 2006
//   responsable : Eloisa Hernandez H
//   descripcion : Resuelve la presentaci�n de la informaci�n ha ser modificada
%>
<%
	String usuario = null;
	String perfil = null;
	String grupos = null;
	usuario = request.getHeader("iv-user");
	grupos = request.getHeader("iv-groups");
	
	if (usuario == null || grupos == null) {
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}	
%>
<HTML>
<HEAD>
<%@ page
language="java"
contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"
%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<%
	String sOculto = request.getParameter("oculto");
	String sNumsuc = request.getParameter("txtNumSuc");
%>
<LINK rel="stylesheet" href="../theme/Master.css" type="text/css">
<SCRIPT language="JavaScript" src="../theme/utiljs.js"></SCRIPT>
<TITLE>Mantenimiento a Sucursales</TITLE>

<%
  try
    {
      String lsXmlServlet   = null;
      int valor = 6;
      Vector vDatos = new Vector();
      String arrayElement = "";
      String parametro = "";
      String radio = "rad";
      /**************************************************/
      String pto_venta="pto_venta";
      String sabado="sabado";
      String nom_maq="nom_maq";
      String dir_ip="dir_ip";
      String sTitulo = "Mantenimiento de Sucursal por Centro de Costo Altair";


      /***************************************************/

      int campos = 13;
      AccesoServlet Conecta =  new AccesoServlet();
      Vector vCadena = new Vector();

/******************************************************************/
      parametro =  sOculto+"|"+sNumsuc;
/*****************************************************************/
      vCadena.addElement(parametro);
      lsXmlServlet = Conecta.LlamaServlet("SucursalServlet",vCadena).toString();

      String limit = "|";
      StringTokenizer strToken_value= new StringTokenizer(lsXmlServlet, limit );
      while(strToken_value.hasMoreElements())
        {
          String tmpVar= (strToken_value.nextToken()).trim();
          vDatos.add(tmpVar);
          System.out.println(tmpVar);
        }

	  arrayElement = (String)vDatos.elementAt(0);
	  if(arrayElement.equals("NO EXISTEN DATOS"))
	    {
		  System.out.println("nO Hay datos: "+lsXmlServlet);
		  response.sendRedirect("03ConFir.jsp?bandera="+sNumsuc);
	    }
      vCadena.clear();

%>

<script>

/********************************************************/
var arrDelete = new Array();
var arrNew = new Array();
var cont = 0;


/** Funcion agrega, se encarga de insertar datos en el arreglo
 ** los datos los obtiene de un vector en java
*/
function fnAgrega(indice)
  {
	<%
	  for(int k=0; k<vDatos.size()/campos; k++)
	    {
	%>
		if(indice == '<%=k%>')
		  {
                    var pto = '<%=vDatos.get(k * campos)%>';
                    var ts = '<%=vDatos.get(k * campos + 1)%>';
                    var nm = '<%=vDatos.get(k * campos + 2)%>';
                    var ip = '<%=vDatos.get(k * campos + 3)%>';
                    var nl = '<%=vDatos.get(k * campos + 4)%>';
                    var desc = '<%=vDatos.get(k * campos + 5)%>';
                    var cn ='<%=vDatos.get(k * campos + 6)%>';
                    var cp = '<%=vDatos.get(k * campos + 7)%>';
                    var col = '<%=vDatos.get(k * campos + 8)%>';
                    var cipo = '<%=vDatos.get(k * campos + 9)%>';
                    var del = '<%=vDatos.get(k * campos + 10)%>';
                    var est = '<%=vDatos.get(k * campos + 11)%>';
                    var open = '<%=vDatos.get(k * campos + 12)%>';
		  }
	<%
            }
	%>
		cont = arrDelete.length;
		arrDelete[cont++] = new Array(indice,pto,ts,nm,ip,nl,desc,cn,cp,col,cipo,del,est,open);

  }
/** Funcion borrar se encarga de borrar datos del arreglo en base a un indice
 ** Deja el arreglo nuevamente listo para usarse
*/
	function fnBorra(indice)
	  {
            var iIndice=indice;
            var iEle=0;
            arrNew = new Array();
		for (var x=0;x<arrDelete.length;x++)
		  {
			if (iIndice!=arrDelete[x][0])
			  {
				arrNew[iEle++] = new Array(arrDelete[x][0],arrDelete[x][1],arrDelete[x][2],arrDelete[x][3],arrDelete[x][4],arrDelete[x][5],arrDelete[x][6],arrDelete[x][7],arrDelete[x][8],arrDelete[x][9],arrDelete[x][10],arrDelete[x][11],arrDelete[x][12]);
			  }
		  }

		iEle=0;
		arrDelete = new Array();
		conQ=0;

		for (var x=0;x<arrNew.length;x++)
		  {
			conQ++;
			arrDelete[iEle++] = new Array(arrNew[x][0],arrNew[x][1],arrNew[x][2],arrNew[x][3],arrNew[x][4],arrNew[x][5],arrNew[x][6],arrNew[x][7],arrNew[x][8],arrNew[x][9],arrNew[x][10],arrNew[x][11],arrNew[x][12]);
  		  }
		if(arrNew.length==0)
		  {
			arrDelete = new Array();
			conQ=0;
		  }
	}
/************************TERMINA**********FUNCION BORRAR*******************************/
/** Esta funcion revisa si se ha echo click en el check box donde se muestran los datos
 *  Si no lo, llama a fnAgrega para agrega a un arreglo
 *  si es si, llama a fnBorra para que lo borre del arreglo
*/
	function fnCheca(indice)
	  {
		var exp = 'document.forma.rad'+indice+'.checked == false;';
		var estado = eval(exp);
		if(estado == false)
		  {
			fnAgrega(indice);
		  }
		else
			fnBorra(indice);
	  }


/** Funcion eliminar revisa el arreglo y pone la opcion 6 para borrarlo de la bd
 *  si no hay nada en el arreglo envia un alert para que seleccione
*/
function fnEliminar()
  {

    if(arrDelete.length != 0)
      {
	    document.forma.oculto.value = 6;
	    document.forma.parametro.value = "D";
	    document.forma.arrSend.value = arrDelete;

	    if(confirmSubmit())
	      {
	        document.forma.action = "03MtoEli.jsp";
	        document.forma.submit();
	      }
	  }
   else
    alert("Selecciona un registro");
  }



<!--

function confirmSubmit()
  {
    var agree=confirm("�Esta seguro que quiere eliminar el registro?");
    if (agree)
      return true ;
    else
	  return false ;
  }
// -->




/** Funcion modificar revisa el arreglo y concatena los datos con separador |
 *  si no hay nada en el arreglo envia un alert para que seleccione
*/
function fnModificar()
  {
	cto="";
	if(arrDelete.length != 0)
	  {
		for(k=1; k<=13; k++)
		  {
			cto = cto + arrDelete[0][k]+"|";
		  }
		document.forma.arrSend.value = cto;
		document.forma.action = "03ModSuc.jsp";
		document.forma.submit();
	  }
	else
	  alert("Selecciona un registro");
  }
/*****************************************************************************/
function code2Status(status){
	if(status == "0" || status == "1"){
		status = "Abierta";
	} else if (status == "2"){
		status = "Cerrada";
	}
	return status;
}
/******************************************************************************/

</script>

</HEAD>
<BODY>

<form name="forma" method="post" >
<input type="hidden" name="arrSend" />
<input type="hidden" name="oculto" />
<input type="hidden" name="parametro" />


<h1>Mantenimiento del Catalogo de Sucursales</h1>


<TABLE align="center" border="0" width="90%">
	<TR class="ALigaonce">
		<TD colspan="3" align="left" class="ALigaonce" >&#160;&#160;1 de 1</TD>
		<TD colspan="10" width="80%" class = "Atexnegro" align="right">&#160;<%=sTitulo%></TD>
	</TR>
	<tr class="Atittabcenazu">
		<TD align="center" width="10%">Selecci�n</TD>
		<TD align="center" width="5%" >C de C ALTAIR</TD>
		<TD align="center" width="10%" >Trabaja Sabado</TD>
		<TD align="center" width="10%" >Servidor</TD>
		<TD align="center" width="10%" >Direcci�n IP</TD>
		<TD align="center" width="10%" >Link</TD>
		<TD align="center" width="10%" >Sucursal</TD>
		<TD align="center" colspan="5">Direcci�n</TD>
		<TD align="center" width="5%">Abierta/Cerrada</TD>
	</tr>

	<%
	for(int i = 0; i<vDatos.size()/campos; i++)
          {
	    if(!(i%2!=0))
	      {
	%>
	<TR>
		<TD align="center" width="10%"><INPUT type="checkbox" name="<%=radio+i%>" value='<%=i%>' onClick="javascript:fnCheca(this.value);"></TD>
		<TD align="center" width="5%"><%= vDatos.get(i * campos) %></TD>
		<TD align="center" width="10%"><%= vDatos.get(i * campos + 1) %></TD>
		<TD align="center" width="10%"><%= vDatos.get(i * campos + 2) %></TD>
		<TD align="center" width="10%"><%= vDatos.get(i * campos + 3) %></TD>
		<TD align="center" width="10%"><%= vDatos.get(i * campos + 4) %></TD>
		<TD align="center" width="10%"><%= vDatos.get(i * campos + 5) %></TD>
		<TD align="center" colspan="5"><%= vDatos.get(i * campos + 6) %>,<%= vDatos.get(i * campos + 7) %>,
		<%= vDatos.get(i * campos + 8) %>,<%= vDatos.get(i * campos + 9) %>,<%= vDatos.get(i * campos + 10) %>,
		<%= vDatos.get(i * campos + 11) %></TD>
		<TD align="center" width="5%"><script>document.write(code2Status('<%=vDatos.get(i * campos + 12)%>'));</script></TD>
	</TR>

<%            }
	    else
	      {
%>
		<TR>
		<TD align="center" class="fondo" width="10%"><INPUT type="checkbox" name="<%=radio+i%>" value='<%=i%>' onClick="javascript:fnCheca(this.value);"></TD>
		<TD align="center" class="fondo" width="5%"><%= vDatos.get(i * campos) %></TD>
		<TD align="center" class="fondo" width="10%"><%= vDatos.get(i * campos + 1) %></TD>
		<TD align="center" class="fondo" width="10%"><%= vDatos.get(i * campos + 2) %></TD>
		<TD align="center" class="fondo" width="10%"><%= vDatos.get(i * campos + 3) %></TD>
		<TD align="center" class="fondo" width="10%"><%= vDatos.get(i * campos + 4) %></TD>
		<TD align="center" class="fondo" width="10%"><%= vDatos.get(i * campos + 5) %></TD>
		<TD align="center" class="fondo" colspan="5"><%= vDatos.get(i * campos + 6) %>,
		<%= vDatos.get(i * campos + 7) %>,<%= vDatos.get(i * campos + 8) %>,
		<%= vDatos.get(i * campos + 9) %>,<%= vDatos.get(i * campos + 10) %>,<%= vDatos.get(i * campos + 11) %></TD>
		<TD align="center" width="5%" class="fondo"><script>document.write(code2Status('<%=vDatos.get(i * campos + 12)%>'));</script></TD>
	</TR>
   <%         }
          } %>
	<TR>
		<TD colspan="9" align="center">&#160;</TD>
	</TR>

</TABLE>
</form>
<CENTER><TABLE width="60%">
<TBODY><TR>
		<TD align="center">&#160;<A href="javascript:fnModificar();"><IMG border="0" src="../images/b_modificar.gif" ></A></TD>
		<TD align="center"><A href="javascript:fnEliminar();"><IMG border="0" src="../images/b_eliminar.gif" ></A></TD>

	</TR>
</TBODY></TABLE></CENTER>
<%  }
    catch (Exception e)
      {
	    System.out.println("problem "+e);
	    System.out.println("error:" + e.getMessage());
      }
%>
</BODY>

</HTML>