<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.santander.sucursal.CargaArchivo,com.santander.sucursal.SucursalClase,java.util.Vector,java.util.Iterator"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%
	String usuario = null;
	String perfil = null;
	String grupos = null;
	usuario = request.getHeader("iv-user");
	grupos = request.getHeader("iv-groups");
	
	if (usuario == null || grupos == null) {
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}	
%>
<html>
<head>
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<SCRIPT language="JavaScript" src="../theme/utiljs.js"></SCRIPT>
<SCRIPT src="../theme/validaciones.js"></SCRIPT>
<LINK href="../theme/Master.css" rel="stylesheet"
	type="text/css">
<script>
function fnColoca(){
	document.forma.txtNomArch.focus();
}
function fnValida(){
	if(!document.forma.txtNomArch.value){
		alert("Debe Seleccionar un archivo");
	}else{
		document.forma.submit();
	}

}

/*********************/
function valida(campo,propiedades){
		   var lTempMssg=assertField(campo, propiedades);

		}
/*<VC autor ="ESC" fecha ="16/Abril/2008" descripcion = "Funcion para abrir ventana de altam masiva de sucurales" OT = "ALTFALTSUC"> */
function abreAltaMasiva()
{
      ventana=window.open('03CargaSuc.jsp','trainerWindow','width=730,height=260,toolbar=no,scrollbars=no,left=150,top=225');
}
/*</VC>*/


var valida_pventa = new Array('txtNumSuc','Numero Sucursal','N',false,4,4);


</script>

<title>Mantenimiento Sucursal.jsp</title>
</head>
<body>

<%
	if(request.getParameter("oculto") == null)
	{
%>
<FORM name="forma" method="post" action="03CargaSuc.jsp?oculto=8"  enctype="multipart/form-data">
<input type="hidden" name="oculto" value="8"/>
<table width="100%" align="left" border="0">
<tr>
		<td colspan="2" align="left" width="100%">
		<p class="ATitulocolor">Mantenimiento de Sucursal</p><BR>
		<BR>
		</td>
</tr>
<tr>
	<td>
	<table width="70%" align="center" border="0" cellspacing="0">
	<tr class="Atittabcenazu">
		<td colspan="3">&#160;Alta Masiva de Sucursales</td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td colspan="3">&#160;</td>

	</tr>
	<tr class="AEtiquetaDentro">
		<td align="right" class="Atexencabezado">Seleccione el archivo a cargar:&#160;&#160;&#160;&#160;</td>
		<td><input Type="file" name="txtNomArch"  class="Atextittab"  onkeypress="" onBlur="" /></td>
		<td valign="top"><IMG border="0" src="../images/invisible.gif" width="120"></td>

	</tr>
	<tr class="AEtiquetaDentro">
		<td colspan="3">&#160;</td>
	</tr>
	<tr class="Atittabcenazu">
		<td colspan="3">&#160;</td>
	</tr>
	<tr>
		<td colspan="3">&#160;<br></td>
	</tr>
	<tr>
		<td align="center" width="45%">&#160;</td>
		<td align="center"><a href="javascript:fnValida(); "><IMG border="0"
			name="continuar" src="../images/b_guardar.gif" align="left" ></a></td>

	</tr>
</table>
</td>
</tr>
</table>

</FORM>
<%}	else if("8".equals(request.getParameter("oculto"))){
	%>

	<table width="100%" align="left" border="0">
<tr>
		<td colspan="2" align="left" width="100%">
		<p class="ATitulocolor">Mantenimiento de Sucursal</p><BR>
		<BR>
		</td>
</tr>
<tr>
	<td>
	<table width="65%" align="center" border="0" cellspacing="0">
	<tr class="Atittabcenazu">
		<td colspan="3">&#160;Alta Masiva de Sucursales</td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td colspan="3">&#160;</td>

	</tr>
	<tr class="AEtiquetaDentro">
	<%boolean resultado ;
		CargaArchivo cargaArchivo = new CargaArchivo();

		Vector datos = new Vector();
		String mensaje;
		String sucursales ="";
		try
		{
		resultado = cargaArchivo.cargaArchivoExcel(request,datos);
		}catch(Exception e){
			e.printStackTrace();
			resultado = false;
			datos = new Vector();
			datos.add("ERR01");
			datos.add("Hubo un error al cargar el archivo");

		}
		java.io.PrintWriter outWriter = response.getWriter();
		Iterator it = null;
		if(resultado){

			SucursalClase sucursal = new SucursalClase();
			//errores = sucursal.sSucursalInsertaMasiva(datos);

			if(datos.size()>0)
			{
				mensaje ="Hubo un error al guardar en catalogo las sucursales, verifique que los datos sean correctos e intente nuevamente.";
				it = datos.iterator();
				while(it.hasNext())
				{
					sucursales += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sucursal:&nbsp;" +(String)it.next()+ "<br>";
				}
			}
			else
			{
				mensaje ="La operaci&oacute;n se ha realizado con &eacute;xito.";
			}

		}else
		{
			String codigo= "";
			mensaje = "";
			it = datos.iterator();
			codigo = (String)it.next();
			mensaje = (String)it.next() + " ";

			%>
			<script>
				alert("<%=mensaje%>");
				document.location="03CargaSuc.jsp";
			</script>
		<%}
	%>
		<td colspan="3" align="center" class="Atexencabezado"><%=mensaje %>&#160;&#160;&#160;&#160;</td>



	</tr>
	<tr class="AEtiquetaDentro">
	<td colspan="3"  align="left" class="Atexencabezado"><%=sucursales %>&#160;&#160;&#160;&#160;</td>

	</tr>

	<tr class="AEtiquetaDentro">
		<td colspan="3">&#160;</td>
	</tr>
	<tr class="Atittabcenazu">
		<td colspan="3">&#160;</td>
	</tr>
	<tr>
		<td colspan="3">&#160;<br></td>
	</tr>
	<tr>
		 <td align="center" width="50%">&#160;</td>

		<td ><a href="03CargaSuc.jsp" align="center">
		<IMG border="0"	name="continuar" src="../images/b_regresar.gif" align="left" ></a></td>
		 <td align="center" width="50%">&#160;</td>

	</tr>
</table>
</td>
</tr>
</table>


	<%}



%>

</body>
</html>