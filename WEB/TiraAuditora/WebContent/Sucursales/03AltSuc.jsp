<%
	String usuario = null;
	String perfil = null;
	String grupos = null;
	usuario = request.getHeader("iv-user");
	grupos = request.getHeader("iv-groups");
	
	if (usuario == null || grupos == null) {
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}	
%>
<HTML>
<HEAD>
<%@ page import="javax.servlet.http.*"%>
<%@ page import="java.util.ArrayList.*"%>
<%@ page
language="java"
contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"
%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<SCRIPT language="JavaScript" src="../theme/utiljs.js"></SCRIPT>
<LINK href="../theme/Master.css" rel="stylesheet"
	type="text/css">

<%
	String sNumSuc = request.getParameter("txtNumSuc");
%>


<TITLE>Alta de Sucursal</TITLE>
<script>

/************VALIDACIONES***********************************************/
function fnValIP(){
		var ipTemp = document.forma.txtDirIp1.value;
		if(ipTemp == "")
			return false;
		else
			ipTemp=ipTemp + ".";

		var ipTemp2= document.forma.txtDirIp2.value;
		if(ipTemp2 == "")
			return false;
		else
			ipTemp+=ipTemp2 + ".";

		var ipTemp3 = document.forma.txtDirIp3.value;
		if(ipTemp3 == "")
			return false;
		else
			ipTemp+=ipTemp3 + ".";

		var ipTemp4 = document.forma.txtDirIp4.value;
		if(ipTemp4 == "")
			return false;
		else{
			ipTemp+=ipTemp4;
			//alert("listo: "+ipTemp);
			document.forma.txtDirIp.value = ipTemp;
			return true;
			}
	}
	function fnValida(){

		var ns = document.forma.txtNumSuc.value;
		var tb = document.forma.txtTraSab.value;
		fnValIP();
		var ip = document.forma.txtDirIp.value;
		var nm = document.forma.txtNomMaq.value;
		var des = document.forma.txtDesc.value;
		var cn = document.forma.txtCaNu.value;
		var cp = document.forma.txtCp.value;
		var cl = document.forma.txtCol.value;
		var cpo = document.forma.txtCidPob.value;
		var dm = document.forma.txtDelMpio.value;
		var ce = document.forma.comboEstado.value;

		if(ns==""||ns=="0000"||tb==""||ip==""||nm==""||des==""||cn==""||cp==""||cl==""||cpo==""||dm==""||ce=="-1"){
			alert("Debe llenar todos los campos");

			if(ce == "-1"){
				document.forma.comboEstado.focus();
			}
			if(dm == ""){
				document.forma.txtDelMpio.focus();
			}
			if(cpo == ""){
				document.forma.txtCidPob.focus();
			}
			if(cl == ""){
				document.forma.txtCol.focus();
			}
			if(cp == ""){
				document.forma.txtCp.focus();
			}
			if(cn == ""){
				document.forma.txtCaNu.focus();
			}
			if(des == ""){
				document.forma.txtDesc.focus();
			}
			if(ip == ""){
				document.forma.txtDirIp1.focus();
			}
			if(tb == ""){
				document.forma.txtTraSab.focus();
			}
			if(ns == ""){
				document.forma.txtNumSuc.focus();
			}
			return false;
		}else{
			return true;
			 }
	}

/****************************************************/
function fnFoco(){
	document.forma.txtTraSab.focus();
}
function regresar(){
			document.location.replace("03MtoSuc.jsp");
	}

function guardar(){
	if(fnValida()){
		document.forma.txtTraSab.value = document.forma.txtTraSab.value.toUpperCase();
		document.forma.submit();
	}
}

/***************************************************************************/
</script>

</HEAD>
<BODY onLoad="javascript:fnFoco();">
<FORM name="forma" method="post" action="03AltGba.jsp">
<input type="hidden" name="oculto" value="5"/>
<input type="hidden" name="txtDirIp" />
<table width="100%" align="center" border="0">
<tr>
	<td>
	<p class="ATitulocolor">Alta de Sucursales al Sistema Tira Auditora</p><BR>
	</td>
</tr>
<tr>
	<td>
	<table width="85%" align="center" border="0" cellspacing="0">
	<TR class="Atittabcenazu">
		<td colspan="3">&#160;Alta de Sucursal</td>
	</TR>
	<tr class="AEtiquetaDentro">
		<td colspan="3">&#160;</td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td width="45%" align="right" class="Atexencabezado">Centro de Costos ALTAIR:&#160;&#160;</td>
		<td><input Type="text" name="txtNumSuc" class="Atextittab"  size="30" value="<%=sNumSuc%>" maxlength="4" readOnly /></td>
		<td>&nbsp;</td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td align="right" class="Atexencabezado">Trabaja Sabado:&#160;&#160;</td>
		<td><input Type="text" name="txtTraSab" class="Atextittab" size="30" maxlength="1" onBlur="javascript:fnValTsab(this.value, this.name)" onkeypress="return Valida_Texto(this,event,'z');" style="TEXT-TRANSFORM: uppercase"; /></td>
		<td>&nbsp;</td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td align="right">Direcci�n IP:&#160;&#160;</td>
		<td><input Type="text" name="txtDirIp1" class="Atextittab" maxlength="3" size="1" onBlur="javascript:fnValIPsola(this.value,this.name);" onkeypress="return Valida_Texto(this,event,'r');" />.
			<input Type="text" name="txtDirIp2" class="Atextittab" maxlength="3" size="1" onBlur="javascript:fnValIPsola(this.value,this.name);" onkeypress="return Valida_Texto(this,event,'r');" />.
			<input Type="text" name="txtDirIp3" class="Atextittab" maxlength="3" size="1" onBlur="javascript:fnValIPsola(this.value,this.name);" onkeypress="return Valida_Texto(this,event,'r');" />.
			<input Type="text" name="txtDirIp4" class="Atextittab" maxlength="3" size="1" onBlur="javascript:fnValIPsola(this.value,this.name);" onkeypress="return Valida_Texto(this,event,'r');" />
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td align="right">Nombre Servidor:&#160;&#160;</td>
		<td><input Type="text" name="txtNomMaq" class="Atextittab" maxlength="25" size="25" onkeypress="return Valida_Texto(this,event,'a2');" /></td>
		<td>&nbsp;</td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td align="right">Nombre de Sucursal:&#160;&#160;</td>
		<td><input Type="text" name="txtDesc" class="Atextittab" maxlength="50" size="30" onkeypress="return Valida_Texto(this,event,'a2');" /></td>
		<td>&nbsp;</td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td align="right">Calle, N�mero:&#160;&#160;</td>
		<td><input Type="text" name="txtCaNu" class="Atextittab" maxlength="60" size="30" onkeypress="return Valida_Texto(this,event,'t1');" /></td>
		<td>&nbsp;</td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td align="right">Codigo Postal:&#160;&#160;</td>
		<td><input Type="text" name="txtCp" class="Atextittab" maxlength="5" size="30" onBlur="javascript:fnValNums(this.value, this.name);" onkeypress="return Valida_Texto(this,event,'e');" /></td>
		<td>&nbsp;</td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td align="right">Colonia:&#160;&#160;</td>
		<td><input Type="text" name="txtCol" class="Atextittab" maxlength="30" size="30" onkeypress="return Valida_Texto(this,event,'a2');" /></td>
		<td>&nbsp;</td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td align="right">Ciudad / Poblaci�n:&#160;&#160;</td>
		<td><input Type="text" name="txtCidPob" class="Atextittab" maxlength="35" size="30" onkeypress="return Valida_Texto(this,event,'a2');" /></td>
		<td>&nbsp;</td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td align="right">Delegaci�n / Municipio:&#160;&#160;</td>
		<td><input Type="text" name="txtDelMpio" class="Atextittab" maxlength="35" size="30" onkeypress="return Valida_Texto(this,event,'a2');" /></td>
		<td>&nbsp;</td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td align="right">Estado:&#160;&#160;</td>
		<td><select size="1" name="comboEstado" class="Atextittab" style="width=180px">
				<option selected value="-1">Elegir...</option>
				<option value="Aguascalientes">Aguascalientes</option>
				<option value="Baja California Norte">Baja California Norte</option>
				<option value="Baja California Sur">Baja California Sur</option>
				<option value="Campeche">Campeche</option>
				<option value="Chiapas">Chiapas</option>
				<option value="Chihuahua">Chihuahua</option>
				<option value="Coahuila">Coahuila</option>
				<option value="Colima">Colima</option>
				<option value="Distrito Federal">Distrito Federal</option>
				<option value="Durango">Durango</option>
				<option value="Estado de M�xico">Estado de M�xico</option>
				<option value="Guanajuato">Guanajuato</option>
				<option value="Guerrero">Guerrero</option>
				<option value="Hidalgo">Hidalgo</option>
				<option value="Jalisco">Jalisco</option>
				<option value="Michoac�n">Michoac�n</option>
				<option value="Morelos">Morelos</option>
				<option value="Nuevo Le�n">Nuevo Le�n</option>
				<option value="Nayarit">Nayarit</option>
				<option value="Oaxaca">Oaxaca</option>
				<option value="Puebla">Puebla</option>
				<option value="Quer�taro">Quer�taro</option>
				<option value="Quintana Roo">Quintana Roo</option>
				<option value="San Luis Potos�">San Luis Potos�</option>
				<option value="Sinaloa">Sinaloa</option>
				<option value="Sonora">Sonora</option>
				<option value="Tabasco">Tabasco</option>
				<option value="Tamaulipas">Tamaulipas</option>
				<option value="Tlaxcala">Tlaxcala</option>
				<option value="Veracruz">Veracruz</option>
				<option value="Yucat�n">Yucat�n</option>
				<option value="Zacatecas">Zacatecas</option>
			</select>
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td colspan="3">&#160;</td>
	</tr>
	<tr class="ATittabcenazu">
		<td colspan="3">&#160;</td>
	</tr>
	<tr>
		<td colspan="3">&#160;<br></td>
	</tr>

	<tr>
		<td colspan ="3" align="center">
			<a href="javascript:regresar();"><IMG border="0"
			id="regresar" src="../images/b_regresar.gif"></a>
			<a href="javascript:guardar();"><IMG border="0"
			id="guardar" src="../images/b_guardar.gif"></a>
		</td>
	</tr>

</table>
</td>
</tr>
</table>

</FORM>
</BODY>
</HTML>