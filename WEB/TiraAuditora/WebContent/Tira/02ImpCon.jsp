<%@ page import="java.util.*, com.santander.tira.*,com.santander.utilerias.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%
	String usuario = null;
	String perfil = null;
	String grupos = null;
	usuario = request.getHeader("iv-user");
	grupos = request.getHeader("iv-groups");
	
	if (usuario == null || grupos == null) {
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}	
%>
<HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="../theme/Master.css" rel="stylesheet" type="text/css">
<TITLE>Tira Auditora</TITLE>
<%-- AUTOR: Ing. David Aguilar G�mez.--%>
<%-- CREACION: 01/02/2006 --%>
	<SCRIPT language="Javascript">
		function mueve_pag(pag,pagant)
		{
			document.frmImpCon.pagina.value = pag;
			document.frmImpCon.pagant.value = pagant;
			document.frmImpCon.submit();
		}
		function limpia()
		{
			document.frmImpCon.action = '02ConTir.jsp';
			document.frmImpCon.submit();
		}
		function imprime()
		{
			document.frmImpCon.action = '../servlet/TiraServlet';
			document.frmImpCon.submit();
		}
	</SCRIPT>
</HEAD>
<BODY>
<FORM id="frmImpCon" name="frmImpCon" method="post"	action="02ImpCon.jsp">
<TABLE width="100%" border="0">
	<TR>
		<TD>
		<P class="ATitulocolor">Consulta de Tira Auditoria</P>
<%-- Generacion de la paginaci�n y verificacion si hay registros seleccionados --%>
		<%
		  LoadProperties rb = LoadProperties.getInstance();
		  
  		java.text.DecimalFormat dec = new java.text.DecimalFormat("#,##0.00");
		  int seleccion = Integer.parseInt(request.getParameter("seleccion"));
		  int nopagina = Integer.parseInt(request.getParameter("pagina")!=null?request.getParameter("pagina"):"0");
		  String mensaje = "";
		  ArrayList pagina = null;
		  HttpSession sesion = request.getSession();
		  ArrayList total = (ArrayList)sesion.getAttribute("consultira");
		  if(total.size()>0 && total.get(0).getClass().equals(ArrayList.class))
		  {
			  if(request.getParameter("pagant")!=null)
			  {
			  	int pag = Integer.parseInt(request.getParameter("pagant"));
			  	ArrayList pagant = (ArrayList)total.get(pag);
			  	for(int j=0;j<pagant.size();j++)
			  	{
			  		TiraValue tira = (TiraValue)pagant.get(j);
			  		if(request.getParameter("sele"+j)!=null)
			  			tira.setImpresion(1);
			  		else
			  			tira.setImpresion(0);
			  	}
			  	pagant = null;
			  }
			  pagina = (ArrayList)total.get(nopagina);
		  }
		  else
		  {
		  	if(total.size()>0 && total.get(0).getClass().equals(String.class))
		  	{
				System.out.println("El texto es:"+(String)total.get(0));
				seleccion = 8;
				mensaje = (String)total.get(0);
		  	}
		  	else
		  	{
				System.out.println("No hay Datos");
				seleccion = 8;
				mensaje = "No existen datos.";
		  	}
		  }
		%>
		<TABLE width="90%" border="0" align="center" cellpadding="0" cellspacing="1">
			<TR>
				<TD colspan="8">
				<P>&nbsp;</P>
				</TD>
			</TR>
					<TR>
						<TD colspan="2"></TD>
						<TD colspan="6" class="Atexnegro">
							<%=request.getParameter("pventa")%>-<%=request.getParameter("nomsuc")%> <%=request.getParameter("fecha")%>
						</TD>
					</TR>
				<%
				System.out.println(seleccion);
				switch(seleccion)
				{
					case 0:
				%>
<%-- Impresion por Punto de Venta --%>
					<TR>
						<TD colspan="2" class="ALigaonce">
							<%if (nopagina != 0)
								{%>
								<A href="javascript:mueve_pag('<%=nopagina-1%>','<%=nopagina%>');" class="txt">&lt;&lt;</A>
							<%}%>
							 <%=(Integer.parseInt(rb.getValue("ta.noregistros"))*nopagina)+1%> a <%=(Integer.parseInt(rb.getValue("ta.noregistros"))*nopagina)+pagina.size()%>
							<%if (nopagina != total.size()-1)
								{%>
								<A href="javascript:mueve_pag('<%=nopagina+1%>','<%=nopagina%>');" class="txt">&gt;&gt;</A>
							<%}%>
						</TD>
						<%
							if("".equalsIgnoreCase(request.getParameter("origen").trim())){
						%>
							<TD colspan="4" class="Atexnegro">
						<%
							}else{
						%>
							<TD colspan="3" class="Atexnegro">
						<%
							}
						%>

							Rastreo por Punto de Venta "<%=request.getParameter("pventa")%>" en la Fecha "<%=request.getParameter("fecha")%>"
						</TD>
					</TR>
					<TR>
						<TD width="20%" class="ATituloTabla">No. Operaci&oacute;n</TD>
						<TD width="10%" class="ATituloTabla">Hora</TD>
						<TD width="50%" class="ATituloTabla">Operaci&oacute;n</TD>
						<TD width="10%" class="ATituloTabla">Usuario</TD>
						<%
							if("".equalsIgnoreCase(request.getParameter("origen").trim())){
						%>
								<TD width="10%" class="ATituloTabla">Origen</TD>
						<%
							}
						%>
						<TD width="10%" class="ATituloTabla">Imprimir</TD>
					</TR>
				<%
				for (int i=0;i<pagina.size();i++)
				{
					TiraValue reg_tira = (TiraValue)pagina.get(i);
					if(i%2==0)
					{
				%>
					<TR  class="Atextabdatcla">
				<%
					}
					else
					{
				%>
					<TR  class="Atextabdatobs">
				<%
					}
				%>
						<TD align="left"><%=reg_tira.getRefer_orig().equals("0") || reg_tira.getNoOperacion().equals(reg_tira.getRefer_orig())?reg_tira.getNoOperacion():reg_tira.getNoOperacion()+"/"+reg_tira.getRefer_orig()%></TD>
						<TD align="center"><%=reg_tira.getHora()%></TD>
						<TD align="left"><%=reg_tira.getOperacion()%></TD>
						<TD align="center"><%=reg_tira.getUsuario()%></TD>
						<%
							if("".equalsIgnoreCase(request.getParameter("origen").trim())){
						%>
								<TD align="center"><%=reg_tira.getOrigen()%></TD>
						<%
							}
						%>
				<%
					if(reg_tira.getImpresion()==0)
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="sele<%=i%>" value="1"></TD>
				<%
					}
					else
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="sele<%=i%>" value="1" checked></TD>
				<%
					}
				%>
					</TR>
				<%
				}
					break;
					case 1:
				%>
<%-- Impresion por Tipo de operaci�n --%>
					<TR>
						<TD colspan="2" class="ALigaonce">
							<%if (nopagina != 0)
								{%>
								<A href="javascript:mueve_pag('<%=nopagina-1%>','<%=nopagina%>');" class="txt">&lt;&lt;</A>
							<%}%>
							 <%=(Integer.parseInt(rb.getValue("ta.noregistros"))*nopagina)+1%> a <%=(Integer.parseInt(rb.getValue("ta.noregistros"))*nopagina)+pagina.size()%>
							<%if (nopagina != total.size()-1)
								{%>
								<A href="javascript:mueve_pag('<%=nopagina+1%>','<%=nopagina%>');"	class="txt">&gt;&gt;</A>
							<%}%>
						</TD>
						<TD colspan="6" class="Atexnegro">
							Rastreo por Tipo de Operaci&oacute;n "<%=request.getParameter("toperacion")%>"
						</TD>
					</TR>
					<TR>
						<TD width="20%" class="ATituloTabla">No. Operaci&oacute;n</TD>
						<TD width="10%" class="ATituloTabla">Hora</TD>
						<TD width="20%" class="ATituloTabla">Tipo Operaci&oacute;n</TD>
						<TD width="10%" class="ATituloTabla">Cuenta</TD>
						<TD width="10%" class="ATituloTabla">Folio</TD>
						<TD width="10%" class="ATituloTabla">Importe</TD>
						<TD width="10%" class="ATituloTabla">Estado</TD>
						<TD width="10%" class="ATituloTabla">Imprimir</TD>
					</TR>
				<%
				for (int i=0;i<pagina.size();i++)
				{
					TiraValue reg_tira = (TiraValue)pagina.get(i);
					if(i%2==0)
					{
				%>
					<TR  class="Atextabdatcla">
				<%
					}
					else
					{
				%>
					<TR  class="Atextabdatobs">
				<%
					}
				%>
						<TD align="left"><%=reg_tira.getRefer_orig().equals("0") || reg_tira.getNoOperacion().equals(reg_tira.getRefer_orig())?reg_tira.getNoOperacion():reg_tira.getNoOperacion()+"/"+reg_tira.getRefer_orig()%></TD>
						<TD align="center"><%=reg_tira.getHora()%></TD>
						<TD align="left"><%=reg_tira.getTipoOperacion()%></TD>
						<TD align="left"><%=reg_tira.getCuenta()%></TD>
						<TD align="right"><%=reg_tira.getFolio()%></TD>
						<TD align="right"><%=dec.format(Double.parseDouble(reg_tira.getImporte()))%></TD>
						<TD align="center"><%=reg_tira.getEstado()%></TD>
				<%
					if(reg_tira.getImpresion()==0)
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="sele<%=i%>" value="1"></TD>
				<%
					}
					else
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="sele<%=i%>" value="1" checked></TD>
				<%
					}
				%>
					</TR>
				<%
				}
					break;
					case 2:
				%>
<%-- Impresion por N�mero de operaci�n --%>
					<TR>
						<TD colspan="2" class="ALigaonce">
							<%if (nopagina != 0)
								{%>
								<A href="javascript:mueve_pag('<%=nopagina-1%>','<%=nopagina%>');" class="txt">&lt;&lt;</A>
							<%}%>
							 <%=(Integer.parseInt(rb.getValue("ta.noregistros"))*nopagina)+1%> a <%=(Integer.parseInt(rb.getValue("ta.noregistros"))*nopagina)+pagina.size()%>
							<%if (nopagina != total.size()-1)
								{%>
								<A href="javascript:mueve_pag('<%=nopagina+1%>','<%=nopagina%>');"	class="txt">&gt;&gt;</A>
							<%}%>
						</TD>
						<TD colspan="6" class="Atexnegro">
							Rastreo por N�mero de Operaci&oacute;n "<%=request.getParameter("nooperacion")%>"
						</TD>
					</TR>
					<TR>
						<TD width="15%" class="ATituloTabla">No. Operaci&oacute;n</TD>
						<TD width="7%" class="ATituloTabla">Hora</TD>
						<TD width="37%" class="ATituloTabla">Tipo Operaci&oacute;n</TD>
						<TD width="10%" class="ATituloTabla">Cuenta</TD>
						<TD width="7%" class="ATituloTabla">Folio</TD>
						<TD width="8%" class="ATituloTabla">Importe</TD>
						<TD width="8%" class="ATituloTabla">Estado</TD>
						<TD width="8%" class="ATituloTabla">Imprimir</TD>
					</TR>
				<%
				for (int i=0;i<pagina.size();i++)
				{
					TiraValue reg_tira = (TiraValue)pagina.get(i);
					if(i%2==0)
					{
				%>
					<TR  class="Atextabdatcla">
				<%
					}
					else
					{
				%>
					<TR  class="Atextabdatobs">
				<%
					}
				%>
						<TD align="left"><%=reg_tira.getRefer_orig().equals("0") || reg_tira.getNoOperacion().equals(reg_tira.getRefer_orig())?reg_tira.getNoOperacion():reg_tira.getNoOperacion()+"/"+reg_tira.getRefer_orig()%></TD>
						<TD align="center"><%=reg_tira.getHora()%></TD>
						<TD align="left"><%=reg_tira.getTipoOperacion()%></TD>
						<TD align="left"><%=reg_tira.getCuenta()%></TD>
						<TD align="right"><%=reg_tira.getFolio()%></TD>
						<TD align="right"><%=dec.format(Double.parseDouble(reg_tira.getImporte()))%></TD>
						<TD align="center"><%=reg_tira.getEstado()%></TD>
				<%
					if(reg_tira.getImpresion()==0)
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="sele<%=i%>" value="1"></TD>
				<%
					}
					else
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="sele<%=i%>" value="1" checked></TD>
				<%
					}
				%>
					</TR>
				<%
				}
					break;
					case 3:
				%>
<%-- Impresion por Cuenta --%>
					<TR>
						<TD colspan="2" class="ALigaonce">
							<%if (nopagina != 0)
								{%>
								<A href="javascript:mueve_pag('<%=nopagina-1%>','<%=nopagina%>');" class="txt">&lt;&lt;</A>
							<%}%>
							 <%=(Integer.parseInt(rb.getValue("ta.noregistros"))*nopagina)+1%> a <%=(Integer.parseInt(rb.getValue("ta.noregistros"))*nopagina)+pagina.size()%>
							<%if (nopagina != total.size()-1)
								{%>
								<A href="javascript:mueve_pag('<%=nopagina+1%>','<%=nopagina%>');"	class="txt">&gt;&gt;</A>
							<%}%>
						</TD>
						<TD colspan="5" class="Atexnegro">
							Rastreo de Operaciones por Cuenta "<%=request.getParameter("cuenta")%>"
						</TD>
					</TR>
					<TR>
						<TD width="20%" class="ATituloTabla">No. Operaci&oacute;n</TD>
						<TD width="10%" class="ATituloTabla">Hora</TD>
						<TD width="20%" class="ATituloTabla">Tipo Operaci&oacute;n</TD>
						<TD width="10%" class="ATituloTabla">Folio</TD>
						<TD width="15%" class="ATituloTabla">Importe</TD>
						<TD width="15%" class="ATituloTabla">Estado</TD>
						<TD width="10%" class="ATituloTabla">Imprimir</TD>
					</TR>
				<%
				for (int i=0;i<pagina.size();i++)
				{
					TiraValue reg_tira = (TiraValue)pagina.get(i);
					if(i%2==0)
					{
				%>
					<TR  class="Atextabdatcla">
				<%
					}
					else
					{
				%>
					<TR  class="Atextabdatobs">
				<%
					}
				%>
						<TD align="left"><%=reg_tira.getRefer_orig().equals("0") || reg_tira.getNoOperacion().equals(reg_tira.getRefer_orig())?reg_tira.getNoOperacion():reg_tira.getNoOperacion()+"/"+reg_tira.getRefer_orig()%></TD>
						<TD align="center"><%=reg_tira.getHora()%></TD>
						<TD align="left"><%=reg_tira.getTipoOperacion()%></TD>
						<TD align="right"><%=reg_tira.getFolio()%></TD>
						<TD align="right"><%=dec.format(Double.parseDouble(reg_tira.getImporte()))%></TD>
						<TD align="center"><%=reg_tira.getEstado()%></TD>
				<%
					if(reg_tira.getImpresion()==0)
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="sele<%=i%>" value="1"></TD>
				<%
					}
					else
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="sele<%=i%>" value="1" checked></TD>
				<%
					}
				%>
					</TR>
				<%
				}
					break;
					case 4:
				%>
<%-- Impresion por Punto Usuario --%>
					<TR>
						<TD colspan="2" class="ALigaonce">
							<%if (nopagina != 0)
								{%>
								<A href="javascript:mueve_pag('<%=nopagina-1%>','<%=nopagina%>');" class="txt">&lt;&lt;</A>
							<%}%>
							 <%=(Integer.parseInt(rb.getValue("ta.noregistros"))*nopagina)+1%> a <%=(Integer.parseInt(rb.getValue("ta.noregistros"))*nopagina)+pagina.size()%>
							<%if (nopagina != total.size()-1)
								{%>
								<A href="javascript:mueve_pag('<%=nopagina+1%>','<%=nopagina%>');"	class="txt">&gt;&gt;</A>
							<%}%>
						</TD>
						<TD colspan="2" class="Atexnegro">
							Rastreo por Usuario "<%=request.getParameter("usuario")%>-<%=TiraClase.descCustodio(request.getParameter("usuario"))%>"
						</TD>
					</TR>
					<TR>
						<TD width="20%" class="ATituloTabla">No. Operaci&oacute;n</TD>
						<TD width="10%" class="ATituloTabla">Hora</TD>
						<TD width="60%" class="ATituloTabla">Operaci&oacute;n</TD>
						<TD width="10%" class="ATituloTabla">Imprimir</TD>
					</TR>
				<%
				for (int i=0;i<pagina.size();i++)
				{
					TiraValue reg_tira = (TiraValue)pagina.get(i);
					if(i%2==0)
					{
				%>
					<TR  class="Atextabdatcla">
				<%
					}
					else
					{
				%>
					<TR  class="Atextabdatobs">
				<%
					}
				%>
						<TD align="left"><%=reg_tira.getRefer_orig().equals("0") || reg_tira.getNoOperacion().equals(reg_tira.getRefer_orig())?reg_tira.getNoOperacion():reg_tira.getNoOperacion()+"/"+reg_tira.getRefer_orig()%></TD>
						<TD align="center"><%=reg_tira.getHora()%></TD>
						<TD align="left"><%=reg_tira.getOperacion()%></TD>
				<%
					if(reg_tira.getImpresion()==0)
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="sele<%=i%>" value="1"></TD>
				<%
					}
					else
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="sele<%=i%>" value="1" checked></TD>
				<%
					}
				%>
					</TR>
				<%
				}
					break;
					case 5:
				%>
<%-- Impresion por Horario --%>
					<TR>
						<TD colspan="2" class="ALigaonce">
							<%if (nopagina != 0)
								{%>
								<A href="javascript:mueve_pag('<%=nopagina-1%>','<%=nopagina%>');" class="txt">&lt;&lt;</A>
							<%}%>
							 <%=(Integer.parseInt(rb.getValue("ta.noregistros"))*nopagina)+1%> a <%=(Integer.parseInt(rb.getValue("ta.noregistros"))*nopagina)+pagina.size()%>
							<%if (nopagina != total.size()-1)
								{%>
								<A href="javascript:mueve_pag('<%=nopagina+1%>','<%=nopagina%>');"	class="txt">&gt;&gt;</A>
							<%}%>
						</TD>
						<TD colspan="6" class="Atexnegro">
							Rastreo por Horario desde "<%=request.getParameter("horaini")%>" hasta "<%=request.getParameter("horafin")%>"
						</TD>
					</TR>
					<TR>
						<TD width="20%" class="ATituloTabla">No. Operaci&oacute;n</TD>
						<TD width="10%" class="ATituloTabla">Hora</TD>
						<TD width="20%" class="ATituloTabla">Tipo Operaci&oacute;n</TD>
						<TD width="10%" class="ATituloTabla">Cuenta</TD>
						<TD width="10%" class="ATituloTabla">Folio</TD>
						<TD width="10%" class="ATituloTabla">Importe</TD>
						<TD width="10%" class="ATituloTabla">Estado</TD>
						<TD width="10%" class="ATituloTabla">Imprimir</TD>
					</TR>
				<%
				for (int i=0;i<pagina.size();i++)
				{
					TiraValue reg_tira = (TiraValue)pagina.get(i);
					if(i%2==0)
					{
				%>
					<TR  class="Atextabdatcla">
				<%
					}
					else
					{
				%>
					<TR  class="Atextabdatobs">
				<%
					}
				%>
						<TD align="left"><%=reg_tira.getRefer_orig().equals("0") || reg_tira.getNoOperacion().equals(reg_tira.getRefer_orig())?reg_tira.getNoOperacion():reg_tira.getNoOperacion()+"/"+reg_tira.getRefer_orig()%></TD>
						<TD align="center"><%=reg_tira.getHora()%></TD>
						<TD align="left"><%=reg_tira.getTipoOperacion()%></TD>
						<TD align="left"><%=reg_tira.getCuenta()%></TD>
						<TD align="right"><%=reg_tira.getFolio()%></TD>
						<TD align="right"><%=dec.format(Double.parseDouble(reg_tira.getImporte()))%></TD>
						<TD align="center"><%=reg_tira.getEstado()%></TD>
				<%
					if(reg_tira.getImpresion()==0)
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="sele<%=i%>" value="1"></TD>
				<%
					}
					else
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="sele<%=i%>" value="1" checked></TD>
				<%
					}
				%>
					</TR>
				<%
				}
					break;
					case 6:
				%>
<%-- Impresion por Importe --%>
					<TR>
						<TD colspan="2" class="ALigaonce">
							<%if (nopagina != 0)
								{%>
								<A href="javascript:mueve_pag('<%=nopagina-1%>','<%=nopagina%>');" class="txt">&lt;&lt;</A>
							<%}%>
							 <%=(Integer.parseInt(rb.getValue("ta.noregistros"))*nopagina)+1%> a <%=(Integer.parseInt(rb.getValue("ta.noregistros"))*nopagina)+pagina.size()%>
							<%if (nopagina != total.size()-1)
								{%>
								<A href="javascript:mueve_pag('<%=nopagina+1%>','<%=nopagina%>');"	class="txt">&gt;&gt;</A>
							<%}%>
						</TD>
						<TD colspan="6" class="Atexnegro">
							Rastreo por Importe desde "<%=dec.format(Double.parseDouble(request.getParameter("importeini")))%>" hasta "<%=dec.format(Double.parseDouble(request.getParameter("importefin")))%>"
						</TD>
					</TR>
					<TR>
						<TD width="20%" class="ATituloTabla">No. Operaci&oacute;n</TD>
						<TD width="10%" class="ATituloTabla">Hora</TD>
						<TD width="20%" class="ATituloTabla">Tipo Operaci&oacute;n</TD>
						<TD width="10%" class="ATituloTabla">Cuenta</TD>
						<TD width="10%" class="ATituloTabla">Folio</TD>
						<TD width="10%" class="ATituloTabla">Importe</TD>
						<TD width="10%" class="ATituloTabla">Estado</TD>
						<TD width="10%" class="ATituloTabla">Imprimir</TD>
					</TR>
				<%
				for (int i=0;i<pagina.size();i++)
				{
					TiraValue reg_tira = (TiraValue)pagina.get(i);
					if(i%2==0)
					{
				%>
					<TR  class="Atextabdatcla">
				<%
					}
					else
					{
				%>
					<TR  class="Atextabdatobs">
				<%
					}
				%>
						<TD align="left"><%=reg_tira.getRefer_orig().equals("0") || reg_tira.getNoOperacion().equals(reg_tira.getRefer_orig())?reg_tira.getNoOperacion():reg_tira.getNoOperacion()+"/"+reg_tira.getRefer_orig()%></TD>
						<TD align="center"><%=reg_tira.getHora()%></TD>
						<TD align="left"><%=reg_tira.getTipoOperacion()%></TD>
						<TD align="left"><%=reg_tira.getCuenta()%></TD>
						<TD align="right"><%=reg_tira.getFolio()%></TD>
						<TD align="right"><%=dec.format(Double.parseDouble(reg_tira.getImporte()))%></TD>
						<TD align="center"><%=reg_tira.getEstado()%></TD>
				<%
					if(reg_tira.getImpresion()==0)
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="sele<%=i%>" value="1"></TD>
				<%
					}
					else
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="sele<%=i%>" value="1" checked></TD>
				<%
					}
				%>
					</TR>

				<%
				}
					break;
					case 7:
				%>
<%-- Impresion por Id de Ats --%>
					<TR>
						<TD colspan="2" class="ALigaonce">
							<%if (nopagina != 0)
								{%>
								<A href="javascript:mueve_pag('<%=nopagina-1%>','<%=nopagina%>');" class="txt">&lt;&lt;</A>
							<%}%>
							 <%=(Integer.parseInt(rb.getValue("ta.noregistros"))*nopagina)+1%> a <%=(Integer.parseInt(rb.getValue("ta.noregistros"))*nopagina)+pagina.size()%>
							<%if (nopagina != total.size()-1)
								{%>
								<A href="javascript:mueve_pag('<%=nopagina+1%>','<%=nopagina%>');"	class="txt">&gt;&gt;</A>
							<%}%>
						</TD>
						<TD colspan="6" class="Atexnegro">
							Rastreo por Identificador de ATS "<%=request.getParameter("idats")%>"
						</TD>
					</TR>
					<TR>
						<TD width="15%" class="ATituloTabla">No. Operaci&oacute;n</TD>
						<TD width="7%" class="ATituloTabla">Hora</TD>
						<TD width="37%" class="ATituloTabla">Tipo Operaci&oacute;n</TD>
						<TD width="10%" class="ATituloTabla">Cuenta</TD>
						<TD width="7%" class="ATituloTabla">Folio</TD>
						<TD width="8%" class="ATituloTabla">Importe</TD>
						<TD width="8%" class="ATituloTabla">Estado</TD>
						<TD width="8%" class="ATituloTabla">Imprimir</TD>


					</TR>
				<%
				for (int i=0;i<pagina.size();i++)
				{
					TiraValue reg_tira = (TiraValue)pagina.get(i);
					if(i%2==0)
					{
				%>
					<TR  class="Atextabdatcla">
				<%
					}
					else
					{
				%>
					<TR  class="Atextabdatobs">
				<%
					}
				%>
						<TD align="left"><%=reg_tira.getRefer_orig().equals("0") || reg_tira.getNoOperacion().equals(reg_tira.getRefer_orig())?reg_tira.getNoOperacion():reg_tira.getNoOperacion()+"/"+reg_tira.getRefer_orig()%></TD>
						<TD align="center"><%=reg_tira.getHora()%></TD>
						<TD align="left"><%=reg_tira.getTipoOperacion()%></TD>
						<TD align="left"><%=reg_tira.getCuenta()%></TD>
						<TD align="right"><%=reg_tira.getFolio()%></TD>
						<TD align="right"><%=dec.format(Double.parseDouble(reg_tira.getImporte()))%></TD>
						<TD align="center"><%=reg_tira.getEstado()%></TD>
				<%
					if(reg_tira.getImpresion()==0)
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="sele<%=i%>" value="1"/></TD>
				<%
					}
					else
					{
				%>
						<TD align="center"><INPUT type="checkbox" name="sele<%=i%>" value="1" checked/></TD>
				<%
					}
				%>
					</TR>
				<%
				}
					break;
					case 8:
				%>
<%-- Impresion cuando es contigente o no hay datos --%>
					<TR>
						<TD colspan="8" height="10" class="Atittabcenazu">&nbsp;</TD>
					</TR>
					<TR class="Atextabdatobs">
						<TD colspan="8">&nbsp;</TD>
					</TR>
					<TR class="Atextabdatobs">
						<TD colspan="8" align="center" class="Atexencabezado" >
							"<%=mensaje%>"
						</TD>
					</TR>
					<TR class="Atextabdatobs">
						<TD colspan="8">&nbsp;</TD>
					</TR>
					<TR>
						<TD colspan="8" height="10" class="Atittabcenazu">&nbsp;</TD>
					</TR>
				<%
					break;
					default:
						System.out.println("No es selecci�n valida");
					break;
				}
				%>
			<TR>
			  <TD colspan="8" height="14"><p>&nbsp;</p></TD>
			</TR>
			<TR>
				<%if(seleccion != 8)
					{%>
				<TD colspan="8" align="center">
					<A href="javascript:limpia();">
						<IMG name="btnlimpiar" border="0" src="../images/b_regresar.gif"></A>
					&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					<A href="javascript:imprime();">
						<IMG name="btnimprimir" border="0" src="../images/b_imprimir.gif"></A>
				</TD>
				<%}
					else
					{
				%>
				<TD colspan="8" align="center">
					<A href="javascript:limpia();">
						<IMG name="btnlimpiar" border="0" src="../images/b_regresar.gif"></A>
				</TD>
				<%}
				%>
			</TR>
		</TABLE>
		<P></P>
		<P>&nbsp;</P>
		</TD>
	</TR>
</TABLE>
<%-- Variables ocultas --%>
<INPUT type="hidden" name="pagina" value="<%=request.getParameter("pagina")!=null?request.getParameter("pagina"):"0"%>">
<INPUT type="hidden" name="pagant" value="<%=request.getParameter("pagant")%>">
<INPUT type="hidden" name="fecha" value="<%=request.getParameter("fecha")%>">
<INPUT type="hidden" name="pventa" value="<%=request.getParameter("pventa")%>">
<INPUT type="hidden" name="nomsuc" value="<%=request.getParameter("nomsuc")%>">
<INPUT type="hidden" name="origen" value="<%=request.getParameter("origen")%>">
<INPUT type="hidden" name="seleccion" value="<%=request.getParameter("seleccion")%>">
<INPUT type="hidden" name="toperacion" value="<%=request.getParameter("toperacion")%>">
<INPUT type="hidden" name="nooperacion" value="<%=request.getParameter("nooperacion")%>">
<INPUT type="hidden" name="cuenta" value="<%=request.getParameter("cuenta")%>">
<INPUT type="hidden" name="usuario" value="<%=request.getParameter("usuario")%>">
<INPUT type="hidden" name="horaini" value="<%=request.getParameter("horaini")%>">
<INPUT type="hidden" name="horafin" value="<%=request.getParameter("horafin")%>">
<INPUT type="hidden" name="importeini" value="<%=request.getParameter("importeini")%>">
<INPUT type="hidden" name="importefin" value="<%=request.getParameter("importefin")%>">
<INPUT type="hidden" name="idats" value="<%=request.getParameter("idats")%>">
<INPUT type="hidden" name="accion" value="imprimir">
</FORM>
</BODY>
</HTML>