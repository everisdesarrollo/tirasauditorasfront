<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%
	String usuario = null;
	String perfil = null;
	String grupos = null;
	usuario = request.getHeader("iv-user");
	grupos = request.getHeader("iv-groups");
	
	if (usuario == null || grupos == null) {
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}	
%>
<HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="../theme/Master.css" rel="stylesheet"
	type="text/css">
<TITLE>Tira Auditora</TITLE>
<!-- AUTOR: Ing. David Aguilar G�mez.-->
<!-- CREACION: 01/02/2006 -->
	<SCRIPT language="Javascript">
		function limpia()
		{
				document.frmBitacora.action = '07CarMan.jsp';
				document.frmBitacora.submit();
		}
	</SCRIPT>
</HEAD>
<BODY>
<FORM id="frmBitacora" name="frmBitacora" method="post"	action="07Bitacora.jsp">
<TABLE width="100%" border="0">
	<TR>
		<TD>
		<P class="ATitulocolor">Bit�cora de Carga Manual</P>
		<%
		  
		  HttpSession sesion = request.getSession();
		  java.util.ArrayList total = (java.util.ArrayList)sesion.getAttribute("bitacora");
		  String mensaje = (String)sesion.getAttribute("mensaje");
		%>
		<TABLE width="90%" border="0" align="center" cellpadding="0" cellspacing="1">
			<TR>
				<TD colspan="2">
				<P>&nbsp;</P>
				</TD>
			</TR>
			<%
			if(total!=null && total.size()>0)
			{
			%>
				<TR>
					<TD class="ALigaonce">&nbsp;</TD>
					<TD class="Atexnegro">
					 Se encontraron "<%=total.size()/2%>" errores en la Carga Manual
					</TD>
				</TR>
				<TR>
					<TD width="15%" class="ATituloTabla">No. Linea</TD>
					<TD width="85%" class="ATituloTabla">Error</TD>
				</TR>
				<%
				int j = 0;
				for (int i=0;i<total.size();i++)
				{
					if(j%2==0)
					{
				%>
				<TR  class="Atextabdatcla">
				<%
					}
					else
					{
				%>
				<TR  class="Atextabdatobs">
				<%
					}
				%>
					<TD align="center"><%=(String)total.get(i)%></TD>
				<% i++; j++;%>
					<TD align="left"><%=(String)total.get(i)%></TD>
				</TR>
				<%}%>
			<%
			}
			else
			{
			%>
				<TR>
					<TD colspan="2" height="10" class="Atittabcenazu">&nbsp;</TD>
				</TR>
				<TR class="Atextabdatobs">
					<TD colspan="2">&nbsp;</TD>
				</TR>
				<TR class="Atextabdatobs">
					<TD colspan="2" align="center" class="Atexencabezado" >
					<%if(mensaje.length()>0)
						{
					%>
						<%=mensaje%>
					<%}
						else
						{
					%>
							"Todos los registros fueron insertados exitosamente"
					<%}
					%>
					</TD>
				</TR>
				<TR class="Atextabdatobs">
					<TD colspan="2">&nbsp;</TD>
				</TR>
				<TR>
					<TD colspan="2" height="10" class="Atittabcenazu">&nbsp;</TD>
				</TR>
			<%}%>
			<TR>
			  <TD colspan="2" height="14"><p>&nbsp;</p></TD>
			</TR>
			<TR>
				<TD colspan="2" align="center">
					<A href="javascript:limpia();">
						<IMG name="btnlimpiar" border="0" src="../images/b_regresar.gif"></A>
				</TD>
			</TR>
		</TABLE>
		<P></P>
		<P>&nbsp;</P>
		</TD>
	</TR>
</TABLE>
</FORM>
</BODY>
</HTML>