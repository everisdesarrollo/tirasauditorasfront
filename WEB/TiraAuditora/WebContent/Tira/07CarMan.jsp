<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%
	String usuario = null;
	String perfil = null;
	String grupos = null;
	usuario = request.getHeader("iv-user");
	grupos = request.getHeader("iv-groups");
	
	if (usuario == null || grupos == null) {
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}	
%>
<HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="../theme/Master.css" rel="stylesheet"
	type="text/css">
<TITLE>Tira Auditora</TITLE>
<!-- AUTOR: Ing. David Aguilar G�mez.-->
<!-- CREACION: 01/02/2006 -->
	<SCRIPT src="../theme/calendario.js"></SCRIPT>
	<SCRIPT src="../theme/validaciones.js"></SCRIPT>
	<SCRIPT language="Javascript">
		function limpia()
		{
			document.frmCarMan.reset();
		}
		function busca()
		{
			var valor = document.frmCarMan.archivo.value;
			if(valor.substr((valor.length-3),3)=='txt')
			{
				document.frmCarMan.accion.value = 'enviar';
				document.frmCarMan.submit();
			}
			else
				alert('No es un archivo valido.');
		}
	</SCRIPT>
</HEAD>

<BODY>
	<form id="frmCarMan" name="frmCarMan" method="post" action="../servlet/CargaServlet" ENCTYPE="multipart/form-data">
	<table width="100%" border="0">
	  <tr>
	    <td>
	      <p class="ATitulocolor">Carga Manual de Tira Auditoria por Sucursal</p>
	      <TABLE width="90%" border="0" cellspacing="0" align="center">
	        <TR>
	          <TD colspan="2">&nbsp;</TD>
	        </TR>
	        <TR>
	          <TD colspan="2" class="Atittabcenazu">&nbsp;Seleccione un archivo</TD>
	        </TR>
	        <TR class="AEtiquetaDentro">
			  <TD class="Atexencabezado" colspan="2">&nbsp;</TD>
			</TR>
	        <TR class="AEtiquetaDentro">
			  <TD class="Atexencabezado" align="right" width="30%">Nombre del Archivo</TD>
			  <TD align="left" width="70%">
			  	<INPUT type="file" name="archivo" size="30"></TD>
			</TR>
	        <TR class="AEtiquetaDentro">
			  <TD class="Atexencabezado" colspan="2">&nbsp;</TD>
			</TR>
			<TR>
			  <TD colspan="2" height="10" class="Atittabcenazu">&nbsp;</TD>
			</TR>
			<TR>
			  <TD colspan="2" height="14"><p>&nbsp;</p></TD>
			</TR>
			<TR>
			  <TD height="17" align="center" colspan="2">
				<A href="javascript:limpia();">
					<IMG name="btnlimpiar" border="0" src="../images/b_limpiar.gif"></A>
				&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
				<A href="javascript:busca();">
					<IMG name="btnbuscar" border="0" src="../images/b_enviar.gif"></A>
			  </TD>
			</TR>
		</TABLE>
        <p></p>
	    <p>&nbsp;</p>
	    </td>
	  </tr>
	</table>
	<INPUT type="hidden" name="accion" value="enviar">
	</form>
</BODY>
</HTML>