<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%
	String usuario = null;
	String perfil = null;
	String grupos = null;
	usuario = request.getHeader("iv-user");
	grupos = request.getHeader("iv-groups");
	
	if (usuario == null || grupos == null) {
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}	
%>
<HTML>
<HEAD>
<%@ page
language="java"
contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"
%>

<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="../theme/Master.css" rel="stylesheet"
	type="text/css">
<SCRIPT src="../theme/calendario.js"></SCRIPT>
<TITLE>Generaci�n de Archivos de Ingresos y Egresos</TITLE>
<script>
function fnLimpia()
  {
	document.forma.reset();
  }
function fnValida()
{
	if(fnCheca()){
		document.forma.parametro.value ="C";
		document.forma.submit();
	}
}

function fnCheca(){
	  	var g1=0;

	  	if(document.forma.GrupoExt1[0].checked == true){
				g1=1;
				document.forma.grupo1.value = g1;
				document.forma.oculto.value = "1";
				}else if(document.forma.GrupoExt1[1].checked == true){
						g1=2;
						document.forma.grupo1.value = g1;
						document.forma.oculto.value = "2";
					  }else{
					  	alert("Debe seleccionar una opci�n: \n Solo informaci�n faltante � Archivos Completos");
					  	return false
					  }

		if(document.forma.GrupoExt2.value == -1){
		  	alert("Debe elegir la informaci�n a generar: \n Saldos, Movientos � Ambos");
		  	document.forma.GrupoExt2.focus();
		  	return false;
		}
		if(g1 == 1 && document.forma.fecha.value == ""){
			alert("Debe elegir la fecha de contingencia");
			document.forma.fecha.focus();
			return false
		}else if(g1 == 2 && document.forma.fecha.value == ""){
					tmp = fnGeneraFecha();
					document.forma.fecha.value = tmp;
				  }
		return true;
}
function fnGeneraFecha(){
	var curdate = new Date();
	var mday = curdate.getDate();
	var month = curdate.getMonth();
	var year = curdate.getFullYear();
	var myFecha = new Date(year,month, mday - 1);

	mday = myFecha.getDate();
	month = myFecha.getMonth();
	year = myFecha.getFullYear();

	if(mday <10 ){
		mday = "0"+mday;
	}
		month = month+1;
			if(month <10 ){
				month = "0"+month;
			}
	var tmp = mday+"/"+month+"/"+year;
	return tmp;
}
</script>
</HEAD>
<BODY >
<FORM name="forma" method="post" action="05ConFtp.jsp">
<input type="hidden" name="oculto" />
<input type="hidden" name="grupo1" value=""/>
<input type="hidden" name="parametro" value=""/>
<table width="100%" align="center" border="0">
<tr>
	<td colspan="2" align="left" width="100%">
		<p class="ATitulocolor">Generaci�n de Archivos de Ingresos y Egresos</p>
		<BR>
	</td>
</tr>
<tr>
	<td>
		<table width="80%" align="center" border="0" cellspacing="0">

			<TR>
			    <TD colspan="3" class="Atittabcenazu">&nbsp;Extracci�n de Informaci�n Concentrada de Ingresos y Egresos</TD>
			</TR>
			<tr>
				<td class="AEtiquetaDentro" colspan="3" align="center">&#160;</td>
			</tr>
			<tr class="AEtiquetaDentro">
			<td colspan="3" align="center" class="Atexencabezado">Fecha:&#160;
				<INPUT type="text" name="fecha" tabindex="1" size="11" maxlength="10" class="Atextittab" readonly>
				<A href="javascript:cal1.popup()">
					<IMG name="Calendario" border="0" src="../images/calendario.gif">
				</A>
				</td>
			</tr>
			<tr class="AEtiquetaDentro">
			<td colspan="3" align="center">
					 <br />
			</tr>
			<tr class="AEtiquetaDentro">
			<td>&#160;</td>
				<td width="46%"  align="Left">
					 <p>
					    <label>
					      <input type="radio" name="GrupoExt1" value="0" />
					      Solo informaci�n faltante de Sucursales Contingentes </label>
					    <br />
					    <label>
					      <input type="radio" name="GrupoExt1" value="1" />
					      Archivos completos del d�a seleccionado </label>
					    <br /> <br />
				  </p>
			  </td>
			<td width="27%"><IMG border="0" src="../images/invisible.gif" width="200"></td>
			</tr>

			<tr class="AEtiquetaDentro">
			<td colspan="3" align="center">
				 <select name="GrupoExt2">
     				<option value="-1">Selecciona</option>
     				<option value="SDO">Solo Saldos</option>
				    <option value="MOV">Solo Movimientos</option>
				    <option value="AMB">Saldos / Movimientos</option>
			    </select>
				</td>
			</tr>

			<tr class="AEtiquetaDentro">
				<td colspan="3">&#160;</td>
			</tr>
			<tr class="Atittabcenazu">
				<td colspan="3">&#160;</td>
			</tr>
			<tr>
				<td colspan="3">&#160;<br></td>
			</tr>
			<tr>
				<td align="center" colspan="3"><a href="javascript:fnLimpia();"><IMG border="0"
					name="continuar" src="../images/b_limpiar.gif" ></a>
				&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
				<a href="javascript:fnValida();"><IMG border="0"
					name="limpia" src="../images/b_extraer.gif" ></a>

				</td>
			</tr>

		</table>
		</td>
	</tr>
</table>
<%
	java.text.SimpleDateFormat formato = new java.text.SimpleDateFormat("MM'/'dd'/'yyyy");
%>
<INPUT type="hidden" name="afecha" value="<%=formato.format(new java.util.Date())%>">
</form>
<SCRIPT language="JavaScript">
	var cal1 = new calendario(document.forma.fecha,document.forma.afecha);
</SCRIPT>
</BODY>

</HTML>