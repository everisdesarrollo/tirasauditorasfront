<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%
	String usuario = null;
	String perfil = null;
	String grupos = null;
	usuario = request.getHeader("iv-user");
	grupos = request.getHeader("iv-groups");
	
	if (usuario == null || grupos == null) {
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}	
%>
<HTML>
<HEAD>
<%@ page
language="java"
contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"
%>

<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="../theme/Master.css" rel="stylesheet"
	type="text/css">
<SCRIPT src="../theme/calendario.js"></SCRIPT>
<TITLE>Cambio de usuario y contraseņa</TITLE>
<script>
function fnValida()
{
	if(fnCheca()){
		document.forma.parametro.value ="CC";
		document.forma.submit();
	}
}

function fnCheca(){
	  	var g1=0;

	  	if(document.forma.usr.value != "" )
	  	  {
			if(document.forma.pass.value != "")
			  {
				return true;

			  }else
			  	  {
			  	  	alert("indique la contraseņa");
			  	  	document.forma.pass.focus();
			  	  	return false;
			  	  }

		   }else
		       {
				  alert("Indique el Usuario");
				  document.forma.usr.focus();
				  return false;
			   }

}

</script>
</HEAD>
<BODY >
<FORM name="forma" method="post" action="05ConUsr.jsp">
<input type="hidden" name="oculto" value="3" />
<input type="hidden" name="parametro" value=""/>
<table width="100%" align="center" border="0">
<tr>
	<td colspan="2" align="left" width="100%">
		<p class="ATitulocolor">Cambio de Usuario y Contraseņa</p>
		<BR>
	</td>
</tr>
<tr>
	<td>
		<table width="80%" align="center" border="0" cellspacing="0">

			<TR>
			    <TD colspan="4" class="Atittabcenazu">&nbsp;Cambio de Usuario y Contraseņa</TD>
			</TR>
			<tr>
				<td class="AEtiquetaDentro" colspan="4" align="center">&#160;</td>
			</tr>
			<tr class="AEtiquetaDentro">
			<td colspan="2" width="50%" align="right" class="Atexencabezado">Usuario:&#160;
			</td>
			<td colspan="2" width="50%" align="left"><INPUT type="text" name="usr" size="12" maxlength="10" class="Atextittab" ></td>
			</tr>

			<tr class="AEtiquetaDentro">
			<td colspan="2" width="50%" align="right" class="Atexencabezado">Contraseņa:&#160;

			</td>
			<TD><INPUT type="password" name="pass" size="12" maxlength="8" class="Atextittab" ></TD>
			</tr>

			<tr class="AEtiquetaDentro">
				<td colspan="4">&#160;</td>
			</tr>
			<tr class="Atittabcenazu">
				<td colspan="4">&#160;</td>
			</tr>
			<tr>
				<td colspan="4">&#160;<br></td>
			</tr>
			<tr>
				<td align="center" colspan="4"><a href="javascript:fnValida();"><IMG border="0"
					name="continuar" src="../images/b_enviar.gif" ></a></td>
			</tr>

		</table>
		</td>
	</tr>
</table>

</form>
</BODY>

</HTML>