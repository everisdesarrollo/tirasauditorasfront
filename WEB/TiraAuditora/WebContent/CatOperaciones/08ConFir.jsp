<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%			
	String usuario = null;		
	String perfil = null;		
	String grupos = null;		
	usuario = request.getHeader("iv-user");		
	grupos = request.getHeader("iv-groups");		
			
	if (usuario == null || grupos == null) {		
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}		
%>			

<HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<SCRIPT language="JavaScript" src="../theme/utiljs.js"></SCRIPT>
<SCRIPT src="../theme/validaciones.js"></SCRIPT>
<LINK href="../theme/Master.css" rel="stylesheet" type="text/css">
<TITLE>Confirmación.</TITLE>
<!-- AUTOR: Ing. David Aguilar Gómez.-->
<!-- CREACION: 28/03/2006 -->
<script>
	function fnSi(){
		document.ConFir.action = "08AltOpe.jsp?cveoperacion="+document.ConFir.cveoperacion.value;
		document.ConFir.accion.value = "nuevo";
		document.ConFir.submit();
	}
	function fnNo(){
		document.ConFir.action = "08MtoOpe.jsp";
		document.ConFir.submit();
	}
</script>

</HEAD>
<BODY>
<FORM name="ConFir" method="post" action="#">
<table width="100%" align="center" border="0">
<tr>
	<td colspan="2" align="left" width="100%">
		<p class="ATitulocolor">Mantenimiento al Catálogo de Operaciones</p>
		<BR>
	</td>
</tr>
<tr>
	<td>
		<table width="70%" align="center" border="0" cellspacing="0">
			<tr class="Atittabcenazu">
			    <TD colspan="2">&nbsp;No existe la operacion: '<%=request.getParameter("cveoperacion")%>'</TD>
			</tr>
			<tr>
				<td colspan="2" class="AEtiquetaDentro" align="center">&#160;</td>
			</tr>

			<tr class="AEtiquetaDentro">
				<td colspan="2" align="center" class="Atexencabezado">No existe la clave de Operacion, la quiere dar de alta?</td>
			</tr>
			<tr class="AEtiquetaDentro">
				<td colspan="2">&#160;</td>
			</tr>
			<tr class="Atittabcenazu">
				<td colspan="2">&#160;</td>
			</tr>
		</table>
		<table align="center" width="25%">
			<tr>
				<td align="center"><a href="javascript:fnSi();"><IMG border="0"
					name="si" src="../images/b_si.gif" align="middle"></a>
				</td>
				<td align="center"><a href="javascript:fnNo();"><IMG border="0"
					name="no" src="../images/b_no.gif" align="middle"></a>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<INPUT type="hidden" name="cveoperacion" value="<%=request.getParameter("cveoperacion")%>">
<INPUT type="hidden" name="accion" value="">
</FORM>
<P><BR></P>

</BODY>

</HTML>
