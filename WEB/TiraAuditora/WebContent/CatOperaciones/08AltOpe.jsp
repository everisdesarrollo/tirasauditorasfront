<%			
	String usuario = null;		
	String perfil = null;		
	String grupos = null;		
	usuario = request.getHeader("iv-user");		
	grupos = request.getHeader("iv-groups");		
			
	if (usuario == null || grupos == null) {		
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}		
%>			

<HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="../theme/Master.css" rel="stylesheet" type="text/css">
<TITLE>Alta de Sucursal</TITLE>
<script>

function fnValida()
{
	if(document.AltOpe.cveoperacion.value.length > 0 && document.AltOpe.descripcion.value.length > 0)
		document.AltOpe.submit();
	else
	{
		if(document.AltOpe.cveoperacion.value.length <= 0)
		{
			alert("El campo Clave de Operación no debe ir vacio");
			document.AltOpe.cveoperacion.focus();
		}
		else
			if(document.AltOpe.descripcion.value.length <= 0)
			{
				alert("El campo Descripción no debe ir vacio");
				document.AltOpe.descripcion.focus();
			}
	}
}

function fnFoco(){
	document.AltOpe.descripcion.focus();
}
function limpia()
{
	document.AltOpe.reset();
}
</script>

</HEAD>
<BODY onLoad="javascript:fnFoco();">
<FORM name="AltOpe" method="post" action="../servlet/OperaServlet">
<input type="hidden" name="accion" value="<%=request.getParameter("accion")%>"/>
<table width="100%" align="center" border="0">
<tr>
	<td>
	<%String accion = request.getParameter("accion");
		if(accion.equals("nuevo"))
		{
	%>
		<p class="ATitulocolor">Alta de Operaciones al Sistema Tira Auditora</p><BR>
	<%}
		else
		{
	%>
		<p class="ATitulocolor">Modificación de Operaciones al Sistema Tira Auditora</p><BR>
	<%}
	%>
	</td>
</tr>
<tr>
	<td>
	<table width="85%" align="center" border="0" cellspacing="0">
	<TR class="Atittabcenazu">
	<%if(accion.equals("nuevo"))
		{
	%>
		<td colspan="2">&#160;Alta de Operación</td>
	<%}
		else
		{
	%>
		<td colspan="2">&#160;Modificación de Operación</td>
	<%}
	%>
	</TR>
	<tr class="AEtiquetaDentro">
		<td colspan="2">&#160;</td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td align="right" class="Atexencabezado">Clave de Operación:&#160;&#160;</td>
		<td><input Type="text" name="cveoperacion" class="Atextittab"  size="10" value="<%=request.getParameter("cveoperacion")%>" maxlength="5" <%= accion.equals("nuevo")?"":"readOnly"%>/></td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td align="right" class="Atexencabezado">Descripción:</td>
		<td><input Type="text" name="descripcion" class="Atextittab" size="50" maxlength="60" style="TEXT-TRANSFORM: uppercase"; value="<%=accion.equals("nuevo")?"":request.getParameter("descripcion").trim()%>"/></td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td align="right">Afecta Diario:&#160;&#160;</td>
		<td><input Type="checkbox" name="diario" class="Atextittab" value="1" <%=accion.equals("nuevo")?"":(request.getParameter("diario").equals("S")?"checked":"")%>/></td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td align="right">Afecta Totales:&#160;&#160;</td>
		<td><input Type="checkbox" name="totales" class="Atextittab" value="1" <%= accion.equals("nuevo")?"":(request.getParameter("totales").equals("S")?"checked":"")%>/></td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td align="right">Permite Offline:&#160;&#160;</td>
		<td><input Type="checkbox" name="offline" class="Atextittab" value="1" <%= accion.equals("nuevo")?"":(request.getParameter("offline").equals("S")?"checked":"")%>/></td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td colspan="2">&#160;</td>
	</tr>
	<tr class="ATittabcenazu">
		<td colspan="2">&#160;</td>
	</tr>
	<tr>
		<td colspan="2">&#160;<br></td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<A href="javascript:limpia();">
					<IMG name="btnlimpiar" border="0" src="../images/b_limpiar.gif"></A>
			&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
		<a href="javascript:fnValida();">
			<IMG border="0" name="continuar" src="../images/b_guardar.gif"></a>
		</td>
	</tr>
</table>
</td>
</tr>
</table>

</FORM>
</BODY>
</HTML>
