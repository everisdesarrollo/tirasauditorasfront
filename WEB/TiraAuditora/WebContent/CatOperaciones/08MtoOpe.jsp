<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%			
	String usuario = null;		
	String perfil = null;		
	String grupos = null;		
	usuario = request.getHeader("iv-user");		
	grupos = request.getHeader("iv-groups");		
			
	if (usuario == null || grupos == null) {		
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}		
%>			

<HTML>
<HEAD>
<%@ page
language="java"
contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"
%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<SCRIPT language="JavaScript" src="../theme/utiljs.js"></SCRIPT>
<SCRIPT src="../theme/validaciones.js"></SCRIPT>
<LINK href="../theme/Master.css" rel="stylesheet"
	type="text/css">
<TITLE>Mantenimiento Operaciones</TITLE>
<!-- AUTOR: Ing. David Aguilar G�mez.-->
<!-- CREACION: 28/03/2006 -->
<script>
function fnColoca()
{
	if("<%=request.getParameter("mensaje")%>" != "null")
		alert("<%=request.getParameter("mensaje")%>");
	document.MtoOpe.cveoperacion.focus();
}
var valida_cveoperacion = new Array('cveoperacion','Clave de Operaci�n','A',false,0,5);

function valida(campo,propiedades)
{
	var lMensaje="";
	var lTempMssg=assertField(campo, propiedades);
	if (lTempMssg.length>1)
  	lMensaje+=assertField(campo, propiedades)+"\n";
  if(lMensaje.length>0)
  	return false;
  else
  	return true;
}
function fnValida()
{
	if(valida(document.MtoOpe.cveoperacion,valida_cveoperacion)){
		if(document.MtoOpe.cveoperacion.value.length > 0)
			document.MtoOpe.submit();
		else{
			alert(" Debe ingresar un Clave de Operaci�n ");
			document.MtoOpe.cveoperacion.focus();
		}
	}
}
function limpia()
{
	document.MtoOpe.reset();
}
</script>
</HEAD>
<BODY onLoad = fnColoca();>
<FORM name="MtoOpe" method="post" action="../servlet/OperaServlet">
<input type="hidden" name="accion" value="buscar"/>
<table width="100%" align="left" border="0">
<tr>
		<td colspan="2" align="left" width="100%">
		<p class="ATitulocolor">Mantenimiento de Operaciones</p><BR>
		</td>
</tr>
<tr>
	<td>
	<table width="80%" align="center" border="0" cellspacing="0">
	<tr class="Atittabcenazu">
		<td colspan="3">&#160;Criterio de Busqueda</td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td colspan="3">&#160;</td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td align="right" class="Atexencabezado">Clave Operaci�n:&#160;&#160;&#160;&#160;</td>
		<td><input Type="text" name="cveoperacion" value="" class="Atextittab" maxlength="5" onBlur="javascript:valida(this,valida_cveoperacion);" /></td>
	</tr>
	<tr class="AEtiquetaDentro">
		<td colspan="3">&#160;</td>
	</tr>
	<tr class="Atittabcenazu">
		<td colspan="3">&#160;</td>
	</tr>
	<tr>
		<td colspan="3">&#160;<br></td>
	</tr>
	<tr>
		<td align="center" colspan="3">
			<A href="javascript:limpia();">
					<IMG name="btnlimpiar" border="0" src="../images/b_limpiar.gif"></A>
			&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
			<A href="javascript:fnValida(); ">
				<IMG border="0" name="continuar" src="../images/b_buscar.gif"></A>
		</td>
	</tr>
</table>
</td>
</tr>
</table>

</FORM>

</BODY>
</HTML>