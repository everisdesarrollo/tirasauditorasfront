<%			
	String usuario = null;		
	String perfil = null;		
	String grupos = null;		
	usuario = request.getHeader("iv-user");		
	grupos = request.getHeader("iv-groups");		
			
	if (usuario == null || grupos == null) {		
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}		
%>			

<HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK rel="stylesheet" href="../theme/Master.css" type="text/css">
<TITLE>Mantenimiento al Catalogo de Operaciones</TITLE>
<!-- AUTOR: Ing. David Aguilar G�mez.-->
<!-- CREACION: 28/03/2006 -->

<script>
function fnEliminar()
  {
	    if(confirmSubmit())
	      {
	        document.ResOpe.accion.value = "borrar";
	        document.ResOpe.action = "../servlet/OperaServlet";
	        document.ResOpe.submit();
	      }
  }

function confirmSubmit()
  {
    var agree=confirm("�Esta seguro que quiere eliminar el registro?");
    if (agree)
      return true;
    else
	  	return false;
  }

function fnModificar()
{
	document.ResOpe.accion.value = "modificar";
	document.ResOpe.submit();
}
</script>
</HEAD>
<BODY>
<form name="ResOpe" method="post" action="08AltOpe.jsp">
<input type="hidden" name="cveoperacion" value="<%=request.getParameter("cveoperacion")%>"/>
<input type="hidden" name="descripcion" value="<%=request.getParameter("descripcion")%>"/>
<input type="hidden" name="diario" value="<%=request.getParameter("diario")%>"/>
<input type="hidden" name="totales" value="<%=request.getParameter("totales")%>"/>
<input type="hidden" name="offline" value="<%=request.getParameter("offline")%>"/>
<input type="hidden" name="accion" value=""/>
<h1>Mantenimiento del Catalogo de Operaciones</h1>
<TABLE align="center" border="0" width="90%">
	<TR class="ALigaonce">
		<TD align="left" class="ALigaonce" >&#160;&#160;1 de 1</TD>
		<TD colspan="4" width="80%" class = "Atexnegro" align="right">&#160;Busqueda por la clave "<%=request.getParameter("cveoperacion")%>"</TD>
	</TR>
	<tr class="Atittabcenazu">
		<TD align="center" width="15%" >Clave Operaci�n</TD>
		<TD align="center" width="40%" >Descripci�n</TD>
		<TD align="center" width="15%" >Afecta Diario</TD>
		<TD align="center" width="15%" >Afecta Totales</TD>
		<TD align="center" width="15%" >Permite Offline</TD>
	</tr>
		<TR>
		<TD align="center"><%= request.getParameter("cveoperacion") %></TD>
		<TD align="left"><%= request.getParameter("descripcion") %></TD>
		<TD align="center"><%= request.getParameter("diario")%></TD>
		<TD align="center"><%= request.getParameter("totales")%></TD>
		<TD align="center"><%= request.getParameter("offline")%></TD>
	</TR>
	<TR>
		<TD colspan="5" align="center">&#160;</TD>
	</TR>
</TABLE>
</form>
<CENTER><TABLE width="60%">
<TBODY><TR>
		<TD align="center" colspan="5">
			<A href="javascript:fnModificar();"><IMG border="0" src="../images/b_modificar.gif" ></A>
			&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
			<A href="javascript:fnEliminar();"><IMG border="0" src="../images/b_eliminar.gif" ></A>
		</TD>
	</TR>
</TBODY></TABLE></CENTER>
</BODY>
</HTML>