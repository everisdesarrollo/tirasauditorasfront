
<%@ page import="java.util.*"%>
<%@ page import="com.santander.scheduler.*"%>
<%@ page import="com.santander.autentificacion.LogueoValue"  %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%
	String usuario = null;
	String perfil = null;
	String grupos = null;
	usuario = request.getHeader("iv-user");
	grupos = request.getHeader("iv-groups");
	
	if (usuario == null || grupos == null) {
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
	}	
%>
<HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="../theme/Master.css" rel="stylesheet" type="text/css">
<SCRIPT language="JavaScript" src="../theme/utiljs.js"></SCRIPT>
<SCRIPT src="../theme/calendario.js"></SCRIPT>
<TITLE>Tira Auditora</TITLE>
<SCRIPT language="Javascript">
	//Funci�n para realizar la paginaci�n
 	function muevePag(pag,pagant)
	{
		window.location.href="14ConSch.jsp?pagina="+pag+"&pagant="+pagant;
	}

	//Funci�n para habilitar
	function fnEnableJob(){
		setActionJob("H", "habilitar");
	}

	//Funci�n para borrar job
	function fnDeleteJob(){
		setActionJob("E", "eliminar");
	}

	//Funcion de seleccion
	function setActionJob(action, textAction){
		var objCheckBoxes = document.frmMonJob.elements["selected"];
		if(!objCheckBoxes)
			return;
		var total="";
		for(var i=0; i < objCheckBoxes.length; i++){
			if(objCheckBoxes[i].checked)
				total += objCheckBoxes[i].value;
		}
		if(total!=""){
			if(confirm("�Desea " + textAction + " los Jobs seleccionados?")){
				document.frmMonJob.seleccion.value = action;
				document.frmMonJob.submit();
			}
		}else {
	    	alert("Debes seleccionar por lo menos un registro");
	    }
	}

	//Funcion para seleccionar todos los checkboxes
	function SetAllCheckBoxes(){
		var objCheckBoxes = document.frmMonJob.elements["selected"];
		if(!objCheckBoxes)
			return;
		var countCheckBoxes = objCheckBoxes.length;
		if(!countCheckBoxes)
			return;
		else
			for(var i = 0; i < countCheckBoxes; i++)
				objCheckBoxes[i].checked = document.frmMonJob.all.checked;
	}

</SCRIPT>
</HEAD>
<BODY>
<FORM id="frmMonJob" name="frmMonJob" method="post"	action="../servlet/SchedulerServlet">
<TABLE width="100%" border="0">
	<TR>
		<TD>
		<P class="ATitulocolor">Consulta de Scheduler Jobs</P>
<%
		/** Rutina que realiza la Consulta de Scheduler Jobs
		 */
		long oneDay = 1000 * 60 * 60 * 24;
		Date currentDay = new Date();
		java.text.SimpleDateFormat formato = new java.text.SimpleDateFormat("MM/dd/yyyy");
		java.text.SimpleDateFormat formato2 = new java.text.SimpleDateFormat("dd/MM/yyyy");
		String tomorrow = formato.format(new Date(currentDay.getTime()+oneDay));
		try {
			int nopagina = Integer.parseInt(request.getParameter("pagina") != null ?
							request.getParameter("pagina")
							: "0");
			int contador = 0;
			HttpSession sesion = request.getSession();
			String permiso = (String) sesion.getAttribute("permiso");
			ArrayList total = (ArrayList) sesion.getAttribute("schedulerJobs");
			ArrayList paginaResult = new ArrayList();
			int opcion = 0;
			if (permiso != null && permiso.equals("sin permisos")) {
				opcion = -1;
			}else if (total != null && total.size() > 0 && total.get(0).getClass().equals(ArrayList.class) && ((ArrayList)total.get(0)).size() > 0) {
				paginaResult = (ArrayList) total.get(nopagina);
				contador = (total.size() * 100) - 100 + ((ArrayList)total.get(total.size()-1)).size();
				System.out.println("TAUD Lista de resultados de Scheduler Jobs (" + paginaResult.size() + ")");
			} else {
				opcion = 1;
			}
%>
			<TABLE width="90%" border="0" align="center" cellpadding="0" cellspacing="1">
				<TR>
					<TD colspan="6">
					<P><font color="gray">&nbsp;<%=sesion.getAttribute("message")%></font></P>
					</TD>
				</TR>
				<TR>
					<TD class="ALigaonce">
<%
			if (nopagina != 0) {
%>
					<A href="javascript:muevePag('<%=nopagina - 1%>','<%=nopagina%>');" class="txt">&lt;&lt;</A>
<%
			}
%>
					<%=(100 * nopagina) + 1%> a <%=(100 * nopagina) + paginaResult.size() + opcion%> de <%=contador%>
<%
			if (opcion == 0 && nopagina != total.size() - 1) {
%>
					 <A href="javascript:muevePag('<%=nopagina + 1%>','<%=nopagina%>');" class="txt">&gt;&gt;</A> <%
	 		}
%>
					</TD>

					<TD colspan="6" class="Atexnegro" >
						Tipo:
						<select name="typeJob">
							<option value = "Pendientes">Seleccione...</option>
							<option value = "Pendientes">Pendientes</option>
							<option value = "Ejecutados">Ejecutados</option>
						</select>
						&nbsp;&nbsp;&nbsp;&nbsp;
						Fecha:
						<INPUT type="text" name="fecha" tabindex="1" size="11" maxlength="10" class="Atextittab" readonly>
						<A href="javascript:cal1.popup()"> <IMG name="Calendario" border="0" src="../images/calendario.gif"></A>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="submit" value="Buscar" onclick="javascript:setFecha();">
					</TD>
				</TR>

<%
			if (opcion == 0) {
%>
				<TR>
					<TD class="ATituloTabla">NOMBRE_JOB</TD>
					<TD class="ATituloTabla">FECHA_EJECUCION</TD>
					<TD class="ATituloTabla">ESTATUS</TD>
<%
				if(paginaResult.size() > 0){
					SchedulerValue regSch = (SchedulerValue) paginaResult.get(0);
					if(regSch.getRunDuration() != null){
%>
					<TD class="ATituloTabla">DETALLE_EJECUCION</TD>
					<TD class="ATituloTabla">DURACION</TD>
<%
					} else {
%>
					<TD class="ATituloTabla">NUMERO_EJECUCION</TD>
					<TD class="ATituloTabla">Todos<input type="checkbox" name="all" value="" onclick="javascript:SetAllCheckBoxes();"></TD>
<%
					}
				}
%>
				</TR>
<%
				for (int i = 0; i < paginaResult.size(); i++) {
					SchedulerValue regSch = (SchedulerValue) paginaResult.get(i);
%>
				<TR<%if (i % 2 != 0){ %> class="fondo"<%} %>>

					<TD align="center"><%=regSch.getJobName()%></TD>
					<TD align="center"><%=regSch.getActualStartDate()%></TD>
					<TD align="center"><%=regSch.getStatus()%></TD>
					<TD align="center"><%=regSch.getAdditionalInfo()%></TD>
<%
					if(regSch.getRunDuration() != null){
%>
					<TD align="center"><%=regSch.getRunDuration()%></TD>
<%
					} else {
%>
					<TD align="center"><input type="checkbox" name="selected" value="<%=i%>"></TD>
<%
					}
				}
%>
				</TR>
<%
			} else if (opcion == 1){
%>
				<TR>
					<TD colspan="6" align="center"><BR><BR><b><%=sesion.getAttribute("log")%></b></TD>
				</TR>
<%
			} else if (opcion == -1){
%>
				<SCRIPT>
					alert("Usuario sin permisos para esta funcionalidad");
					document.frmMonJob.action = '../Menu/01Tira.jsp';
					document.frmMonJob.submit();
				</SCRIPT>
<%
			}
%>
				<TR>
					<TD colspan="5">
					<P>&nbsp;</P>
					</TD>
				</TR>
				<TABLE>
					<TR>
						<TD align="center" width="53%">&nbsp;</TD>
<%
			if (opcion != 1) {
				if(permiso.equals("con botones")){
%>
						<TD align="center"><A href="javascript:fnEnableJob();"><IMG
							border="0" name="eliminar" src="../images/b_habilitar.gif"
							align="left"> </A>
						</TD>
						<TD align="center" width="10">&nbsp;</TD>
						<TD align="center"><A href="javascript:fnDeleteJob();"><IMG
							border="0" name="eliminar" src="../images/b_eliminar.gif"
							align="left"> </A>
						</TD>
					</TR>
<%
				}
			}
		} catch (Exception ex) {
			System.out.println("Error en Consulta de Sucursales Schedulers/14ConSch.jsp: " + ex.getMessage());
		}
%>
				</TABLE>
			</TABLE>
		</TD>
	</TR>
</TABLE>

<INPUT type="hidden" name="seleccion" value="read">
<INPUT type="hidden" name="pagina" value="<%=request.getParameter("pagina")!=null?request.getParameter("pagina"):"0"%>">
<INPUT type="hidden" name="pagant" value="<%=request.getParameter("pagant")%>">
<INPUT type="hidden" name="afecha" value="<%=tomorrow%>">
</FORM>
<SCRIPT language="JavaScript">
	var cal1 = new calendario(document.frmMonJob.fecha,document.frmMonJob.afecha);
	function setFecha(){
		if(document.frmMonJob.fecha.value.length != 10){
			document.frmMonJob.afecha.value = "<%=formato2.format(new java.util.Date())%>";
		} else {
			document.frmMonJob.afecha.value = document.frmMonJob.fecha.value;
		}
	}
</SCRIPT>
</BODY>
</HTML>