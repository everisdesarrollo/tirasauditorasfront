



import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.santander.autentificacion.LogueoClase;
import com.santander.autentificacion.LogueoValue;



/**
 *
 * @author ORO01083
 * @version
 */
public class LogInSAM extends HttpServlet {

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 */
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		System.out.println("**************LogInSam.java**********************");
		HttpSession session = request.getSession(true);
		String usuario = null;
		String perfil = null;
		String grupos = null;
		usuario = request.getHeader("iv-user");
		grupos = request.getHeader("iv-groups");
		if (grupos != null){
			perfil = getPerfil(grupos);
		}
		System.out.println("Usuario ingresado: " + usuario);
		System.out.println("Perfil ingresado: " + perfil);

		if (perfil != null) {
			LogueoClase loginiClase = new LogueoClase();
			ArrayList<LogueoValue> resul = loginiClase.consultaLogueo(usuario,perfil);
			session.setAttribute("consulogueo",resul);
			session.setAttribute("autentifica","0");
			response.sendRedirect("/TiraAuditora/Menu/01Tira.jsp");
		} else if (session.getAttribute("desarrollo") != null){
			LogueoClase loginiClase = new LogueoClase();
			ArrayList<LogueoValue> resul = loginiClase.consultaLogueo("TESTUSER","TA_TES");
			session.setAttribute("consulogueo",resul);
			session.setAttribute("autentifica","0");
			response.sendRedirect("/TiraAuditora/Menu/01Tira.jsp");
		} else {
			System.out.println("Se redirecciona a: LogOutSAM");
			response.sendRedirect("/TiraAuditora/servlet/LogOutSAM");
		}
	}

	// <editor-fold defaultstate="collapsed"
	// desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 */
	public String getServletInfo() {
		return "Short description";
	}
	/**
	 * Regresa el perfil de Tiras Auditoras de los grupos de SAM
	 * @param grupos
	 * @return
	 */
	private String getPerfil(String grupos){
		String perfil = "TA";
		String grupoTAUD = "grp_tirasaudi";
		StringTokenizer token = new StringTokenizer(grupos, ",");
		while(token.hasMoreElements()){
			String grupo = token.nextToken();
			int index = grupo.indexOf(grupoTAUD);
			if(index != -1){
				perfil += grupo.substring(index + grupoTAUD.length(), grupo.length() - 1).toUpperCase();
			}
		}
		return perfil;
	}
	// </editor-fold>
}