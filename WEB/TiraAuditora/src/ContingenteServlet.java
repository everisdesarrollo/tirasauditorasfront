/** Banco Santander Mexicano
*   Clase ContingenteServlet  Servlet
*   @author Alfredo Resendiz Vargas
*   @version 1.0
*   Fecha de creacion : 2 de Febrero del 2006
*   Responsable : Eloisa Hernandez H
*   Descripcion : Realiza la funci�n de control de E/S para la consulta de
* 				  Sucursales Contingentes, as� como la del PDF
*   Modificacion :
*/

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.santander.contingente.*;

public class ContingenteServlet extends HttpServlet
{
/**
* @see M�todo vac�o pero necesario
*/
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		doGeneral(req, resp);
	}

/**
* @see M�todo vac�o pero necesario
*/
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		doGeneral(req, resp);
	}

/**
* @see Realiza las conexiones necesarias para para el control del proceso
*/

	protected void doGeneral(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		ContingenteClase contingenteClase = new ContingenteClase();
		HttpSession sesion = req.getSession();
		String accion = req.getParameter("accion");
		System.out.println("accion = " + accion);
		sesion.setAttribute("message","");
		if(accion==null)
		{
			ArrayList resul = null;
			if(req.getParameter("seleccion")==null){
				int seleccion = 1;
				if(seleccion==1){
				  resul = contingenteClase.consultaContingente();
				}
			}
			sesion.setAttribute("consulctg",resul);
			sesion.setAttribute("message","Resultado de la b�squeda del d�a h�bil anterior a la fecha actual");
			resp.sendRedirect("/TiraAuditora/Contingentes/04ConCtg.jsp");
			accion = "";

		} else if(accion.equals("bitacora")){
			String estatus = req.getParameter("descSistema")==null?"%":req.getParameter("descSistema");
			CargaDiariaClase cargaDiaria = new CargaDiariaClase();
			ArrayList<ArrayList<CargaDiariaValue>> bitacoraResult = cargaDiaria.bitacoraCargaDiaria(estatus);
			sesion.setAttribute("bitacoraResult", bitacoraResult);
			resp.sendRedirect("/TiraAuditora/Contingentes/04BitCar.jsp");

		} else if(accion.equals("read")){
			String fecha = req.getParameter("afecha");
			String error = req.getParameter("descSistema");
			ArrayList<ArrayList<ContingenteValue>> resul = null;
			String message = "Resultado de la b�squeda ";
			if(fecha.equals("ALL")){
				resul = contingenteClase.consultaContingente(error);
				if(!error.equals("%")){
					message += " con error: " + error;
				}
			} else {
				resul = contingenteClase.consultaContingente(fecha, error);
				message += "del d�a " + fecha;
				if(!error.equals("%")){
					message += " con error: " + error;
				}
			}
			sesion.setAttribute("consulctg",resul);
			sesion.setAttribute("message",message);
			resp.sendRedirect("/TiraAuditora/Contingentes/04ConCtg.jsp");

		} else if(accion.equals("imprimir")){
			ArrayList total = (ArrayList)sesion.getAttribute("consulctg");
			if(req.getParameter("pagina")!=null)
			{
			  int pag = Integer.parseInt(req.getParameter("pagina"));
			  ArrayList datpag = (ArrayList)total.get(pag);
			  for(int j=0;j<datpag.size();j++)
			  {
				  ContingenteValue contingente = (ContingenteValue)datpag.get(j);
				  if(req.getParameter("sele"+j)!=null)
					  contingente.set_impresion(1);
				  else
					  contingente.set_impresion(0);
			  }
			  datpag = null;
			}
			ArrayList impresion = new ArrayList();
			ArrayList todos = new ArrayList();
			for(int i=0;i<total.size();i++)
			{
				ArrayList pag = (ArrayList)total.get(i);
				for(int j=0;j<pag.size();j++)
				{
					ContingenteValue reg = (ContingenteValue)pag.get(j);
					todos.add(reg);
					if(reg.get_impresion()==1)
					{
						impresion.add(reg);
					}
				}
			}
			if(impresion.size()==0)
			{
				impresion = todos;
				todos = null;
			}
			System.out.println("Total a Imprimir: "+impresion.size());

			ByteArrayOutputStream baos = contingenteClase.makePdf(impresion);
			// setting some response headers
			resp.setHeader("Expires", "0");
			resp.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
			resp.setHeader("Pragma", "public");
			// setting the content type
			resp.setContentType("application/pdf");
			// the contentlength is needed for MSIE!!!
			resp.setContentLength(baos.size());
			// write ByteArrayOutputStream to the ServletOutputStream
			ServletOutputStream out = resp.getOutputStream();
			baos.writeTo(out);
			out.flush();
		}
	}
}