/** Banco Santander Mexicano
*   Clase SucursalServlet  controlador de casos para las Sucursales
*   @author Angel Gabriel Ramirez Alva
*   @version 1.0
*   fecha de creacion : 2 de Febrero del 2006
*   Responsable : Eloisa Hernandez Hernandez
*   @param sVec Vector con la informaci�n para todos los casos
*   Descripci�n : Conecta los diferentes casos con su busqueda en bd en la claseSucursal
*   modificacion : 24 de Mayo 2006
*/
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.santander.ingresoegreso.DesEnc;
import com.santander.ingresoegreso.LlamaProc;
import com.santander.ingresoegreso.execShellSSH;
import com.santander.utilerias.LoadProperties;
/**
 * @version 	1.0
 * @author
 */


public class IngresoEgresoServlet extends HttpServlet {
	private LoadProperties rb = LoadProperties.getInstance();

/***********************************************/
  public void service(HttpServletRequest request, HttpServletResponse response) throws
	  ServletException, IOException
    {
	  System.out.println("Esta en el metodo SERVICE  IngresoEgresoServlet");
	  try
	    {
		DesEnc myencriptador = new DesEnc();
		//Telnet mytelnet = new Telnet();
		LlamaProc callmetodo = new LlamaProc();
		DesEnc cambia = new DesEnc();
		HttpSession sesion=request.getSession();
		execShellSSH ejecutaShell = new execShellSSH();

		ObjectInputStream in 	= null;
		ObjectOutputStream out  = null;

		int imetodo 		= 0;
		String valor		= "";
		Vector svec = new Vector();

		//Para obtener los par�metros
		in = new ObjectInputStream(request.getInputStream());
 		//Para regresar el resultado
		out = new ObjectOutputStream(response.getOutputStream());

  		//Obtiene la accion a realizar
  	valor = (String) in.readObject();
		String limit = "|";
		StringTokenizer strtoken_value= new StringTokenizer(valor, limit );
		while(strtoken_value.hasMoreElements())
		  {
			String tmpvar= (strtoken_value.nextToken()).trim();
			svec.add(tmpvar);
			System.out.println(tmpvar);
		  }

		System.out.println("***el valor de cadena es:" + valor);
		System.out.println(svec);
		String val = (String)svec.elementAt(0);
		imetodo = Integer.parseInt(val);
		System.out.println("***el valor del M�todo: " + imetodo);
		switch(imetodo)
		  {
      		/* Llama store procedure en base a tipo de operacion contingente o normal
      		 * Despues ejecuta un shell para enpaquetar
      		 * @param sNumsuc n�mero de sucursal
      		 */
      	  case 1:
			try
			  {
				System.out.println("Ingresa al case 1 de Ingresos y egresos: Llama store procedure, realiza desEncripciones y copia los archivos para su descarga para Contingentes");
				String sfecha = (String)svec.elementAt(1);
				String sstore = (String)svec.elementAt(2);
				String stipmov = (String)svec.elementAt(3);
				char stipomov = stipmov.charAt(0);

				String usr = rb.getValue("ie.loco");
				//String servidor = rb.getString("ie.host");
				//servidor = myencriptador.DesEncriptarCadena(servidor);
				//servidor =(servidor.substring(0,servidor.length()-1));
				usr = myencriptador.DesEncriptarCadena(usr);
				//System.out.println("ie.loco desenc: ");
				//System.out.println("ie.host desenc: ");

				String tmpFec = usr.substring(0,2);
				tmpFec+="/"+ usr.substring(2,4);
				tmpFec+="/"+ usr.substring(4,8);
				System.out.println("tmpFec: "+tmpFec);
				callmetodo.llamaStor1(stipmov, sfecha);
				System.out.println("Procedimientos ejecutados con exito");
				String nfecha = sfecha.substring(0,2);
				nfecha += sfecha.substring(3,5);
				nfecha += sfecha.substring(6,10);

				String sh = "Zipea_ArchsContingentes.sh "+nfecha;
				String scmd = rb.getValue("ie.cmd");
				System.out.println("cmd "+scmd);
				System.out.println("antes de ir a base: por la clave ");

				usr = callmetodo.sBuscaClave(tmpFec);
				usr =(usr.substring(0,usr.length()-1));
				usr = myencriptador.DesEncriptarCadena(usr);
				System.out.println("clave: ");
				String musu = callmetodo.sBuscaClave2(tmpFec);
				System.out.println("que traje a musu: ");
				musu =(musu.substring(0,musu.length()-1));
				System.out.println("musu: ");
				musu = myencriptador.DesEncriptarCadena(musu);

				System.out.println("despues de desencriptar: al usuario musu: ");

				//System.out.println("Case hace la conexion telnet + fecha: "+nfecha);
				//mytelnet.connect(servidor, 23);

				//String lr = mytelnet.loginResult(musu,usr);
				//System.out.println("Resultado lr = "+lr);
				//String ex = mytelnet.senda(scmd);
				//ex = mytelnet.senda2(sh, "TAUD");
				System.out.println("Case hace la ejecucion del Shell + fecha: "+nfecha);
				String ex = ejecutaShell.processCmd(rb.getValue("ie.cmdShell") + " " + nfecha);
				System.out.println("Respuesta: "+ex);
				//mytelnet.disconnect();

				System.out.println("****************************");
				String sarch1 = "MovimientosContingentes" + nfecha + ".tar.gz";
				String sarch2 = "SaldosContingentes"+nfecha+".tar.gz";
				String sdatos = "";

				String ruta_descarga = rb.getValue("ie.ruta_desc");
				/***********Trae el o los archivos segun sea el caso ***********/
				switch(stipomov)
				  {
				  	case 'M':
					  System.out.println("dentro del equals MOV");
					  sdatos = ruta_descarga+sarch1;
					break;

					case 'S':
					  sdatos = ruta_descarga+sarch2;
					break;

					case 'A':
					  sdatos = ruta_descarga+sarch1+"|"+ruta_descarga+sarch2;
					break;

					default:
					  // En esta opcion nunca entrara.
					  System.out.println("No es selecci�n valida.");
					break;
				  }
				/******************************************************************/
				sdatos = "0|"+sdatos+"|";
				if(ex.equals("1"))
					sdatos="EMPTY";
				System.out.println("sDatos: "+sdatos);
				out.writeObject(sdatos);
			  }
			  catch (Exception e8)
			    {
				  System.out.println ("Error en el servlet IngresoEgreso case 1: " + e8.getMessage());
				  String sdatos = "1";
				  out.writeObject(sdatos);
				  throw new ServletException (e8.getMessage());
				}
		  break;
		  case 2:
			try
			  {
				System.out.println("Ingresa al case 2 de Ingresos y egresos: Llama store procedure, realiza desEncripciones y copia los archivos para su descarga para Diarios");
			  String sfecha = (String)svec.elementAt(1);
				String sstore = (String)svec.elementAt(2);
				String stipmov = (String)svec.elementAt(3);
				char stipomov = stipmov.charAt(0);

				System.out.println("fecha enviada al store: "+sfecha);
				callmetodo.llamaStor2(stipmov, sfecha);
				System.out.println("Procedimientos ejecutados con exito");
				String nfecha = sfecha.substring(0,2);
				nfecha += sfecha.substring(3,5);
				nfecha += sfecha.substring(6,10);
				String usr = rb.getValue("ie.loco");

				System.out.println("fecha enviada al Zipea_ArchsDiarios: "+nfecha);
				String sh = "Zipea_ArchsDiarios.sh "+nfecha;
				String scmd = rb.getValue("ie.cmd");
				//System.out.println("Case hace la conexion telnet");
				//String servidor = rb.getString("ie.host");
				//servidor = myencriptador.DesEncriptarCadena(servidor);
				//servidor =(servidor.substring(0,servidor.length()-1));
				usr = myencriptador.DesEncriptarCadena(usr);
				String tmpFec = usr.substring(0,2);
				tmpFec+="/"+ usr.substring(2,4);
				tmpFec+="/"+ usr.substring(4,8);

				System.out.println("antes de ir a base: por clave");
				usr = callmetodo.sBuscaClave(tmpFec);
				usr =(usr.substring(0,usr.length()-1));
				System.out.println("despues de ir a base: regreso con clave");
				usr = myencriptador.DesEncriptarCadena(usr);
				System.out.println("despues de desencriptar: la clave ");

				String musu = callmetodo.sBuscaClave2(tmpFec);
				System.out.println("que traje a musu: ");
				musu =(musu.substring(0,musu.length()-1));
				musu = myencriptador.DesEncriptarCadena(musu);

				//System.out.println("envio este dato al servidor: ");
				//mytelnet.connect(servidor, 23);
				//String lr = mytelnet.loginResult(musu,usr);
				//System.out.println("Res lr = "+lr);
				//String ex = mytelnet.senda(scmd);
				//ex = mytelnet.senda2(sh, "TAUD");
				System.out.println("Case hace la ejecucion del Shell + fecha: "+nfecha);
				String ex = ejecutaShell.processCmd(rb.getValue("ie.cmdShell2") + " " + nfecha);
				System.out.println("Respuesta: "+ex);
				System.out.println("****************************");
				//mytelnet.disconnect();
				System.out.println("****************************");
				String sarch1 = "MovimientosDiarios" + nfecha + ".tar.gz";
				String sarch2 = "SaldosDiarios"+nfecha+".tar.gz";
				String sdatos = "";
				String srespuesta = "";
				String ruta_descarga = rb.getValue("ie.ruta_desc");
				/***********Trae el o los archivos segun sea el caso ***********/

				switch(stipomov)
				  {
					case 'M':
					  System.out.println("dentro del equals MOV");
					  sdatos = ruta_descarga+sarch1;
					break;

					case 'S':
					  sdatos = ruta_descarga+sarch2;
					break;

					case 'A':
					  sdatos = ruta_descarga+sarch1+"|"+ruta_descarga+sarch2;
					break;

					default:
					  // En esta opcion nunca entrara.
					  System.out.println("No es selecci�n valida.");
					break;
				  }

				/******************************************************************/
				sdatos = "0|"+sdatos+"|";
				if(ex.equals("1"))
					sdatos="EMPTY";
				out.writeObject(sdatos);

			  }
			catch (Exception e8)
			  {
				System.out.println ("Error en el servlet IngresoEgreso case 2: " + e8.getMessage());
				String sdatos = "1|";
				out.writeObject(sdatos);
				throw new ServletException (e8.getMessage());
			  }
			break;
			case 3:
				try
				  {
					String sdatos = "1";
					String usr = (String)svec.elementAt(1);
					String pass = (String)svec.elementAt(2);

		/*************************************************************************************/
					sdatos = callmetodo.sModificaClave(svec);
					out.writeObject(sdatos);
				  }
				  catch (Exception e8)
					{
					  System.out.println ("Error en el servlet IngresoEgreso case 3: " + e8.getMessage());
					  String sdatos = "1";
					  out.writeObject(sdatos);
					  throw new ServletException (e8.getMessage());
				}
			break;
			case 4:
				try
				  {
					String sdatos = "0";
					String sdatos2 = "0";
					String sdatos3 = "0";
					String usr = (String)svec.elementAt(1);

		/*************************************************************************************/
					sdatos = cambia.EncriptarCadena(usr);
					sdatos2= cambia.DesEncriptarCadena(sdatos);
					sdatos3 = sdatos+"@"+sdatos2;
					out.writeObject(sdatos3);
				  }
				  catch (Exception e8)
					{
					  System.out.println ("Error en el servlet IngresoEgreso case 3: " + e8.getMessage());
					  String sdatos = "1";
					  out.writeObject(sdatos);
					  throw new ServletException (e8.getMessage());
				}
			break;
			default:
			  // En esta opcion nunca entrara.
			  System.out.println("No es selecci�n valida.");
			  String sdatos = "1";
			  out.writeObject(sdatos);
			break;
}//Cierra el Switch

		} //Cierra TRY PRINCIPAL
		catch (Exception eg) {
			String sdatos = "1";
			ObjectOutputStream out = new ObjectOutputStream(response.getOutputStream());
			out.writeObject(sdatos);
			 	 System.out.println("Error en IngresoEgresoServlet::service " + eg.getMessage());
		}

	  } //Cierra service


	/**
	* @see javax.servlet.http.HttpServlet#void (javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	*/
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {

	}

	/**
	* @see javax.servlet.http.HttpServlet#void (javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	*/
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {

	}


}//End Servlet Sucursal