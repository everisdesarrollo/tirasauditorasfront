/** Banco Santander Mexicano
*   Clase SucursalServlet  controlador de casos para las Sucursales
*   @author Angel Gabriel Ramirez Alva
*   @version 1.0
*   fecha de creacion : 2 de Febrero del 2006
*   Responsable : Eloisa Hernandez Hernandez
*   @param sVec Vector con la información para todos los casos
*   Descripción : Conecta los diferentes casos con su busqueda en bd en la claseSucursal
*   modificacion :
*/
import java.io.*;
import java.io.IOException;
import java.util.*;
import java.util.Properties;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.naming.InitialContext;
import javax.rmi.PortableRemoteObject;
import javax.servlet.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.santander.utilerias.*;
import com.santander.sucursal.*;
/**
 * @version 	1.0
 * @author
 */


public class SucursalServlet extends HttpServlet {

/***********************************************/
	public void service(HttpServletRequest request, HttpServletResponse response) throws
		  ServletException, IOException {

		System.out.println("Esta en el metodo SERVICE  sucursalServlet");

	/*************************************************************************/
try{
	SucursalClase myMetodo =new SucursalClase();
	HttpSession sesion=request.getSession();

	ObjectInputStream in 	= null;
	ObjectOutputStream out  = null;

		int iMetodo 		= 0;
		String valor		= "";
		Vector sVec = new Vector();

		//Para obtener los parámetros
		  in = new ObjectInputStream(request.getInputStream());
 		//Para regresar el resultado
		  out = new ObjectOutputStream(response.getOutputStream());

  //Obtiene la accion a realizar

  		valor = (String) in.readObject();
  		String limit = "|";
		StringTokenizer strToken_value= new StringTokenizer(valor, limit );
		while(strToken_value.hasMoreElements())
			{
				String tmpVar= (strToken_value.nextToken()).trim();
				sVec.add(tmpVar);
				System.out.println(tmpVar);
			}

		System.out.println("***el valor de cadena es:" + valor);
		System.out.println(sVec);
		String val = (String)sVec.elementAt(0);
		iMetodo = Integer.parseInt(val);
  		System.out.println("***el valor del Método: " + iMetodo);
		switch(iMetodo)
		{
      		/* Consulta Sucursales dadas de alta en la tira auditora
      		 * en base al parametro Numero de Sucursal
      		 * @param sNumsuc número de sucursal
      		 */
      		case 1:
						try{
							System.out.println("Ingresa al Case 1 que Consulta Sucursales dadas de alta en la tira auditora");
							String sNumsuc = (String)sVec.elementAt(1);
							String sDatos = myMetodo.sSucursal(sNumsuc);
							System.out.println(" Todos los Datos recibidos");
							//System.out.println(sDatos);
							out.writeObject(sDatos);
						}catch (Exception e8){
								System.out.println ("Error en el servlet Sucursal case 1: " + e8.getMessage());
								throw new ServletException (e8.getMessage());
						}
				break;
			case 2: // Consulta Sucursales dadas de alta en la tira auditora
				try{
					System.out.println("Ingresa al Case 2 que consulta por nombre de servidor");
					String sServidor = (String)sVec.elementAt(1);
					String sDatos = myMetodo.sSucursal2(sServidor);
					System.out.println(" Todos los Datos recibidos");
					System.out.println(sDatos);
					out.writeObject(sDatos);
					}catch (Exception e8){
						System.out.println ("Error en el servlet Sucursal case 1: " + e8.getMessage());
						throw new ServletException (e8.getMessage());
					}
			break;
			case 3: // Consulta Sucursales dadas de alta en la tira auditora
					try{
						System.out.println("Ingresa al Case 3 que consulta por nombre de sucursal");
						String sNombre = (String)sVec.elementAt(1);
						String sDatos = myMetodo.sSucursal3(sNombre);
						System.out.println(" Todos los Datos recibidos");
						System.out.println(sDatos);
						out.writeObject(sDatos);
						}catch (Exception e8){
							System.out.println ("Error en el servlet Sucursal case 1: " + e8.getMessage());
							throw new ServletException (e8.getMessage());
						}
						break;
			case 4: // Consulta Todas las Sucursales dadas de alta en la tira auditora
					try{
						System.out.println("Ingresa al Case 4 que consulta todas las sucursales en el servlet");
						String sDatos = myMetodo.sSucursal4();
						System.out.println(" Todos los Datos recibidos al servlet");
						out.writeObject(sDatos);
						}catch (Exception e8){
							System.out.println ("Error en el servlet Sucursal case #4: " + e8.getMessage());
							throw new ServletException (e8.getMessage());
						}
				break;
			case 5: // Da de alta una sucursal
					try{
						System.out.println("Ingresa al Case 5 que Inserta una sucursal a la bd");
						String sDatos = myMetodo.sSucursalInserta(sVec);
						System.out.println(" Todos los Datos recibidos");
						System.out.println(sDatos);
						out.writeObject(sDatos);
						}catch (Exception e8){
							System.out.println ("Error en el servlet Sucursal case 5: " + e8.getMessage());
								throw new ServletException (e8.getMessage());
						}
			break;
			case 6: // Eliminación de una sucursal
					try{
						System.out.println("Ingresa al Case 6 que Elimina una sucursal a la bd");
						String sVecs = (String)sVec.elementAt(1);
						String sDatos = myMetodo.sEliminaSuc(sVecs);
						System.out.println(sDatos);
						out.writeObject(sDatos);
						}catch (Exception e8){
								System.out.println ("Error en el servlet Sucursal case 6: " + e8.getMessage());
								throw new ServletException (e8.getMessage());
						}
			break;

			case 7: // Modificación de una sucursal
				try {
					System.out
							.println("Ingresa al Case 7 que Modifica una sucursal de la bd");
					String sDatos = myMetodo.sModificaSucursal(sVec);
					System.out.println(sDatos);
					out.writeObject(sDatos);
				} catch (Exception e8) {
					System.out.println("Error en el servlet Sucursal case 7: "
							+ e8.getMessage());
					throw new ServletException(e8.getMessage());
				}
				break;
			case 8: // Modificación de una sucursal
				try {
					System.out
							.println("Ingresa al Case 8 Modificación (basica) de una sucursal en la bd");
					String sDatos = myMetodo.sModificaBasicaSucursal(sVec);
					System.out.println(sDatos);
					out.writeObject(sDatos);
				} catch (Exception e8) {
					System.out.println("Error en el servlet Sucursal case 8: "
							+ e8.getMessage());
					throw new ServletException(e8.getMessage());
				}
			break;
	}//Cierra el Switch

	} //Cierra TRY PRINCIPAL
		catch (Exception eg) {
			 System.out.println(eg);
			 System.out.println("Error en SucursalServlet::service " + eg.getMessage());
		}

  } //Cierra service


	/**
	* @see javax.servlet.http.HttpServlet#void (javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	*/
	public void doGet(HttpServletRequest req, HttpServletResponse response)
		throws ServletException, IOException {

	}

	/**
	* @see javax.servlet.http.HttpServlet#void (javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	*/
	public void doPost(HttpServletRequest req, HttpServletResponse response)
		throws ServletException, IOException {

	}


}//End Servlet Sucursal