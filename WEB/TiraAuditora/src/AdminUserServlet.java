/** Banco Santander Mexicano
*   Clase AdminUserServlet  Servlet
*   @author Alfredo Resendiz Vargas
*   @version 1.0
*   Fecha de creacion : 28 de Marzo del 2006
*   Responsable : Eloisa Hernandez H
*   Descripcion : Realiza la funci�n de control de E/S de altas y bajas de
*                 cuantas de usuario
*   Modificacion :
*/

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.santander.usuarios.usuariosClase;


public class AdminUserServlet extends HttpServlet
{

/**
* @see M�todo vac�o pero necesario
*/

  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
  {
	doGeneral(req, resp);
  }

/**
* @see M�todo vac�o pero necesario
*/
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
  {
	doGeneral(req, resp);
  }

/**
* @see Realiza las conexiones necesarias para para el control del proceso de
*      administraci�n de cuentas de usuario
*/
  protected void doGeneral(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
  {
	usuariosClase alta_user = new usuariosClase();
	String accion = req.getParameter("accion");
	System.out.println("accion = " + accion);
	String user = req.getParameter("usuario");
	String password = req.getParameter("contrasena");
	String perfil = req.getParameter("comboPerfil");

	String facultad = req.getParameter("comboFacultad");
	String nomperfil = req.getParameter("nomperfil");

	String asigna_facultad = req.getParameter("asignaFacultad");
	String asigna_perfil = req.getParameter("asignaPerfil");

	String quita_facultad = req.getParameter("quitaFacultad");
	String quita_perfil = req.getParameter("quitaPerfil");

	String elimina_perfil = req.getParameter("eliminaPerfil");

	String res = "";

/**
 * Realiza la b�squeda del perfil del usuario
 */
  if(accion.equals("buscar"))
  {
    HttpSession sesion = req.getSession();
    ArrayList res_busca_perfil = null;
    String url = "?usuario="+user;
	res_busca_perfil = alta_user.consultaUsuarios();
    sesion.setAttribute("buscaperfil",res_busca_perfil);
	int seleccion = Integer.parseInt(req.getParameter("seleccion"));
	System.out.println("seleccion: "+seleccion);
	if(seleccion == 1)
	{
  	  resp.sendRedirect("/TiraAuditora/AdminUser/09AltUsu.jsp"+url);
	}
  	else
  	{
  	  if(seleccion == 2)
  	  {
	    resp.sendRedirect("/TiraAuditora/AdminUser/09AltFac.jsp"+url);
  	  }
	  else
	  {
	  	if(seleccion == 3)
		  resp.sendRedirect("/TiraAuditora/AdminUser/09BajFac.jsp"+url);
        else
	      resp.sendRedirect("/TiraAuditora/AdminUser/09EliPer.jsp"+url);
	  }
  	}
  }
/**
 * Realiza la alta de usuario
 */
    if(accion.equals("alta"))
    {
      HttpSession sesion = req.getSession();
      String res_alta_user = null;
      String res_alta_perfil = null;
      String url = "?usuario="+user;
      res_alta_user = alta_user.altaUser(user,password);
      sesion.setAttribute("consuluser",res_alta_user);
	  int resultado_user=Integer.parseInt(res_alta_user);
	  if(resultado_user==0)
	  {
		res_alta_perfil = alta_user.altaPerfil(user,perfil);
	    sesion.setAttribute("consulperfil",res_alta_perfil);
	    int resultado_perfil=Integer.parseInt(res_alta_perfil);

        if(resultado_perfil==0)
        {
          resp.sendRedirect("/TiraAuditora/AdminUser/09ConFir.jsp"+url);
        }
        else
        {
		  if(resultado_perfil==1)
		  {
		    System.out.println("resultado_perfil = " + resultado_perfil);
		    resp.sendRedirect("/TiraAuditora/AdminUser/09ConFi1.jsp"+url);
		  }
		  else
		  {
	        resp.sendRedirect("/TiraAuditora/AdminUser/09ConGra.jsp"+url);
		  }
        }
	  }
	  else
	  {
	    resp.sendRedirect("/TiraAuditora/AdminUser/09ConFi2.jsp"+url);
	  }
    }
/**
 * Realiza la baja de usuario
 */
  if(accion.equals("baja"))
  {
    HttpSession sesion = req.getSession();
    String res_baja_user = null;
    String res_borra_user = null;
    String url = "?usuario="+user;
    res_baja_user = alta_user.bajaUser(user);
    sesion.setAttribute("consuluser",res_baja_user);
    res_borra_user = alta_user.borraUser(user);
    sesion.setAttribute("consulborra",res_baja_user);
    int resultado_user=Integer.parseInt(res_baja_user);
    int resultado_borra=Integer.parseInt(res_borra_user);

    if(resultado_user==0 && resultado_borra==0)
    {
      resp.sendRedirect("/TiraAuditora/AdminUser/09ConFir.jsp"+url);
    }
    else
    {
      resp.sendRedirect("/TiraAuditora/AdminUser/09ConGra.jsp"+url);
    }
  }
/**
 * Realiza el desbloqueo de usuario
 */
  if(accion.equals("desbloqueo"))
  {
    HttpSession sesion = req.getSession();
    String res_desbloqueo = null;
    String url = "?usuario="+user;
    res_desbloqueo = alta_user.desbloqueoUser(user);
    sesion.setAttribute("consuldesb",res_desbloqueo);
    int resultado_desb=Integer.parseInt(res_desbloqueo);

    if(resultado_desb==0)
    {
   	  resp.sendRedirect("/TiraAuditora/AdminUser/09ConFir.jsp"+url);
    }
    else
    {
  	  resp.sendRedirect("/TiraAuditora/AdminUser/09ConGra.jsp"+url);
	}
  }
/**
 * Realiza la alta de perfil de usuario
 */
  if(accion.equals("altaPerfil"))
  {
	HttpSession sesion = req.getSession();
	String res_veri_perfil = null;
	String res_crea_perfil = null;
	String url = "?usuario="+user;
	res_veri_perfil = alta_user.verificaPerfil(facultad);
	sesion.setAttribute("veriperfil",res_veri_perfil);

	if(res_veri_perfil == "0")
	{
	  res_crea_perfil = alta_user.creaPerfil(facultad, nomperfil);
	  sesion.setAttribute("creaperfil",res_crea_perfil);
	  int resul_crea_perfil=Integer.parseInt(res_crea_perfil);
	  if(resul_crea_perfil==0)
	  {
		resp.sendRedirect("/TiraAuditora/AdminUser/09ConFi0.jsp"+url);
	  }
	  else
	  {
	    resp.sendRedirect("/TiraAuditora/AdminUser/09ConGra.jsp"+url);
	  }
	}
	else
	{
	  resp.sendRedirect("/TiraAuditora/AdminUser/09ConFi3.jsp"+url);
	}
  }
/**
 * Asigna Facultades a un perfil de usuario
 */
  if(accion.equals("asignarPerfil"))
  {
    HttpSession sesion = req.getSession();
    String res_asigna_perfil = null;
    String url = "?usuario="+user;
    res_asigna_perfil = alta_user.creaPerfil(asigna_facultad, asigna_perfil);
    sesion.setAttribute("asignaperfil",res_asigna_perfil);
    int resul_asigna_perfil=Integer.parseInt(res_asigna_perfil);
    if(resul_asigna_perfil==0)
    {
      resp.sendRedirect("/TiraAuditora/AdminUser/09ConFi4.jsp"+url);
    }
    else
    {
      resp.sendRedirect("/TiraAuditora/AdminUser/09ConGra.jsp"+url);
    }
  }
/**
 * Quita Facultades a un perfil de usuario
 */
  if(accion.equals("quitaPerfil"))
  {
    HttpSession sesion = req.getSession();
    String res_quita_perfil = null;
	String url = "?usuario="+user;
	res_quita_perfil = alta_user.bajaPerfil(quita_facultad, quita_perfil);
    sesion.setAttribute("quitaperfil",res_quita_perfil);
    int resul_quita_perfil=Integer.parseInt(res_quita_perfil);
    if(resul_quita_perfil==0)
    {
  	  resp.sendRedirect("/TiraAuditora/AdminUser/09ConFi5.jsp"+url);
    }
	else
	{
	  resp.sendRedirect("/TiraAuditora/AdminUser/09ConGra.jsp"+url);
	}
  }

/**
 * Elimina un perfil de usuario
 */
  if(accion.equals("eliminaPerfil"))
  {
    HttpSession sesion = req.getSession();
    String res_elimina_perfil = null;
    String res_veri_perfil = null;
    res_veri_perfil = alta_user.veriPerfil(elimina_perfil);
    sesion.setAttribute("veriperfil",res_veri_perfil);
    String url = "?usuario="+user;
    if(res_veri_perfil == "0")
    {
  	  res_elimina_perfil = alta_user.eliminaPerfil(elimina_perfil);
	  sesion.setAttribute("eliminaperfil",res_elimina_perfil);
	  int resul_elimina_perfil=Integer.parseInt(res_elimina_perfil);
	  if(resul_elimina_perfil==0)
	  {
	    resp.sendRedirect("/TiraAuditora/AdminUser/09ConFi5.jsp"+url);
	  }
	  else
	  {
	    resp.sendRedirect("/TiraAuditora/AdminUser/09ConGra.jsp"+url);
	  }
	}
	else
	{
	  resp.sendRedirect("/TiraAuditora/AdminUser/09ConFi6.jsp"+url);
	}
  }

/**
 * Actualiza el password
 */
   if(accion.equals("actpass"))
   {
    HttpSession sesion = req.getSession();
    String url = "?usuario="+user;
    String contrase�a = req.getParameter("contrasena");
    String usuario = req.getParameter("usuario");
    res = alta_user.actualizaPass(usuario,contrase�a);
    sesion.setAttribute("consulact",res);
    int result=Integer.parseInt(res);
    switch(result)
	{
	  case 0:
		resp.sendRedirect("/TiraAuditora/AdminUser/09ResSav.jsp"+url);
	    break;
	  case 1:
	    resp.sendRedirect("/TiraAuditora/AdminUser/09Reuso.jsp"+url);
	    break;
	  case 2:
	    resp.sendRedirect("/TiraAuditora/AdminUser/09ResSav.jsp"+url);
	    break;
	  case 3:
	    resp.sendRedirect("/TiraAuditora/AdminUser/09ConGra.jsp"+url);
	  default:
	    System.out.println("Error desconocido");
	}
   }
  }
}