/** Banco Santander Mexicano
*   Clase TiraServlet  Resuelve el flujo de las operaciones de Carga Manual
*   @author Ing. David Aguilar G�mez
*   @version 1.0
*   fecha de creacion : 23 de Febrero del 2006
*   responsable : Eloisa Hernandez H
*   descripcion : resuelve el flujo de las operaciones de carga manual de la Tira Auditora.
*   modificacion :
*/
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.servlet.*;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.oreilly.servlet.multipart.*;
import com.santander.tira.CargaClase;


public class CargaServlet extends HttpServlet
{
	/** El m�todo doGeneral(HttpServletRequest, HttpServletResponse) Control de flujo del modulo
	*   @param req  Valor Request
	*   @param resp Valor Response
	*/
	protected void doGeneral(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		String accion = "";
		MultipartParser mparser = null;
		Part par=null;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try
		{
			mparser = new MultipartParser(req,Integer.MAX_VALUE);
			while((par=mparser.readNextPart())!=null)
			{
				if(par.isFile())
				{
					FilePart fpart = (FilePart)par;
					fpart.writeTo(baos);
					baos.close();
				}
				else
					if(par.isParam())
					{
						ParamPart ppart = (ParamPart)par;
						String name=par.getName();
						System.out.println("name = " + name);
						if (name.compareToIgnoreCase("accion") == 0)
						  accion = ppart.getStringValue();
						System.out.println("accion = " + accion);
						req.setAttribute(par.getName(), ppart.getStringValue());
					}
					else
					{
						throw new InternalError("Part de tipo desconocido. Incapaz de parsear request.");
					}
			}
			if(accion.equals("enviar")){
				BufferedReader br = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(baos.toByteArray())));
				String linea = br.readLine();
				int numLinea = 1;
				int vacio = 0;
				String pventa = null;
				String fecha = null;
				CargaClase c_clase = new CargaClase();
				ArrayList bitacora = new ArrayList();
				while(linea!=null){
					System.out.println("Linea "+numLinea+": "+linea);
					if(linea.trim().length()>0)
					{
						StringTokenizer strtkbis = new StringTokenizer(linea,"^");
						if(strtkbis.countTokens()>0 && c_clase.getLugar_registro()==0)
						{
							strtkbis.nextElement();
							pventa = (String)strtkbis.nextElement();
							pventa = pventa.substring(1,pventa.length()-1);
							fecha = (String)strtkbis.nextElement();
							fecha = fecha.substring(1,fecha.length()-1);
							if(validaCve_ptovta(pventa) && validaFecha_Dia(fecha))
							{
								boolean escontingente = c_clase.estaContingente(fecha,pventa);
								boolean esdiario = c_clase.estaDiario(fecha,pventa);
								if(escontingente && !esdiario)
									c_clase.setLugar_registro(CargaClase.ESTACONTINGENTE);
								else
									if(esdiario)
										c_clase.setLugar_registro(CargaClase.ESTADIARIO);
									else
										c_clase.setLugar_registro(CargaClase.NOESTA);
							}
						}
						if(c_clase.getLugar_registro()!=CargaClase.ESTADIARIO)
							c_clase.procesaLinea(bitacora, linea, numLinea);
						vacio++;
					}
					linea = br.readLine();
					numLinea++;
				}
				if(c_clase.getLugar_registro()!=0 && fecha!=null && pventa!=null)
					c_clase.mueveConHis(fecha,pventa);
				HttpSession sesion = req.getSession();
				sesion.setAttribute("bitacora",bitacora);
				if(vacio>0)
				{
					sesion.setAttribute("mensaje","");
					if(c_clase.getLugar_registro()==CargaClase.ESTADIARIO)
						sesion.setAttribute("mensaje","Ya esta cargada la informaci�n");
				}
				else
					sesion.setAttribute("mensaje","El archivo esta vacio.");
			}
			resp.sendRedirect("/TiraAuditora/Tira/07Bitacora.jsp");
		}
		catch(Exception e2)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e2);
		}
	}
/** El m�todo doGet(HttpServletRequest, HttpServletResponse) entrada por Get
*   @param req  Valor Request
*   @param resp Valor Response
*/
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
			doGeneral(req, resp);
	}

/** El m�todo doPost(HttpServletRequest, HttpServletResponse) entrada por Post
*   @param req  Valor Request
*   @param resp Valor Response
*/
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
			doGeneral(req, resp);
	}
	/** El m�todo validaCve_ptovta(String) revisa que el campo este correcto
	*   @param campo Valor del campo
	*   @return  boolean valor de respuesta de la validaci�n
	*/
		private boolean validaCve_ptovta(String campo)
		{
			if(campo.length() == 4)
				try
				{
					Integer.parseInt(campo);
					return true;
				}catch(NumberFormatException e)
				{
					return false;
				}
			else
			{
				return false;
			}
		}
	/** El m�todo validaFecha_Dia(ArrayList, String, int, String) revisa que el campo este correcto
	*   @param bitacora Arreglo donde se agregan los errores
	*   @param linea N�mero de linea
	*   @param num_campo N�mero de campo
	*   @param campo Valor del campo
	*   @return  boolean valor de respuesta de la validaci�n
	*/
		private boolean validaFecha_Dia(String campo)
		{
			if(campo.length() == 19)
				try
				{
					SimpleDateFormat df = new SimpleDateFormat("MM'/'dd'/'yyyy HH:mm:ss");
					df.parse(campo);
					return true;
				}catch(ParseException e)
				{
					return false;
				}
			else
			{
				return false;
			}
		}

}