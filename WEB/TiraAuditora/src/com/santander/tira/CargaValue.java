/** Banco Santander Mexicano
*   Clase CargaValue  Contiene los valores de la Carga Manual
*   @author Ing. David Aguilar G�mez
*   @version 1.0
*   fecha de creacion : 01 de Marzo del 2006
*   responsable : Eloisa Hernandez H
*   descripcion : Contiene los valores de la Carga Manual a insertar.
*   modificacion :
*/
package com.santander.tira;

public class CargaValue {

	private String cve_ptovta;
	private String fecha_dia;
	private String referencia;
	private String cve_divisa;
	private String cve_operacion;
	private String cve_custodio;
	private String cod_error;
	private String en_linea;
	private String importe;
	private String estado;
	private String cve_custodio2;
	private String terminal;
	private String refer_orig;
	private String arrastre_efvo;
	private String observacion;

	private String tipo;
	private String denominacion;
	private String cantidad;

	private String idmov;
	private String sdo_inicial;
	private String sdo_final;
	private String fch_apertura;
	private String fch_cierre;
	private String desc_mvto;
	private String detalle_mvto;

	private String folio;
	private String cuenta;
	private String verifica_firma;
	private String plaza;
	private String banco;


	/**
	 * @return
	 */
	public String getArrastre_efvo() {
		return arrastre_efvo;
	}

	/**
	 * @return
	 */
	public String getCod_error() {
		return cod_error;
	}

	/**
	 * @return
	 */
	public String getCve_custodio() {
		return cve_custodio;
	}

	/**
	 * @return
	 */
	public String getCve_custodio2() {
		return cve_custodio2;
	}

	/**
	 * @return
	 */
	public String getCve_divisa() {
		return cve_divisa;
	}

	/**
	 * @return
	 */
	public String getCve_operacion() {
		return cve_operacion;
	}

	/**
	 * @return
	 */
	public String getCve_ptovta() {
		return cve_ptovta;
	}

	/**
	 * @return
	 */
	public String getEn_linea() {
		return en_linea;
	}

	/**
	 * @return
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @return
	 */
	public String getFecha_dia() {
		return fecha_dia;
	}

	/**
	 * @return
	 */
	public String getImporte() {
		return importe;
	}

	/**
	 * @return
	 */
	public String getObservacion() {
		return observacion;
	}

	/**
	 * @return
	 */
	public String getRefer_orig() {
		return refer_orig;
	}

	/**
	 * @return
	 */
	public String getReferencia() {
		return referencia;
	}

	/**
	 * @return
	 */
	public String getTerminal() {
		return terminal;
	}

	/**
	 * @param string
	 */
	public void setArrastre_efvo(String string) {
		arrastre_efvo = string;
	}

	/**
	 * @param string
	 */
	public void setCod_error(String string) {
		cod_error = string;
	}

	/**
	 * @param string
	 */
	public void setCve_custodio(String string) {
		cve_custodio = string;
	}

	/**
	 * @param string
	 */
	public void setCve_custodio2(String string) {
		cve_custodio2 = string;
	}

	/**
	 * @param string
	 */
	public void setCve_divisa(String string) {
		cve_divisa = string;
	}

	/**
	 * @param string
	 */
	public void setCve_operacion(String string) {
		cve_operacion = string;
	}

	/**
	 * @param string
	 */
	public void setCve_ptovta(String string) {
		cve_ptovta = string;
	}

	/**
	 * @param string
	 */
	public void setEn_linea(String string) {
		en_linea = string;
	}

	/**
	 * @param string
	 */
	public void setEstado(String string) {
		estado = string;
	}

	/**
	 * @param string
	 */
	public void setFecha_dia(String string) {
		fecha_dia = string;
	}

	/**
	 * @param string
	 */
	public void setImporte(String string) {
		importe = string;
	}

	/**
	 * @param string
	 */
	public void setObservacion(String string) {
		observacion = string;
	}

	/**
	 * @param string
	 */
	public void setRefer_orig(String string) {
		refer_orig = string;
	}

	/**
	 * @param string
	 */
	public void setReferencia(String string) {
		referencia = string;
	}

	/**
	 * @param string
	 */
	public void setTerminal(String string) {
		terminal = string;
	}

	/**
	 * @return
	 */
	public String getCantidad() {
		return cantidad;
	}

	/**
	 * @return
	 */
	public String getDenominacion() {
		return denominacion;
	}

	/**
	 * @return
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param string
	 */
	public void setCantidad(String string) {
		cantidad = string;
	}

	/**
	 * @param string
	 */
	public void setDenominacion(String string) {
		denominacion = string;
	}

	/**
	 * @param string
	 */
	public void setTipo(String string) {
		tipo = string;
	}

	/**
	 * @return
	 */
	public String getDesc_mvto() {
		return desc_mvto;
	}

	/**
	 * @return
	 */
	public String getDetalle_mvto() {
		return detalle_mvto;
	}

	/**
	 * @return
	 */
	public String getFch_apertura() {
		return fch_apertura;
	}

	/**
	 * @return
	 */
	public String getFch_cierre() {
		return fch_cierre;
	}

	/**
	 * @return
	 */
	public String getIdmov() {
		return idmov;
	}

	/**
	 * @return
	 */
	public String getSdo_final() {
		return sdo_final;
	}

	/**
	 * @return
	 */
	public String getSdo_inicial() {
		return sdo_inicial;
	}

	/**
	 * @param string
	 */
	public void setDesc_mvto(String string) {
		desc_mvto = string;
	}

	/**
	 * @param string
	 */
	public void setDetalle_mvto(String string) {
		detalle_mvto = string;
	}

	/**
	 * @param string
	 */
	public void setFch_apertura(String string) {
		fch_apertura = string;
	}

	/**
	 * @param string
	 */
	public void setFch_cierre(String string) {
		fch_cierre = string;
	}

	/**
	 * @param string
	 */
	public void setIdmov(String string) {
		idmov = string;
	}

	/**
	 * @param string
	 */
	public void setSdo_final(String string) {
		sdo_final = string;
	}

	/**
	 * @param string
	 */
	public void setSdo_inicial(String string) {
		sdo_inicial = string;
	}

	/**
	 * @return
	 */
	public String getBanco() {
		return banco;
	}

	/**
	 * @return
	 */
	public String getCuenta() {
		return cuenta;
	}

	/**
	 * @return
	 */
	public String getFolio() {
		return folio;
	}

	/**
	 * @return
	 */
	public String getPlaza() {
		return plaza;
	}

	/**
	 * @return
	 */
	public String getVerifica_firma() {
		return verifica_firma;
	}

	/**
	 * @param string
	 */
	public void setBanco(String string) {
		banco = string;
	}

	/**
	 * @param string
	 */
	public void setCuenta(String string) {
		cuenta = string;
	}

	/**
	 * @param string
	 */
	public void setFolio(String string) {
		folio = string;
	}

	/**
	 * @param string
	 */
	public void setPlaza(String string) {
		plaza = string;
	}

	/**
	 * @param string
	 */
	public void setVerifica_firma(String string) {
		verifica_firma = string;
	}

}