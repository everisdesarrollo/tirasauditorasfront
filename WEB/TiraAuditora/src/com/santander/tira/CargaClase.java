/** Banco Santander Mexicano
*   Clase CargaClase  Resuelve las operaciones de la Carga Manual
*   @author Ing. David Aguilar G�mez
*   @version 1.0
*   fecha de creacion : 28 de Febrero del 2006
*   responsable : Eloisa Hernandez H
*   descripcion : resuelve operaciones de carga manual y creacion de bitacora.
*   modificacion :
*/
package com.santander.tira;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

import com.santander.utilerias.Conex03;

public class CargaClase
{
	private int lugar_registro;
	public static final int NOESTA = 1;
	public static final int ESTACONTINGENTE = 2;
	public static final int ESTADIARIO = 3;
/** El m�todo procesaLinea(ArrayList, String) revisa que la linea sea correcta
*   para la insercion del registro
*   @param bitacora Arreglo de datos de posibles fallas
*   @param linea Cadena a verificar
*   @param numLinea N�mero de linea a verificar
*/
	public void procesaLinea(ArrayList bitacora, String linea, int numLinea)
	{
		StringTokenizer strtk = new StringTokenizer(linea,"^");
		int campos = strtk.countTokens();
		boolean bandera;
		System.out.println("Total de tokens:"+campos);
		if(campos > 0)
		{
			String tabla = (String)strtk.nextElement();
			tabla = tabla.substring(1,tabla.length()-1);
			if(tabla.equals("TA_DIARIO"))
			{
				if((campos-1) == 15)
				{
					CargaValue valores = new CargaValue();
					int num_campo = 1;
					bandera = true;
					while(strtk.hasMoreElements())
					{
						String campo = (String)strtk.nextElement();
						campo = campo.substring(1,campo.length()-1);
						System.out.println(num_campo+":"+campo);
						if(!validaTa_Diario(bitacora, valores, ""+numLinea, num_campo, campo))
							bandera = false;
						num_campo++;
					}
					if(bandera)
						insertaTa_Diario(bitacora, valores, ""+numLinea);
				}
				else
				{
					bitacora.add(""+numLinea);
					bitacora.add("No es la cantidad de campos que requiere TA_DIARIO");
				}
			}
			if(tabla.equals("TA_INV_AMON"))
			{
				if((campos-1) == 7)
				{
					CargaValue valores = new CargaValue();
					int num_campo = 1;
					bandera = true;
					while(strtk.hasMoreElements())
					{
						String campo = (String)strtk.nextElement();
						campo = campo.substring(1,campo.length()-1);
						System.out.println(num_campo+":"+campo);
						if(!validaTa_Inv_Amon(bitacora, valores, ""+numLinea, num_campo, campo))
							bandera = false;
						num_campo++;
					}
					if(bandera)
						insertaTa_Inv_Amon(bitacora, valores, ""+numLinea);
				}
				else
				{
					bitacora.add(""+numLinea);
					bitacora.add("No es la cantidad de campos que requiere TA_INV_AMON");
				}
			}
			if(tabla.equals("TA_SUCU_EFECTIVO"))
			{
				if((campos-1) == 11)
				{
					CargaValue valores = new CargaValue();
					int num_campo = 1;
					bandera = true;
					while(strtk.hasMoreElements())
					{
						String campo = (String)strtk.nextElement();
						campo = campo.substring(1,campo.length()-1);
						System.out.println(num_campo+":"+campo);
						if(!validaTa_Sucu_Efectivo(bitacora, valores, ""+numLinea, num_campo, campo))
							bandera = false;
						num_campo++;
					}
					if(bandera)
						insertaTa_Sucu_Efectivo(bitacora, valores, ""+numLinea);
				}
				else
				{
					bitacora.add(""+numLinea);
					bitacora.add("No es la cantidad de campos que requiere TA_SUCU_EFECTIVO");
				}
			}
			if(tabla.equals("TA_CHEQUES"))
			{
				if((campos-1) == 9)
				{
					CargaValue valores = new CargaValue();
					int num_campo = 1;
					bandera = true;
					while(strtk.hasMoreElements())
					{
						String campo = (String)strtk.nextElement();
						campo = campo.substring(1,campo.length()-1);
						System.out.println(campo.length()+","+num_campo+":"+campo);
						if(!validaTa_Cheques(bitacora, valores, ""+numLinea, num_campo, campo))
							bandera = false;
						num_campo++;
					}
					if(bandera)
						insertaTa_Cheques(bitacora, valores, ""+numLinea);
				}
				else
				{
					bitacora.add(""+numLinea);
					bitacora.add("No es la cantidad de campos que requiere TA_CHEQUES");
				}
			}
		}
	}
/** El m�todo validaTa_Diario(ArrayList, CargaValue, String, int, String) revisa que el campo este correcto
*   @param bitacora Arreglo donde se agregan los errores
*   @param valores Arreglo donde se agregan los valores
*   @param linea N�mero de linea
*   @param numCampo N�mero de campo
*   @param campo Valor del campo
*   @return  boolean valor de respuesta de la validaci�n
*/
	private boolean validaTa_Diario(ArrayList bitacora, CargaValue valores, String linea, int num_campo, String campo)
	{
		boolean bandera = true;
		switch(num_campo)
		{
			case 1:
				if(bandera && validaCve_ptovta(bitacora, linea, num_campo, campo))
					valores.setCve_ptovta(campo);
				else
					bandera = false;
			break;
			case 2:
				if(bandera && validaFecha_Dia(bitacora, linea, num_campo, campo))
					valores.setFecha_dia(campo);
				else
					bandera = false;
			break;
			case 3:
				if(bandera && validaReferencia(bitacora, linea, num_campo, campo))
					valores.setReferencia(campo);
				else
					bandera = false;
			break;
			case 4:
				if(bandera && validaCaracter(bitacora, 4, linea, num_campo, campo))
					valores.setCve_divisa(campo);
				else
					bandera = false;
			break;
			case 5:
				if(bandera && validaCaracter(bitacora, 5, linea, num_campo, campo))
					valores.setCve_operacion(campo);
				else
					bandera = false;
			break;
			case 6:
				if(bandera && validaCaracter(bitacora, 7, linea, num_campo, campo))
					valores.setCve_custodio(campo);
				else
					bandera = false;
			break;
			case 7:
				if(bandera && validaCaracter(bitacora, 8, linea, num_campo, campo))
					valores.setCod_error(campo);
				else
					bandera = false;
			break;
			case 8:
				if(bandera && validaCaracter(bitacora, 1, linea, num_campo, campo))
					valores.setEn_linea(campo);
				else
					bandera = false;
			break;
			case 9:
				if(bandera && validaDecimal(bitacora, 22, linea, num_campo, campo))
					valores.setImporte(campo);
				else
					bandera = false;
			break;
			case 10:
				if(bandera && validaCaracter(bitacora, 1, linea, num_campo, campo))
					valores.setEstado(campo);
				else
					bandera = false;
			break;
			case 11:
				if(bandera && validaCaracter(bitacora, 7, linea, num_campo, campo))
					valores.setCve_custodio2(campo);
				else
					bandera = false;
			break;
			case 12:
				if(bandera && validaCaracter(bitacora, 4, linea, num_campo, campo))
					valores.setTerminal(campo);
				else
					bandera = false;
			break;
			case 13:
				if(bandera && validaNumerico(bitacora, 7, linea, num_campo, campo))
					valores.setRefer_orig(campo);
				else
					bandera = false;
			break;
			case 14:
				if(bandera && validaDecimal(bitacora, 22, linea, num_campo, campo))
					valores.setArrastre_efvo(campo);
				else
					bandera = false;
			break;
			case 15:
				if(bandera && validaCaracter(bitacora, 800, linea, num_campo, campo))
					valores.setObservacion(campo);
				else
					bandera = false;
			break;
			default:
				System.out.println("No aplica esta seleccion.");
			break;
		}
		return bandera;
	}
/** El m�todo validaTa_Inv_Amon(ArrayList, CargaValue, String, int, String) revisa que el campo este correcto
*   @param bitacora Arreglo donde se agregan los errores
*   @param valores Arreglo donde se agregan los valores
*   @param linea N�mero de linea
*   @param num_campo N�mero de campo
*   @param campo Valor del campo
*   @return  boolean valor de respuesta de la validaci�n
*/
private boolean validaTa_Inv_Amon(ArrayList bitacora, CargaValue valores, String linea, int num_campo, String campo)
{
	boolean bandera = true;
	switch(num_campo)
	{
		case 1:
			if(bandera && validaCve_ptovta(bitacora, linea, num_campo, campo))
				valores.setCve_ptovta(campo);
			else
				bandera = false;
		break;
		case 2:
			if(bandera && validaFecha_Dia(bitacora, linea, num_campo, campo))
				valores.setFecha_dia(campo);
			else
				bandera = false;
		break;
		case 3:
			if(bandera && validaReferencia(bitacora, linea, num_campo, campo))
				valores.setReferencia(campo);
			else
				bandera = false;
		break;
		case 4:
			if(bandera && validaTipo(bitacora, linea, num_campo, campo))
				valores.setTipo(campo);
			else
				bandera = false;
		break;
		case 5:
			if(bandera && validaDenominacion(bitacora, 22, linea, num_campo, campo))
				valores.setDenominacion(campo);
			else
				bandera = false;
		break;
		case 6:
			if(bandera && validaNumerico(bitacora, 7, linea, num_campo, campo))
				valores.setRefer_orig(campo);
			else
				bandera = false;
		break;
		case 7:
			if(bandera && validaDecimal(bitacora, 22, linea, num_campo, campo))
				valores.setCantidad(campo);
			else
				bandera = false;
		break;
		default:
			System.out.println("No aplica esta seleccion.");
		break;
	}
	return bandera;
}
/** El m�todo validaTa_Sucu_Efectivo(ArrayList, CargaValue, String, int, String) revisa que el campo este correcto
*   @param bitacora Arreglo donde se agregan los errores
*   @param valores Arreglo donde se agregan los valores
*   @param linea N�mero de linea
*   @param num_campo N�mero de campo
*   @param campo Valor del campo
*   @return  boolean valor de respuesta de la validaci�n
*/
	private boolean validaTa_Sucu_Efectivo(ArrayList bitacora, CargaValue valores, String linea, int num_campo, String campo)
	{
		boolean bandera = true;
		switch(num_campo)
		{
			case 1:
				if(bandera && validaCve_ptovta(bitacora, linea, num_campo, campo))
					valores.setCve_ptovta(campo);
				else
					bandera = false;
			break;
			case 2:
				if(bandera && validaFecha_Dia(bitacora, linea, num_campo, campo))
					valores.setFecha_dia(campo);
				else
					bandera = false;
			break;
			case 3:
				if(bandera && validaIdmov(bitacora, linea, num_campo, campo))
					valores.setIdmov(campo);
				else
					bandera = false;
			break;
			case 4:
				if(bandera && validaReferencia(bitacora, linea, num_campo, campo))
					valores.setReferencia(campo);
				else
					bandera = false;
			break;
			case 5:
				if(bandera && validaCaracter(bitacora, 4,linea, num_campo, campo))
					valores.setCve_divisa(campo);
				else
					bandera = false;
			break;
			case 6:
				if(bandera && validaDecimal(bitacora, 22, linea, num_campo, campo))
					valores.setSdo_inicial(campo);
				else
					bandera = false;
			break;
			case 7:
				if(bandera && validaDecimal(bitacora, 22, linea, num_campo, campo))
					valores.setSdo_final(campo);
				else
					bandera = false;
			break;
			case 8:
				if(bandera && validaFecha(bitacora, linea, num_campo, campo))
					valores.setFch_apertura(campo);
				else
					bandera = false;
			break;
			case 9:
				if(bandera && validaFecha(bitacora, linea, num_campo, campo))
					valores.setFch_cierre(campo);
				else
					bandera = false;
			break;
			case 10:
				if(bandera && validaCaracter(bitacora, 800, linea, num_campo, campo))
					valores.setDesc_mvto(campo);
				else
					bandera = false;
			break;
			case 11:
				if(bandera && validaCaracter(bitacora, 800, linea, num_campo, campo))
					valores.setDetalle_mvto(campo);
				else
					bandera = false;
			break;
			default:
				System.out.println("No aplica esta seleccion.");
			break;
		}
		return bandera;
	}
/** El m�todo validaTa_Cheques(ArrayList, CargaValue, String, int, String) revisa que el campo este correcto
*   @param bitacora Arreglo donde se agregan los errores
*   @param valores Arreglo donde se agregan los valores
*   @param linea N�mero de linea
*   @param num_campo N�mero de campo
*   @param campo Valor del campo
*   @return  boolean valor de respuesta de la validaci�n
*/
	private boolean validaTa_Cheques(ArrayList bitacora, CargaValue valores, String linea, int num_campo, String campo)
	{
		boolean bandera = true;
		switch(num_campo)
		{
			case 1:
				if(bandera && validaCve_ptovta(bitacora, linea, num_campo, campo))
					valores.setCve_ptovta(campo);
				else
					bandera = false;
			break;
			case 2:
				if(bandera && validaFecha_Dia(bitacora, linea, num_campo, campo))
					valores.setFecha_dia(campo);
				else
					bandera = false;
			break;
			case 3:
				if(bandera && validaReferencia(bitacora, linea, num_campo, campo))
					valores.setReferencia(campo);
				else
					bandera = false;
			break;
			case 4:
				if(bandera && validaFolio(bitacora, 12, linea, num_campo, campo))
					valores.setFolio(campo);
				else
					bandera = false;
			break;
			case 5:
				if(bandera && validaCaracter(bitacora, 16, linea, num_campo, campo))
					valores.setCuenta(campo);
				else
					bandera = false;
			break;
			case 6:
				if(bandera && validaCaracter(bitacora, 1, linea, num_campo, campo))
					valores.setVerifica_firma(campo);
				else
					bandera = false;
			break;
			case 7:
				if(bandera && validaDecimal(bitacora, 22, linea, num_campo, campo))
					valores.setImporte(campo);
				else
					bandera = false;
			break;
			case 8:
				if(bandera && validaCaracter(bitacora, 5, linea, num_campo, campo))
					valores.setPlaza(campo);
				else
					bandera = false;
			break;
			case 9:
				if(bandera && validaCaracter(bitacora, 3, linea, num_campo, campo))
					valores.setBanco(campo);
				else
					bandera = false;
			break;
			default:
				System.out.println("No aplica esta seleccion.");
			break;
		}
		return bandera;
	}
/** El m�todo validaCve_ptovta(ArrayList, String, int, String) revisa que el campo este correcto
*   @param bitacora Arreglo donde se agregan los errores
*   @param linea N�mero de linea
*   @param num_campo N�mero de campo
*   @param campo Valor del campo
*   @return  boolean valor de respuesta de la validaci�n
*/
	private boolean validaCve_ptovta(ArrayList bitacora, String linea, int num_campo, String campo)
	{
		if(campo.length() == 4)
			try
			{
				Integer.parseInt(campo);
				return true;
			}catch(NumberFormatException e)
			{
				bitacora.add(linea);
				bitacora.add("El campo n�mero "+num_campo+" debe ser numerico");
				return false;
			}
		else
		{
			bitacora.add(linea);
			bitacora.add("El campo n�mero "+num_campo+" debe contener 4 digitos");
			return false;
		}
	}
/** El m�todo validaFecha_Dia(ArrayList, String, int, String) revisa que el campo este correcto
*   @param bitacora Arreglo donde se agregan los errores
*   @param linea N�mero de linea
*   @param num_campo N�mero de campo
*   @param campo Valor del campo
*   @return  boolean valor de respuesta de la validaci�n
*/
	private boolean validaFecha_Dia(ArrayList bitacora, String linea, int num_campo, String campo)
	{
		if(campo.length() == 19)
			try
			{
				SimpleDateFormat df = new SimpleDateFormat("MM'/'dd'/'yyyy HH:mm:ss");
				df.parse(campo);
				return true;
			}catch(ParseException e)
			{
				bitacora.add(linea);
				bitacora.add("El campo n�mero "+num_campo+" debe ser fecha");
				return false;
			}
		else
		{
			bitacora.add(linea);
			bitacora.add("El campo n�mero "+num_campo+" debe contener 19 digitos");
			return false;
		}
	}
/** El m�todo validaReferencia(ArrayList, String, int, String) revisa que el campo este correcto
*   @param bitacora Arreglo donde se agregan los errores
*   @param linea N�mero de linea
*   @param num_campo N�mero de campo
*   @param campo Valor del campo
*   @return  boolean valor de respuesta de la validaci�n
*/
	private boolean validaReferencia(ArrayList bitacora, String linea, int num_campo, String campo)
	{
		if(campo.length() > 0 && campo.length() <= 7)
			try
			{
				if(Long.parseLong(campo)<=0)
				{
					bitacora.add(linea);
					bitacora.add("El campo n�mero "+num_campo+" debe ser mayor a cero");
					return false;
				}
				else
				{
					return true;
				}
			}catch(NumberFormatException e)
			{
				bitacora.add(linea);
				bitacora.add("El campo n�mero "+num_campo+" debe ser numerico");
				return false;
			}
		else
		{
			bitacora.add(linea);
			bitacora.add("El campo n�mero "+num_campo+" debe contener entre 1 y 7 digitos");
			return false;
		}
	}
/** El m�todo validaTipo(ArrayList, String, int, String) revisa que el campo este correcto
*   @param bitacora Arreglo donde se agregan los errores
*   @param linea N�mero de linea
*   @param num_campo N�mero de campo
*   @param campo Valor del campo
*   @return  boolean valor de respuesta de la validaci�n
*/
	private boolean validaTipo(ArrayList bitacora, String linea, int num_campo, String campo)
	{
		if(campo.length() == 1)
			return true;
		else
		{
			bitacora.add(linea);
			bitacora.add("El campo n�mero "+num_campo+" debe contener 1 digitos");
			return false;
		}
	}
/** El m�todo validaDenominacion(ArrayList, int, String, int, String) revisa que el campo este correcto
*   @param bitacora Arreglo donde se agregan los errores
*   @param digitos N�mero de digitos del campo
*   @param linea N�mero de linea
*   @param num_campo N�mero de campo
*   @param campo Valor del campo
*   @return  boolean valor de respuesta de la validaci�n
*/
	private boolean validaDenominacion(ArrayList bitacora, int digitos, String linea, int num_campo, String campo)
	{
		if(campo.length() > 0 && campo.length() <= digitos)
			try
			{
				Double.parseDouble(campo);
				return true;
			}catch(NumberFormatException e)
			{
				bitacora.add(linea);
				bitacora.add("El campo n�mero "+num_campo+" debe ser numerico");
				return false;
			}
		else
		{
			bitacora.add(linea);
			bitacora.add("El campo n�mero "+num_campo+" debe contener entre 1 y "+digitos+" digitos");
			return false;
		}
	}
/** El m�todo validaIdmov(ArrayList, String, int, String) revisa que el campo este correcto
*   @param bitacora Arreglo donde se agregan los errores
*   @param linea N�mero de linea
*   @param num_campo N�mero de campo
*   @param campo Valor del campo
*   @return  boolean valor de respuesta de la validaci�n
*/
	private boolean validaIdmov(ArrayList bitacora, String linea, int num_campo, String campo)
	{
		if(campo.length() > 0 && campo.length() <= 4)
				return true;
		else
		{
			bitacora.add(linea);
			bitacora.add("El campo n�mero "+num_campo+" debe contener entre 1 y 4 digitos");
			return false;
		}
	}
/** El m�todo validaFolio(ArrayList, int, String, int, String) revisa que el campo este correcto
*   @param bitacora Arreglo donde se agregan los errores
*   @param digitos N�mero de digitos del campo
*   @param linea N�mero de linea
*   @param num_campo N�mero de campo
*   @param campo Valor del campo
*   @return  boolean valor de respuesta de la validaci�n
*/
	private boolean validaFolio(ArrayList bitacora, int digitos, String linea, int num_campo, String campo)
	{
		if(campo.length() > 0 && campo.length() <= digitos)
			try
			{
				if(Long.parseLong(campo)<0)
				{
					bitacora.add(linea);
					bitacora.add("El campo n�mero "+num_campo+" mayor o igual a cero");
					return false;
				}
				else
				{
					return true;
				}
			}catch(NumberFormatException e)
			{
				bitacora.add(linea);
				bitacora.add("El campo n�mero "+num_campo+" debe ser numerico");
				return false;
			}
		else
		{
			bitacora.add(linea);
			bitacora.add("El campo n�mero "+num_campo+" debe contener entre 1 y "+digitos+" digitos");
			return false;
		}
	}
/** El m�todo validaCaracter(ArrayList, int, String, int, String) revisa que el campo este correcto
*   @param bitacora Arreglo donde se agregan los errores
*   @param digitos N�mero de digitos del campo
*   @param linea N�mero de linea
*   @param num_campo N�mero de campo
*   @param campo Valor del campo
*   @return  boolean valor de respuesta de la validaci�n
*/
	private boolean validaCaracter(ArrayList bitacora, int digitos, String linea, int num_campo, String campo)
	{
		if(campo.length() <= digitos){
			return true;
		}
		else
		{
			bitacora.add(linea);
			bitacora.add("El campo n�mero "+num_campo+" debe contener como maximo "+digitos+" digitos");
			return false;
		}
	}
/** El m�todo validaDecimal(ArrayList, int, String, int, String) revisa que el campo este correcto
*   @param bitacora Arreglo donde se agregan los errores
*   @param digitos N�mero de digitos del campo
*   @param linea N�mero de linea
*   @param num_campo N�mero de campo
*   @param campo Valor del campo
*   @return  boolean valor de respuesta de la validaci�n
*/
	private boolean validaDecimal(ArrayList bitacora, int digitos, String linea, int num_campo, String campo)
	{
		if(campo.length() > 0 && campo.length() <= digitos)
			try
			{
				Double.parseDouble(campo);
				return true;
			}catch(NumberFormatException e)
			{
				bitacora.add(linea);
				bitacora.add("El campo n�mero "+num_campo+" debe ser numerico");
				return false;
			}
		else
		{
			if(campo.length() == 0)
			{
				return true;
			}
			else
			{
				bitacora.add(linea);
				bitacora.add("El campo n�mero "+num_campo+" debe contener entre 1 y "+digitos+" digitos");
				return false;
			}
		}
	}
/** El m�todo validaNumerico(ArrayList, int, String, int, String) revisa que el campo este correcto
*   @param bitacora Arreglo donde se agregan los errores
*   @param digitos N�mero de digitos del campo
*   @param linea N�mero de linea
*   @param num_campo N�mero de campo
*   @param campo Valor del campo
*   @return  boolean valor de respuesta de la validaci�n
*/
	private boolean validaNumerico(ArrayList bitacora, int digitos, String linea, int num_campo, String campo)
	{
		if(campo.length() > 0 && campo.length() <= digitos)
			try
			{
				if(Long.parseLong(campo)< 0)
				{
					bitacora.add(linea);
					bitacora.add("El campo n�mero "+num_campo+" debe ser mayor o igual a cero");
					return false;
				}
				else
				{
					return true;
				}
			}catch(NumberFormatException e)
			{
				bitacora.add(linea);
				bitacora.add("El campo n�mero "+num_campo+" debe ser numerico");
				return false;
			}
		else
		{
			if(campo.length() == 0)
			{
				return true;
			}
			else
			{
				bitacora.add(linea);
				bitacora.add("El campo n�mero "+num_campo+" debe contener entre 1 y "+digitos+" digitos");
				return false;
			}
		}
	}
/** El m�todo validaFecha(ArrayList, String, int, String) revisa que el campo este correcto
*   @param bitacora Arreglo donde se agregan los errores
*   @param linea N�mero de linea
*   @param num_campo N�mero de campo
*   @param campo Valor del campo
*   @return  boolean valor de respuesta de la validaci�n
*/
	private boolean validaFecha(ArrayList bitacora, String linea, int num_campo, String campo)
	{
		if(campo.length() == 19)
			try
			{
				SimpleDateFormat df = new SimpleDateFormat("MM'/'dd'/'yyyy HH:mm:ss");
				df.parse(campo);
				return true;
			}catch(ParseException e)
			{
				bitacora.add(linea);
				bitacora.add("El campo n�mero "+num_campo+" debe ser fecha");
				return false;
			}
		else
		{
			if(campo.length()>0)
			{
				bitacora.add(linea);
				bitacora.add("El campo n�mero "+num_campo+" debe contener 19 digitos");
				return false;
			}
			else
				return true;
		}
	}
/** El m�todo insertaTa_Diario(ArrayList, CargaValue, String) inserta registro de Ta_Diario
*   @param bitacora Arreglo donde se agregan los errores
*   @param valores Arreglo de datos a insertar
*   @param linea N�mero de linea
*/
	private void insertaTa_Diario(ArrayList bitacora, CargaValue valores, String linea)
	{
		System.out.println("M�todo Inserta registro TA_DIARIO");
		Conex03 con= new Conex03();
		StringBuffer lsb_query = new StringBuffer();
		try
		{
			con.conexionDB();
			lsb_query = lsb_query.delete(0, lsb_query.length());
			lsb_query.append("INSERT INTO TA_DIARIO ");
			lsb_query.append("(CVE_PTOVTA, ").append("FECHA_DIA, ").append("REFERENCIA, ").append("CVE_DIVISA, ");
			lsb_query.append("CVE_OPERACION, ").append("CVE_CUSTODIO, ").append("FECHA_PROCESO, ").append("COD_ERROR, ");
			lsb_query.append("EN_LINEA, ").append("IMPORTE, ").append("ESTADO, ").append("CVE_CUSTODIO2, ");
			lsb_query.append("TERMINAL, ").append("REFER_ORIG, ").append("ARRASTRE_EFVO, ").append("OBSERVACION) ");
			lsb_query.append("VALUES ('"+valores.getCve_ptovta()+"', ").append("TO_DATE('"+valores.getFecha_dia()+"','MM/DD/YYYY HH24:MI:SS'), ");
			lsb_query.append(valores.getReferencia()+", ").append("'"+valores.getCve_divisa()+"', ");
			lsb_query.append("'"+valores.getCve_operacion()+"', ").append("'"+valores.getCve_custodio()+"', ");
			lsb_query.append("SYSDATE, ").append("'"+valores.getCod_error()+"', ");
			lsb_query.append("'"+valores.getEn_linea()+"', ").append("TO_NUMBER('"+valores.getImporte()+"'), ");
			lsb_query.append("'"+valores.getEstado()+"', ").append("'"+valores.getCve_custodio2()+"', ");
			lsb_query.append("'"+valores.getTerminal()+"', ").append("TO_NUMBER('"+valores.getRefer_orig()+"'), ");
			lsb_query.append("TO_NUMBER('"+valores.getArrastre_efvo()+"'), ").append("'"+valores.getObservacion()+"') ");
			System.out.println("lsb_query: "+lsb_query.toString());
			try
			{
				con.ejecutaSQL(lsb_query.toString());
				System.out.println("al final de ejecutar el query");
				con.terminaTransaccion(true);
			}
			catch (SQLException e1)
			{
				con.cierraConexionDB();
				System.out.println("Error in " + getClass().getName() + "\n" + e1);
				bitacora.add(linea);
				bitacora.add(e1.toString());
			}
		}
		catch (SQLException e1)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
			con.cierraConexionDB();
			bitacora.add(linea);
			bitacora.add(e1.toString());
		}
		catch(Exception e2)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e2);
			bitacora.add(linea);
			bitacora.add(e2.toString());
		}
		finally
		{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}
/** El m�todo insertaTa_Inv_Amon(ArrayList, CargaValue, String) inserta registro de Ta_Inv_Amon
*   @param bitacora Arreglo donde se agregan los errores
*   @param valores Arreglo de datos a insertar
*   @param linea N�mero de linea
*/
	private void insertaTa_Inv_Amon(ArrayList bitacora, CargaValue valores, String linea)
	{
		System.out.println("M�todo Inserta registro TA_INV_AMON");
		Conex03 con= new Conex03();
		StringBuffer lsb_query = new StringBuffer();
		try
		{
			con.conexionDB();
			lsb_query = lsb_query.delete(0, lsb_query.length());
			lsb_query.append("INSERT INTO TA_INV_AMON ");
			lsb_query.append("(CVE_PTOVTA, ").append("FECHA_DIA, ").append("REFERENCIA, ").append("TIPO, ");
			lsb_query.append("DENOMINACION, ").append("REFER_ORIG, ").append("CANTIDAD) ");
			lsb_query.append("VALUES ('"+valores.getCve_ptovta()+"', ").append("TO_DATE('"+valores.getFecha_dia()+"','MM/DD/YYYY HH24:MI:SS'), ");
			lsb_query.append(valores.getReferencia()+", ").append("'"+valores.getTipo()+"', ");
			lsb_query.append(valores.getDenominacion()+", ").append("TO_NUMBER('"+valores.getRefer_orig()+"'), ");
			lsb_query.append("TO_NUMBER('"+valores.getCantidad()+"')) ");
			System.out.println("lsb_query: "+lsb_query.toString());
			try
			{
				con.ejecutaSQL(lsb_query.toString());
				System.out.println("al final de ejecutar el query");
				con.terminaTransaccion(true);
			}
			catch (SQLException e1)
			{
				con.cierraConexionDB();
				System.out.println("Error in " + getClass().getName() + "\n" + e1);
				bitacora.add(linea);
				bitacora.add(e1.toString());
			}
		}
		catch (SQLException e1)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
			con.cierraConexionDB();
			bitacora.add(linea);
			bitacora.add(e1.toString());
		}
		catch(Exception e2)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e2);
			bitacora.add(linea);
			bitacora.add(e2.toString());
		}
		finally
		{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}
/** El m�todo insertaTa_Sucu_Efectivo(ArrayList, CargaValue, String) inserta registro de Ta_Sucu_Efectivo
*   @param bitacora Arreglo donde se agregan los errores
*   @param valores Arreglo de datos a insertar
*   @param linea N�mero de linea
*/
	private void insertaTa_Sucu_Efectivo(ArrayList bitacora, CargaValue valores, String linea)
	{
		System.out.println("M�todo Inserta registro TA_SUCU_EFECTIVO");
		Conex03 con= new Conex03();
		StringBuffer lsb_query = new StringBuffer();
		try
		{
			con.conexionDB();
			lsb_query = lsb_query.delete(0, lsb_query.length());
			lsb_query.append("INSERT INTO TA_SUCU_EFECTIVO ");
			lsb_query.append("(CVE_PTOVTA, ").append("FECHA_DIA, ").append("IDMOV, ").append("REF_MVTO, ");
			lsb_query.append("CVE_DIVISA, ").append("SDO_INICIAL, ").append("SDO_FINAL, ").append("FCH_APERTURA, ");
			lsb_query.append("FCH_CIERRE, ").append("DESC_MVTO, ").append("DETALLE_MOVTO) ");
			lsb_query.append("VALUES ('"+valores.getCve_ptovta()+"', ").append("TO_DATE('"+valores.getFecha_dia()+"','MM/DD/YYYY HH24:MI:SS'), ");
			lsb_query.append("'"+valores.getIdmov()+"', ").append(valores.getReferencia()+", ");
			lsb_query.append("'"+valores.getCve_divisa()+"', ").append("TO_NUMBER('"+valores.getSdo_inicial()+"'), ");
			lsb_query.append("TO_NUMBER('"+valores.getSdo_final()+"'), ").append("TO_DATE('"+valores.getFch_apertura()+"','MM/DD/YYYY HH24:MI:SS'), ");
			lsb_query.append("TO_DATE('"+valores.getFch_cierre()+"','MM/DD/YYYY HH24:MI:SS'), ").append("'"+valores.getDesc_mvto()+"', ");
			lsb_query.append("'"+valores.getDetalle_mvto()+"') ");
			System.out.println("lsb_query: "+lsb_query.toString());
			try
			{
				con.ejecutaSQL(lsb_query.toString());
				System.out.println("al final de ejecutar el query");
				con.terminaTransaccion(true);
			}
			catch (SQLException e1)
			{
				con.cierraConexionDB();
				System.out.println("Error in " + getClass().getName() + "\n" + e1);
				bitacora.add(linea);
				bitacora.add(e1.toString());
			}
		}
		catch (SQLException e1)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
			con.cierraConexionDB();
			bitacora.add(linea);
			bitacora.add(e1.toString());
		}
		catch(Exception e2)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e2);
			bitacora.add(linea);
			bitacora.add(e2.toString());
		}
		finally
		{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}
/** El m�todo insertaTa_Cheques(ArrayList, CargaValue, String) inserta registro de Ta_Cheques
*   @param bitacora Arreglo donde se agregan los errores
*   @param valores Arreglo de datos a insertar
*   @param linea N�mero de linea
*/
	private void insertaTa_Cheques(ArrayList bitacora, CargaValue valores, String linea)
	{
		System.out.println("M�todo Inserta registro TA_CHEQUES");
		Conex03 con= new Conex03();
		StringBuffer lsb_query = new StringBuffer();
		try
		{
			con.conexionDB();
			lsb_query = lsb_query.delete(0, lsb_query.length());
			lsb_query.append("INSERT INTO TA_CHEQUES ");
			lsb_query.append("(CVE_PTOVTA, ").append("FECHA_DIA, ").append("REFERENCIA, ").append("FOLIO, ");
			lsb_query.append("CUENTA, ").append("VERIFICA_FIRMA, ").append("IMPORTE, ").append("PLAZA, ");
			lsb_query.append("BANCO) ");
			lsb_query.append("VALUES ('"+valores.getCve_ptovta()+"', ").append("TO_DATE('"+valores.getFecha_dia()+"','MM/DD/YYYY HH24:MI:SS'), ");
			lsb_query.append(valores.getReferencia()+", ").append(valores.getFolio()+", ");
			lsb_query.append("'"+valores.getCuenta()+"', ").append("'"+valores.getVerifica_firma()+"', ");
			lsb_query.append("TO_NUMBER('"+valores.getImporte()+"'), ").append("'"+valores.getPlaza()+"', ");
			lsb_query.append("'"+valores.getBanco()+"') ");
			System.out.println("lsb_query: "+lsb_query.toString());
			try
			{
				con.ejecutaSQL(lsb_query.toString());
				System.out.println("al final de ejecutar el query");
				con.terminaTransaccion(true);
			}
			catch (SQLException e1)
			{
				con.cierraConexionDB();
				System.out.println("Error in " + getClass().getName() + "\n" + e1);
				bitacora.add(linea);
				bitacora.add(e1.toString());
			}
		}
		catch (SQLException e1)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
			con.cierraConexionDB();
			bitacora.add(linea);
			bitacora.add(e1.toString());
		}
		catch(Exception e2)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e2);
			bitacora.add(linea);
			bitacora.add(e2.toString());
		}
		finally
		{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}
/** El m�todo estaContingente(String, String) revisa si los datos se encuentran
*   en la secion de contingente en base al punto de venta y fecha
*   @param fecha  Fecha de consulta
*   @param pventa Punto de venta
*   @return  boolean respuesta de si se encuentra el registro
*/
	public boolean estaContingente(String fecha, String pventa)
	{
		System.out.println("M�todo estaContingente");
		StringBuffer lsb_query = new StringBuffer();
		ResultSet rs = null;
		boolean regreso = false;
		Conex03 con= new Conex03();
		lsb_query = lsb_query.delete(0, lsb_query.length());
		lsb_query.append("SELECT CVE_PTOVTA ");
		lsb_query.append("FROM TA_SUC_CONTINGENTE ");
		lsb_query.append("WHERE CVE_PTOVTA = '").append(pventa).append("' ");
		lsb_query.append("AND TO_CHAR(FECHA_DIA, 'MM/DD/YYYY') = '").append(fecha.substring(0,10)).append("' ");
		System.out.println("lsb_query: "+lsb_query.toString());
		try
		{
			con.conexionDB();
			rs=con.queryDB(lsb_query.toString());
			if(rs.next())
				regreso = true;
			if(rs!=null) rs.close();
		}
		catch (SQLException e1)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
			con.cierraConexionDB();
		}
		catch (Exception e2)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e2);
			con.cierraConexionDB();
		}
		finally
		{
			try
			{
				if(rs!=null) rs.close();
				con.cierraConexionDB();
			}
			catch(SQLException e2)
			{
				System.out.println("Error in " + getClass().getName() + "\n" + e2);
			}
		}
		return regreso;
	}
	/** El m�todo estaDiario(String, String) revisa si los datos se encuentran
	*   en la secion de Ta_Diario en base al punto de venta y fecha
	*   @param fecha  Fecha de consulta
	*   @param pventa Punto de venta
	*   @return  boolean respuesta de si se encuentra el registro
	*/
		public boolean estaDiario(String fecha, String pventa)
		{
			System.out.println("M�todo estaDiario");
			StringBuffer lsb_query = new StringBuffer();
			ResultSet rs = null;
			boolean regreso = false;
			Conex03 con= new Conex03();
			lsb_query = lsb_query.delete(0, lsb_query.length());
			lsb_query.append("SELECT CVE_PTOVTA ");
			lsb_query.append("FROM TA_DIARIO ");
			lsb_query.append("WHERE CVE_PTOVTA = '").append(pventa).append("' ");
			lsb_query.append("AND TO_CHAR(FECHA_DIA, 'MM/DD/YYYY') = '").append(fecha.substring(0,10)).append("' ");
			System.out.println("lsb_query: "+lsb_query.toString());
			try
			{
				con.conexionDB();
				rs=con.queryDB(lsb_query.toString());
				if(rs.next())
					regreso = true;
				if(rs!=null) rs.close();
			}
			catch (SQLException e1)
			{
				System.out.println("Error in " + getClass().getName() + "\n" + e1);
				con.cierraConexionDB();
			}
			catch (Exception e2)
			{
				System.out.println("Error in " + getClass().getName() + "\n" + e2);
				con.cierraConexionDB();
			}
			finally
			{
				try
				{
					if(rs!=null) rs.close();
					con.cierraConexionDB();
				}
				catch(SQLException e2)
				{
					System.out.println("Error in " + getClass().getName() + "\n" + e2);
				}
			}
			return regreso;
		}
	/** El m�todo mueveConHis(String, String) mueve el registro de Contingentes a Historico
	*   @param fecha  Fecha de consulta
	*   @param pventa Punto de venta
	*/
		public void mueveConHis(String fecha, String pventa)
		{
			System.out.println("M�todo mueveConHis");
			Conex03 con= new Conex03();
			StringBuffer lsb_query = new StringBuffer();
			try
			{
				lsb_query = lsb_query.delete(0, lsb_query.length());
				lsb_query.append("SELECT CVE_PTOVTA ");
				lsb_query.append("FROM TA_SUC_CONST_HIST ");
				lsb_query.append("WHERE CVE_PTOVTA = '").append(pventa).append("' ");
				lsb_query.append("AND TO_CHAR(FECHA_DIA, 'MM/DD/YYYY') = '").append(fecha.substring(0,10)).append("' ");
				System.out.println("lsb_query: "+lsb_query.toString());
				ResultSet rs = null;
				boolean esta = false;
				con.conexionDB();
				rs=con.queryDB(lsb_query.toString());
				if(rs.next())
					esta = true;
				if(esta)
				{
					lsb_query = lsb_query.delete(0, lsb_query.length());
					lsb_query.append("UPDATE TA_SUC_CONST_HIST ");
					lsb_query.append("SET FECHA_INTENTO = SYSDATE, NUM_INTENTOS = NUM_INTENTOS+1, ");
					lsb_query.append("MANUAL = '2' ");
					lsb_query.append("WHERE CVE_PTOVTA = '").append(pventa).append("' ");
					lsb_query.append("AND TO_CHAR(FECHA_DIA, 'MM/DD/YYYY') = '").append(fecha.substring(0,10)).append("' ");
					System.out.println("lsb_query: "+lsb_query.toString());
					try
					{
						con.ejecutaSQL(lsb_query.toString());
						con.terminaTransaccion(true);
					}
					catch (SQLException e1)
					{
						con.cierraConexionDB();
						System.out.println("Error in " + getClass().getName() + "\n" + e1);
					}
				}
				else
				{
					lsb_query = lsb_query.delete(0, lsb_query.length());
					lsb_query.append("INSERT INTO TA_SUC_CONST_HIST( ");
					lsb_query.append("SELECT CVE_PTOVTA, FECHA_DIA, SYSDATE, NUM_INTENTOS+1, DESC_SISTEMA, DESC_USUARIO, '2' ");
					lsb_query.append("FROM TA_SUC_CONTINGENTE ");
					lsb_query.append("WHERE CVE_PTOVTA = '").append(pventa).append("' ");
					lsb_query.append("AND TO_CHAR(FECHA_DIA, 'MM/DD/YYYY') = '").append(fecha.substring(0,10)).append("') ");
					System.out.println("lsb_query: "+lsb_query.toString());
					try
					{
						con.ejecutaSQL(lsb_query.toString());
						con.terminaTransaccion(true);
					}
					catch (SQLException e1)
					{
						con.cierraConexionDB();
						System.out.println("Error in " + getClass().getName() + "\n" + e1);
					}
				}
				if(rs!=null) rs.close();
				lsb_query = lsb_query.delete(0, lsb_query.length());
				lsb_query.append("DELETE TA_SUC_CONTINGENTE ");
				lsb_query.append("WHERE CVE_PTOVTA = '").append(pventa).append("' ");
				lsb_query.append("AND TO_CHAR(FECHA_DIA, 'MM/DD/YYYY') = '").append(fecha.substring(0,10)).append("' ");
				System.out.println("lsb_query: "+lsb_query.toString());
				try
				{
					con.ejecutaSQL(lsb_query.toString());
					con.terminaTransaccion(true);
				}
				catch (SQLException e1)
				{
					con.cierraConexionDB();
					System.out.println("Error in " + getClass().getName() + "\n" + e1);
				}
			}
			catch (SQLException e1)
			{
				System.out.println("Error in " + getClass().getName() + "\n" + e1);
				con.cierraConexionDB();
			}
			catch(Exception e2)
			{
				System.out.println("Error in " + getClass().getName() + "\n" + e2);
			}
			finally
			{
				if(con.hayConexionAbierta())
					con.cierraConexionDB();
			}
		}
	/**
	 * @return
	 */
	public int getLugar_registro()
	{
		return lugar_registro;
	}

	/**
	 * @param i
	 */
	public void setLugar_registro(int i)
	{
		lugar_registro = i;
	}

}