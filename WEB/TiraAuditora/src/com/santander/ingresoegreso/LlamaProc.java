package com.santander.ingresoegreso;

/** Banco Santander Mexicano
*   Clase LamaProc  Realiza llamada a procedimientos almacenados
*   @author Ing. Angel Gabriel Ramirez Alva
*   @version 1.0
*   fecha de creacion : 6 de Marzo del 2006
*   responsable : Eloisa Hernandez H
*   descripcion : Contiene metodos que llaman a procedimientos almacenados para la creaci�n de archivos
*   modificacion :
*/

import com.santander.utilerias.*;
import java.util.*;
import java.sql.*;

public class LlamaProc
{
	private LoadProperties rb = LoadProperties.getInstance();  
	
/* El metodo llama al store procedure que genera los archivos diarios
 * de ingresos y egresos
 */
public void llamaStor2(String tipo, String fecha)
	throws SQLException
{
   System.out.println("Metodo llamaSotor2 Genera archivos Diarios");
   Connection con = null;
   CallableStatement proc = null;
   ConexionDAO conexion= new ConexionDAO();

   try
   {
		 System.out.println(tipo);
		 fecha = fecha.replace('/','-');
		 System.out.println(fecha);
		 con  = conexion.getConexion();
		 proc = con.prepareCall("{ call GeneraMov_Sdos(?, ?) }");
		 proc.setString(1, tipo);
		 proc.setString(2, fecha);
		 proc.execute();

   }
   finally
   {
	  try
	  {
		 proc.close();
	  }
	  catch (SQLException e) {}
	  con.close();
   }

}
/* El metodo llama al store procedure que genera los archivos contingentes
 * de ingresos y egresos en base a una fecha especifica
 */
public void llamaStor1(String tipo, String  fecha)
	throws SQLException
{
   System.out.println("Metodo 'llamaSotor1' Genera archivos Contingentes");
   Connection con = null;
   CallableStatement proc = null;
   ConexionDAO conexion= new ConexionDAO();

   try
   {
	  System.out.println(tipo);
	  fecha = fecha.replace('/','-');
	  System.out.println(fecha);
	  con  = conexion.getConexion();
	  proc = con.prepareCall("{ call GeneraMov_Sdos_Contin(?, ?) }");
	  proc.setString(1, tipo);
	  proc.setString(2, fecha);
	  proc.execute();
   }
   finally
   {
	  try
	  {
		 proc.close();
	  }
	  catch (SQLException e) {}
	  con.close();
   }
}


   /**  El m�todo sModificaSucursal(Vector lvDatos) realiza la modificaci�n de un registro en la base de datos,
		**  los parametros llegan en forma de vector: lvDatos
	   */
	 public String sModificaClave(Vector lvDatos)
	   {
		 System.out.println("M�todo Modifica clave");
		 Conex03 con= new Conex03();

		 String  sResultado = "1";
		 int NumElementos = lvDatos.size();
		 StringBuffer lsbquery = new StringBuffer();
		 DesEnc transf = new DesEnc();
		 String usr = rb.getValue("ie.usr");

		 try
		   {
		   	 usr = transf.DesEncriptarCadena(usr);
			 if(usr.equals(lvDatos.elementAt(1))){
				 String pass = transf.EncriptarCadena((String)lvDatos.elementAt(2));
				 usr = transf.EncriptarCadena(usr);
				 con.conexionDB();
				 lsbquery = lsbquery.delete(0, lsbquery.length());
				 lsbquery.append(" UPDATE TA_USUARIOS ");
				 lsbquery.append(" SET PERFIL = '"+pass+"', ");
				 lsbquery.append(" fec_inac  = sysDATE ");
				 lsbquery.append(" WHERE USUARIO = '"+usr+"' ");
				 try
				   {
					 System.out.println(lsbquery);
					 con.ejecutaSQL(lsbquery.toString());
					 System.out.println("al final de ejecutar el query");
					 sResultado = "0";
					 con.terminaTransaccion(true);
				   }
					 catch(Exception e)
					   {
						 con.cierraConexionDB();
						 System.out.println("Error en query: " + lsbquery.toString());
						 e.printStackTrace();
					   }
		     }
		   }catch (Exception e)
			 {
			   e.printStackTrace();
			   con.cierraConexionDB();
			 }
		   finally
			 {
			   if(con.hayConexionAbierta())
				 {
				   con.cierraConexionDB();
				 }
			 }
	   return sResultado; //devuelve
	 }//fin sModificaClave
/*****************************************************************************/
/**  El m�todo sModificaSucursal(Vector lvDatos) realiza la modificaci�n de un registro en la base de datos,
	   **  los parametros llegan en forma de vector: lvDatos
	  */
	public String sBuscaClave(String sUsr)
	  {
		System.out.println("M�todo trae la clave");
		Conex03 con= new Conex03();

		String  sResultado = "1";
		StringBuffer lsbquery = new StringBuffer();

		try
		  {
				con.conexionDB();
				lsbquery = lsbquery.delete(0, lsbquery.length());
				lsbquery.append(" SELECT  PERFIL ");
				lsbquery.append(" FROM  TA_USUARIOS ");
				lsbquery.append(" WHERE fec_inac = TO_DATE('"+sUsr+"', 'DD/MM/YYYY') ");
				try
				  {
					System.out.println(lsbquery);
					sResultado = con.ejecutaQueryCom(lsbquery.toString());
					System.out.println("al final de ejecutar el query");
					con.terminaTransaccion(true);
				  }
					catch(Exception e)
					  {
						con.cierraConexionDB();
						System.out.println("Error en query: " + lsbquery.toString());
						e.printStackTrace();
					  }
			}
		  catch (Exception e)
			{
			  e.printStackTrace();
			}
		  finally
			{
			  if(con.hayConexionAbierta())
				{
				  con.cierraConexionDB();
				}
			}
	  return sResultado; //devuelve
	}//fin sModificaClave

/*************************************************************/
public String sBuscaClave2(String sUsr)
	  {
		System.out.println("M�todo trae la clave");
		Conex03 con= new Conex03();

		String  sResultado = "1";
		StringBuffer lsbquery = new StringBuffer();

		try
		  {
				con.conexionDB();
				lsbquery = lsbquery.delete(0, lsbquery.length());
				lsbquery.append(" SELECT  usuario ");
				lsbquery.append(" FROM  TA_USUARIOS ");
				lsbquery.append(" WHERE fec_inac = TO_DATE('"+sUsr+"', 'DD/MM/YYYY') ");
				try
				  {
					System.out.println(lsbquery);
					sResultado = con.ejecutaQueryCom(lsbquery.toString());
					System.out.println("al final de ejecutar el query");
					con.terminaTransaccion(true);
				  }
					catch(Exception e)
					  {
						con.cierraConexionDB();
						System.out.println("Error en query: " + lsbquery.toString());
						e.printStackTrace();
					  }
			}
		  catch (Exception e)
			{
			  e.printStackTrace();
			}
		  finally
			{
			  if(con.hayConexionAbierta())
				{
				  con.cierraConexionDB();
				}
			}
	  return sResultado; //devuelve
	}//fin 2



}//fin clase