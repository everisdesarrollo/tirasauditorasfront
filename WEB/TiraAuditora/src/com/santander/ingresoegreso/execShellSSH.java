/** Banco Santander Mexicano
*   Clase execShellSSH  Realiza llamada a un Shell via SSH
*   @author Rodrigo S�nchez
*   @version 1.0
*   fecha de creacion : 24 de Mayo del 2006
*   responsable : Eloisa Hernandez H
*   descripcion : Ejecuta un Shell que busca como respuesta la palabra TAUD para saber que el proceso se
*         ejecut� satisfactoriamente
*   modificacion :
*/

package com.santander.ingresoegreso;
import java.io.BufferedReader;
import java.io.InputStreamReader;


public class execShellSSH
{
	/**
	 * Realiza la ejecuci�n del comando recibido por String cmd, busca en la salida
	 * de la consola si existe la palabra TAUD para saber que se ejcuto el shell
	 * con �xito, caso contrario retorna cadena vacia
	 ***/
 public String processCmd (String cmd) throws java.io.IOException{
    String strRes="";
	  try{
        Process proc = Runtime.getRuntime().exec( cmd );
        BufferedReader input = new BufferedReader(new InputStreamReader(proc.getInputStream()));
        BufferedReader input2 = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
        String line = "";

        while((line = input.readLine()) != null){
          if(line.indexOf("TAUD")>-1){
             strRes=line.substring(line.indexOf("TAUD"),4);
           }
        }

        while((line = input2.readLine()) != null){
            System.out.println("stderr: "  + line);
         }
        if(strRes.length()==0){
         strRes="1";
        }
        proc.destroy();
        input.close();
        input2.close();
        line=null;

     	}catch( Exception ioe ){
          System.out.println( "[Tiras Auditoras]Error en execShellSSH.processCmd : " + ioe.getMessage() );
      	}
       return strRes;
	  	} //fin processCmd


}//fin clase