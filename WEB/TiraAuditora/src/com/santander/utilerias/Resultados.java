/** Banco Santander Mexicano
*   Clase Resultados
*   @author Alfredo Resendiz vargas
*   @version 1.0
*   fecha de creacion : 15 de Marzo del 2006
*   responsable : Eloisa Hernandez Hernandez
*   descripcion : M�todo creado para optimizar el codifo de la clase ManCtgClase
*   modificacion :
*/

package com.santander.utilerias;

import com.santander.contingente.*;
import com.santander.utilerias.*;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.IOException;
import javax.naming.*;

/** El m�todo consulTotManCtg() obtiene la consulta de todas
 * las sucursales contingentes.
 *   @return  ArrayList valor de respuesta de la consulta
 */
public class Resultados
{

  public ArrayList result_consul(String bufer)
  {
//	Se realiza la conexi�n a la BD
	ConexionDAO conexion= new ConexionDAO();
	Connection con;
	ArrayList resultado = new ArrayList();
	ArrayList res_interno = new ArrayList();
	PreparedStatement query = null;
	StringBuffer lsbQuery = new StringBuffer();
	ResultSet rs = null;

	int j = 0;
	try
	{
	  con = conexion.getConexion();
	  query = con.prepareStatement(bufer);
	  rs=query.executeQuery();
	  int i=0;
//	  Se recuperan los valores de la consulta
	  while(rs.next())
	  {
		ManCtgValue valores = new ManCtgValue();
		valores.set_suc_altair(rs.getString("CVE_PTOVTA"));
		valores.set_fecha(rs.getString("FECHA_DIA"));
		valores.set_nom_sucursal(rs.getString("DESCRIPCION"));
		valores.set_desc_sistema(rs.getString("DESC_SISTEMA"));
		valores.set_desc_usuario(rs.getString("DESC_USUARIO"));
    valores.set_num_intentos(Integer.parseInt(rs.getString("NUM_INTENTOS")));
    valores.set_verificacion_sis(rs.getString("MANUAL"));
		res_interno.add(valores);
		j++;
		if(j==100)
		{
		  resultado.add(res_interno);
		  res_interno = new ArrayList();
		  j=0;
		}
	  }
	  if(j!=0)
	  {
		resultado.add(res_interno);
	  }
	}
	catch (Exception e1)
	{
	  e1.printStackTrace();
	}
	finally
	{
	  try
	  {
		rs.close();
		query.close();
		conexion.closeConexion();
	  }
	  catch(SQLException e2)
	  {
		System.out.println("Error in " + getClass().getName() + "\n" + e2);
	  }
	}
	return resultado;
  }
}