/** Banco Santander Mexicano
*   Clase ConexionDAO
*   @author Angel Gabriel Ramirez Alva
*   @version 1.0
*   fecha de creacion : 2 de Febrero del 2006
*   responsable : Eloisa Hernandez Hernandez
*   descripcion : Conecta a la base de datos
*   modificacion :
*/

package com.santander.utilerias;

import java.io.PrintStream;
import java.sql.*;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ConexionDAO
  {
    public ConexionDAO()
      {
        conexion1 = null;
      }
    public Connection getConexion() throws SQLException
    {
    //27/06/2008 ESC se valida si hay una conexion abierta para reutilizarla
    if(conexion1 == null || conexion1.isClosed())
    {
      try
        {
          InitialContext ic = new InitialContext();
          DataSource ds = (DataSource)ic.lookup("java:comp/env/db");
          // 27/06/2008 ESC se quita la invocacion doble de conexion.
          //ds.getConnection();
          conexion1 = ds.getConnection();
        }
      catch(NamingException e)
        {
          e.printStackTrace();
          System.out.println("Error en el contexto inicial o en el nombre del JNDI" + e);
        }
      catch(SQLException e)
        {
          e.printStackTrace();
          System.out.println("Error al abrir la conexion" + e);
        }
    }

      return conexion1;
    }

    public void closeConexion()
    {
    if(conexion1 !=null)
    {
      try
        {
          conexion1.close();
        }
        catch(SQLException e)
        {
          e.printStackTrace();
          System.out.println("Error al cerra la conexion" + e);
        }
    }
    conexion1 = null;
    }
    Connection conexion1;
}