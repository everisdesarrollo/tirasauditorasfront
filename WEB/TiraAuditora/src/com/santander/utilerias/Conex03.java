package com.santander.utilerias;
import java.sql.*;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Descripcion: Es un objeto que proporciona la conexion
 * @version 	1.0
 * Fecha: 07/02/2006
 * @author: Angel Ramirez
 * call:
 */
public class Conex03 {

  public Connection getConexion() throws NamingException, SQLException
	{
	  Connection conexion = null;
	  try
	    {
		  InitialContext ic = new InitialContext();
		  javax.sql.DataSource ds = (javax.sql.DataSource) ic.lookup("java:comp/env/db");
		  ds.getConnection();
		  conexion = ds.getConnection();
		}
		catch(NamingException e)
		  {
		    e.printStackTrace();
			System.out.println("Error en el contexto inicial o en el nombre del JNDI"+e);
		  }
	  catch(SQLException e)
	    {
		  e.printStackTrace();
		  System.out.println("Error al abrir la conexion"+e);
		}
	return conexion;
  }

  public void closeConexion(Connection conexion)throws SQLException
	{
	  try
	    {
		  conexion.close();
		}
	  catch(SQLException e)
	    {
		  System.out.println("Error al cerra la conexion"+e);
		}
	}
	/*
	 * * Descripcion: Es un objeto que sirve que proporciona la conexion
	*********@author: Angel Gabriel Ramierz Alva
	*/
  public void conexionDB()
			throws SQLException, NamingException
	{
	  String pool_jdbc = "java:comp/env/db";
  	  javax.naming.InitialContext naming = new javax.naming.InitialContext();
	  javax.sql.DataSource ds = (javax.sql.DataSource) naming.lookup(pool_jdbc);
	  m_conn = ds.getConnection();
	  m_conn.setAutoCommit(false);
	  m_conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	}

	/**
	 * Ejecuta una consulta dinamicamente regresando un objeto de tipo ResultSet
	 * @param query Se refiere a la consulta que deseamos ejecutar
	 * @return ResultSet
	 *@exception SQLException
	 *@author: Angel Gabriel Ramierz Alva
	 **/
  public ResultSet queryDB(String query) throws SQLException
    {
	  try
	    {
		  stm = m_conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
		  ResultSet.CONCUR_READ_ONLY);
		  ResultSet rs = stm.executeQuery(query);
		  return rs;
		}
		catch (SQLException e)
		  {
		    error.append(e.toString()+" Query:*****" + query + "*****");
			System.out.println("Error en el query:\n\n"+query);
			throw e;
		  }
	}

	/**
	 * Ejecuta instrucciones de SQL DML dinamicamente como INSERT, DELETE, UPDATE.
	 *@param sql La cadena DML que deseamos se ejecute.
	 *@exception SQLException
	 **@author: Angel Gabriel Ramierz Alva
	 */
  public void ejecutaSQL(String sql) throws SQLException
    {
	  try
	    {
		  Statement stm = m_conn.createStatement();
		  stm.executeUpdate(sql);
		  stm.close();
		  //return lsNumRegs;
		}
	  catch (SQLException e)
	    {
		  System.out.println("ejecutaSQL(). Error SQL: " + e.toString());
		  System.out.println(" Query:*****"+sql+"*****");
		  error.append(e.toString()+" Query:*****"+sql+"*****");
		  throw e;
		}
	}
	/**
	 * Regresa un objeto de tipo Connection
	 *@return Connection
	 **@author: Angel Gabriel Ramierz Alva
	 */
  public Connection getConnection()
    {
	  return this.m_conn;
	}
	/**  Cierra la conexion a la base de datos
	 * *@author: Angel Gabriel Ramierz Alva
	 *  */
  public void cierraConexionDB()
    {
	  try
	    {
		  if (m_conn != null)
		    {
			  m_conn.rollback();
			  m_conn.close();
			}
		  else
		    {
			  System.out.println("cierraConexion(). No existe ninguna conexion.");
			}
		}
	  catch (SQLException e)
	    {
		  System.out.println("cierraConexion(). Error SQL "+ e.toString());
		}
	m_conn = null;
	}
  /**  Cierra el objeto Statement creado con la conexi�n
	*@exception SQLException
	**@author: Angel Gabriel Ramierz Alva
	*/
  public void cierraStatement() throws SQLException
    {
	  try
	    {
		  stm.close();
		}
	  catch(SQLException sqle)
	    {
		  error.append("Error al Terminar la Sentencia: "+sqle.getMessage());
		  System.out.println("Error SQL en cierraStatement(): " + error.toString());
		  throw sqle;
		}
	}

	/**  El m�todo indica si hay una conexion abierta o no.
	 *@return boolean
	 **@author: Angel Gabriel Ramierz Alva
	 */
  public boolean hayConexionAbierta()
    {
	  if (m_conn != null)
	    {
		  return true;
		}
	  else
	    {
		  return false;
		}
	}
	/**
	 * Ejecuta un query regresando una cadena que forma un String separado con comas
	 * con los datos recuperados
	 *@param query La consulta a realizar
	 *@return String
	 *@exception SQLException
	 **@author: Angel Gabriel Ramierz Alva
	 **/
  public String ejecutaQueryCom (String query) throws SQLException
    {
	  String slCadenaXml="";
	  ResultSet grResultSet;
	  try
	  	{
		  grResultSet = queryDB (query);
		  slCadenaXml = mRetornaComas(grResultSet);
		}
	  catch(SQLException e)
	    {
		  System.out.println("Error en ejecutaSQL: "+ e.toString());
		}
	return slCadenaXml;
	}

	/**
	 * Termina la transacci�n de la base de datos conectada, si pasamos el parametro
	 * como verdadero.
	 *@param todo_ok valor booleando, si es verdadero se detiene la transacci�n a la base de datos
	 **@author: Angel Gabriel Ramierz Alva
	 **/
  public void terminaTransaccion(boolean todo_ok)
    {
	  try
	    {
		  if (todo_ok)
		    {
			  m_conn.commit();
			  System.out.println("terminaTransaccion(). Commit");
		    }
		  else
		    {
		      m_conn.rollback();
			  System.out.println("terminaTransaccion(). Rollback");
		    }
	     }
	  catch (SQLException e)
	    {
		  System.out.println("ejecutaSQL(). Error SQL: " + e.toString());
	    }
	}


	/**
	 * Regresa una cadena de datos separada por comas de acuerdo a un objeto ResultSet pasado
	 *@param grResultSet
	 *@return String
	 **@author: Angel Gabriel Ramierz Alva
	 */
  public String  mRetornaComas(ResultSet grResultSet)
    {
	  ResultSetMetaData grResMD = null;
	  int giCol =0;
	  int a = 0;
	  String valor;
	  StringBuffer lsbCadXml = new StringBuffer();
	  StringBuffer    lsbTemporal = new StringBuffer();
	  try
	    {
		  if (grResultSet!=null)
		    {
			  grResMD = grResultSet.getMetaData();
			  giCol = grResMD.getColumnCount();
			  while (grResultSet.next())
			    {
				  a = 1;
				  lsbTemporal.delete(0,lsbTemporal.length());
				  for (int i = 1; i <= giCol; i++)
				    {
					  valor = grResultSet.getString(i);
					  if (valor == null)
						valor = ".";
						lsbTemporal.append(valor.trim() + "|");
					}
				  lsbCadXml.append(lsbTemporal);
				}
				if (a == 0)
				  {
				 	lsbCadXml.append("NO EXISTEN DATOS");
				  }
		    }
			grResultSet.close();
		}
		catch(Exception e)
		  {
			System.out.println("Hubo un Error en Clase 'CconexionDB'"+ e);
			lsbCadXml.append("Error en ArmaComas" + e);
		  }
	return lsbCadXml.toString();
  }

  private Connection		m_conn;
  private Statement 		stm;
  private String			pool_jdbc;
  private StringBuffer	error=new StringBuffer();
}