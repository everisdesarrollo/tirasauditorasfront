/*****************************************************
 *
 * Nombre de Clase:   AccesoDomino
 *
 * Version:           1.0
 *
 * Fecha de Creaci�n:
 *
 * Autor: Netro/Angel Gabriel Ramirez Alva
 *
 * Descripci�n de Clase:    Clase utilizada para realizar llamadas a un servlet
 *
 ****************************************************/
package com.santander.utilerias;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Vector;

public class AccesoServlet {
		private LoadProperties loadProperties = LoadProperties.getInstance();
        private URLConnection m_oConnexion = null;
    	private String m_paquete = "TiraAuditora";
        private String m_sURL = loadProperties.getValue("as.url")+"/"+m_paquete; 


 	public AccesoServlet() {
        }

	public void conexionServlet(String vsServlet) throws Exception{
		try {
        		URL servidorURL = new URL( m_sURL);
			//URL ServletURL = new URL( servidorURL, vsServlet);
			int indice = 0;
			String lsDirServlet = new String();
			String lsParServlet = new String();
			lsParServlet = vsServlet;
			/*
			while (1==1){
				indice = lsParServlet.indexOf("/");
				if (indice < 0)
					break;
				else
					lsParServlet = lsParServlet.substring(indice + 1, lsParServlet.length());
			}
			*/
			lsParServlet = lsParServlet.substring(lsParServlet.lastIndexOf("/")+ 1, lsParServlet.length());

			lsDirServlet = "TiraAuditora/servlet/" + lsParServlet;

		    URL ServletURL = new URL( servidorURL, lsDirServlet);

			m_oConnexion = ServletURL.openConnection();
			//No usar cache
			m_oConnexion.setUseCaches (false);
			m_oConnexion.setDefaultUseCaches (false);
			m_oConnexion.setDoOutput(true);
			//setDoOutput(true);

                        // Especifica que el tipo de contenido a enviar es binario
			m_oConnexion.setRequestProperty ("Content-Type", "application/octet-stream");

   		} catch (MalformedURLException em) {
//			System.err.println("Error en AccesoServlet::conexionServlet " + em.getMessage());
                        throw new Exception(em.getMessage());
                } catch (IOException e1){
//			System.out.println("Error en AccesoServlet::conexionServlet al conectar con el servlet" + e1.getMessage());
                        throw new Exception(e1.getMessage());
		}
      }

	public void conexionServlet(String esURL, String vsServlet) throws Exception{
		try {
        		URL servidorURL = new URL( esURL);
			URL ServletURL = new URL( servidorURL, vsServlet);

			System.out.println("URL: " + ServletURL);

			m_oConnexion = ServletURL.openConnection();
			//No usar cache
			m_oConnexion.setUseCaches (false);
			m_oConnexion.setDefaultUseCaches (false);
			m_oConnexion.setDoOutput(true);
			//setDoOutput(true);

                        // Especifica que el tipo de contenido a enviar es binario
			m_oConnexion.setRequestProperty ("Content-Type", "application/octet-stream");

   		} catch (MalformedURLException em) {
			System.err.println("Error en AccesoServlet::conexionServlet " + em.getMessage());
                        throw new Exception(em.getMessage());
                } catch (IOException e1){
			System.out.println("Error en AccesoServlet::conexionServlet al conectar con el servlet" + e1.getMessage());
                        throw new Exception(e1.getMessage());
		}

      }

      // Llama al servlet {in/out}
      public Object LlamaServlet(String vsServlet, Vector voParametros)
      	 throws Exception
      {
          Object resp = null;
          int liObjetos = 0;

//		  String lsResultado,   lsMensaje;

          ObjectOutputStream out = null;
          ObjectInputStream input = null;


          try {
//       		System.out.println("Conectando al Servlet");
            	conexionServlet(vsServlet);
//       		System.out.println("Llamado del Servlet");

		// Envia la clave del producto al servlet
//		System.out.println("Escribe los valores");
		    out = new ObjectOutputStream(m_oConnexion.getOutputStream());
		    int tam =  voParametros.size();
            for(liObjetos=0;liObjetos < tam ;liObjetos++){
            	  out.writeObject(voParametros.elementAt(liObjetos));
            }

//                System. out.println("Envi� parametros al servlet");

		/** Obtener respuesta del servlet **/
//		System.out.println("Declaracion para obtener Respuestas");
//            InputStream in = m_oConnexion.getInputStream();
			input = new ObjectInputStream(m_oConnexion.getInputStream());

//                System.out.println("Obtiene Respuesta");

         	resp = input.readObject();

          } catch (IOException er)  {
               System.out.println("Error en AccesoServlet::LlamaServlet " + er.getMessage());
               throw new Exception(er.getMessage());
          } catch (ClassNotFoundException ex) {
               System.out.println("Error en AccesoServlet::LlamaServlet " + ex.getMessage());
               throw new Exception(ex.getMessage());
          } catch (Exception e) {
               System.out.println("Error en AccesoServlet::LlamaServlet " + e.getMessage());
               throw new Exception(e.getMessage());
          } finally {
		out.flush();
		out.close();
      		input.close();
          }

          return resp;
      }

}
