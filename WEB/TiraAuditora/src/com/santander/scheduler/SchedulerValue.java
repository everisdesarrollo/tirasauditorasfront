package com.santander.scheduler;

public class SchedulerValue {
	private String jobName;
	private String actualStartDate;
	private String runDuration;
	private String status;
	private String additionalInfo;
	private String error;
	private int runCount;

	public int getRunCount() {
		return runCount;
	}
	public void setRunCount(int runCount) {
		this.runCount = runCount;
	}
	public SchedulerValue() {
	}
	public SchedulerValue(String jobName) {
		this.jobName = jobName;
	}
	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getActualStartDate() {
		return actualStartDate;
	}

	public void setActualStartDate(String actualStartDate) {
		this.actualStartDate = actualStartDate;
	}

	public String getRunDuration() {
		return runDuration;
	}

	public void setRunDuration(String runDuration) {
		this.runDuration = runDuration;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}


	/**
	 * @param args
	 */
	public static void main(String[] args) {


	}

}