package com.santander.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.santander.utilerias.ConexionDAO;

public class SchedulerClase {

	public SchedulerClase(){

	}
	/**
	 * VCM 23/07/12 El m�todo consulDescSistema() obtiene los distintos tipos de
	 * error de las sucursales contingentes.
	 *
	 * @return ArrayList valor de respuesta de la consulta
	 */
	public ArrayList<ArrayList<SchedulerValue>> readJobs(String iniDate) {
		System.out.println("Consulta para el monitoreo de scheduler jobs pendientes");
		// Se realiza la conexi�n a la BD
		ConexionDAO conexion = new ConexionDAO();
		Connection con = null;
		ArrayList<ArrayList<SchedulerValue>> resultado = new ArrayList<ArrayList<SchedulerValue>>();
		ArrayList<SchedulerValue> resInterno = new ArrayList<SchedulerValue>();
		PreparedStatement query = null;
		StringBuffer lsbQuery = new StringBuffer();
		ResultSet rs = null;
		int j = 0;
		// Consulta de scheduler jobs

		lsbQuery = lsbQuery.delete(0, lsbQuery.length());
		lsbQuery.append(" SELECT JOB_NAME AS NOMBRE_JOB, TO_CHAR(START_DATE, 'DD/MM/YYYY HH24:MI:SS') AS FECHA, STATE AS ESTATUS, RUN_COUNT NUMERO_EJECUCIONES ");
		lsbQuery.append(" FROM ALL_SCHEDULER_JOBS ");
		lsbQuery.append(" WHERE STATE = 'DISABLED' AND TO_CHAR(START_DATE, 'DD/MM/YYYY') = '").append(iniDate).append("' ");
		lsbQuery.append(" ORDER BY START_DATE ");
		System.out.println("lsbQuery: " + lsbQuery.toString());

		try {
			con = conexion.getConexion();
			query = con.prepareStatement(lsbQuery.toString());
			rs = query.executeQuery();
			while (rs.next()) {
				SchedulerValue valores = new SchedulerValue(rs.getString("NOMBRE_JOB"));
				valores.setActualStartDate(rs.getString("FECHA"));
				valores.setStatus(rs.getString("ESTATUS"));
				valores.setAdditionalInfo(rs.getString("NUMERO_EJECUCIONES"));
				resInterno.add(valores);
				j++;
				if (j == 100) {
					resultado.add(resInterno);
					resInterno = new ArrayList<SchedulerValue>();
					j = 0;
				}
			}
			resultado.add(resInterno);
		} catch (SQLException e1) {
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (query != null)
					query.close();
				if (con != null)
					con.close();
				conexion.closeConexion();
			} catch (SQLException e2) {
				System.out.println("Error in " + getClass().getName() + "\n"+ e2);
			}
		}
		return resultado;
	}
	/**
	 * VCM 23/07/12 El m�todo consulDescSistema() obtiene los distintos tipos de
	 * error de las sucursales contingentes.
	 *
	 * @return ArrayList valor de respuesta de la consulta
	 */
	public ArrayList<ArrayList<SchedulerValue>> readJobsDetail(String iniDate) {
		System.out.println("Consulta para el monitoreo de scheduler jobs ejecutados");
		// Se realiza la conexi�n a la BD
		ConexionDAO conexion = new ConexionDAO();
		Connection con = null;
		ArrayList<ArrayList<SchedulerValue>> resultado = new ArrayList<ArrayList<SchedulerValue>>();
		ArrayList<SchedulerValue> resInterno = new ArrayList<SchedulerValue>();
		PreparedStatement query = null;
		StringBuffer lsbQuery = new StringBuffer();
		ResultSet rs = null;
		int j = 0;
		// Consulta de scheduler jobs

		lsbQuery = lsbQuery.delete(0, lsbQuery.length());
		lsbQuery.append(" SELECT D.JOB_NAME AS NOMBRE_JOB, TO_CHAR(D.LOG_DATE, 'DD/MM/YYYY HH24:MI:SS') AS FECHA_EJECUCION, D.STATUS AS ESTATUS, D.ADDITIONAL_INFO AS DETALLE_EJECUCION, D.RUN_DURATION AS DURACION ");
		lsbQuery.append(" FROM ALL_SCHEDULER_JOB_RUN_DETAILS D, ALL_SCHEDULER_JOBS I ");
		lsbQuery.append(" WHERE I.JOB_NAME = D.JOB_NAME AND TO_CHAR(D.LOG_DATE, 'DD/MM/YYYY') = '").append(iniDate).append("' ");
		lsbQuery.append(" ORDER BY D.LOG_DATE ");
		System.out.println("lsbQuery: " + lsbQuery.toString());

		try {
			con = conexion.getConexion();
			query = con.prepareStatement(lsbQuery.toString());
			rs = query.executeQuery();
			while (rs.next()) {
				SchedulerValue valores = new SchedulerValue(rs.getString("NOMBRE_JOB"));
				valores.setActualStartDate(rs.getString("FECHA_EJECUCION"));
				valores.setStatus(rs.getString("ESTATUS"));
				valores.setAdditionalInfo(rs.getString("DETALLE_EJECUCION"));
				valores.setRunDuration(rs.getString("DURACION"));
				resInterno.add(valores);
				j++;
				if (j == 100) {
					resultado.add(resInterno);
					resInterno = new ArrayList<SchedulerValue>();
					j = 0;
				}
			}
			resultado.add(resInterno);
		} catch (SQLException e1) {
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (query != null)
					query.close();
				if (con != null)
					con.close();
				conexion.closeConexion();
			} catch (SQLException e2) {
				System.out.println("Error in " + getClass().getName() + "\n"+ e2);
			}
		}
		return resultado;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {


	}

}