/** Banco Santander Mexicano
*   Clase OperClase  Resuelve las operaciones del catalogo de operaciones.
*   @author Ing. David Aguilar G�mez
*   @version 1.0
*   fecha de creacion : 28 de Marzo del 2006
*   responsable : Eloisa Hernandez H
*   descripcion : resuelve operaciones del catalogo de operaciones .
*   modificacion :
*/
package com.santander.CatOperaciones;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.santander.utilerias.Conex03;

public class OperClase
{
/** El m�todo consultaOperacion(String) revisa si los datos se encuentran
*   en la secion de contingente en base al punto de venta y fecha
*   @param fecha  Fecha de consulta
*   @return  boolean respuesta de si se encuentra el registro
*/
	public OperValue consultaOperacion(String cve_operacion)
	{
		System.out.println("M�todo consultaOperacion");
		StringBuffer lsb_query = new StringBuffer();
		ResultSet rs = null;
		OperValue valores = null;
		Conex03 con= new Conex03();
		lsb_query = lsb_query.delete(0, lsb_query.length());
		lsb_query.append("SELECT CVE_OPERACION, DESCRIPCION, AFECTA_DIARIO, AFECTA_TOTALES, PERMITE_OFFLINE ");
		lsb_query.append("FROM TA_COMU_OPERACIONES ");
		lsb_query.append("WHERE CVE_OPERACION = '").append(cve_operacion).append("' ");
		System.out.println("lsb_query: "+lsb_query.toString());
		try
		{
			con.conexionDB();
			rs=con.queryDB(lsb_query.toString());
			if(rs.next()){
				valores = new OperValue();
				valores.setCve_operacion(rs.getString("CVE_OPERACION"));
				valores.setDescripcion(rs.getString("DESCRIPCION"));
				valores.setAfecta_diario(rs.getString("AFECTA_DIARIO"));
				valores.setAfecta_totales(rs.getString("AFECTA_TOTALES"));
				valores.setPermite_offline(rs.getString("PERMITE_OFFLINE"));
			}
			if(rs!=null) rs.close();
		}
		catch (SQLException e1)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
			con.cierraConexionDB();
		}
		catch (Exception e2)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e2);
			con.cierraConexionDB();
		}
		finally
		{
			try
			{
				if(rs!=null) rs.close();
				con.cierraConexionDB();
			}
			catch(SQLException e2)
			{
				System.out.println("Error in " + getClass().getName() + "\n" + e2);
			}
		}
		return valores;
	}
/** El m�todo modificaOperacion(OperValue) actualiza registro de Ta_Comu_Operaciones
*   @param val Arreglo de datos a insertar
*   @return  boolean respuesta de si se inserto el registro
*/
	public boolean modificaOperacion(OperValue val)
	{
		System.out.println("M�todo Modifica registro Ta_Comu_Operaciones");
		Conex03 con= new Conex03();
		StringBuffer lsb_query = new StringBuffer();
		boolean regreso = false;
		try
		{
			con.conexionDB();
			lsb_query = lsb_query.delete(0, lsb_query.length());
			lsb_query.append("UPDATE TA_COMU_OPERACIONES ");
			lsb_query.append("SET DESCRIPCION = '").append(val.getDescripcion()).append("', ");
			lsb_query.append("AFECTA_DIARIO = '").append(val.getAfecta_diario()).append("', ");
			lsb_query.append("AFECTA_TOTALES = '").append(val.getAfecta_totales()).append("', ");
			lsb_query.append("PERMITE_OFFLINE = '").append(val.getPermite_offline()).append("' ");
			lsb_query.append("WHERE CVE_OPERACION = '").append(val.getCve_operacion()).append("' ");
			System.out.println("lsb_query: "+lsb_query.toString());
			try
			{
				con.ejecutaSQL(lsb_query.toString());
				System.out.println("al final de ejecutar el query");
				con.terminaTransaccion(true);
				regreso = true;
			}
			catch (SQLException e1)
			{
				con.cierraConexionDB();
				System.out.println("Error in " + getClass().getName() + "\n" + e1);
			}
		}
		catch (SQLException e1)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
			con.cierraConexionDB();
		}
		catch(Exception e2)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e2);
		}
		finally
		{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		return regreso;
	}

/** El m�todo insertaOperacion(OperValue) inserta registro de Ta_Comu_Operaciones
*   @param val Arreglo de datos a insertar
*   @return  boolean respuesta de si se inserto el registro
*/
	public boolean insertaOperacion(OperValue val)
	{
		System.out.println("M�todo Inserta registro Ta_Comu_Operaciones");
		Conex03 con= new Conex03();
		StringBuffer lsb_query = new StringBuffer();
		boolean regreso = false;
		try
		{
			con.conexionDB();
			lsb_query = lsb_query.delete(0, lsb_query.length());
			lsb_query.append("INSERT INTO TA_COMU_OPERACIONES ");
			lsb_query.append("(CVE_OPERACION, ").append("DESCRIPCION, ").append("AFECTA_DIARIO, ").append("AFECTA_TOTALES, ");
			lsb_query.append("PERMITE_OFFLINE) ");
			lsb_query.append("VALUES ('"+val.getCve_operacion()+"', ").append("'"+val.getDescripcion()+"', ");
			lsb_query.append("'"+val.getAfecta_diario()+"', ").append("'"+val.getAfecta_totales()+"', ").append("'"+val.getPermite_offline()+"') ");
			System.out.println("lsb_query: "+lsb_query.toString());
			try
			{
				con.ejecutaSQL(lsb_query.toString());
				System.out.println("al final de ejecutar el query");
				con.terminaTransaccion(true);
				regreso = true;
			}
			catch (SQLException e1)
			{
				con.cierraConexionDB();
				System.out.println("Error in " + getClass().getName() + "\n" + e1);
			}
		}
		catch (SQLException e1)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
			con.cierraConexionDB();
		}
		catch(Exception e2)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e2);
		}
		finally
		{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		return regreso;
	}
/** El m�todo borraOperacion(OperValue) borra registro de Ta_Comu_Operaciones
*   @param val Arreglo de datos a insertar
*   @return  boolean respuesta de si se inserto el registro
*/
	public boolean borraOperacion(OperValue val)
	{
		System.out.println("M�todo borra registro Ta_Comu_Operaciones");
		Conex03 con= new Conex03();
		StringBuffer lsb_query = new StringBuffer();
		boolean regreso = false;
		try
		{
			con.conexionDB();
			lsb_query = lsb_query.delete(0, lsb_query.length());
			lsb_query.append("DELETE TA_COMU_OPERACIONES ");
			lsb_query.append("WHERE CVE_OPERACION = '").append(val.getCve_operacion()).append("' ");
			System.out.println("lsb_query: "+lsb_query.toString());
			try
			{
				con.ejecutaSQL(lsb_query.toString());
				System.out.println("al final de ejecutar el query");
				con.terminaTransaccion(true);
				regreso = true;
			}
			catch (SQLException e1)
			{
				con.cierraConexionDB();
				System.out.println("Error in " + getClass().getName() + "\n" + e1);
			}
		}
		catch (SQLException e1)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
			con.cierraConexionDB();
		}
		catch(Exception e2)
		{
			System.out.println("Error in " + getClass().getName() + "\n" + e2);
		}
		finally
		{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		return regreso;
	}
}