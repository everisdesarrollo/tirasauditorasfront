/** Banco Santander Mexicano
*   Clase OperValue  Contiene los valores del Catalogo de Operaciones
*   @author Ing. David Aguilar G�mez
*   @version 1.0
*   fecha de creacion : 28 de Marzo del 2006
*   responsable : Eloisa Hernandez H
*   descripcion : Contiene los valores del Catalogo de Operaciones.
*   modificacion :
*/
package com.santander.CatOperaciones;

public class OperValue
{
	private String cve_operacion;
	private String descripcion;
	private String afecta_diario;
	private String afecta_totales;
	private String permite_offline;


	/**
	 * @return
	 */
	public String getAfecta_diario()
	{
		return afecta_diario;
	}

	/**
	 * @return
	 */
	public String getAfecta_totales()
	{
		return afecta_totales;
	}

	/**
	 * @return
	 */
	public String getCve_operacion()
	{
		return cve_operacion;
	}

	/**
	 * @return
	 */
	public String getDescripcion()
	{
		return descripcion;
	}

	/**
	 * @return
	 */
	public String getPermite_offline()
	{
		return permite_offline;
	}

	/**
	 * @param string
	 */
	public void setAfecta_diario(String string)
	{
		afecta_diario = string;
	}

	/**
	 * @param string
	 */
	public void setAfecta_totales(String string)
	{
		afecta_totales = string;
	}

	/**
	 * @param string
	 */
	public void setCve_operacion(String string)
	{
		cve_operacion = string;
	}

	/**
	 * @param string
	 */
	public void setDescripcion(String string)
	{
		descripcion = string;
	}

	/**
	 * @param string
	 */
	public void setPermite_offline(String string)
	{
		permite_offline = string;
	}

}