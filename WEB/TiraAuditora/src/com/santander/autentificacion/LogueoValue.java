/** Banco Santander Mexicano
*   Clase LogueoValue  Valores de los Datos de la consulta
*                      de los permisos para el logueo
*   @author Alfredo Resendiz Vargas
*   @version 1.0
*   Fecha de creacion : 27 de Febrero del 2006
*   Responsable : Eloisa Hernandez H.
*   Descripcion : Se encarga de contener los valores de la consulta realizada.
*   modificacion :
*/
package com.santander.autentificacion;

public class LogueoValue
{
  private String fac01;
  private String fac02;
  private String fac03;
  private String fac04;
  private String fac05;
  private String fac06;
  private String fac07;
  private String fac08;
  private String fac09;
  private String fac10;
  private String fac11;
  private String fac12;
  private String fac13;
  private String fac14;
  private String fac15;
  private String fac16;

public String getfac16() {
	return fac16;
}

public void setfac16(String fac16) {
	this.fac16 = fac16;
}

private String fecInac;
  private String usuario;
  private String log_off;

/**
 * @return (String)Valor del permiso de la Consulta Tira Auditora
 */
  public String getfac01()
  {
	return fac01;
  }

/**
 * @return (String)Valor del permiso de la Consulta de Catalogo de Sucursales
*/
  public String getfac02()
  {
	return fac02;
  }

/**
 * @return (String)Valor del permiso del  Mantto. de Catálogo de Sucursales
 */
  public String getfac03()
  {
	return fac03;
  }

/**
 * @return (String)Valor del permiso de la Consulta de Sucursales Contingentes
 */
  public String getfac04()
  {
 	return fac04;
  }

/**
 * @return (String)Valor del permiso del  Mantto. de Sucursales Contingentes
 */
  public String getfac05()
  {
 	return fac05;
  }

/**
 * @return (String)Valor del permiso de la Carga Manual de Tira Auditora
 */
  public String getfac06()
  {
	return fac06;
  }

/**
 * @return (String)Valor del permiso de la Generación de Archs. Ingresos/Egresos
 */
  public String getfac07()
  {
	return fac07;
  }

/**
 * @return (String)Valor del permiso de la Información Historica de la BD
 */
  public String getfac08()
  {
 	return fac08;
  }

/**
 * @return (String)Valor del permiso de la Información Historica Sucursales
 *         Contingentes
 */
  public String getfac09()
  {
	return fac09;
  }

 /**
  * @return (String)Valor del permiso del cambio de password del telnet
  */
  public String getfac10()
  {
    return fac10;
  }

/**
 * @return (String)Valor del permiso del Mantto. Comu_operaciones
 */
  public String getfac11()
  {
    return fac11;
  }

/**
 * @return (String)Valor del permiso del Mantto. Usr del aplicativo
 */
  public String getfac12()
  {
    return fac12;
  }

/**
 * @return (String)Valor del permiso del Mantto. del Perfil
 */
  public String getfac13()
  {
    return fac13;
  }
  /**
   * @return (String)Valor del permiso del Mantto. basico de Sucursal
   */
    public String getfac14()
    {
      return fac14;
    }

/**
 * @return (String)Valor de la fecha de inactividad de la cuenta de usuario
 */
  public String getfecInac()
  {
 	return fecInac;
  }

/**
 * @return (String)Valor del usuario
 */
  public String getusuario()
  {
	return usuario;
  }

 /**
   * @return (String)Valor de la bandera para el logoff
   */
  public String getlog_off()
  {
    return log_off;
  }


/**
 * @param (String)Valor del permiso de la Consulta Tira Auditora
 */
  public void setfac01(String string)
  {
	fac01 = string;
  }

/**
 * @param (String)Valor del permiso de la Consulta de Catalogo de Sucursales
 */
  public void setfac02(String string)
  {
	fac02 = string;
  }

/**
 * @param (String)Valor del permiso del  Mantto. de Catálogo de Sucursales
 */
  public void setfac03(String string)
  {
	fac03 = string;
  }

/**
 * @param (String)Valor del permiso de la Consulta de Sucursales Contingentes
 */
  public void setfac04(String string)
  {
	fac04 = string;
  }

/**
 * @param (String)Valor del permiso del  Mantto. de Sucursales Contingentes
 */
  public void setfac05(String string)
  {
	fac05 = string;
  }

/**
 * @param (String)Valor del permiso de la Carga Manual de Tira Auditora
 */
  public void setfac06(String string)
  {
	fac06 = string;
  }

/**
 * @param (String)Valor del permiso de la Generación de Archs. Ingresos/Egresos
 */
  public void setfac07(String string)
  {
	fac07 = string;
  }

/**
 * @param (String)Valor del permiso de la Información Historica de la BD
 */
  public void setfac08(String string)
  {
	fac08 = string;
  }

/**
 * @param (String)Valor del permiso de la Información Historica Sucursales
 *        Contingentes
 */
  public void setfac09(String string)
  {
	fac09 = string;
  }

/**
 * @param (String)Valor del permiso del password del Telnet
 */
  public void setfac10(String string)
  {
    fac10 = string;
  }

/**
 * @param (String)Valor del permiso del Mantto. de Comu_operaciones
 */
  public void setfac11(String string)
  {
    fac11 = string;
  }

/**
 * @param (String)Valor del permiso del Mantto. de Usr del Aplicativo
 */
  public void setfac12(String string)
  {
    fac12 = string;
  }

/**
 * @param (String)Valor del permiso del Mantto. del Perfil
 */
  public void setfac13(String string)
  {
    fac13 = string;
  }

  /**
   * @param (String)Valor del permiso del Mantto. basico de sucursal
   */
    public void setfac14(String string)
    {
      fac14 = string;
    }
/**
 * @param (String)Valor de la fecha de inactividad de la cuenta de usuario
 */
  public void setfecInac(String string)
  {
 	fecInac = string;
  }

/**
 * @param (String)Valor del usuario
 */
  public void setusuario(String string)
  {
	usuario = string;
  }

 /**
  * @param (String)Valor de la bancera para el logoff
  */
  public void setlog_off(String string)
  {
    log_off = string;
  }
  public String getfac15() {
		return fac15;
	}

	public void setfac15(String fac15) {
		this.fac15 = fac15;
	}
}