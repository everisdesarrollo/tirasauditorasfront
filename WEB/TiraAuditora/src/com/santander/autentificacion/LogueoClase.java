/** Banco Santander Mexicano
*   Clase LogueoClase  Logueo
*   @author Alfredo Resendiz Vargas
*   @version 1.0
*   Fecha de creacion : 2 de Febrero del 2006
*   Responsable : Eloisa Hernandez H
*   Descripcion : Obtiene los permisos para el logueo a la aplicaci�n.
*   Modificacion :
*/
package com.santander.autentificacion;

import com.santander.utilerias.*;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import com.lowagie.text.*;
import com.lowagie.text.pdf.*;
import java.awt.Color;
import java.sql.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.IOException;
import javax.naming.*;
import javax.sql.DataSource;
import java.lang.*;
import java.util.Date;
import java.text.SimpleDateFormat;


public class LogueoClase
{

/** El m�todo logueoContingente() obtiene los permisos para el logueo
*	a la aplicaci�n.
*   @return  ArrayList valor de respuesta.
*/
  public ArrayList<LogueoValue> consultaLogueo(String user, String perfil)
  {
	System.out.println("Consulta para obtener los permisos y perfiles del usuario");
	ConexionDAO conexion= new ConexionDAO();
	Connection con;
	ArrayList<LogueoValue> resultado = new ArrayList<LogueoValue>();
	PreparedStatement query = null;
	StringBuffer lsb_query = new StringBuffer();
	ResultSet rs = null;
	int cont = 0;

//	Consulta que recupera los perfiles de los usuarios
	lsb_query = lsb_query.delete(0, lsb_query.length());
	lsb_query.append(" SELECT FACULTAD FROM TA_PERMISOS WHERE PERFIL = '").append(perfil).append("' ");
	System.out.println("lsbQuery: " + lsb_query.toString());

	try
	{
	  con = conexion.getConexion();
	  query = con.prepareStatement(lsb_query.toString());
	  rs=query.executeQuery();
	  while(rs.next())
	  {
		cont++;
		LogueoValue valores = new LogueoValue();
		if(rs.getString("FACULTAD").equals("F01"))
	  	  valores.setfac01(rs.getString("FACULTAD"));

		if(rs.getString("FACULTAD").equals("F02"))
		  valores.setfac02(rs.getString("FACULTAD"));

		if(rs.getString("FACULTAD").equals("F03"))
		  valores.setfac03(rs.getString("FACULTAD"));

		if(rs.getString("FACULTAD").equals("F04"))
		  valores.setfac04(rs.getString("FACULTAD"));

		if(rs.getString("FACULTAD").equals("F05"))
		  valores.setfac05(rs.getString("FACULTAD"));

		if(rs.getString("FACULTAD").equals("F06"))
		  valores.setfac06(rs.getString("FACULTAD"));

		if(rs.getString("FACULTAD").equals("F07"))
		  valores.setfac07(rs.getString("FACULTAD"));

		if(rs.getString("FACULTAD").equals("F08"))
		  valores.setfac08(rs.getString("FACULTAD"));

		if(rs.getString("FACULTAD").equals("F09"))
		  valores.setfac09(rs.getString("FACULTAD"));

		if(rs.getString("FACULTAD").equals("F10"))
		  valores.setfac10(rs.getString("FACULTAD"));

		if(rs.getString("FACULTAD").equals("F11"))
		  valores.setfac11(rs.getString("FACULTAD"));

		if(rs.getString("FACULTAD").equals("F12"))
		  valores.setfac12(rs.getString("FACULTAD"));

		if(rs.getString("FACULTAD").equals("F13"))
		  valores.setfac13(rs.getString("FACULTAD"));

		if(rs.getString("FACULTAD").equals("F14"))
		  valores.setfac14(rs.getString("FACULTAD"));

		if(rs.getString("FACULTAD").equals("F15"))
		  valores.setfac15(rs.getString("FACULTAD"));

		if(rs.getString("FACULTAD").equals("F16"))
		  valores.setfac16(rs.getString("FACULTAD"));

		valores.setusuario(user);

		resultado.add(valores);
	  }
	}
	catch (Exception e1)
	{
	  e1.printStackTrace();
	}
	finally
	{
	  try
	  {
		rs.close();
		query.close();
		conexion.closeConexion();
	  }
	  catch(SQLException e2)
	  {
			System.out.println("Error in " + getClass().getName() + "\n" + e2);
	  }
	}
	return resultado;
  }

/** El m�todo autentifica() realiza la conexion con el usuario y el password
 *  del logueo para validar dichos datos de entrada
 *   @return  estatus del movimiento
 */
  public String autentifica(String url,String user, String password)
  {
	Connection con;
	String resultado = "";
	try
	{
	  System.out.println("Se valida usuario y password");
  	  con=DriverManager.getConnection(url,user,password);

	  if(con!=null)
	  {
		System.out.println("Conexion exitosa");
		resultado = "0";
	  }
	  else
	  {
		System.out.println("Conexion fracaso");
		resultado = "1";
	  }
		con.close();
	  }
	  catch (SQLException e1)
	  {
		while (e1 != null)
		{
		  if(e1.getMessage().substring(0,9).equals("ORA-01017"))
		  {
			System.out.println("Message:   " + e1.getMessage ());
			resultado = "2";
			System.out.println("resultado: " + resultado);
			return resultado;
		  }
		  if(e1.getMessage().substring(0,9).equals("ORA-28000"))
		  {
			System.out.println("Message:   " + e1.getMessage ());
			resultado = "3";
			System.out.println("resultado: " + resultado);
			return resultado;
		  }
		  if(e1.getMessage().substring(0,9).equals("ORA-28001"))
		  {
		    System.out.println("Message:   " + e1.getMessage ());
		    resultado = "4";
		    System.out.println("resultado: " + resultado);
		    return resultado;
		  }
		  if(e1.getMessage().substring(0,9).equals("ORA-28002"))
		  {
			System.out.println("Message:   " + e1.getMessage ());
			resultado = "5";
			System.out.println("resultado: " + resultado);
			return resultado;
		  }
		  e1 = e1.getNextException();
		  e1.printStackTrace();
		}
	  }
	  return resultado;
  }

/** El m�todo actualizaPass() actualiza el password antes de que expire c/30 d�as
 *   @return  estatus del movimiento
 */
  public String actualizaPass(String user,String password)
  {
	System.out.println("Se realiza el cambio de contrase�a");
	ConexionDAO conexion= new ConexionDAO();
	Connection con;
	String resultado = "";
	PreparedStatement query = null;
	StringBuffer lsb_query = new StringBuffer();
	ResultSet rs = null;
	int cont = 0;
	lsb_query = lsb_query.delete(0, lsb_query.length());
	lsb_query.append(" ALTER USER ").append(user).append(" IDENTIFIED BY ").append(password);
	System.out.println("lsbQuery: "+lsb_query.toString());

	try
	{
 	  con = conexion.getConexion();
	  query = con.prepareStatement(lsb_query.toString());
	  rs=query.executeQuery();
	  resultado = "0";
	}
	catch (SQLException e1)
	{
	  while (e1 != null)
	  {
		if(e1.getMessage().substring(0,9).equals("ORA-28007"))
		{
		  System.out.println("Message:   " + e1.getMessage ());
		  resultado = "1";
		  System.out.println("resultado:   " + resultado);
		  return resultado;
		}
		else
		{
		  if(e1.getMessage().substring(0,9).equals("ORA-01003"))
		  {
			System.out.println("Message:   " + e1.getMessage ());
			resultado = "2";
			System.out.println("resultado: " + resultado);
			return resultado;
		  }
		  else
		  {
			System.out.println("Message:   " + e1.getMessage ());
			resultado = "3";
			System.out.println("resultado: " + resultado);
			return resultado;
		  }
		}
	  }
	  e1 = e1.getNextException();
	  try
	  {
		rs.close();
		query.close();
		conexion.closeConexion();
	  }
	  catch(SQLException e2)
	  {
		System.out.println("Error in " + getClass().getName() + "\n" + e2);
	  }
	}
	return resultado;
  }

/** El m�todo validaCaducidad() obtiene la fecha de inactividad
 *  en d�as de la cuanta de usuario.
 *  @return  ArrayList valor de respuesta.
*/
  public long validaCaducidad(String user)
  {
	System.out.println("Se valida la caducidad de la cuenta de usuario");
	ConexionDAO conexion= new ConexionDAO();
	Connection con;
	String resultado = "";
	PreparedStatement query = null;
	StringBuffer lsb_query = new StringBuffer();
	ResultSet rs = null;
	long numDias = 0;
	String fecha = "";

//	Se recupera la fecha de inactividad para validarla
	lsb_query = lsb_query.delete(0, lsb_query.length());
	lsb_query.append(" SELECT FEC_INAC ");
	lsb_query.append(" FROM TA_USUARIOS ");
	lsb_query.append(" WHERE USUARIO = '").append(user).append("' ");
	System.out.println("lsbQuery: "+lsb_query.toString());

	try
	{
	  con = conexion.getConexion();
	  query = con.prepareStatement(lsb_query.toString());
	  rs=query.executeQuery();
	  while(rs.next())
	  {
		fecha=rs.getString("FEC_INAC");
		JhDate formatoFecha = new JhDate(fecha,"",3);
		JhDate calculo_dias = new JhDate();
		numDias=calculo_dias.getTiempoTranscurrido(formatoFecha,1);
	  }
	}
	catch (Exception e1)
	{
	 e1.printStackTrace();
	}
	finally
	{
	  try
	  {
		rs.close();
		query.close();
		conexion.closeConexion();
	  }
	  catch(SQLException e2)
	  {
		System.out.println("Error in " + getClass().getName() + "\n" + e2);
	  }
	}
	return numDias;
  }

/** El m�todo guradaFecCaduc() guarda la fecha en la que el usuario se loguea
 *  para el control de la caducidad de la cuenta de usuario.
 *  @return  String de control.
*/
  public String guradaFecCaduc(String user)
  {
	System.out.println("Se actualiza la fecha de caducidad");
	ConexionDAO conexion= new ConexionDAO();
	Connection con;
	String resultado = "";
	PreparedStatement query = null;
	StringBuffer lsb_query = new StringBuffer();
	ResultSet rs = null;
	String res="";
	Date fecha = new Date();
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	String fecha1= sdf.format(fecha);
	lsb_query = lsb_query.delete(0, lsb_query.length());
	lsb_query.append(" UPDATE TA_USUARIOS SET FEC_INAC = TO_DATE('").append(fecha1).append("','DD/MM/YYYY') ");
	lsb_query.append(" WHERE USUARIO = '").append(user).append("' ");
	System.out.println("lsbQuery: "+lsb_query.toString());

	try
	{
	  con = conexion.getConexion();
	  query = con.prepareStatement(lsb_query.toString());
	  rs=query.executeQuery();
	  res="0";
	}
	catch (Exception e1)
	{
	  e1.printStackTrace();
	}
	finally
	{
	  try
	  {
		rs.close();
		query.close();
		conexion.closeConexion();
	  }
	  catch(SQLException e2)
	  {
		System.out.println("Error in " + getClass().getName() + "\n" + e2);
	  }
	}
	return resultado;
  }
}