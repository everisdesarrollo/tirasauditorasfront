/** Banco Santander Mexicano
*   Clase usuariosClase  administraci�n de cuantas de usuarios
*   @author Alfredo Resendiz Vargas
*   @version 1.0
*   Fecha de creacion : 28 de Marzo del 2006
*   Responsable : Eloisa Hernandez H
*   Descripcion : Realiza las altas y bajas de cuantas de usuarios, asi como altas,
*                 bajas y modificaciones del perfil del usuario
*   Modificacion :
*/
package com.santander.usuarios;

import com.santander.utilerias.*;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.awt.Color;
import java.sql.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.IOException;
import javax.naming.*;
import javax.sql.DataSource;
import java.lang.*;
import java.util.Date;
import java.text.SimpleDateFormat;


public class usuariosClase
{

/** El m�todo AltaUser() crea el usuario con su respectivo password
 *   @return  estatus del movimiento
 */
  public String altaUser(String user,String password)
  {
	System.out.println("Se da de alta a un usuario");
	ConexionDAO conexion= new ConexionDAO();
	Connection con;
	String resultado = "";
	PreparedStatement query = null;
	StringBuffer lsb_query = new StringBuffer();
	ResultSet rs = null;
	int cont = 0;
	lsb_query = lsb_query.delete(0, lsb_query.length());
	lsb_query.append(" CREATE USER ").append(user).append(" IDENTIFIED BY ").append(password).append(" PROFILE PERFIL_TIRAS ");
	System.out.println("lsbQuery: "+lsb_query.toString());

	try
	{
	  con = conexion.getConexion();
	  query = con.prepareStatement(lsb_query.toString());
	  rs=query.executeQuery();
	  lsb_query = lsb_query.delete(0, lsb_query.length());
	  lsb_query.append(" GRANT CONNECT, RESOURCE TO ").append(user);
	  System.out.println("lsbQuery: "+lsb_query.toString());
	  try
	  {
		System.out.println("Grant OK");
		query = con.prepareStatement(lsb_query.toString());
	    rs=query.executeQuery();
		lsb_query = lsb_query.delete(0, lsb_query.length());
		lsb_query.append(" COMMIT ");
		System.out.println("lsbQuery: "+lsb_query.toString());
		query = con.prepareStatement(lsb_query.toString());
		rs=query.executeQuery();
		resultado = "0";
	  }
	  catch(SQLException e1)
	  {
		e1.printStackTrace();
	  }
	}
	catch (SQLException e1)
	{
	  while (e1 != null)
	  {
		if(e1.getMessage().substring(0,9).equals("ORA-01920"))
		{
		  System.out.println("Message:   " + e1.getMessage ());
		  resultado = "1";
		  System.out.println("resultado:   " + resultado);
		  return resultado;
		}
		else
		{
		  System.out.println("Error con la BD");
		  resultado = "2";
		  System.out.println("resultado:   " + resultado);
		  return resultado;
		}
	  }
	  e1 = e1.getNextException();
	  e1.printStackTrace();
	  try
	  {
		rs.close();
		query.close();
		conexion.closeConexion();
	  }
	  catch(SQLException e2)
	  {
		System.out.println("Error in " + getClass().getName() + "\n" + e2);
	  }
	}
	return resultado;
  }

/** El m�todo bajauser() da de baja un usuario
  *   @return  estatus del movimiento
  */
  public String bajaUser(String user)
  {
    System.out.println("Se da de baja a un usuario");
    ConexionDAO conexion= new ConexionDAO();
    Connection con;
    String resultado = "";
    PreparedStatement query = null;
    StringBuffer lsb_query = new StringBuffer();
    ResultSet rs = null;
    int cont = 0;
    lsb_query = lsb_query.delete(0, lsb_query.length());
    lsb_query.append(" DROP USER ").append(user).append(" CASCADE ");
    System.out.println("lsbQuery: "+lsb_query.toString());

    try
    {
	  con = conexion.getConexion();
	  query = con.prepareStatement(lsb_query.toString());
	  rs=query.executeQuery();
	  resultado = "0";
    }
    catch (SQLException e1)
    {
  	  while (e1 != null)
	  {
	    if(e1.getMessage().substring(0,9).equals("ORA-01918"))
	    {
	      System.out.println("Message:   " + e1.getMessage ());
		  resultado = "1";
		  System.out.println("resultado:   " + resultado);
		  return resultado;
		}
		else
		{
		  System.out.println("Error desconocido con la BD");
		  resultado = "2";
		  System.out.println("resultado:   " + resultado);
		  return resultado;
		}
	  }
	  e1 = e1.getNextException();
	  e1.printStackTrace();
	  try
	  {
	    rs.close();
	    query.close();
	    conexion.closeConexion();
	  }
	  catch(SQLException e2)
	  {
	    System.out.println("Error in " + getClass().getName() + "\n" + e2);
	  }
    }
    return resultado;
 }

/** El m�todo borraUser() Borra un usuario de la BD en la baja de usuario
  *   @return  estatus del movimiento
  */
  public String borraUser(String user)
  {
    System.out.println("Se borra el usuario de la BD Taud");
    ConexionDAO conexion= new ConexionDAO();
    Connection con;
    String resultado = "";
    PreparedStatement query = null;
    StringBuffer lsb_query = new StringBuffer();
    ResultSet rs = null;
    int cont = 0;
    lsb_query = lsb_query.delete(0, lsb_query.length());
    lsb_query.append(" DELETE TA_USUARIOS ");
    lsb_query.append(" WHERE USUARIO = '").append(user).append("'");
    System.out.println("lsbQuery: "+lsb_query.toString());

    try
    {
  	  con = conexion.getConexion();
	  query = con.prepareStatement(lsb_query.toString());
	  rs=query.executeQuery();
	  resultado = "0";
    }
    catch (SQLException e1)
    {
   	  while (e1 != null)
	  {
	   	System.out.println("Message:   " + e1.getMessage ());
		resultado = "1";
		System.out.println("resultado:   " + resultado);
		return resultado;
	  }
	  e1 = e1.getNextException();
	  try
	  {
	    rs.close();
		query.close();
		conexion.closeConexion();
	  }
	  catch(SQLException e2)
	  {
	    System.out.println("Error in " + getClass().getName() + "\n" + e2);
	  }
	}
	return resultado;
  }

/** El m�todo altaPerfil() da de alta el perfil de un usuario nuevo
 *  @return  Status del Movimiento.
*/
  public String altaPerfil(String user, String perfil)
  {
	System.out.println("Se da de alta el usuario en la BD Taud");
	ConexionDAO conexion= new ConexionDAO();
	Connection con;
	String resultado = "";
	PreparedStatement query = null;
	StringBuffer lsb_query = new StringBuffer();
	ResultSet rs = null;

//	Query para dar de alta el perfil del usuario
	lsb_query = lsb_query.delete(0, lsb_query.length());
	lsb_query.append(" INSERT INTO TA_USUARIOS ");
	lsb_query.append(" (USUARIO, PERFIL) ");
	lsb_query.append(" VALUES ('").append(user).append("' , '").append(perfil).append("' )");
	System.out.println("lsbQuery: "+lsb_query.toString());

	try
	{
	  con = conexion.getConexion();
	  query = con.prepareStatement(lsb_query.toString());
	  rs=query.executeQuery();
	  resultado="0";
	}
	catch (SQLException e1)
	{
	  while (e1 != null)
	  {
	    if(e1.getMessage().substring(0,9).equals("ORA-00001"))
	    {
	  	  System.out.println("Message:   " + e1.getMessage ());
		  resultado = "1";
		  System.out.println("resultado:   " + resultado);
		  return resultado;
		}
		else
		{
		  System.out.println("Error desconocido con la BD");
		  resultado = "2";
		  System.out.println("resultado:   " + resultado);
		  return resultado;
		}
	  }
	  e1 = e1.getNextException();
	  e1.printStackTrace();
	}
	finally
	{
	  try
	  {
		rs.close();
		query.close();
		conexion.closeConexion();
	  }
	  catch(SQLException e2)
	  {
		System.out.println("Error in " + getClass().getName() + "\n" + e2);
	  }
	}
	return resultado;
  }
/** El m�todo desbloqueoUser() Desbloquea una cuenta de usuario
 *   @return  estatus del movimiento
 */
  public String desbloqueoUser(String user)
  {
    System.out.println("Se desbloquea una cuenta de usuario de la BD Taud");
    ConexionDAO conexion= new ConexionDAO();
    Connection con;
    String resultado = "";
    PreparedStatement query = null;
    StringBuffer lsb_query = new StringBuffer();
	ResultSet rs = null;
	int cont = 0;
	lsb_query = lsb_query.delete(0, lsb_query.length());
	lsb_query.append("  ALTER USER ").append(user).append(" ACCOUNT UNLOCK ");
	System.out.println("lsbQuery: "+lsb_query.toString());

	try
	{
	  con = conexion.getConexion();
	  query = con.prepareStatement(lsb_query.toString());
	  rs=query.executeQuery();
	  resultado = "0";
	}
	catch (SQLException e1)
	{
	  while (e1 != null)
	  {
	    System.out.println("Message:   " + e1.getMessage ());
	    resultado = "1";
	    System.out.println("resultado:   " + resultado);
	    return resultado;
	  }
	  e1 = e1.getNextException();
	  try
	  {
	    rs.close();
	    query.close();
	    conexion.closeConexion();
	  }
	  catch(SQLException e2)
	  {
	    System.out.println("Error in " + getClass().getName() + "\n" + e2);
	  }
	}
	return resultado;
  }
/** El m�todo verificaPerfil() Verifica la existencia del perfil en TA_PERMISOS
  *   @return  estatus del movimiento
  */
  public String verificaPerfil(String nomperfil)
  {
    System.out.println("Verifica la existencia del perfil en TA_PERMISOS");
    ConexionDAO conexion= new ConexionDAO();
    Connection con;
    String resultado = "";
    String resFac="";
    PreparedStatement query = null;
    StringBuffer lsb_query = new StringBuffer();
    ResultSet rs = null;
    int cont = 0;
    lsb_query = lsb_query.delete(0, lsb_query.length());
    lsb_query.append("  SELECT PERFIL ");
    lsb_query.append("  FROM TA_PERMISOS ");
    lsb_query.append("  WHERE PERFIL = '").append(nomperfil).append("' ");
    System.out.println("lsbQuery: "+lsb_query.toString());

    try
    {
      con = conexion.getConexion();
	  query = con.prepareStatement(lsb_query.toString());
	  rs=query.executeQuery();
	  while(rs.next())
	  {
	    resFac=rs.getString("PERFIL");
	  }
	  if(resFac == null || resFac == "")
	    resultado = "0";
	  else
	    resultado = "1";
	}
	catch (SQLException e1)
	{
	  e1.printStackTrace();
	}
	finally
	{
	  try
	  {
	    rs.close();
	    query.close();
	    conexion.closeConexion();
	  }
	  catch(SQLException e2)
	  {
	    System.out.println("Error in " + getClass().getName() + "\n" + e2);
	  }
	}
    return resultado;
  }

/** El m�todo veriPerfil() Verifica la existencia del perfil en TA_USUARIOS
 *   @return  estatus del movimiento
 */
 public String veriPerfil(String perfil)
	{
	  System.out.println("Verifica la existencia del perfil en TA_USUARIOS");
	  ConexionDAO conexion= new ConexionDAO();
	  Connection con;
	  String resultado = "";
	  String resFac="";
	  PreparedStatement query = null;
	  StringBuffer lsb_query = new StringBuffer();
	  ResultSet rs = null;
	  int cont = 0;
	  lsb_query = lsb_query.delete(0, lsb_query.length());
	  lsb_query.append("  SELECT PERFIL ");
	  lsb_query.append("  FROM TA_USUARIOS ");
	  lsb_query.append("  WHERE PERFIL = '").append(perfil).append("' ");
	  System.out.println("lsbQuery: "+lsb_query.toString());

	  try
	  {
		con = conexion.getConexion();
		query = con.prepareStatement(lsb_query.toString());
		rs=query.executeQuery();
		if(rs.next())
		{
		//rs.next();
		resFac=rs.getString("PERFIL");
		}
		System.out.println("PERFIL(***): "+resFac);
		if(resFac == null || resFac == "")
		  resultado = "0";
		else
		  resultado = "1";
	  }
	  catch (SQLException e1)
	  {
		e1.printStackTrace();
	  }
	  catch (Exception e1)
	  {
	    e1.printStackTrace();
	  }
	  finally
	  {
		try
		{
		  rs.close();
		  query.close();
		  conexion.closeConexion();
		}
		catch(SQLException e2)
		{
		  System.out.println("Error in " + getClass().getName() + "\n" + e2);
		}
	  }
	  return resultado;
	}

/** El m�todo creaPerfil() da de alta un perfil nuevo
 *  @return  Status del Movimiento.
*/
  public String creaPerfil(String facultad, String nomperfil)
  {
    System.out.println("Se da de alta el nuevo perfil en la BD Taud");
    ConexionDAO conexion= new ConexionDAO();
    Connection con;
    String resultado = "";
    PreparedStatement query = null;
    StringBuffer lsb_query = new StringBuffer();
    ResultSet rs = null;

// Query para dar de alta el perfil del usuario
    lsb_query = lsb_query.delete(0, lsb_query.length());
    System.out.println("Se Obtiene el ID de la tabla de permisos");
    lsb_query = lsb_query.delete(0, lsb_query.length());
    lsb_query.append(" SELECT ID_PERMISO FROM  TA_PERMISOS ");
    lsb_query.append(" ORDER BY 1 DESC ");
    System.out.println("lsbQuery: "+lsb_query.toString());

    try
    {
   	  con = conexion.getConexion();
	  query = con.prepareStatement(lsb_query.toString());
	  rs=query.executeQuery();
	  rs.next();
	  String idPermiso=rs.getString("ID_PERMISO");
	  int id=Integer.parseInt(idPermiso)+1;
	  System.out.println("ID: "+id);
	  System.out.println("Se da de alta el nuevo perfil en la BD Taud");
	  lsb_query = lsb_query.delete(0, lsb_query.length());
	  lsb_query.append(" INSERT INTO TA_PERMISOS ");
	  lsb_query.append(" (ID_PERMISO, PERFIL, FACULTAD) ");
	  lsb_query.append(" VALUES (" ).append(id).append(" , '").append(nomperfil).append("' , '").append(facultad).append("' )");
	  System.out.println("lsbQuery: "+lsb_query.toString());
	  query = con.prepareStatement(lsb_query.toString());
	  rs=query.executeQuery();
	  resultado="0";
	}
	catch (SQLException e1)
	{
	  e1.printStackTrace();
	}
	finally
	{
	  try
	  {
	    rs.close();
	    query.close();
	    conexion.closeConexion();
	  }
	  catch(SQLException e2)
	  {
	    System.out.println("Error in " + getClass().getName() + "\n" + e2);
	  }
	}
	return resultado;
  }

/** El m�todo bajaPerfil() da de alta un perfil nuevo
   *  @return  Status del Movimiento.
  */
	public String bajaPerfil(String facultad, String perfil)
	{
	  System.out.println("Se da de baja un perfil en la BD Taud");
	  ConexionDAO conexion= new ConexionDAO();
	  Connection con;
	  String resultado = "";
	  PreparedStatement query = null;
	  StringBuffer lsb_query = new StringBuffer();
	  ResultSet rs = null;

//	 Query para dar de baja el perfil del usuario
	  lsb_query = lsb_query.delete(0, lsb_query.length());
	  lsb_query = lsb_query.delete(0, lsb_query.length());
	  lsb_query.append(" DELETE  TA_PERMISOS ");
	  lsb_query.append(" WHERE PERFIL = '").append(perfil).append("'");
	  lsb_query.append(" AND FACULTAD = '").append(facultad).append("'");
	  System.out.println("lsbQuery: "+lsb_query.toString());

	  try
	  {
		con = conexion.getConexion();
		query = con.prepareStatement(lsb_query.toString());
		rs=query.executeQuery();
		//rs.next();
		resultado="0";
	  }
	  catch (SQLException e1)
	  {
		e1.printStackTrace();
	  }
	  finally
	  {
		try
		{
		  rs.close();
		  query.close();
		  conexion.closeConexion();
		}
		catch(SQLException e2)
		{
		  System.out.println("Error in " + getClass().getName() + "\n" + e2);
		}
	  }
	  return resultado;
	}

/** El m�todo eliminaPerfil() da de baja un perfil
 *  @return  Status del Movimiento.
 */
  public String eliminaPerfil(String perfil)
  {
    System.out.println("Se da de baja un perfil en la BD Taud");
    ConexionDAO conexion= new ConexionDAO();
    Connection con;
    String resultado = "";
    PreparedStatement query = null;
    StringBuffer lsb_query = new StringBuffer();
    ResultSet rs = null;

//  Query para dar de baja el perfil del usuario
    lsb_query = lsb_query.delete(0, lsb_query.length());
	lsb_query = lsb_query.delete(0, lsb_query.length());
    lsb_query.append(" DELETE  TA_PERMISOS ");
	lsb_query.append(" WHERE PERFIL = '").append(perfil).append("'");
	System.out.println("lsbQuery: "+lsb_query.toString());

	try
	{
	  con = conexion.getConexion();
	  query = con.prepareStatement(lsb_query.toString());
	  rs=query.executeQuery();
	  //rs.next();
	  resultado="0";
	}
	catch (SQLException e1)
	{
	  e1.printStackTrace();
	}
	finally
	{
	  try
	  {
	    rs.close();
	    query.close();
	    conexion.closeConexion();
	  }
	  catch(SQLException e2)
	  {
	    System.out.println("Error in " + getClass().getName() + "\n" + e2);
	  }
	}
	return resultado;
  }

/** El m�todo consultaLogueo() obtiene los permisos para el logueo
 *	a la aplicaci�n.
 *  @return  ArrayList valor de respuesta.
 */
  public ArrayList consultaUsuarios()
  {
	System.out.println("Consulta para obtener el perfil del usuario");
	ConexionDAO conexion= new ConexionDAO();
	Connection con;
	ArrayList resultado = new ArrayList();
	PreparedStatement query = null;
	StringBuffer lsb_query = new StringBuffer();
	ResultSet rs = null;
	int cont = 0;

//	Consulta que recupera los perfiles de los usuarios
	lsb_query = lsb_query.delete(0, lsb_query.length());
	lsb_query.append(" SELECT  DISTINCT PERFIL ");
	lsb_query.append(" FROM TA_PERMISOS");
	System.out.println("lsbQuery: "+lsb_query.toString());

	try
	{
	  con = conexion.getConexion();
	  query = con.prepareStatement(lsb_query.toString());
	  rs=query.executeQuery();
      while(rs.next())
	  {
		usuariosValue valores = new usuariosValue();
		valores.setperfil(rs.getString("PERFIL"));
		resultado.add(valores);
	  }
	}
	catch (Exception e1)
	{
	  e1.printStackTrace();
	}
	finally
	{
	  try
	  {
		rs.close();
		query.close();
		conexion.closeConexion();
	  }
	  catch(SQLException e2)
	  {
			System.out.println("Error in " + getClass().getName() + "\n" + e2);
	  }
	}
	return resultado;
  }

/** El m�todo actualizaPass() actualiza el password
 *   @return  estatus del movimiento
 */
  public String actualizaPass(String user,String password)
  {
    System.out.println("Se realiza el cambio de contrase�a seguridad");
    ConexionDAO conexion= new ConexionDAO();
    Connection con;
    String resultado = "";
	PreparedStatement query = null;
	StringBuffer lsb_query = new StringBuffer();
	ResultSet rs = null;
	int cont = 0;
	lsb_query = lsb_query.delete(0, lsb_query.length());
	lsb_query.append(" ALTER USER ").append(user).append(" IDENTIFIED BY ").append(password);
	System.out.println("lsbQuery: "+lsb_query.toString());

	try
	{
	  con = conexion.getConexion();
      query = con.prepareStatement(lsb_query.toString());
	  rs=query.executeQuery();
	  rs.next();
	  resultado = "0";
	}
	catch (SQLException e1)
	{
	  while (e1 != null)
	  {
		if(e1.getMessage().substring(0,9).equals("ORA-01009"))
		{
		  System.out.println("Message:   " + e1.getMessage ());
		  resultado = "0";
		  System.out.println("resultado:   " + resultado);
		  return resultado;
		}
		else
		{
	      if(e1.getMessage().substring(0,9).equals("ORA-28007"))
	      {
	  	    System.out.println("Message:   " + e1.getMessage ());
		    resultado = "1";
		    System.out.println("resultado:   " + resultado);
		    return resultado;
		  }
		  else
		  {
		    if(e1.getMessage().substring(0,9).equals("ORA-01003"))
		    {
		      System.out.println("Message:   " + e1.getMessage ());
		      resultado = "2";
		      System.out.println("resultado: " + resultado);
		      return resultado;
		    }
		    else
		    {
		      System.out.println("Message:   " + e1.getMessage ());
		      resultado = "3";
		      System.out.println("resultado: " + resultado);
		      return resultado;
		    }
		  }
		}
	  }
	  e1 = e1.getNextException();
	  try
	  {
	    rs.close();
	    query.close();
	    conexion.closeConexion();
	  }
	  catch(SQLException e2)
	  {
	    System.out.println("Error in " + getClass().getName() + "\n" + e2);
	  }
	}
	return resultado;
  }
}