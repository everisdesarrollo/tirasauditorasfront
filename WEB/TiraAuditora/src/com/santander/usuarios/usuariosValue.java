/** Banco Santander Mexicano
*   Clase usuariosValue  Valores de los Datos de la consulta
*                      de los perfiles para el logueo
*   @author Alfredo Resendiz Vargas
*   @version 1.0
*   Fecha de creacion : 11 de Abril del 2006
*   Responsable : Eloisa Hernandez H.
*   Descripcion : Se encarga de contener los valores de la consulta realizada.
*   modificacion :
*/
package com.santander.usuarios;

public class usuariosValue
{
    private String perfil;

/**
 * @return (String)Valor del perfil de usuario
 */
  public String getperfil()
  {
	return perfil;
  }

/**
 * @param (String)Valor del perfil de usuario
 */
  public void setperfil(String string)
  {
	perfil = string;
  }
}