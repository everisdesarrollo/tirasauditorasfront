/** Banco Santander Mexicano
*   Clase SucursalClase  Catalogo de Sucursales
*   @author Angel Gabriel Ramirez Alva
*   @version 1.0
*   fecha de creacion : 2 de Febrero del 2006
*   responsable : Eloisa Hernandez Hernandez
*   @param txtNumSuc  n�mero de sucursal
*   @param txtServidor Nombre del servidor
*   @param txtNomSuc Nombre de la sucursal
*   @return  sResultado datos de las sucursales
*   descripcion : Realiza una busqueda por sucursales a travez de diferentes tipos de consulta
*   Ultima Modificaci�n : VSWF
*   Fecha: 30/Abril/2008
*   Descripcion: se agrega la funcion sSucursalInsertaMasiva
*
*/
package com.santander.sucursal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.naming.NamingException;

import com.santander.utilerias.Conex03;
import com.santander.utilerias.ConexionDAO;

public class SucursalClase {
	/** El m�todo sSucursal(String lsNumsuc) obtiene los datos de las sucursales
	*   en base al parametro numero de sucursal
	*/
  public String sSucursal(String lsNumsuc)
  {
    System.out.println("M�todo 'sSucursal' obtiene los datos de las sucursales");
    Conex03 con= new Conex03();
	String  sResultado = "";
	StringBuffer lsbquery = new StringBuffer();
	try
	  {
	     con.conexionDB();
		 lsbquery = lsbquery.delete(0, lsbquery.length());
		 lsbquery.append(" SELECT CVE_PTOVTA,TRAB_SABADO,NOMBRE_MAQUINA,DIRECCION_IP,NOMBRE_LINK,DESCRIPCION, ");
		 lsbquery.append(" CALLE_NUMERO,COD_POSTAL,COLONIA,CIUDAD_POBLACION,DELEG_MUNICIPIO,ESTADO,ESTATUS ");
		 lsbquery.append(" FROM TA_SUCURSAL ");
		 lsbquery.append(" WHERE TRIM(CVE_PTOVTA) = '"+ lsNumsuc +"' ");
		 try
		   {
			  System.out.println(lsbquery);
			  sResultado = con.ejecutaQueryCom(lsbquery.toString());
		   }
		   catch(Exception e)
		   {
			 con.cierraConexionDB();
			 System.out.println("Error en query: " + lsbquery.toString());
		   }
	   }
	   catch (Exception e)
	   {
		  con.cierraConexionDB();
	   }
	   finally
	   {
		  con.cierraConexionDB();
	   }
	   return sResultado; //devuelve
  }//fin sSucursal
  /** El m�todo sSucursal2(String lsServidor) obtiene los datos de las sucursales
   ** en base al parametro nombre de servidor
  */
  public String sSucursal2(String lsServidor)
    {
	  System.out.println("M�todo 'sSucursal2' obtiene los datos de las sucursales");
	  Conex03 con= new Conex03();
	  int cont = 0;
	  String  sResultado = "";
	  StringBuffer lsbquery = new StringBuffer();
 	  try
 	    {
		  con.conexionDB();
		  lsbquery = lsbquery.delete(0, lsbquery.length());
		  lsbquery.append(" SELECT CVE_PTOVTA,TRAB_SABADO,NOMBRE_MAQUINA,DIRECCION_IP,NOMBRE_LINK,DESCRIPCION, ");
		  lsbquery.append(" CALLE_NUMERO,COD_POSTAL,COLONIA,CIUDAD_POBLACION,DELEG_MUNICIPIO,ESTADO,ESTATUS ");
		  lsbquery.append(" FROM TA_SUCURSAL ");
		  lsbquery.append(" WHERE TRIM(NOMBRE_MAQUINA) = '"+ lsServidor +"'  ");
		  try
		  	{
			  System.out.println(lsbquery);
			  sResultado = con.ejecutaQueryCom(lsbquery.toString());
			}
		  catch(Exception e)
		    {
			  con.cierraConexionDB();
			  System.out.println("Error en query: " + lsbquery.toString());
			}
		}
		catch(Exception e)
		  {
			con.cierraConexionDB();
		  }
		finally
		  {
			con.cierraConexionDB();
  		  }
	return sResultado; //devuelve
  }//fin sSucursal
  /** El m�todo sSucursal3(String lsNombre) obtiene los datos de las sucursales
   ** en base al parametro nombre de sucursal
  */
  public String sSucursal3(String lsNombre)
    {
	  System.out.println("M�todo 'sSucursal3' obtiene los datos de las sucursales");
	  Conex03 con= new Conex03();
	  int cont = 0;
	  String  sResultado = "";
	  StringBuffer lsbquery = new StringBuffer();
      try
        {
          con.conexionDB();
		  lsbquery = lsbquery.delete(0, lsbquery.length());
		  lsbquery.append(" SELECT CVE_PTOVTA,TRAB_SABADO,NOMBRE_MAQUINA,DIRECCION_IP,NOMBRE_LINK,DESCRIPCION, ");
		  lsbquery.append(" CALLE_NUMERO,COD_POSTAL,COLONIA,CIUDAD_POBLACION,DELEG_MUNICIPIO,ESTADO,ESTATUS ");
		  lsbquery.append(" FROM TA_SUCURSAL ");
		  lsbquery.append(" WHERE TRIM(DESCRIPCION) = '"+ lsNombre +"'  ");
		  try
		    {
		      System.out.println(lsbquery);
			  sResultado = con.ejecutaQueryCom(lsbquery.toString());
			}
		  catch(Exception e)
		    {
		      con.cierraConexionDB();
			  System.out.println("Error en query: " + lsbquery.toString());
			}
		}
	  catch (Exception e)
	    {
		  con.cierraConexionDB();
		}
	  finally
	    {
		  con.cierraConexionDB();
		}
	return sResultado; //devuelve
  }//fin sSucursal
  /** El m�todo sSucursal4() obtiene los datos de las sucursales
   **
  */
  public String sSucursal4()
    {
	  System.out.println("M�todo sSucursal4() obtiene los datos de las sucursales");
	  Conex03 con= new Conex03();
	  String sResultado = "";
	  StringBuffer lsbquery = new StringBuffer();
	  try
	    {
		  System.out.println("antes de hacer conexion");
		  con.conexionDB();
		  System.out.println("Despues de hacer conexi�n");
		  lsbquery = lsbquery.delete(0, lsbquery.length());
		  lsbquery.append(" SELECT CVE_PTOVTA,TRAB_SABADO,NOMBRE_MAQUINA,DIRECCION_IP,NOMBRE_LINK,DESCRIPCION, ");
		  lsbquery.append(" CALLE_NUMERO,COD_POSTAL,COLONIA,CIUDAD_POBLACION,DELEG_MUNICIPIO,ESTADO,ESTATUS ");
		  lsbquery.append(" FROM TA_SUCURSAL ");
		  lsbquery.append(" ORDER BY CVE_PTOVTA ");
		  try
		    {
			  System.out.println(lsbquery);
			  sResultado = con.ejecutaQueryCom(lsbquery.toString());
			  System.out.println("Si regeso con los datos bien: "+sResultado);
			}
		  catch(Exception e)
		    {
			  con.cierraConexionDB();
			  System.out.println("Error en query: " + lsbquery.toString());
			}
		}
	  catch(Exception e)
	    {
	      e.printStackTrace();
		  con.cierraConexionDB();
		}
	  finally
	    {
		  if(con.hayConexionAbierta())
		    {
			  con.cierraConexionDB();
			}
		}
	return sResultado; //devuelve
  }//fin sSucursal
  /** El m�todo sSucursalInserta(Vector lvDatos) realiza la insersi�n de un registro en la base de datos
   ** los parametros llegan en forma de vector: lvDatos
  */
  public String sSucursalInserta(Vector lvDatos)
    {
	  System.out.println("M�todo 'sSucursalInserta' realiza creacion de DBLink");
	  String dbLinkName = createDBLink(lvDatos.elementAt(3).toString());

	  System.out.println("M�todo 'sSucursalInserta' realiza la insersion de un registro en la base de datos");
	  Conex03 con= new Conex03();
	  String  sResultado = null;
	  if (dbLinkName == null) {
		  return "Hubo un error en la operaci�n, favor de contactar a Seguridad Inform�tica.";
	  }
	  StringBuffer lsbquery = new StringBuffer();
      try
        {
	 	  con.conexionDB();
		  lsbquery = lsbquery.delete(0, lsbquery.length());
		  lsbquery.append(" INSERT INTO TA_SUCURSAL ");
		  lsbquery.append(" (CVE_PTOVTA,TRAB_SABADO,NOMBRE_MAQUINA,DIRECCION_IP, ");
		  lsbquery.append(" NOMBRE_LINK,DESCRIPCION,CALLE_NUMERO,COD_POSTAL, ");
		  lsbquery.append(" COLONIA,CIUDAD_POBLACION,DELEG_MUNICIPIO,ESTADO,ORDEN,ESTATUS ) ");
		  lsbquery.append(" VALUES ('"+lvDatos.elementAt(1)+"', ");System.out.println("1: "+lvDatos.elementAt(1));
		  lsbquery.append(" '"+lvDatos.elementAt(2)+"', ");System.out.println("2: "+lvDatos.elementAt(2));
		  lsbquery.append(" '"+lvDatos.elementAt(3)+"', ");System.out.println("3:"+lvDatos.elementAt(3));
		  lsbquery.append(" '"+lvDatos.elementAt(4)+"', ");System.out.println("4: "+lvDatos.elementAt(4));
		  lsbquery.append(" '"+dbLinkName+"', ");System.out.println("5: "+dbLinkName);
		  lsbquery.append(" '"+lvDatos.elementAt(6)+"', ");System.out.println("6: "+lvDatos.elementAt(6));
		  lsbquery.append(" '"+lvDatos.elementAt(7)+"', ");System.out.println("7: "+lvDatos.elementAt(7));
		  lsbquery.append(" '"+lvDatos.elementAt(8)+"', ");System.out.println("8: "+lvDatos.elementAt(8));
		  lsbquery.append(" '"+lvDatos.elementAt(9)+"', ");System.out.println("9: "+lvDatos.elementAt(9));
		  lsbquery.append(" '"+lvDatos.elementAt(10)+"', ");System.out.println("10: "+lvDatos.elementAt(10));
		  lsbquery.append(" '"+lvDatos.elementAt(11)+"', ");	System.out.println("11: "+lvDatos.elementAt(11));
		  lsbquery.append(" '"+lvDatos.elementAt(12)+"',  ");System.out.println("12: "+lvDatos.elementAt(12));
		  lsbquery.append(" "+lvDatos.elementAt(13)+",  ");System.out.println("13: "+ lvDatos.elementAt(13));
		  lsbquery.append(" '"+lvDatos.elementAt(14)+"' ) ");System.out.println("14: " + lvDatos.elementAt(14));
		  try
		  	{
			  System.out.println(lsbquery);
			  con.ejecutaSQL(lsbquery.toString());
			  System.out.println("al final de ejecutar el query");
			  sResultado = "La operaci�n fue realizada con �xito.";
			  con.terminaTransaccion(true);
			}
		  catch(Exception e)
		    {
			  con.cierraConexionDB();
			  System.out.println("Error en query: " + lsbquery.toString());
			  e.printStackTrace();
			}
		}
	  catch(Exception e)
		{
		  e.printStackTrace();
		  con.cierraConexionDB();
		}
	  finally
	    {
		  if(con.hayConexionAbierta())
		    {
			  con.cierraConexionDB();
			}
		}
	return sResultado; //devuelve
  }//fin sSucursalInserta
 /**  El m�todo sEliminaSuc(String lvsDatos) realiza la eliminaci�n de un registro en la base de datos,
  **  los parametros llegan en forma de vector: lvDatos
 */
  public String sEliminaSuc(String lvsDatos)
    {
	  System.out.println("M�todo 'sEliminaSuc' realiza la eliminaci�n de un registro en la base de datos");
	  Conex03 con= new Conex03();
	  StringBuffer lsbquery = new StringBuffer();
	  String  sResultado = "";
	  Vector lvDatos = new Vector();
	  String limit = ",";
	  StringTokenizer strToken_value= new StringTokenizer(lvsDatos, limit );
	  while(strToken_value.hasMoreElements())
		{
		  String tmpVar= (strToken_value.nextToken()).trim();
		  lvDatos.add(tmpVar);
		}
	  try
	    {
		  con.conexionDB();
		  lsbquery = lsbquery.delete(0, lsbquery.length());
		  lsbquery.append(" DELETE FROM TA_SUCURSAL ");
		  lsbquery.append(" WHERE CVE_PTOVTA = '"+lvDatos.elementAt(1).toString().trim()+"' ");
		try
		  {
		    System.out.println(lsbquery);
			con.ejecutaSQL(lsbquery.toString());
			System.out.println("al final de ejecutar el query");
			sResultado = "0";
			con.terminaTransaccion(true);
		  }
		catch(Exception e)
		  {
			con.cierraConexionDB();
			System.out.println("Error en query: " + lsbquery.toString());
			e.printStackTrace();
		  }
	  }
	  catch(Exception e)
	    {
		  e.printStackTrace();
		  con.cierraConexionDB();
		}
	  finally
	    {
	 	  if(con.hayConexionAbierta())
	 	    {
			  con.cierraConexionDB();
			}
		}
	return sResultado; //devuelve
  }//fin sSucursalInserta
	/**  El m�todo sModificaSucursal(Vector lvDatos) realiza la modificaci�n de un registro en la base de datos,
	 **  los parametros llegan en forma de vector: lvDatos
 	*/
  public String sModificaSucursal(Vector lvDatos)
    {
	  System.out.println("M�todo 'sModificaSucursal' realiza la modificaci�n de un registro en la base de datos");
	  Conex03 con= new Conex03();
	  int cont = 0;
	  String  sResultado = "Error en la Transacci�n";
	  int NumElementos = lvDatos.size();
	  StringBuffer lsbquery = new StringBuffer();
	  try
	    {
		  con.conexionDB();
		  lsbquery = lsbquery.delete(0, lsbquery.length());
		  lsbquery.append(" UPDATE TA_SUCURSAL ");
		  lsbquery.append(" SET CVE_PTOVTA = '"+lvDatos.elementAt(1)+"', ");
		  lsbquery.append(" TRAB_SABADO = '"+lvDatos.elementAt(2)+"', ");
		  lsbquery.append(" NOMBRE_MAQUINA = '"+lvDatos.elementAt(3)+"', ");
		  lsbquery.append(" DIRECCION_IP = '"+lvDatos.elementAt(4)+"', ");
		  lsbquery.append(" NOMBRE_LINK = '"+lvDatos.elementAt(5)+"', ");
		  lsbquery.append(" DESCRIPCION = '"+lvDatos.elementAt(6)+"', ");
		  lsbquery.append(" CALLE_NUMERO = '"+lvDatos.elementAt(7)+"', ");
		  lsbquery.append(" COD_POSTAL = '"+lvDatos.elementAt(8)+"', ");
		  lsbquery.append(" COLONIA = '"+lvDatos.elementAt(9)+"', ");
		  lsbquery.append(" CIUDAD_POBLACION = '"+lvDatos.elementAt(10)+"', ");
		  lsbquery.append(" DELEG_MUNICIPIO = '"+lvDatos.elementAt(11)+"', ");
		  lsbquery.append(" ESTADO = '"+lvDatos.elementAt(12)+"', ");
		  lsbquery.append(" ESTATUS = '"+lvDatos.elementAt(13)+"' ");
		  lsbquery.append(" WHERE NOMBRE_LINK = '"+lvDatos.elementAt(5)+"' ");
		  try
		  	{
			  System.out.println(lsbquery);
			  con.ejecutaSQL(lsbquery.toString());
			  System.out.println("al final de ejecutar el query");
			  sResultado = "Modificaci�n Realizada";
			  con.terminaTransaccion(true);
			}
		  catch(Exception e)
		    {
			  con.cierraConexionDB();
			  System.out.println("Error en query: " + lsbquery.toString());
			  e.printStackTrace();
			}
		}
		catch (Exception e)
		  {
		    e.printStackTrace();
			con.cierraConexionDB();
	      }
		finally
		  {
		    if(con.hayConexionAbierta())
		      {
			    con.cierraConexionDB();
			  }
		  }
	return sResultado; //devuelve
  }//fin sModificaSucursal

	/**  El m�todo sModificaSucursal(Vector lvDatos) realiza la modificaci�n basica de un registro en la base de datos,
	 **  los parametros llegan en forma de vector: lvDatos
	*/
public String sModificaBasicaSucursal(Vector lvDatos)
  {
	  System.out.println("M�todo 'sModificaBasicaSucursal' realiza la modificaci�n basica de un registro en la base de datos");
	  Conex03 con= new Conex03();
	  int cont = 0;
	  String  sResultado = "Error en la Transacci�n";
	  int NumElementos = lvDatos.size();
	  StringBuffer lsbquery = new StringBuffer();
	  try
	    {
		  con.conexionDB();
		  lsbquery = lsbquery.delete(0, lsbquery.length());
		  lsbquery.append(" UPDATE TA_SUCURSAL ");
		  lsbquery.append(" SET TRAB_SABADO = '"+lvDatos.elementAt(2)+"', ");
		  lsbquery.append(" ESTATUS = '"+lvDatos.elementAt(3)+"' ");
		  lsbquery.append(" WHERE CVE_PTOVTA = '"+lvDatos.elementAt(1)+"' ");
		  try
		  	{
			  System.out.println(lsbquery);
			  con.ejecutaSQL(lsbquery.toString());
			  System.out.println("al final de ejecutar el query");
			  sResultado = "Modificaci�n Realizada";
			  con.terminaTransaccion(true);
			}
		  catch(Exception e)
		    {
			  con.cierraConexionDB();
			  System.out.println("Error en query: " + lsbquery.toString());
			  e.printStackTrace();
			}
		}
		catch (Exception e)
		  {
		    e.printStackTrace();
			con.cierraConexionDB();
	      }
		finally
		  {
		    if(con.hayConexionAbierta())
		      {
			    con.cierraConexionDB();
			  }
		  }
	return sResultado; //devuelve
}//fin sModificaBasicaSucursal
	/**
	 * Alta Masiva de sucursales.
	 * @author Emmanuel S�nchez Castillo
	 * Fecha de creaci�n : May 1, 2008
	 * Modificaci�n:
	 * 06 May
	 * Emmanuel Sanchez
	 * se modifica el uso de vectores
	 * @param datosSucursales
	 * @return
	 */
	public boolean sSucursalInsertaMasiva(Vector datosSucursales)
	{


		boolean resultado = false;
		Conex03 con = new Conex03();

		CallableStatement proc = null;

		if(datosSucursales.size() < 14 )
		 {
			System.out.println("Numeros de campos insuficientes para insertar la sucursal");
			return false;
		  }

		try {
			con.conexionDB();
		} catch (SQLException e1) {
			System.out.println(e1.getCause().getMessage());
			e1.printStackTrace();
			System.out.println("No se pudo establecer la conexion con la BD");
			return false;
		} catch (NamingException e1) {
			System.out.println(e1.getCause().getMessage());
			e1.printStackTrace();
			System.out.println("No se pudo establecer la conexion con la BD");
			return false;
		}

		if(con.getConnection() == null)
		{
			System.out.println("No se pudo establecer la conexion con la BD");
			return false;
		}

		StringBuffer lsbquery = null;
		StringBuffer lsbqueryTmp = null;

			 lsbquery = new StringBuffer();
			 lsbqueryTmp = new StringBuffer();
			 try {

			  lsbqueryTmp.append(" '"+datosSucursales.elementAt(0)+"', ");
			  lsbqueryTmp.append(" '"+datosSucursales.elementAt(1)+"', ");
			  lsbqueryTmp.append(" '"+datosSucursales.elementAt(2)+"', ");
			  lsbqueryTmp.append(" '"+datosSucursales.elementAt(3)+"', ");
			  lsbqueryTmp.append(" '"+datosSucursales.elementAt(4)+"', ");
			  lsbqueryTmp.append(" '"+datosSucursales.elementAt(5)+"', ");
			  lsbqueryTmp.append(" '"+datosSucursales.elementAt(6)+"', ");
			  lsbqueryTmp.append(" '"+datosSucursales.elementAt(7)+"', ");
			  lsbqueryTmp.append(" '"+datosSucursales.elementAt(8)+"', ");
			  lsbqueryTmp.append(" '"+datosSucursales.elementAt(9)+"', ");
			  lsbqueryTmp.append(" '"+datosSucursales.elementAt(10)+"', ");
			  lsbqueryTmp.append(" '"+datosSucursales.elementAt(11)+"', ");
			  lsbqueryTmp.append(" '"+datosSucursales.elementAt(12)+"', ");
			  lsbqueryTmp.append(" '"+datosSucursales.elementAt(13)+"' )  ");
			  System.out.println("Insertado sucursal con datos " + lsbqueryTmp.toString());


			  lsbquery.append("CALL S_SUCURSAL_INSERTA(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

			  proc = con.getConnection().prepareCall(lsbquery.toString());

			  proc.setString(1,(String)datosSucursales.elementAt(0));
			  proc.setString(2,(String)datosSucursales.elementAt(1));
			  proc.setString(3,(String)datosSucursales.elementAt(2));
			  proc.setString(4,(String)datosSucursales.elementAt(3));
			  proc.setString(5,(String)datosSucursales.elementAt(4));
			  proc.setString(6,(String)datosSucursales.elementAt(5));
			  proc.setString(7,(String)datosSucursales.elementAt(6));
			  proc.setString(8,(String)datosSucursales.elementAt(7));
			  proc.setString(9,(String)datosSucursales.elementAt(8));
			  proc.setString(10,(String)datosSucursales.elementAt(9));
			  proc.setString(11,(String)datosSucursales.elementAt(10));
			  proc.setString(12,(String)datosSucursales.elementAt(11));
			  proc.setInt(13,Integer.parseInt((String) datosSucursales.elementAt(12)));
			  proc.setString(14,(String)datosSucursales.elementAt(13));

			  try
			  {
				  proc.execute();
				  con.terminaTransaccion(true);
				  resultado = true;
			  }catch(SQLException e)
			  {
				  resultado = false;
				  e.printStackTrace();
			  }

			 } catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					resultado = false;
			}finally{
				try{
				proc.close();
				}catch(SQLException e)
				{
					e.printStackTrace();
				}
			}
		 con.cierraConexionDB();


		return resultado;
	}
	/**
	 * VCM 07/09/12 El m�todo loadOnLine() realiza la carga en linea de una sucursal
	 *
	 * @return String valor de respuesta de la consulta
	 */
	public String createDBLink(String nombreServidor) {
		System.out.println("Crea un nuevo DBLink");
		// Se realiza la conexi�n a la BD
		ConexionDAO conexion = new ConexionDAO();
		Connection con = null;
		String resultado = null;
		CallableStatement query = null;
		StringBuffer lsbQuery = new StringBuffer();
		ResultSet rs = null;

		lsbQuery = lsbQuery.delete(0, lsbQuery.length());
		lsbQuery.append("{? = call new_dblink(?)}");
		System.out.println("lsbQuery: call new_dblink(" + nombreServidor + ")");

		try {
			con = conexion.getConexion();
			query = con.prepareCall(lsbQuery.toString());
			query.registerOutParameter(1, Types.VARCHAR);
			query.setString(2, nombreServidor);
			query.execute();
			resultado = query.getString(1);
			System.out.println(resultado);
		} catch (SQLException e1) {
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (query != null)
					query.close();
				if (con != null)
					con.close();
				conexion.closeConexion();
			} catch (SQLException e2) {
				System.out.println("Error in " + getClass().getName() + "\n"+ e2);
			}
		}
		return resultado;
	}

}//fin Sucursal Clase