/** Banco Santander Mexicano
*   Clase TiraClase  Resuelve las operaciones de Tira Auditora
*   @author Ing. David Aguilar G�mez
*   @version 1.0
*   fecha de creacion : 1 de Febrero del 2006
*   responsable : Eloisa Hernandez H
*   descripcion : resuelve operaciones de consulta, creacion PDF y carga manual de la Tira Auditora.
*   modificacion :
*/
package com.santander.sucursal;

import java.io.ByteArrayOutputStream;
import java.util.*;
import com.lowagie.text.*;
import com.lowagie.text.pdf.*;
import java.awt.Color;
import com.santander.utilerias.ConexionDAO;
import com.santander.utilerias.LoadProperties;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SucImpClase
{
private LoadProperties rb = LoadProperties.getInstance();
/** El m�todo makePdf(int, ArrayList, String, String) genera el PDF
*   @param propiedad  Colecci�n de propiedades
*   @param seleccion  criterio de busqueda
*   @param datos Valores que se insertara en el PDF
*   @param dato1 Valor que se insertara en el encabezado
*   @param dato2 Valor que se insertara en el encabezado
*   @return ByteArrayOutputStream El PDF
*/
	public ByteArrayOutputStream makePdf(Properties propiedad, int seleccion, Vector vDatos, String dato1, String dato2)
	{
		System.out.println("M�todo makePdf");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Font black = FontFactory.getFont(FontFactory.HELVETICA, Font.DEFAULTSIZE, Font.NORMAL, new Color(0x00, 0x00, 0x00));
		Rectangle hoja = null;
		if(seleccion== 0 || seleccion == 4)
		  hoja = PageSize.LETTER.rotate();
		else
		  hoja = PageSize.LETTER;
		Document document = new Document(hoja,10,10,10,10);
		try
		  {
			// create simple doc and write to a ByteArrayOutputStream
			PdfWriter.getInstance(document, baos);
			document.open();
			PdfPTable datatable = creaEncabezado(propiedad, seleccion,dato1,dato2);
			datatable.getDefaultCell().setBorderWidth(1);
			switch(seleccion)
			  {
				case 0:
				  int cuantos = vDatos.size()/12;
				  for (int x=0;x<cuantos;x++)
				    {
					  if (x%2 == 0)
					    {
					  	  datatable.getDefaultCell().setBackgroundColor(new Color(0xFF, 0xFF, 0xFF));
						}
					  else
					    {
						  datatable.getDefaultCell().setBackgroundColor(new Color(0xEB, 0xEB, 0xEB));
						}

					System.out.println("punto de prueba: "+x);
					String sconcatenado = "";
					for(int r=0; r<12; r++)
					  {
					    if(r<6)
						  datatable.addCell((String)vDatos.elementAt(x * 12 + r));
						else
						  sconcatenado = sconcatenado +  (String)vDatos.elementAt(x * 12 + r) + ",";
					  }
					datatable.addCell(sconcatenado);
				  }
				break;
				default:
			  	  // En esta opcion nunca entrara.
			  	  System.out.println("No es selecci�n valida.");
				break;

		  	  }
			document.add(datatable);
			document.close();
		  }
		catch (Exception e2)
		  {
			System.out.println("Error in " + getClass().getName() + "\n" + e2);
		  }
		return baos;
	}

/** El m�todo creaEncabezado(int, String, String) genera el encabezado del PDF
*   @param propiedad  Colecci�n de propiedades
*   @param seleccion  criterio de busqueda
*   @param dato1 Valor que se insertara en el encabezado
*   @param dato2 Valor que se insertara en el encabezado
*   @return PdfPTable Encabezado del PDF
*/
  private PdfPTable creaEncabezado(Properties propiedad, int seleccion, String dato1, String dato2)
    {
	  System.out.println("M�todo creaEncabezado");
	  PdfPTable datatable = null;
	  Font white = FontFactory.getFont(FontFactory.HELVETICA, Font.DEFAULTSIZE, Font.BOLD, new Color(0xFF, 0xFF, 0xFF));
	  Font whiteG = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, new Color(0xFF, 0xFF, 0xFF));
	  int columnas = 0;
	  Paragraph titulo = null;
	  try
	    {
	 	  switch(seleccion)
		    {
			  case 0:
			    columnas = 7;
				titulo = new Paragraph("Consulta de Sucursales ",whiteG);
 			  break;
			  default:
			    // En esta opcion nunca entrara.
				System.out.println("No es selecci�n valida.");
			  break;
			}
		  datatable = new PdfPTable(columnas);
		  Image logo = Image.getInstance(rb.getValue("ta.logosantander"));
		  PdfPCell cell = new PdfPCell(logo);
		  cell.setColspan(2);
		  cell.setBorderWidth(2);
		  cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
		  cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		  datatable.addCell(cell);
		  cell = new PdfPCell(titulo);
		  cell.setColspan(columnas-2);
		  cell.setBorderWidth(2);
		  cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		  cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		  cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
		  cell.setBackgroundColor(new Color(0xF8, 0x00, 0x00));
		  datatable.addCell(cell);
		  datatable.setWidthPercentage(100); // porcentaje
		  datatable.getDefaultCell().setPadding(3);
		  datatable.getDefaultCell().setBorderWidth(2);
		  datatable.getDefaultCell().setBorderColor(new Color(0xFF, 0xFF, 0xFF));
		  datatable.getDefaultCell().setBackgroundColor(new Color(0x99, 0x99, 0x99));
		  datatable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		  datatable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		  switch(seleccion)
		    {
			  case 0:
			    int headerwidths0[] = { 12, 10, 12,15,10,14,50}; // porcentaje
			    datatable.setWidths(headerwidths0);
			    datatable.addCell(new Paragraph("C de C ALTAIR",white));
			    datatable.addCell(new Paragraph("T. Sabado",white));
			    datatable.addCell(new Paragraph("Nombre Servidor",white));
			    datatable.addCell(new Paragraph("Direcci�n IP",white));
			    datatable.addCell(new Paragraph("Link",white));
			    datatable.addCell(new Paragraph("Sucursal",white));
			    datatable.addCell(new Paragraph("Direcci�n",white));
			  break;
			  default:
				// En esta opcion nunca entrara.
			    System.out.println("No es selecci�n valida.");
			  break;
		    }
		  datatable.setHeaderRows(2); // this is the end of the table header
		}
	  catch (Exception e)
	    {
		  System.out.println("Error in " + getClass().getName() + "\n" + e);
		}
	return datatable;
  }

}//fin clase