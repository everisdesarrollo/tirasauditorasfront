package com.santander.sucursal;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

/**
 *  VSWF
*   Clase CargaArchivo.java  Clase para cargar archivo Excel y validar que los datos tengan un formatos valido.
*   @author Emmanuel S�nchez Castillo
*   @version 1.0
*   Fecha de creaci�n : Apr 26, 2008
*   Descripci�n : Clase para cargar archivo Excel y validar que los datos tengan un formatos valido.
*   Ultima modificaci�n:
*/
public class CargaArchivo {

	public static final String TIPO_ERR1="ERR01";
	public static final String TIPO_ERR2="ERR02";
	/**
	 * Funcion para cargar el archivo de excel.
	 * @author Emmanuel S�nchez Castillo
	 * Fecha de creaci�n : Apr 26, 2008
	 * Modificaci�n:
	 * @param request
	 * @return
	 */
	public boolean cargaArchivoExcel(HttpServletRequest request,Vector datos) throws Exception
	{
		//
		//Vector datosSucursales = new Vector();

		//Validamos que sea un archivo multipart
		if(FileUpload.isMultipartContent(request))
		{

			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);

			List items = null ;
			String tmpCelda ="";
			//Se realiza la carga del archivo
			try {
				 items = upload.parseRequest(request);
			} catch (FileUploadException e) {
				//Error al cargar el Archivo
				datos.add(TIPO_ERR1);
				datos.add("Hubo un error al cargar el archivo");
				System.out.println("Se genero un error al cargar el archivo");
				e.printStackTrace();
				return false ;
			}


			Iterator it = items.iterator();
			FileItem archivo ;
			// se recorre la lista de campos
			while(it.hasNext())
			{
				archivo = (FileItem)it.next();
				//validar que archivo se un archivo multipart
				if(archivo.isFormField())
				{
					System.out.println("No es un campo multipart");
				}
				else
				{
					System.out.println("Es un campo multipart");

					if(!"ta_sucursalesnvas.xls".equals(obtenNombreArchivo(archivo.getName())))
					{
						datos.add(TIPO_ERR1);
						datos.add("El Nombre del archivo no es correcto, el archivo debe llamarse ta_sucursalesnvas.xls");
						return false;
					}

					//Se realiza la lectura del archivo excel

					try {
						//Se abre el archivo excel
						FileInputStream stream = (FileInputStream)archivo.getInputStream();
						POIFSFileSystem archivoSucursales = new  POIFSFileSystem(stream);
						HSSFWorkbook libro = new HSSFWorkbook(archivoSucursales);
						HSSFSheet hoja = libro.getSheetAt(0);
						Iterator it2 = hoja.rowIterator();
						//System.out.println("Sucursales a insertar " + hoja.getLastRowNum() + 1);
						//Se recorre las filas del archivo
						SucursalClase sucursal = new SucursalClase();
						int total = 0;
						int totalOk = 0;
						while(it2.hasNext())
						{
							HSSFRow renglon = (HSSFRow) it2.next();
							short numCelda = renglon.getFirstCellNum();
							Vector vRenglon = new Vector();
							String srenglon = "";
							int numCeldas = renglon.getLastCellNum();
							for(short i = renglon.getFirstCellNum(); i<= numCeldas; i++)
							{
								HSSFCell celda = renglon.getCell(i);

								if(celda != null)
								{
									switch(celda.getCellType())
									{
										case HSSFCell.CELL_TYPE_BLANK: tmpCelda =""; break;
										case HSSFCell.CELL_TYPE_BOOLEAN: tmpCelda = celda.getBooleanCellValue() +"" ; break;
										case HSSFCell.CELL_TYPE_NUMERIC: tmpCelda = celda.getNumericCellValue() +"" ; break;
										case HSSFCell.CELL_TYPE_STRING: tmpCelda = celda.getStringCellValue() +""; break;
										default:

									}
								}
								//Se obtiene el valor de la celda y se valida si es nula
								//tmpCelda = celda.getStringCellValue();

								tmpCelda = (tmpCelda == null ? "" : tmpCelda);
								//System.out.print(tmpCelda);
								if(celda != null)
								{
								switch (i)
									{
									case 0:
										if(tmpCelda.indexOf(".") >0)
										{
											tmpCelda = tmpCelda.substring(0,tmpCelda.indexOf("."));
										}
										if(esCadenaNumerica(tmpCelda))
											tmpCelda = rellenar(tmpCelda,"I",4,'0');
										vRenglon.add((tmpCelda.length() > 4 ? tmpCelda.substring(0,4) : tmpCelda) );
									break;
									case 1:
										vRenglon.add((tmpCelda.length() > 1 ? tmpCelda.substring(0,1) : tmpCelda) );
									break;
									case 2:
										vRenglon.add((tmpCelda.length() > 20 ? tmpCelda.substring(0,20) : tmpCelda) );
									break;
									case 3:
										vRenglon.add((tmpCelda.length() > 16 ? tmpCelda.substring(0,16) : tmpCelda) );
									break;
									case 4:
										vRenglon.add((tmpCelda.length() > 25 ? tmpCelda.substring(0,25) : tmpCelda) );
									break;
									case 5:
										vRenglon.add((tmpCelda.length() > 50 ? tmpCelda.substring(0,50) : tmpCelda) );
									break;
									case 6:
										vRenglon.add((tmpCelda.length() > 60 ? tmpCelda.substring(0,60) : tmpCelda) );
									break;
									case 7:
										vRenglon.add((tmpCelda.length() > 5 ? tmpCelda.substring(0,5) : tmpCelda) );
									break;
									case 8:
										vRenglon.add((tmpCelda.length() > 30 ? tmpCelda.substring(0,30) : tmpCelda) );
									break;
									case 9:
										vRenglon.add((tmpCelda.length() > 35 ? tmpCelda.substring(0,35) : tmpCelda) );
									break;
									case 10:
										vRenglon.add((tmpCelda.length() > 35 ? tmpCelda.substring(0,35) : tmpCelda) );
									break;
									case 11:
										vRenglon.add((tmpCelda.length() > 35 ? tmpCelda.substring(0,35) : tmpCelda) );
									break;
									case 12:
										if(tmpCelda.indexOf(".") >0)
										{
											tmpCelda = tmpCelda.substring(0,tmpCelda.indexOf("."));
										}
										vRenglon.add((tmpCelda.length() > 4 ? tmpCelda.substring(0,4) : tmpCelda) );
									break;
									case 13:
										vRenglon.add((tmpCelda.length() > 1 ? tmpCelda.substring(0,1) : tmpCelda) );
									break;
								}
							}
							}



							String tmpSuc = (String)vRenglon.get(0);
							if(tmpSuc !=null && !"".equals(tmpSuc.trim()) )
							{
								total++;
								totalOk++;
								if(esCadenaNumerica((String)vRenglon.get(0)))
								{
									if(!sucursal.sSucursalInsertaMasiva(vRenglon))
									{
										System.out.println("Hubo un problema al insertar la sucursal" + vRenglon.get(0));
										datos.add(vRenglon.get(0));
										totalOk--;
									}
								}
								else{
									totalOk--;
									datos.add(vRenglon.get(0));
								}

							}
							//datos.add(vRenglon);
							//informacion.add(renglon);
						}
						System.out.println("Total Sucursales procesadas: " + total + " Total Sucursales insertadas: " + (totalOk >=0?totalOk:"0" ));
					} catch (IOException e1) {
						datos.add("Hubo un error al cargar el archivo");
						System.out.println("No es un archivo multipart");
						return false;
					}


				}
			}

		}
		else {
			datos.add(TIPO_ERR1);
			datos.add("Hubo un error al cargar el archivo");
			System.out.println("No es un formulario multipart");
			return false ;
		}
		return true;
	}

	public boolean esCadenaNumerica(String cad)
	{
		int longitud = cad.length();
		if(cad!= null)
		{
			for(int i = 0; i < longitud; i++)
			{
				if(!Character.isDigit( cad.charAt(i)))
					return false;
			}
		}
		return true;
	}

	public String rellenar(String cadena,String pos,int tamanio ,char carac )
	{
		int lon;
		String aux1="";
		if(cadena == null)
			cadena ="";
		lon = cadena.length();
		if(lon > tamanio)//Validamos la longitud de la cadena, si es mayor al tama�o deseado se corta
			cadena = cadena.substring(0,tamanio);
		lon = (tamanio - lon < 0 ? 0 : tamanio - lon ); //obtenemos la longitud de la cadena de relleno

		for(int i = 1 ; i <= lon ; i++)
		{
			aux1 += carac;
		}
		//validamos si se rellena a la izquierda o a la derecha
		if("I".equals(pos))
			aux1 = aux1 + cadena;
		else
			aux1 = cadena + aux1;
		return aux1;
	}

	private String obtenNombreArchivo(String nombreArchivo)
	{
		String nombreAux = "";
		//System.out.println("Nombre archivo entrada " + nombreAux);
		if(nombreArchivo == null)
			return "";
		//Remplazamos diagonales por diagonales inversas
		nombreAux = nombreArchivo.replace('/','\\');
		if(nombreAux.lastIndexOf('\\') >= 0)
		{
			nombreAux = nombreAux.substring(nombreArchivo.lastIndexOf('\\')+1);
		}
		System.out.println("Nombre Archivo " + nombreAux);
		return nombreAux.toLowerCase();
	}



}