/** Banco Santander Mexicano
*   Clase ContingenteValue  Valores de los Datos de la consulta
*                           de sucursales contingentes
*   @author Alfredo Resendiz Vargas
*   @version 1.0
*   Fecha de creacion : 2 de Febrero del 2006
*   Responsable : Eloisa Hernandez H.
*   Descripcion : Se encarga de contener los valores de la consulta realizada.
*   modificacion :
*/
package com.santander.contingente;

public class ManCtgValue
{
  private int verificacion;
  private int elimina;
  private int num_intentos;
  private String suc_altair;
  private String fecha;
  private String nom_sucursal;
  private String desc_sistema;
  private String desc_usuario;
  private String verificacion_sis;

/**
 * @return (String)Valor de la sucursal
 */
  public String get_suc_altair()
  {
	return suc_altair;
  }

/**
 * @return (String)Valor de la fecha
*/
  public String get_fecha()
  {
 	return fecha;
  }

/**
 * @return (String)Valor del nombre de la sucursal
 */
  public String get_nom_sucursal()
  {
  	return nom_sucursal;
  }

/**
 * @return (String)Valor de la descripción del sistema
 */
  public String get_desc_sistema()
  {
 	return desc_sistema;
  }

/**
 * @return (String)Valor de la descripción del usuario
 */
  public String get_desc_usuario()
  {
	return desc_usuario;
  }

/**
 * @return (String)Valor de la casilla de verificación
 */
  public String get_verificacion_sis()
  {
	return verificacion_sis;
  }

/**
 * @param (String)Valor de la sucursal
 */
  public void set_suc_altair(String string)
  {
	suc_altair = string;
  }

/**
 * @param (String)Valor de la fecha
 */
  public void set_fecha(String string)
  {
	fecha = string;
  }

/**
 * @param (String)Valor del nombre de la sucursal
 */
  public void set_nom_sucursal(String string)
  {
	nom_sucursal = string;
  }

/**
 * @param (String)Valor de la descripción del sistema
 */
  public void set_desc_sistema(String string)
  {
	desc_sistema = string;
  }

/**
 * @param (String)Valor de la descripción del usuario
 */
  public void set_desc_usuario(String string)
  {
 	desc_usuario = string;
  }

/**
 * @param (String)Valor de la casilla de verificación
 */
  public void set_verificacion_sis(String string)
  {
	verificacion_sis = string;
  }

/**
 * @return (int)Bandera de verificacion
 */
  public int get_verificacion()
  {
	return verificacion;
  }

/**
 * @param (int)Bandera de verificacion
 */
  public void set_verificacion(int i)
  {
	verificacion = i;
  }

/**
 * @return (int)Bandera de eliminación
 */
  public int getElimina()
  {
	return elimina;
  }

/**
 * @param (int)Bandera de eliminación
 */
  public void setElimina(int i)
  {
 	elimina = i;
  }

  /**
 * @return (int)Número de intentos
 */
  public int get_num_intentos()
  {
	return num_intentos;
  }

/**
 * @param (int)Número de intentos
 */
  public void set_num_intentos(int i)
  {
 	num_intentos= i;
  }
}