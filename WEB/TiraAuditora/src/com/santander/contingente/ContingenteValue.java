/** Banco Santander Mexicano
*   Clase ContingenteValue  Valores de los Datos de la consulta
*                           de sucursales contingentes
*   @author Alfredo Resendiz Vargas
*   @version 1.0
*   Fecha de creacion : 2 de Febrero del 2006
*   Responsable : Eloisa Hernandez H.
*   Descripcion : Se encarga de contener los valores de la consulta realizada.
*   modificacion :
*/
package com.santander.contingente;

public class ContingenteValue
{
	private int impresion;
	private String suc_altair;
	private String fecha;
	private String nom_sucursal;
	private String desc_sistema;
	private String desc_usuario;
	private String verificacion;

/**
 * @return (String)Valor de la sucursal
 */
	public String get_suc_altair()
	{
	return suc_altair;
	}

/**
 * @return (String)Valor de la fecha
 */
	public String get_fecha()
	{
		return fecha;
	}

/**
 * @return (String)Valor del nombre de la sucursal
 */
	public String get_nom_sucursal()
	{
		return nom_sucursal;
	}

/**
 * @return (String)Valor de la descripción del sistema
 */
	public String get_desc_sistema()
	{
		return desc_sistema;
	}

/**
 * @return (String)Valor de la descripción del usuario
 */
	public String get_desc_usuario()
	{
		return desc_usuario;
	}

/**
 * @return (String)Valor de la casilla de verificación
 */
	public String get_verificacion()
	{
		return verificacion;
	}

/**
 * @param (String) Valor de la sucursal
 */
	public void set_suc_altair(String string)
	{
		suc_altair = string;
	}

/**
 * @param (String) Valor de la fecha
 */
	public void set_fecha(String string)
	{
		fecha = string;
	}

/**
 * @param (String) Valor delnombre de la sucursal
 */
	public void set_nom_sucursal(String string)
	{
		nom_sucursal = string;
	}

/**
 * @param (String) Valor de la descripcion del sistema
 */
	public void set_desc_sistema(String string)
	{
		desc_sistema = string;
	}

/**
 * @param (String) Valor de la descripcion del usuario
 */
	public void set_desc_usuario(String string)
	{
		desc_usuario = string;
	}

/**
 * @param (String) Valor de la casilla de verificación
 */
	public void set_verificacion(String string)
	{
		verificacion = string;
	}

/**
 * @return(int) Bandera de impresión
 */
	public int get_impresion()
	{
		return impresion;
	}

/**
 * @param (int) Bandera de impresión
 */
	public void set_impresion(int i)
	{
		impresion = i;
	}
}