package com.santander.contingente;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.santander.utilerias.ConexionDAO;

public class CargaDiariaClase {
	static Connection con;
	ConexionDAO conexion = new ConexionDAO();

	public CargaDiariaClase() {
		try {
			if (con == null)
				con = conexion.getConexion();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	protected void finalize() throws Throwable {
		con.close();
		conexion.closeConexion();
		super.finalize();
	}
	/**
	 * El m�todo bitacoraCargaDiaria() obtiene la consulta de las sucursales
	 * cargadas diariamente.
	 *
	 * @return ArrayList valor de respuesta de la consulta
	 */
	public ArrayList<ArrayList<CargaDiariaValue>> bitacoraCargaDiaria(String estatus) {
		System.out.println("Bitacora de Carga Diaria");
		ConexionDAO conexion = new ConexionDAO();
		Connection con;
		ArrayList<ArrayList<CargaDiariaValue>> resultado = new ArrayList<ArrayList<CargaDiariaValue>>();
		ArrayList<CargaDiariaValue> resInterno = new ArrayList<CargaDiariaValue>();
		PreparedStatement query = null;
		ResultSet rs = null;
		int j = 0;
		try {
			con = conexion.getConexion();
			String queryString = " SELECT CVE_PTOVTA SUCURSAL, MENSAJE_ERR ESTATUS, TS_INI_PRO HORA_DE_CARGA, TS_FIN_PRO FIN_DE_CARGA " +
					"FROM TA_PROCESA WHERE MENSAJE_ERR LIKE ? ORDER BY MENSAJE_ERR, CVE_PTOVTA ";
			System.out.println(queryString);
			query = con.prepareStatement(queryString);
			query.setString(1, estatus);
			rs = query.executeQuery();

			while (rs.next()) {
				CargaDiariaValue valores = new CargaDiariaValue();
				valores.setSucursal(rs.getString("SUCURSAL"));
				valores.setEstatus(rs.getString("ESTATUS"));
				valores.setHoraDeCarga(rs.getString("HORA_DE_CARGA"));
				valores.setFinDeCarga(rs.getString("FIN_DE_CARGA"));

				resInterno.add(valores);
				j++;
				if (j == 100) {
					resultado.add(resInterno);
					resInterno = new ArrayList<CargaDiariaValue>();
					j = 0;
				}
			}
			if (j != 0) {
				resultado.add(resInterno);
			}
		} catch (Exception e1) {
			e1.printStackTrace();
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
		} finally {
			try {
				rs.close();
				query.close();
				conexion.closeConexion();
			} catch (SQLException e2) {
				System.out.println("Error in " + getClass().getName() + "\n" + e2);
			}
		}
		return resultado;
	}
	/**
	 * VCM 23/07/12 El m�todo consulDescSistema() obtiene los distintos estatus de
	 * la bitacora
	 *
	 * @return ArrayList valor de respuesta de la consulta
	 */
	public ArrayList<String> consulDescSistema() {
		// Se realiza la conexi�n a la BD
		ConexionDAO conexion = new ConexionDAO();
		Connection con = null;
		ArrayList<String> resultado = new ArrayList<String>();
		PreparedStatement query = null;
		StringBuffer lsbQuery = new StringBuffer();
		ResultSet rs = null;
		// Consulta distintos tipos de error de las sucursales contingentes

		lsbQuery = lsbQuery.delete(0, lsbQuery.length());
		lsbQuery.append(" SELECT DISTINCT MENSAJE_ERR AS DESC_SISTEMA FROM TA_PROCESA ");
		System.out.println("lsbQuery: " + lsbQuery.toString());

		try {
			con = conexion.getConexion();
			query = con.prepareStatement(lsbQuery.toString());
			rs = query.executeQuery();
			while (rs.next()) {
				resultado.add(rs.getString("DESC_SISTEMA"));
			}
		} catch (SQLException e1) {
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (query != null)
					query.close();
				if (con != null)
					con.close();
				conexion.closeConexion();
			} catch (SQLException e2) {
				System.out.println("Error in " + getClass().getName() + "\n"+ e2);
			}
		}
		return resultado;
	}
}