/** Banco Santander Mexicano
 *   Clase ContingenteClase  Consulta de Sucursales Contingentes
 *   @author Alfredo Resendiz Vargas
 *   @version 1.0
 *   Fecha de creacion : 2 de Febrero del 2006
 *   Responsable : Eloisa Hernandez H
 *   Descripcion : Despliega las sucursales que por alg�n motivo
 * 				  no se extrajeron con el proceso normal de extracci�n.
 *   Modificacion :
 */
package com.santander.contingente;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.santander.utilerias.ConexionDAO;
import com.santander.utilerias.LoadProperties;

public class ContingenteClase {
	static Connection con;
	ConexionDAO conexion = new ConexionDAO();
	private LoadProperties rb = LoadProperties.getInstance(); 

	public ContingenteClase() {
		try {
			if (con == null)
				con = conexion.getConexion();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void finalize() throws Throwable {
		con.close();
		conexion.closeConexion();
		super.finalize();
	}
	/**
	 * El m�todo consultaContingente() obtiene la consulta de las sucursales
	 * contingentes.
	 *
	 * @return ArrayList valor de respuesta de la consulta
	 */
	public ArrayList<ArrayList<ContingenteValue>> consultaContingente() {
		long oneDay = 1000 * 60 * 60 * 24;
		long unit = 1;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		if (calendar.get(Calendar.DAY_OF_WEEK) == 2){ //Lunes
			unit = 2;
		}
		return consultaContingente(sdf.format(new Date(calendar.getTimeInMillis()-(unit * oneDay))), "%");
	}

	/**
	 * El m�todo consultaContingente() obtiene la consulta de las sucursales
	 * contingentes.
	 *
	 * @return ArrayList valor de respuesta de la consulta
	 */
	public ArrayList<ArrayList<ContingenteValue>> consultaContingente(String fecha, String error) {
		System.out.println("Consulta de sucursales contingentes");
		ConexionDAO conexion = new ConexionDAO();
		Connection con;
		ArrayList<ArrayList<ContingenteValue>> resultado = new ArrayList<ArrayList<ContingenteValue>>();
		ArrayList<ContingenteValue> resInterno = new ArrayList<ContingenteValue>();
		PreparedStatement query = null;
		ResultSet rs = null;
		int j = 0;
		try {
			con = conexion.getConexion();
			query = con.prepareStatement(" SELECT C.CVE_PTOVTA, TO_CHAR(FECHA_DIA,'DD/MM/YYYY') FECHA_DIA, S.DESCRIPCION, " +
					"C.DESC_SISTEMA,C.DESC_USUARIO,C.MANUAL FROM TA_SUC_CONTINGENTE C, TA_SUCURSAL S " +
					"WHERE C.CVE_PTOVTA = S.CVE_PTOVTA AND C.MANUAL <> 4 AND C.FECHA_DIA = TO_DATE(?,'DD/MM/YYYY') " +
					"AND C.DESC_SISTEMA LIKE ? ORDER BY C.FECHA_DIA DESC, C.CVE_PTOVTA ");
			query.setString(1, fecha);
			query.setString(2, error);
			rs = query.executeQuery();
			System.out.println(" SELECT C.CVE_PTOVTA, TO_CHAR(FECHA_DIA,'DD/MM/YYYY'), S.DESCRIPCION, C.DESC_SISTEMA,C.DESC_USUARIO,C.MANUAL " +
					"FROM TA_SUC_CONTINGENTE C, TA_SUCURSAL S WHERE C.CVE_PTOVTA = S.CVE_PTOVTA AND C.MANUAL <> 4 " +
					"AND C.FECHA_DIA = TO_DATE('" + fecha + "','DD/MM/YYYY') AND C.DESC_SISTEMA LIKE '" + error +
					"' ORDER BY C.FECHA_DIA DESC, C.CVE_PTOVTA ");
			while (rs.next()) {
				ContingenteValue valores = new ContingenteValue();
				valores.set_suc_altair(rs.getString("CVE_PTOVTA"));
				valores.set_fecha(rs.getString("FECHA_DIA"));
				valores.set_nom_sucursal(rs.getString("DESCRIPCION"));
				valores.set_desc_sistema(rs.getString("DESC_SISTEMA"));
				valores.set_desc_usuario(rs.getString("DESC_USUARIO"));
				valores.set_verificacion(rs.getString("MANUAL"));
				resInterno.add(valores);
				j++;
				if (j == 100) {
					resultado.add(resInterno);
					resInterno = new ArrayList<ContingenteValue>();
					j = 0;
				}
			}
			if (j != 0) {
				resultado.add(resInterno);
			}
		} catch (Exception e1) {
			e1.printStackTrace();
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
		} finally {
			try {
				rs.close();
				query.close();
				conexion.closeConexion();
			} catch (SQLException e2) {
				System.out.println("Error in " + getClass().getName() + "\n"
						+ e2);
			}
		}
		return resultado;
	}
	/**
	 * El m�todo consultaContingente() obtiene la consulta de las sucursales
	 * contingentes.
	 *
	 * @return ArrayList valor de respuesta de la consulta
	 */
	public ArrayList<ArrayList<ContingenteValue>> consultaContingente(String error) {
		System.out.println("Consulta de sucursales contingentes");
		ConexionDAO conexion = new ConexionDAO();
		Connection con;
		ArrayList<ArrayList<ContingenteValue>> resultado = new ArrayList<ArrayList<ContingenteValue>>();
		ArrayList<ContingenteValue> resInterno = new ArrayList<ContingenteValue>();
		PreparedStatement query = null;
		ResultSet rs = null;
		int j = 0;
		try {
			con = conexion.getConexion();
			query = con.prepareStatement(" SELECT C.CVE_PTOVTA, TO_CHAR(FECHA_DIA,'DD/MM/YYYY') FECHA_DIA, S.DESCRIPCION, " +
					"C.DESC_SISTEMA,C.DESC_USUARIO,C.MANUAL FROM TA_SUC_CONTINGENTE C, TA_SUCURSAL S " +
					"WHERE C.CVE_PTOVTA = S.CVE_PTOVTA AND C.MANUAL <> 4 " +
					"AND C.DESC_SISTEMA LIKE ? ORDER BY C.FECHA_DIA DESC, C.CVE_PTOVTA ");
			query.setString(1, error);
			rs = query.executeQuery();
			System.out.println(" SELECT C.CVE_PTOVTA, TO_CHAR(FECHA_DIA,'DD/MM/YYYY'), S.DESCRIPCION, C.DESC_SISTEMA,C.DESC_USUARIO,C.MANUAL " +
					"FROM TA_SUC_CONTINGENTE C, TA_SUCURSAL S WHERE C.CVE_PTOVTA = S.CVE_PTOVTA AND C.MANUAL <> 4 " +
					"AND C.DESC_SISTEMA LIKE '" + error + "' ORDER BY C.FECHA_DIA DESC, C.CVE_PTOVTA ");
			while (rs.next()) {
				ContingenteValue valores = new ContingenteValue();
				valores.set_suc_altair(rs.getString("CVE_PTOVTA"));
				valores.set_fecha(rs.getString("FECHA_DIA"));
				valores.set_nom_sucursal(rs.getString("DESCRIPCION"));
				valores.set_desc_sistema(rs.getString("DESC_SISTEMA"));
				valores.set_desc_usuario(rs.getString("DESC_USUARIO"));
				valores.set_verificacion(rs.getString("MANUAL"));
				resInterno.add(valores);
				j++;
				if (j == 100) {
					resultado.add(resInterno);
					resInterno = new ArrayList<ContingenteValue>();
					j = 0;
				}
			}
			if (j != 0) {
				resultado.add(resInterno);
			}
		} catch (Exception e1) {
			e1.printStackTrace();
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
		} finally {
			try {
				rs.close();
				query.close();
				conexion.closeConexion();
			} catch (SQLException e2) {
				System.out.println("Error in " + getClass().getName() + "\n"
						+ e2);
			}
		}
		return resultado;
	}
	/**
	 * El m�todo makePdf(int, ArrayList) genera el PDF
	 *
	 * @param datos
	 *            Valores que se insertara en el PDF
	 * @return ByteArrayOutputStream El PDF
	 */

	public ByteArrayOutputStream makePdf(ArrayList datos) {
		System.out
				.println("Se genera la impresi�n PDF de las sucursales contingentes");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Font black = FontFactory.getFont(FontFactory.HELVETICA,
				Font.DEFAULTSIZE, Font.NORMAL, new Color(0x00, 0x00, 0x00));
		Rectangle hoja = null;
		hoja = PageSize.LETTER.rotate();
		Document document = new Document(hoja, 10, 10, 10, 10);
		try {
			/** Genera la salida para la impresion de los datos */
			PdfWriter.getInstance(document, baos);
			document.open();
			PdfPTable datatable = creaEncabezado();
			datatable.getDefaultCell().setBorderWidth(1);
			int tamano = datos.size();
			for (int i = 0; i < tamano; i++) {
				if (i % 2 == 0) {
					datatable.getDefaultCell().setBackgroundColor(
							new Color(0xFF, 0xFF, 0xFF));
				} else {
					datatable.getDefaultCell().setBackgroundColor(
							new Color(0xEB, 0xEB, 0xEB));
				}
				/** Se carga la tabla con los valores a imprimir */
				ContingenteValue reg_imp = (ContingenteValue) datos.get(i);
				datatable.addCell(reg_imp.get_suc_altair());
				datatable.addCell(reg_imp.get_fecha().substring(0, 10));
				datatable.addCell(reg_imp.get_nom_sucursal());
				datatable.addCell(reg_imp.get_desc_sistema());
				datatable.addCell(reg_imp.get_desc_usuario());
				datatable.addCell(reg_imp.get_verificacion());
			}
			document.add(datatable);
			document.close();
		} catch (Exception e2) {
			System.out.println("Error in " + getClass().getName() + "\n" + e2);
		}
		return baos;
	}

	/**
	 * El m�todo creaEncabezado() genera el encabezado del PDF
	 *
	 * @return PdfPTable Encabezado del PDF
	 */

	private PdfPTable creaEncabezado() {
		PdfPTable datatable = null;
		Font white = FontFactory.getFont(FontFactory.HELVETICA,
				Font.DEFAULTSIZE, Font.BOLD, new Color(0xFF, 0xFF, 0xFF));
		Font whiteG = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD,
				new Color(0xFF, 0xFF, 0xFF));
		Font black = FontFactory.getFont(FontFactory.HELVETICA,
				Font.DEFAULTSIZE, Font.NORMAL, new Color(0x00, 0x00, 0x00));
		int columnas = 6;
		Paragraph titulo = null;
		try {
			datatable = new PdfPTable(columnas);
			Image logo = Image.getInstance(rb.getValue("ta.logosantander"));
			PdfPCell cell = new PdfPCell(logo);
			cell.setColspan(2);
			cell.setBorderWidth(2);
			cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			datatable.addCell(cell);
			titulo = new Paragraph("Reporte de Sucursales Contingentes ",
					whiteG);
			cell = new PdfPCell(titulo);
			cell.setColspan(4);
			cell.setBorderWidth(2);
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorderColor(new Color(0xFF, 0xFF, 0xFF));
			cell.setBackgroundColor(new Color(0xF8, 0x00, 0x00));
			datatable.addCell(cell);
			int headerwidths[] = { 10, 10, 15, 25, 25, 15 }; // porcentaje
			datatable.setWidths(headerwidths);
			datatable.setWidthPercentage(100); // porcentaje
			datatable.getDefaultCell().setPadding(3);
			datatable.getDefaultCell().setBorderWidth(2);
			datatable.getDefaultCell().setBorderColor(
					new Color(0xFF, 0xFF, 0xFF));
			datatable.getDefaultCell().setBackgroundColor(
					new Color(0x99, 0x99, 0x99));
			datatable.getDefaultCell().setHorizontalAlignment(
					Element.ALIGN_CENTER);
			datatable.getDefaultCell().setVerticalAlignment(
					Element.ALIGN_MIDDLE);
			datatable.addCell(new Paragraph("CdeC ALTAIR", white));
			datatable.addCell(new Paragraph("Fecha", white));
			datatable.addCell(new Paragraph("Nombre Sucursal", white));
			datatable.addCell(new Paragraph("Descripci�n Sistema", white));
			datatable.addCell(new Paragraph("Descripci�n Usuario", white));
			datatable.addCell(new Paragraph("Reactivaci�n Manual", white));
			datatable.setHeaderRows(2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return datatable;
	}
}