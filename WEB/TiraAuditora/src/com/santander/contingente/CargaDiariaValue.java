package com.santander.contingente;

public class CargaDiariaValue {
	public String sucursal;
	public String estatus;
	public String getSucursal() {
		return sucursal;
	}

	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getHoraDeCarga() {
		return horaDeCarga;
	}

	public void setHoraDeCarga(String horaDeCarga) {
		this.horaDeCarga = horaDeCarga;
	}

	public String getFinDeCarga() {
		return finDeCarga;
	}

	public void setFinDeCarga(String finDeCarga) {
		this.finDeCarga = finDeCarga;
	}

	public String horaDeCarga;
	public String finDeCarga;

	public CargaDiariaValue(){

	}

}