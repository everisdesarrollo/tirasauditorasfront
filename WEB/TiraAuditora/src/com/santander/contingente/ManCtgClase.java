/** Banco Santander Mexicano
 *   Clase ManCtgClase  Mantenimiento e Hist�rico de Sucursales Contingentes
 *   @author Alfredo Resendiz Vargas
 *   @version 1.0
 *   Fecha de creacion : 2 de Febrero del 2006
 *   Responsable : Eloisa Hernandez H
 *   Descripcion : Despliega las sucursales que por alg�n motivo
 * 				  no se extrajeron con el proceso normal de extracci�n.
 *   Modificacion :
 */
package com.santander.contingente;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

import com.santander.utilerias.ConexionDAO;
import com.santander.utilerias.Resultados;

/**
 * El m�todo consultaManCtg() obtiene la consulta de las sucursales contingentes
 * seg�n Pto. de Vta. y Fecha.
 *
 * @return ArrayList valor de respuesta de la consulta
 */
public class ManCtgClase {
	public ArrayList consultaManCtg(String fecha, String pventa, String descSistema) {
		System.out
				.println("Consulta para el mannto. de sucursales contingentes por ptovta-fecha");
		// Se realiza la conexi�n a la BD
		ConexionDAO conexion = new ConexionDAO();
		Connection con;
		ArrayList resultado = new ArrayList();
		ArrayList res_interno = new ArrayList();
		PreparedStatement query = null;
		StringBuffer lsbQuery = new StringBuffer();
		ResultSet rs = null;
		// Consulta de las sucursales que por alg�n motivo no se centralizaron
		// con el proceso
		// normal de extracci�n, dando como parametros la sucursal y la fecha
		lsbQuery = lsbQuery.delete(0, lsbQuery.length());
		lsbQuery
				.append(" SELECT C.CVE_PTOVTA, TO_CHAR(C.FECHA_DIA,'DD/MM/YYYY') FECHA_DIA, S.DESCRIPCION, C.DESC_SISTEMA,C.DESC_USUARIO,C.NUM_INTENTOS,C.MANUAL ");
		lsbQuery.append(" FROM TA_SUC_CONTINGENTE C, TA_SUCURSAL S ");
		lsbQuery.append(" WHERE C.CVE_PTOVTA = S.CVE_PTOVTA ");
		lsbQuery.append(" AND C.CVE_PTOVTA = '").append(pventa).append("' ");
		lsbQuery.append(" AND C.FECHA_DIA = TO_DATE('").append(fecha).append("','DD/MM/YYYY') ");
		lsbQuery.append(" AND C.DESC_SISTEMA LIKE '").append(descSistema).append("' ");
		lsbQuery.append(" AND C.MANUAL <> 4 ORDER BY C.FECHA_DIA DESC, C.CVE_PTOVTA ");
		System.out.println("lsbQuery: " + lsbQuery.toString());

		Resultados res = new Resultados();
		resultado = res.result_consul(lsbQuery.toString());
		return resultado;
	}

	/**
	 * El m�todo consulTotManCtg() obtiene la consulta de todas las sucursales
	 * contingentes.
	 *
	 * @return ArrayList valor de respuesta de la consulta
	 */
	public ArrayList consulTotManCtg(String descSistema) {
		System.out
				.println("Consulta para el mannto. de sucursales contingentes total");
		// Se realiza la conexi�n a la BD
		ConexionDAO conexion = new ConexionDAO();
		Connection con;
		ArrayList resultado = new ArrayList();
		ArrayList res_interno = new ArrayList();
		PreparedStatement query = null;
		StringBuffer lsbQuery = new StringBuffer();
		ResultSet rs = null;
		// Consulta de tosdas las sucursales que por alg�n motivo no se
		// centralizaron con el proceso
		// normal de extracci�n

		lsbQuery = lsbQuery.delete(0, lsbQuery.length());
		lsbQuery
				.append(" SELECT C.CVE_PTOVTA, TO_CHAR(C.FECHA_DIA,'DD/MM/YYYY') FECHA_DIA, S.DESCRIPCION, C.DESC_SISTEMA,C.DESC_USUARIO,C.NUM_INTENTOS,C.MANUAL ");
		lsbQuery.append(" FROM TA_SUC_CONTINGENTE C, TA_SUCURSAL S ");
		lsbQuery.append(" WHERE C.CVE_PTOVTA = S.CVE_PTOVTA ");
		lsbQuery.append(" AND C.DESC_SISTEMA LIKE '").append(descSistema).append("' ");
		lsbQuery.append(" AND C.MANUAL <> 4 ORDER BY C.FECHA_DIA DESC, C.CVE_PTOVTA ");
		System.out.println("lsbQuery: " + lsbQuery.toString());

		Resultados res = new Resultados();
		resultado = res.result_consul(lsbQuery.toString());
		return resultado;
	}

	/**
	 * El m�todo consultaFecCtg() obtiene la consulta de todas las sucursales
	 * contingentes seg�n fecha.
	 *
	 * @return ArrayList valor de respuesta de la consulta
	 */
	public ArrayList consultaFecCtg(String fecha, String descSistema) {
		System.out
				.println("Consulta para el mannto. de sucursales contingentes por fecha");
		// Se realiza la conexi�n a la BD
		ConexionDAO conexion = new ConexionDAO();
		Connection con;
		ArrayList resultado = new ArrayList();
		ArrayList res_interno = new ArrayList();
		PreparedStatement query = null;
		StringBuffer lsbQuery = new StringBuffer();
		ResultSet rs = null;
		// Consulta de las sucursales que por alg�n motivo no se centralizaron
		// con el proceso
		// normal de extracci�n, dando como parametro la fecha

		lsbQuery = lsbQuery.delete(0, lsbQuery.length());
		lsbQuery.append(" SELECT C.CVE_PTOVTA, TO_CHAR(C.FECHA_DIA,'DD/MM/YYYY') FECHA_DIA, S.DESCRIPCION, C.DESC_SISTEMA,C.DESC_USUARIO,C.NUM_INTENTOS,C.MANUAL ");
		lsbQuery.append(" FROM TA_SUC_CONTINGENTE C, TA_SUCURSAL S ");
		lsbQuery.append(" WHERE C.CVE_PTOVTA = S.CVE_PTOVTA ");
		lsbQuery.append(" AND C.FECHA_DIA = TO_DATE('").append(fecha).append("','DD/MM/YYYY') ");
		lsbQuery.append(" AND C.DESC_SISTEMA LIKE '").append(descSistema).append("' ");
		lsbQuery.append(" AND C.MANUAL <> 4 ORDER BY C.FECHA_DIA DESC, C.CVE_PTOVTA ");
		System.out.println("lsbQuery: " + lsbQuery.toString());

		Resultados res = new Resultados();
		resultado = res.result_consul(lsbQuery.toString());
		return resultado;
	}

	/**
	 * VCM 23/07/12 El m�todo consulDescSistema() obtiene los distintos tipos de
	 * error de las sucursales contingentes.
	 *
	 * @return ArrayList valor de respuesta de la consulta
	 */
	public ArrayList<String> consulDescSistema() {
		System.out.println("Consulta para el mannto. de sucursales contingentes tipos de error");
		// Se realiza la conexi�n a la BD
		ConexionDAO conexion = new ConexionDAO();
		Connection con = null;
		ArrayList<String> resultado = new ArrayList<String>();
		PreparedStatement query = null;
		StringBuffer lsbQuery = new StringBuffer();
		ResultSet rs = null;
		// Consulta distintos tipos de error de las sucursales contingentes

		lsbQuery = lsbQuery.delete(0, lsbQuery.length());
		lsbQuery.append(" SELECT DISTINCT C.DESC_SISTEMA ");
		lsbQuery.append(" FROM TA_SUC_CONTINGENTE C, TA_SUCURSAL S ");
		lsbQuery.append(" WHERE C.CVE_PTOVTA = S.CVE_PTOVTA ");
		lsbQuery.append(" AND C.MANUAL <> 4 ORDER BY C.DESC_SISTEMA");
		System.out.println("lsbQuery: " + lsbQuery.toString());

		try {
			con = conexion.getConexion();
			query = con.prepareStatement(lsbQuery.toString());
			rs = query.executeQuery();
			while (rs.next()) {
				resultado.add(rs.getString("DESC_SISTEMA"));
			}
		} catch (SQLException e1) {
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (query != null)
					query.close();
				if (con != null)
					con.close();
				conexion.closeConexion();
			} catch (SQLException e2) {
				System.out.println("Error in " + getClass().getName() + "\n"+ e2);
			}
		}
		return resultado;
	}

	/**
	 * El m�todo histManCtg() obtiene la consulta el hist�rico las sucursales
	 * contingentes seg�n Pto. de Vta. y Fecha.
	 *
	 * @return ArrayList valor de respuesta de la consulta
	 */

	public ArrayList histManCtg(String fecha, String pventa) {
		System.out
				.println("Consulta del hist�rico de sucursales contingentes por ptovta-fecha");
		// Se realiza la conexi�n a la BD
		ConexionDAO conexion = new ConexionDAO();
		Connection con;
		ArrayList resultado = new ArrayList();
		ArrayList res_interno = new ArrayList();
		PreparedStatement query = null;
		StringBuffer lsbQuery = new StringBuffer();
		ResultSet rs = null;
		// Consulta de la historia de las sucursales que por alg�n motivo no se
		// centralizaron
		// con el proceso normal de extracci�n, dando como parametros la
		// sucursal y la fecha
		lsbQuery = lsbQuery.delete(0, lsbQuery.length());
		lsbQuery
				.append(" SELECT C.CVE_PTOVTA, TO_CHAR(C.FECHA_DIA,'DD/MM/YYYY') FECHA_DIA, S.DESCRIPCION, C.DESC_SISTEMA,C.DESC_USUARIO,C.MANUAL,C.NUM_INTENTOS ");
		lsbQuery.append(" FROM TA_SUC_CONST_HIST C, TA_SUCURSAL S ");
		lsbQuery.append(" WHERE C.CVE_PTOVTA = S.CVE_PTOVTA ");
		lsbQuery.append(" AND C.CVE_PTOVTA = '").append(pventa).append("' ");
		lsbQuery.append(" AND C.FECHA_DIA = TO_DATE('").append(fecha).append(
				"','DD/MM/YYYY') ");
		System.out.println("lsbQuery: " + lsbQuery.toString());

		int j = 0;
		try {
			con = conexion.getConexion();
			query = con.prepareStatement(lsbQuery.toString());
			rs = query.executeQuery();
			int i = 0;
			// Se recuperan los valores de la consulta
			while (rs.next()) {
				ManCtgValue valores = new ManCtgValue();
				valores.set_suc_altair(rs.getString("CVE_PTOVTA"));
				valores.set_fecha(rs.getString("FECHA_DIA"));
				valores.set_nom_sucursal(rs.getString("DESCRIPCION"));
				valores.set_desc_sistema(rs.getString("DESC_SISTEMA"));
				valores.set_desc_usuario(rs.getString("DESC_USUARIO"));
				valores.set_verificacion_sis(rs.getString("MANUAL"));
				valores.set_num_intentos(Integer.parseInt(rs
						.getString("NUM_INTENTOS")));
				res_interno.add(valores);
				j++;
				if (j == 100) {
					resultado.add(res_interno);
					res_interno = new ArrayList();
					j = 0;
				}
			}
			if (j != 0) {
				resultado.add(res_interno);
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		} finally {
			try {
				rs.close();
				query.close();
				conexion.closeConexion();
			} catch (SQLException e2) {
				System.out.println("Error in " + getClass().getName() + "\n"
						+ e2);
			}
		}
		return resultado;
	}

	/**
	 * El m�todo histFecCtg() obtiene la consulta hist�rica de todas las
	 * sucursales contingentes seg�n fecha.
	 *
	 * @return ArrayList valor de respuesta de la consulta
	 */
	public ArrayList histFecCtg(String fecha) {
		System.out
				.println("Consulta del hist�rico de sucursales contingentes por fecha");
		// Se realiza la conexi�n a la BD
		ConexionDAO conexion = new ConexionDAO();
		Connection con;
		ArrayList resultado = new ArrayList();
		ArrayList res_interno = new ArrayList();
		PreparedStatement query = null;
		StringBuffer lsbQuery = new StringBuffer();
		ResultSet rs = null;

		// Consulta de la historia de las sucursales que por alg�n motivo no se
		// centralizaron
		// con el proceso normal de extracci�n, dando como parametro la fecha
		lsbQuery = lsbQuery.delete(0, lsbQuery.length());
		lsbQuery
				.append(" SELECT C.CVE_PTOVTA, TO_CHAR(C.FECHA_DIA,'DD/MM/YYYY') FECHA_DIA, S.DESCRIPCION, C.DESC_SISTEMA,C.DESC_USUARIO,MANUAL,C.NUM_INTENTOS ");
		lsbQuery.append(" FROM TA_SUC_CONST_HIST C, TA_SUCURSAL S ");
		lsbQuery.append(" WHERE C.CVE_PTOVTA = S.CVE_PTOVTA ");
		lsbQuery.append(" AND C.FECHA_DIA = TO_DATE('").append(fecha).append(
				"','DD/MM/YYYY') ");
		System.out.println("lsbQuery: " + lsbQuery.toString());

		int j = 0;
		try {
			con = conexion.getConexion();
			query = con.prepareStatement(lsbQuery.toString());
			rs = query.executeQuery();
			int i = 0;
			// Se recuperan los valores de la consulta
			while (rs.next()) {
				ManCtgValue valores = new ManCtgValue();
				valores.set_suc_altair(rs.getString("CVE_PTOVTA"));
				valores.set_fecha(rs.getString("FECHA_DIA"));
				valores.set_nom_sucursal(rs.getString("DESCRIPCION"));
				valores.set_desc_sistema(rs.getString("DESC_SISTEMA"));
				valores.set_desc_usuario(rs.getString("DESC_USUARIO"));
				valores.set_verificacion_sis(rs.getString("MANUAL"));
				valores.set_num_intentos(Integer.parseInt(rs
						.getString("NUM_INTENTOS")));
				res_interno.add(valores);
				j++;
				if (j == 100) {
					resultado.add(res_interno);
					res_interno = new ArrayList();
					j = 0;
				}
			}
			if (j != 0) {
				resultado.add(res_interno);
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		} finally {
			try {
				rs.close();
				query.close();
				conexion.closeConexion();
			} catch (SQLException e2) {
				System.out.println("Error in " + getClass().getName() + "\n"
						+ e2);
			}
		}
		return resultado;
	}

	/**
	 * El m�todo guardaManCtg() guarda la descripci�n del usuario en la tabla de
	 * sucursales contingentes y marca la bandera para la extracci�n manual
	 *
	 * @return estatus del movimiento
	 */

	public String guardaManCtg(String pventa, String fecha, String descuser,
			String opcion) {
		System.out
				.println("Se guarda la descripci�n del usuario en el mantto. de sucursales contingentes");
		// Se realiza la conexi�n a la BD
		ConexionDAO conexion = new ConexionDAO();
		Connection con;
		PreparedStatement query = null;
		StringBuffer lsbQuery = new StringBuffer();
		ResultSet rs = null;
		String resultado = "";
		// Se guarda la descripci�n de usuario dependiendo la opci�n se almacena
		// la
		// reactivaci�n Manual=0 o Manual=1
		lsbQuery = lsbQuery.delete(0, lsbQuery.length());
		if (opcion.equals("1")) {
			lsbQuery.append(" UPDATE TA_SUC_CONTINGENTE SET DESC_USUARIO= '")
					.append(descuser).append("', MANUAL = '1' ");
		} else {
			lsbQuery.append(" UPDATE TA_SUC_CONTINGENTE SET DESC_USUARIO= '")
					.append(descuser).append("', MANUAL = '0' ");
		}
		lsbQuery.append(" WHERE  CVE_PTOVTA = '").append(pventa).append("' ");
		lsbQuery.append(" AND FECHA_DIA = TO_DATE('").append(
				fecha.substring(0, 10)).append("','DD/MM/YYYY') ");
		System.out.println("lsbQuery: " + lsbQuery.toString());

		int j = 0;
		try {
			con = conexion.getConexion();
			query = con.prepareStatement(lsbQuery.toString());
			rs = query.executeQuery();
			resultado = "0";
		} catch (Exception e1) {
			e1.printStackTrace();
		} finally {
			try {
				rs.close();
				query.close();
				conexion.closeConexion();
			} catch (SQLException e2) {
				System.out.println("Error in " + getClass().getName() + "\n"
						+ e2);
			}
		}
		return resultado;
	}

	/**
	 * El m�todo eliminaManCtg() elimina los registros selecionados en el
	 * mantto. de sucursales contingentes.
	 *
	 * @return status del movimiento
	 */

	public String eliminaManCtg(String pventa, String fecha, String opcion) {
		System.out
				.println("Borra los registros en el mantto. de  sucursales contingentes por ptovta-fecha");
		// Se realiza la conexi�n a la BD
		ConexionDAO conexion = new ConexionDAO();
		Connection con;
		PreparedStatement query = null;
		StringBuffer lsbQuery = new StringBuffer();
		ResultSet rs = null;
		String resultado = "";
		// Se borran los registros seleccionados por el usuario
		lsbQuery = lsbQuery.delete(0, lsbQuery.length());
		lsbQuery.append(" DELETE TA_SUC_CONTINGENTE ");
		lsbQuery.append(" WHERE  CVE_PTOVTA = '").append(pventa).append("' ");
		lsbQuery.append(" AND FECHA_DIA = TO_DATE('").append(
				fecha.substring(0, 10)).append("','DD/MM/YYYY') ");
		System.out.println("lsbQuery: " + lsbQuery.toString());

		int j = 0;
		try {
			con = conexion.getConexion();
			query = con.prepareStatement(lsbQuery.toString());
			rs = query.executeQuery();
			resultado = "0";
		} catch (Exception e1) {
			e1.printStackTrace();
		} finally {
			try {
				rs.close();
				query.close();
				conexion.closeConexion();
			} catch (SQLException e2) {
				System.out.println("Error in " + getClass().getName() + "\n"
						+ e2);
			}
		}
		return resultado;
	}

	/**
	 * El m�todo eliminaFecCtg() elimina todos los registros tomando como
	 * par�metro la fecha en el mantto. de sucursales contingentes.
	 *
	 * @return status del movimiento
	 */

	public String eliminaFecCtg(String fecha) {
		System.out
				.println("Borra los registros en el mantto. de  sucursales contingentes por fecha");
		// Se realiza la conexi�n a la BD
		ConexionDAO conexion = new ConexionDAO();
		Connection con;
		PreparedStatement query = null;
		StringBuffer lsbQuery = new StringBuffer();
		ResultSet rs = null;
		String resultado = "";
		// Se borran los registros seleccionados por el usuario
		lsbQuery = lsbQuery.delete(0, lsbQuery.length());
		lsbQuery.append(" DELETE TA_SUC_CONTINGENTE ");
		lsbQuery.append(" WHERE FECHA_DIA = TO_DATE('").append(
				fecha.substring(0, 10)).append("','DD/MM/YYYY') ");
		System.out.println("lsbQuery: " + lsbQuery.toString());

		int j = 0;
		try {
			con = conexion.getConexion();
			query = con.prepareStatement(lsbQuery.toString());
			rs = query.executeQuery();
			resultado = "0";
		} catch (Exception e1) {
			e1.printStackTrace();
		} finally {
			try {
				rs.close();
				query.close();
				conexion.closeConexion();
			} catch (SQLException e2) {
				System.out.println("Error in " + getClass().getName() + "\n"
						+ e2);
			}
		}
		return resultado;
	}

	/**
	 * El m�todo eliminaTotCtg() elimina todos los registros la fecha en el
	 * mantto. de sucursales contingentes.
	 *
	 * @return status del movimiento
	 */

	public String eliminaTotCtg() {
		System.out
				.println("Borra todos los registros en el mantto. de  sucursales contingentes");
		// Se realiza la conexi�n a la BD
		ConexionDAO conexion = new ConexionDAO();
		Connection con;
		PreparedStatement query = null;
		StringBuffer lsbQuery = new StringBuffer();
		ResultSet rs = null;
		String resultado = "";
		// Se borran los registros seleccionados por el usuario
		lsbQuery = lsbQuery.delete(0, lsbQuery.length());
		lsbQuery.append(" DELETE TA_SUC_CONTINGENTE ");
		System.out.println("lsbQuery: " + lsbQuery.toString());

		try {
			con = conexion.getConexion();
			query = con.prepareStatement(lsbQuery.toString());
			rs = query.executeQuery();
			resultado = "0";
		} catch (Exception e1) {
			e1.printStackTrace();
		} finally {
			try {
				rs.close();
				query.close();
				conexion.closeConexion();
			} catch (SQLException e2) {
				System.out.println("Error in " + getClass().getName() + "\n"
						+ e2);
			}
		}
		return resultado;
	}

	/**
	 * VCM 09/08/12 El m�todo loadOnLine() realiza la carga en linea de una sucursal
	 *
	 * @return String valor de respuesta de la consulta
	 */
	public String loadOnLine(String lsNumsuc, String fechaCont, String jobName, String action) {
		System.out.println("Consulta para el mannto. de sucursales contingentes tipos de error");
		// Se realiza la conexi�n a la BD
		ConexionDAO conexion = new ConexionDAO();
		Connection con = null;
		String resultado = null;
		CallableStatement query = null;
		StringBuffer lsbQuery = new StringBuffer();
		ResultSet rs = null;
		// Consulta distintos tipos de error de las sucursales contingentes

		lsbQuery = lsbQuery.delete(0, lsbQuery.length());
		lsbQuery.append("{call CargaOnLine(?,?,?,?,?)}");
		System.out.println("lsbQuery: call CargaOnLine('" + fechaCont + "','" + lsNumsuc + "','" + jobName + "','" + action + "',OUT");

		try {
			con = conexion.getConexion();
			query = con.prepareCall(lsbQuery.toString());
			query.setString(1, fechaCont);
			query.setString(2, lsNumsuc);
			query.setString(3, jobName);
			query.setString(4, action);
			query.registerOutParameter(5, Types.VARCHAR);
			query.execute();
			resultado = query.getString(5);
			System.out.println(resultado);
		} catch (SQLException e1) {
			System.out.println("Error in " + getClass().getName() + "\n" + e1);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (query != null)
					query.close();
				if (con != null)
					con.close();
				conexion.closeConexion();
			} catch (SQLException e2) {
				System.out.println("Error in " + getClass().getName() + "\n"+ e2);
			}
		}
		return resultado;
	}

}