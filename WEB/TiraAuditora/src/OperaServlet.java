/** Banco Santander Mexicano
*   Clase OperaServlet  Resuelve el flujo de las operaciones del catalogo de operaciones
*   @author Ing. David Aguilar G�mez
*   @version 1.0
*   fecha de creacion : 28 de Marzo del 2006
*   responsable : Eloisa Hernandez H
*   descripcion : resuelve el flujo de las operaciones de consulta, altas, bajas y cambios del Catalogo de Operaciones.
*   modificacion :
*/
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.santander.CatOperaciones.OperClase;
import com.santander.CatOperaciones.OperValue;

public class OperaServlet extends HttpServlet
{
	/** El m�todo doGet(HttpServletRequest, HttpServletResponse) entrada por Get
	*   @param req  Valor Request
	*   @param resp Valor Response
	*/
	public void doGet(HttpServletRequest req, HttpServletResponse resp)	throws ServletException, IOException
	{
		doGeneral(req, resp);
	}

	/** El m�todo doPost(HttpServletRequest, HttpServletResponse) entrada por Post
	*   @param req  Valor Request
	*   @param resp Valor Response
	*/
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		doGeneral(req, resp);
	}
	/** El m�todo doGeneral(HttpServletRequest, HttpServletResponse) Control de flujo del modulo
	*   @param req  Valor Request
	*   @param resp Valor Response
	*/
	protected void doGeneral(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		OperClase oper_clase = new OperClase();
		String accion = req.getParameter("accion");
		System.out.println("accion = " + accion);
		String cve_operacion = req.getParameter("cveoperacion");
		System.out.println("cve_operacion = " + cve_operacion);
		HttpSession sesion = req.getSession();
		if(accion.equals("nuevo"))
		{
			OperValue valores = new OperValue();
			valores.setCve_operacion(cve_operacion);
			valores.setDescripcion(req.getParameter("descripcion").toUpperCase());
			if(req.getParameter("diario")!=null)
				valores.setAfecta_diario("S");
			else
				valores.setAfecta_diario("N");
			if(req.getParameter("totales")!=null)
				valores.setAfecta_totales("S");
			else
				valores.setAfecta_totales("N");
			if(req.getParameter("offline")!=null)
				valores.setPermite_offline("S");
			else
				valores.setPermite_offline("N");
			if(oper_clase.insertaOperacion(valores))
				accion = "buscar";
			else
				resp.sendRedirect("/TiraAuditora/CatOperaciones/08MtoOpe.jsp?mensaje=Error! No se pudo insertar el registro.");
		}
		if(accion.equals("modificar"))
		{
			OperValue valores = new OperValue();
			valores.setCve_operacion(cve_operacion);
			valores.setDescripcion(req.getParameter("descripcion").toUpperCase());
			if(req.getParameter("diario")!=null)
				valores.setAfecta_diario("S");
			else
				valores.setAfecta_diario("N");
			if(req.getParameter("totales")!=null)
				valores.setAfecta_totales("S");
			else
				valores.setAfecta_totales("N");
			if(req.getParameter("offline")!=null)
				valores.setPermite_offline("S");
			else
				valores.setPermite_offline("N");
			if(oper_clase.modificaOperacion(valores))
				accion = "buscar";
			else
				resp.sendRedirect("/TiraAuditora/CatOperaciones/08MtoOpe.jsp?mensaje=Error! No se pudo modificar el registro.");
		}
		if(accion.equals("buscar"))
		{
			OperValue dato = oper_clase.consultaOperacion(cve_operacion);
			if(dato!=null)
			{
				StringBuffer url =  new StringBuffer();
				url.append("?cveoperacion="+cve_operacion).append("&descripcion="+dato.getDescripcion());
				url.append("&diario="+dato.getAfecta_diario()).append("&totales="+dato.getAfecta_totales());
				url.append("&offline="+dato.getPermite_offline());
				resp.sendRedirect("/TiraAuditora/CatOperaciones/08ResOpe.jsp"+url);
			}
			else
				resp.sendRedirect("/TiraAuditora/CatOperaciones/08ConFir.jsp?cveoperacion="+cve_operacion);
		}
		if(accion.equals("borrar"))
		{
			OperValue valores = new OperValue();
			valores.setCve_operacion(cve_operacion);
			if(oper_clase.borraOperacion(valores))
				resp.sendRedirect("/TiraAuditora/CatOperaciones/08MtoOpe.jsp?mensaje=Se ha eliminado el registro.");
			else
				resp.sendRedirect("/TiraAuditora/CatOperaciones/08MtoOpe.jsp?mensaje=Error! No se pudo eliminar el registro.");

		}
	}

}