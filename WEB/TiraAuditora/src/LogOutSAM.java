



import java.io.*;
import java.net.*;

import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author ORO01083
 * @version
 */
public class LogOutSAM extends HttpServlet {

    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        System.out.println("Entro en LogOutSAM");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession(false);
        if(session == null){
            System.out.println("Sesion es null, se redirecciona a LogInSAM");
            response.sendRedirect("LogInSAM");
        }else{
            System.out.println("Sesion no es nula, se eliminan las variables de sesion");
            if(session.getAttribute("consulogueo") != null){
                session.removeAttribute("consulogueo");
            }
            if(session.getAttribute("autentifica") != null){
                session.removeAttribute("autentifica");
            }
            session = null;
            System.out.println("Se redirecciona a: ../../../pkmslogout?redir=/tirasAudi/TiraAuditora=tirasAudi");// TODO Cambiar APP
            response.sendRedirect("../../../pkmslogout?redir=/tirasAudi/TiraAuditora=tirasAudi");

        }
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}