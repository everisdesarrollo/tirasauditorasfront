import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.santander.autentificacion.LogueoValue;
import com.santander.contingente.ManCtgClase;
import com.santander.scheduler.SchedulerClase;
import com.santander.scheduler.SchedulerValue;

/**
 * Servlet implementation class SchedulerServlet
 */
public class SchedulerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static long day = 1000 * 60 * 60 * 24;

	/**
	 * @see Realiza las conexiones necesarias para para el control del proceso
	 */

	protected void doGeneral(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String seleccion = req.getParameter("seleccion") == null ? "" : req.getParameter("seleccion");
		System.out.println("seleccion = " + seleccion);

		boolean bandera = false;
		HttpSession sesion = req.getSession();
		sesion.setAttribute("permiso", "con permisos");
		sesion.setAttribute("message", "");
		sesion.setAttribute("log", "No hay datos");
		ArrayList<LogueoValue> consulogueo = (ArrayList) sesion.getAttribute("consulogueo");

		if(consulogueo != null){
			LogueoValue logueoValue ;
			for(int i = 0; i < consulogueo.size(); i++){
				logueoValue = (LogueoValue)consulogueo.get(i);
				bandera = bandera | (logueoValue !=null && logueoValue.getfac15() != null ? true : false);
			}
		}
		if (!bandera){
			sesion.setAttribute("permiso", "sin permisos");
			System.out.println("Scheduler Jobs - No tiene permisos");

		} else if (seleccion.equals("read")) {
			readJobDetail(req, sesion);

		} else if (seleccion.equals("E") || seleccion.equals("H")){
			String[] selected = req.getParameterValues("selected");
			String log = "";
			if(selected != null){
				ArrayList<ArrayList<SchedulerValue>> temp = (ArrayList)sesion.getAttribute("schedulerJobs");
				if(temp != null && temp.size() > 0){
					ArrayList<SchedulerValue> pagina = temp.get(Integer.valueOf(req.getParameter("pagina")));
					ManCtgClase contingente = new ManCtgClase();

					for(int i = 0; i < selected.length; i++){
						String jobName = ((SchedulerValue)pagina.get(Integer.valueOf(selected[i]))).getJobName();
						log += jobName + "-> " +contingente.loadOnLine(null, null, jobName, seleccion) + " <br> \n";
					}
				}
			}
			sesion.setAttribute("schedulerJobs", null);
			sesion.setAttribute("log", log);
			System.out.println(log);
		}
		resp.sendRedirect("/TiraAuditora/Schedulers/14ConSch.jsp");
	}

	private void readJobDetail(HttpServletRequest req, HttpSession sesion){
		SchedulerClase schedulerClase = new SchedulerClase();

		String afecha = req.getParameter("afecha");
		String type = req.getParameter("typeJob");
		ArrayList<ArrayList<SchedulerValue>> result = null;

		if(type != null && type.equals("Pendientes")){
			result = schedulerClase.readJobs(afecha);
			sesion.setAttribute("permiso", "con botones");
		} else if(type != null && type.equals("Ejecutados")){
			result = schedulerClase.readJobsDetail(afecha);
			sesion.setAttribute("permiso", "sin botones");
		}

		sesion.setAttribute("schedulerJobs", result);
		String message = "Resultado de la b�squeda:  " + type + " con fecha del " + afecha;
		sesion.setAttribute("message", message);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGeneral(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGeneral(request, response);
	}

}