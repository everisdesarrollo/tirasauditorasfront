/** Banco Santander Mexicano
 *   Clase ManttoCtgServlet  Servlet
 *   @author Alfredo Resendiz Vargas
 *   @version 1.0
 *   Fecha de creacion : 2 de Febrero del 2006
 *   Responsable : Eloisa Hernandez H
 *   Descripcion : Realiza la funci�n de control de E/S de las consulta de
 * 				  Mantto. de Sucursales Contingentes y Hist�rico de Sucursales
 * 				  Contingentes
 *   Modificacion :
 */

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.santander.contingente.ManCtgClase;
import com.santander.contingente.ManCtgValue;

/**
 * @version 1.0
 * @author
 */
public class ManttoCtgServlet extends HttpServlet {

	/**
	 * @see M�todo vac�o pero necesario
	 */

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGeneral(req, resp);
	}

	/**
	 * @see M�todo vac�o pero necesario
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGeneral(req, resp);
	}

	/**
	 * @see Realiza las conexiones necesarias para para el control del proceso
	 */
	protected void doGeneral(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		ManCtgClase manCtgClase = new ManCtgClase();
		HttpSession sesion = req.getSession();
		sesion.setAttribute("messageOL", "");
		String accion = req.getParameter("accion");
		System.out.println("accion = " + accion);
		String fecha = req.getParameter("fecha");
		String pventa = req.getParameter("pventa");
		String descSistema = req.getParameter("descSistema");
		int seleccion = Integer.parseInt(req.getParameter("seleccion"));

		/**
		 * @see Realiza la Consulta del Mantto. de Sucursales Contingentes: 1)
		 *      Total 2) Por Fecha 3) Por Pto. de Vta. y Fecha
		 */

		if (accion.equals("buscar")) {
			ArrayList resul = null;
			String url = "?seleccion=" + seleccion + "&fecha=" + fecha + "&pventa=" + pventa;
			if (descSistema == null) {
				descSistema = "%";
			} else if (descSistema.trim().equals("")){
				descSistema = "%";
			}
			switch (seleccion) {
			case 0:
				if (fecha.equals("") && pventa.equals("")) {
					resul = manCtgClase.consulTotManCtg(descSistema);
					sesion.setAttribute("consulTotManCtg", resul);
					if (resul.size() > 0)
						resp.sendRedirect("/TiraAuditora/Contingentes/04ImpCtg.jsp" + url);
					else
						resp.sendRedirect("/TiraAuditora/Contingentes/04ConFir.jsp" + url);
				} else {
					if (fecha != null && pventa.equals("")) {
						resul = manCtgClase.consultaFecCtg(fecha, descSistema);
						sesion.setAttribute("consultafecCtg", resul);
						if (resul.size() > 0)
							resp.sendRedirect("/TiraAuditora/Contingentes/04ImpCtg.jsp" + url);
						else
							resp.sendRedirect("/TiraAuditora/Contingentes/04ConFir.jsp" + url);
					} else {
						resul = manCtgClase.consultaManCtg(fecha, pventa, descSistema);
						sesion.setAttribute("consultaManCtg", resul);
						if (resul.size() > 0)
							resp.sendRedirect("/TiraAuditora/Contingentes/04ImpCtg.jsp" + url);
						else
							resp.sendRedirect("/TiraAuditora/Contingentes/04ConFir.jsp" + url);
					}
				}
				break;
			case 1:
				if (fecha != null && pventa.equals("0000")) {
					resul = manCtgClase.histFecCtg(fecha);
					sesion.setAttribute("histfecCtg", resul);
					if (resul.size() > 0)
						resp.sendRedirect("/TiraAuditora/Contingentes/04ImpHis.jsp" + url);
					else
						resp.sendRedirect("/TiraAuditora/Contingentes/04ConFi2.jsp" + url);
				} else {
					resul = manCtgClase.histManCtg(fecha, pventa);
					sesion.setAttribute("historicoCtg", resul);
					resp.sendRedirect("/TiraAuditora/Contingentes/04ImpHis.jsp" + url);
				}
				break;
			default:
				System.out.println("Error desconocido");
			}
		}

		/**
		 * @see Activa la Reactivaci�n Manual y guarda la Descripci�n del
		 *      usuario
		 */

		if (accion.equals("guardar")) {
			String bandera = req.getParameter("bandera");
			String res = "";
			String descuser = req.getParameter("descuser");
			String url = "?fecha=" + fecha + "&pventa=" + pventa + "&descuser="
					+ descuser;
			int cont = 0;
			if (bandera.equals("0")) {
				res = manCtgClase.guardaManCtg(pventa, fecha, descuser, bandera);
				sesion.setAttribute("guardadesc", res);
				resp.sendRedirect("/TiraAuditora/Contingentes/04ResSav.jsp" + url);

			} else {
				if (bandera.equals("1")) {
					ArrayList total = (ArrayList) sesion
							.getAttribute("consultafecCtg");
					if (req.getParameter("pagina") != null) {
						int pag = Integer.parseInt(req.getParameter("pagina"));
						ArrayList datpag = (ArrayList) total.get(pag);
						for (int j = 0; j < datpag.size(); j++) {
							ManCtgValue regvalue = (ManCtgValue) datpag.get(j);
							if (req.getParameter("veri" + j) != null)
								regvalue.set_verificacion(1);
							else
								regvalue.set_verificacion(0);
						}
						datpag = null;
					}
					ArrayList verifica = new ArrayList();
					for (int i = 0; i < total.size(); i++) {
						ArrayList pag = (ArrayList) total.get(i);
						for (int j = 0; j < pag.size(); j++) {
							ManCtgValue reg = (ManCtgValue) pag.get(j);
							verifica.add(reg);
							cont++;
							res = manCtgClase.guardaManCtg(
									reg.get_suc_altair(), reg.get_fecha()
											.substring(0, 10), req
											.getParameter("descuser" + j),
									Integer.toString(reg.get_verificacion()));
							sesion.setAttribute("guardadesc", res);
							resp.sendRedirect("/TiraAuditora/Contingentes/04ResSav.jsp" + url);
						}
					}
				} else {
					ArrayList total = (ArrayList) sesion
							.getAttribute("consulTotManCtg");
					if (req.getParameter("pagina") != null) {
						int pag = Integer.parseInt(req.getParameter("pagina"));
						ArrayList datpag = (ArrayList) total.get(pag);
						for (int j = 0; j < datpag.size(); j++) {
							ManCtgValue regvalue = (ManCtgValue) datpag.get(j);
							if (req.getParameter("veri" + j) != null)
								regvalue.set_verificacion(1);
							else
								regvalue.set_verificacion(0);
						}
						datpag = null;
					}
					ArrayList verifica = new ArrayList();
					for (int i = 0; i < total.size(); i++) {
						ArrayList pag = (ArrayList) total.get(i);
						for (int j = 0; j < pag.size(); j++) {
							ManCtgValue reg = (ManCtgValue) pag.get(j);
							verifica.add(reg);
							cont++;
							res = manCtgClase.guardaManCtg(
									reg.get_suc_altair(), reg.get_fecha()
											.substring(0, 10), req
											.getParameter("descuser" + j),
									Integer.toString(reg.get_verificacion()));
							sesion.setAttribute("guardadesc", res);
							resp.sendRedirect("/TiraAuditora/Contingentes/04ResSav.jsp" + url);
						}
					}
				}
			}
		}

		/**
		 * @see Elimina los regitros de la BD de Sucursales Contingentes
		 */

		if (accion.equals("eliminar")) {
			String bandera = req.getParameter("bandera");
			String res = "";
			String url = "?fecha=" + fecha + "&pventa=" + pventa;
			int cont = 0;
			if (bandera.equals("0")) {
				res = manCtgClase.eliminaManCtg(pventa, fecha, bandera);
				sesion.setAttribute("eliminaRegs", res);
				resp.sendRedirect("/TiraAuditora/Contingentes/04ResEli.jsp" + url);
			} else {
				if (bandera.equals("1")) {
					ArrayList total = (ArrayList) sesion
							.getAttribute("consultafecCtg");
					if (req.getParameter("pagina") != null) {
						int pag = Integer.parseInt(req.getParameter("pagina"));
						ArrayList datpag = (ArrayList) total.get(pag);
						for (int j = 0; j < datpag.size(); j++) {
							ManCtgValue regvalue = (ManCtgValue) datpag.get(j);
							if (req.getParameter("sele" + j) != null)
								regvalue.setElimina(1);
							else
								regvalue.setElimina(0);
						}
						datpag = null;
					}
					ArrayList eliminar = new ArrayList();
					ArrayList todos = new ArrayList();
					for (int i = 0; i < total.size(); i++) {
						ArrayList pag = (ArrayList) total.get(i);
						for (int j = 0; j < pag.size(); j++) {
							ManCtgValue reg = (ManCtgValue) pag.get(j);
							todos.add(reg);
							if (reg.getElimina() == 1) {
								eliminar.add(reg);
								cont++;
								res = manCtgClase.eliminaManCtg(reg
										.get_suc_altair(), reg.get_fecha()
										.substring(0, 10), bandera);
								sesion.setAttribute("eliminaRegs", res);
							}
						}
						if (eliminar.size() == 0) {
							eliminar = todos;
							res = manCtgClase.eliminaFecCtg(fecha);
							sesion.setAttribute("eliminaRegs", res);
							todos = null;
						}
						System.out.println("Total a Eliminar: "
								+ eliminar.size());
						resp.sendRedirect("/TiraAuditora/Contingentes/04ResEli.jsp" + url);
					}
				} else {
					ArrayList total = (ArrayList) sesion
							.getAttribute("consulTotManCtg");
					if (req.getParameter("pagina") != null) {
						int pag = Integer.parseInt(req.getParameter("pagina"));
						ArrayList datpag = (ArrayList) total.get(pag);
						for (int j = 0; j < datpag.size(); j++) {
							ManCtgValue regValue = (ManCtgValue) datpag.get(j);
							if (req.getParameter("sele" + j) != null) {
								regValue.setElimina(1);
							} else {
								regValue.setElimina(0);
							}
						}
						datpag = null;
					}
					ArrayList eliminar = new ArrayList();
					ArrayList todos = new ArrayList();
					for (int i = 0; i < total.size(); i++) {
						ArrayList pag = (ArrayList) total.get(i);
						for (int j = 0; j < pag.size(); j++) {
							ManCtgValue reg = (ManCtgValue) pag.get(j);
							todos.add(reg);
							if (reg.getElimina() == 1) {
								eliminar.add(reg);
								cont++;
								res = manCtgClase.eliminaManCtg(reg
										.get_suc_altair(), reg.get_fecha()
										.substring(0, 10), bandera);
								sesion.setAttribute("eliminaRegs", res);
							}
						}
						if (eliminar.size() == 0) {
							eliminar = todos;
							todos = null;
							res = manCtgClase.eliminaTotCtg();
							sesion.setAttribute("eliminaRegs", res);
						}
						System.out.println("Total a Eliminar+: " + eliminar.size());
						resp.sendRedirect("/TiraAuditora/Contingentes/04ResEli.jsp" + url);
					}
				}
			}
		}

		/**
		 * @see VCM 23/07/12 Extraccion en linea de contingentes
		 */
		if (accion.equals("linea")) {
			String lsNumsuc = req.getParameter("sucursalLinea");
			String fechaCont = req.getParameter("fechaLinea");
			String result = manCtgClase.loadOnLine(lsNumsuc, fechaCont, null, null);
			if (result != null && result.startsWith("0000")){
				result = "Sucursal " + lsNumsuc + " con fecha " + fechaCont + " cargada exitosamente";
			} else {
				result = "Se presento un error, no se pudo realizar la carga";
			}
			sesion.setAttribute("messageOL", result);
			resp.sendRedirect("/TiraAuditora/index.jsp");
		}
	}
}