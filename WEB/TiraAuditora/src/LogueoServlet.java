/** Banco Santander Mexicano
*   Clase logueoServlet  Servlet
*   @author Alfredo Resendiz Vargas
*   @version 1.0
*   Fecha de creacion : 27 de Febrero del 2006
*   Responsable : Eloisa Hernandez H
*   Descripcion : Realiza la funci�n de control de E/S del Logueo de la Aplicaci�n
*   Modificacion :
*/

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.santander.autentificacion.LogueoClase;
import com.santander.autentificacion.LogueoValue;
import com.santander.utilerias.LoadProperties;


public class LogueoServlet extends HttpServlet
{

/**
* @see M�todo vac�o pero necesario
*/

  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
  {
	doGeneral(req, resp);
  }

/**
* @see M�todo vac�o pero necesario
*/
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
  {
	doGeneral(req, resp);
  }

/**
* @see Realiza las conexiones necesarias para para el control del proceso de logueo
*/
  protected void doGeneral(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
  {
	LogueoClase logini_clase = new LogueoClase();
	
	LoadProperties rb = LoadProperties.getInstance();
	String version = rb.getValue("ta.version");
	System.out.println("version = " + version);
	String accion = req.getParameter("accion");
	String error = req.getParameter("error");
	System.out.println("accion = " + accion);
	String user = req.getParameter("usuario");
	String password = req.getParameter("contrasena");
	String url1 = rb.getValue("ta.dirhost");
	String res = "";
	int intentos = 0;
/**
 * Realiza las validaciones del Logueo
 */
	if(accion.equals("enviar"))
	{
	  HttpSession sesion = req.getSession();
	  ArrayList resul = null;
	  String url = "?usuario="+user;
	  long resCaduc = logini_clase.validaCaducidad(user);
	  long dias_permitidos = 45;
	  if(resCaduc >= dias_permitidos)
	  {
		resp.sendRedirect("/TiraAuditora/Menu/01ConFi4.jsp"+url);
	  }
	  else
	  {
		String guardaCaduc = logini_clase.guradaFecCaduc(user);
	  }
	  resul = logini_clase.consultaLogueo(user,password);
	  sesion.setAttribute("consulogueo",resul);
	  if(resul.size()>0)
	  {
		res = logini_clase.autentifica(url1,user,password);
		sesion.setAttribute("autentifica",res);
		int result=Integer.parseInt(res);
		switch(result)
		{
		case 0:
			resp.sendRedirect("/TiraAuditora/Menu/01Tira.jsp");
			break;
		case 1:
			resp.sendRedirect("/TiraAuditora/Menu/01ConFir.jsp"+url);
			break;
		case 2:
			resp.sendRedirect("/TiraAuditora/Menu/01ConFi2.jsp"+url);
			break;
		case 3:
			resp.sendRedirect("/TiraAuditora/Menu/01ConFi3.jsp"+url);
			break;
		case 4:
			resp.sendRedirect("/TiraAuditora/Menu/01ConFi4.jsp"+url);
			break;
		case 5:
			resp.sendRedirect("/TiraAuditora/Menu/01ConFi5.jsp"+url);
			break;
		default:
			resp.sendRedirect("/TiraAuditora/Menu/01ConGra.jsp"+url);
	    }
	  }
	  else
	  {
		res = logini_clase.autentifica(url1,user,password);
		sesion.setAttribute("autentifica",res);
		resp.sendRedirect("/TiraAuditora/Menu/01ConFir.jsp"+url);
	  }
	}
/**
 * Actualiza el password cuando esta expirando
 */

	if(accion.equals("actualizar"))
	{
	  HttpSession sesion = req.getSession();
	  ArrayList total = (ArrayList)sesion.getAttribute("consulogueo");
	  LogueoValue regLogueo = (LogueoValue)total.get(0);
	  String url = "?usuario="+user;
	  String contrase�aNew = req.getParameter("contrasena");
	  String usuario = regLogueo.getusuario();
	  res = logini_clase.actualizaPass(usuario,contrase�aNew);
	  sesion.setAttribute("consulact",res);
	  int result=Integer.parseInt(res);
	  switch(result)
	  {
		case 0:
		    resp.sendRedirect("/TiraAuditora/Menu/01ResSav.jsp"+url);
	 	  break;
		case 1:
		  resp.sendRedirect("/TiraAuditora/Menu/01ConAc1.jsp"+url);
		  break;
		case 2:
		  resp.sendRedirect("/TiraAuditora/Menu/01ResSav.jsp"+url);
		  break;
		case 3:
		  resp.sendRedirect("/TiraAuditora/Menu/01ConGra.jsp"+url);
		default:
		  System.out.println("Error desconocido");
	  }
	}
/**
 * Hace el logoff de la aplicaci�n
 */

	if(accion.equals("cerrar"))
	{
	  HttpSession sesion = req.getSession();
	  sesion.removeAttribute("consulogueo");
	  sesion.removeAttribute("autentifica");
	  sesion.invalidate();
	  resp.sendRedirect("http://altec.mx.bsch/intranet.htm");
	}
  }
}