/** Banco Santander Mexicano
*   Clase TiraServlet  Resuelve el flujo de las operaciones de Tira Auditora
*   @author Ing. David Aguilar G�mez
*   @version 1.0
*   fecha de creacion : 1 de Febrero del 2006
*   responsable : Eloisa Hernandez H
*   descripcion : resuelve el flujo de las operaciones de consulta y creacion PDF de la Tira Auditora.
*   modificacion :
*/
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.santander.tira.TiraClase;
import com.santander.tira.TiraValue;

public class TiraServlet extends HttpServlet
{

/**
 * El m�todo doGet(HttpServletRequest, HttpServletResponse) entrada por Get.
 *
 * @param req  Valor Request
 * @param resp Valor Response
 * @throws ServletException en caso de que suceda una servlet exception
 * @throws IOException Se�ala que ocurrio una excepcion de tipo I/O.
 */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		doGeneral(req, resp);
	}

	/**
	 * El m�todo doPost(HttpServletRequest, HttpServletResponse) entrada por Post.
	 *
	 * @param req  Valor Request
	 * @param resp Valor Response
	 * @throws ServletException en caso de que suceda una servlet exception
	 * @throws IOException Se�ala que ocurrio una excepcion de tipo I/O.
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		doGeneral(req, resp);
	}

	/**
	 * El m�todo doGeneral(HttpServletRequest, HttpServletResponse) Control de flujo del modulo.
	 *
	 * @param req  Valor Request
	 * @param resp Valor Response
	 * @throws ServletException en caso de que suceda una servlet exception
	 * @throws IOException Se�ala que ocurrio una excepcion de tipo I/O.
	 */
	protected void doGeneral(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		TiraClase tira_clase = new TiraClase();
		String accion = req.getParameter("accion");
		System.out.println("accion = " + accion);
		if("buscar".equals(accion))
		{
			HttpSession sesion = req.getSession();
			ArrayList resul = null;
			String fecha = req.getParameter("fecha");
			String pventa = req.getParameter("pventa");
			String nomsuc = req.getParameter("nomsuc");
			String origen = req.getParameter("cboOrigen");
			int seleccion = Integer.parseInt(req.getParameter("seleccion"));
			StringBuilder url = new StringBuilder("?seleccion="+seleccion+"&fecha="+fecha+"&pventa="+pventa+"&nomsuc="+nomsuc+"&origen="+origen);
			// Diferentes opciones del proceso de la consulta
			switch(seleccion)
			{
				case 0:
					// Consulta general por fecha y punto de venta
					resul = tira_clase.consultaTira(fecha, pventa,origen);
				break;
				case 1:
					// Consulta general por fecha, punto de venta y clave de operacion
					String toperacion = req.getParameter("cbtoperacion");
					System.out.println("toperacion = " + toperacion);
					String dscoperacion = req.getParameter("dscoperacion");
					System.out.println("dscoperacion = " + dscoperacion);
					url.append("&toperacion="+dscoperacion);
				 	resul = tira_clase.consultaTiraTipo(fecha, pventa, toperacion, origen);
				break;
				case 2:
					// Consulta general por fecha, punto de venta y numero de operacion
					String nooperacion = req.getParameter("nooperacion");
					System.out.println("nooperacion = " + nooperacion);
					url.append("&nooperacion="+nooperacion);
				 	resul = tira_clase.consultaTiraOperacion(fecha, pventa, nooperacion,origen);
				break;
				case 3:
					// Consulta general por fecha, punto de venta y numero de cuenta
					String cuenta1 = req.getParameter("cuenta1");
					System.out.println("cuenta1 = " + cuenta1);
					String cuenta2 = req.getParameter("cuenta2");
					System.out.println("cuenta2 = " + cuenta2);
					url.append("&cuenta="+cuenta1+cuenta2);
				 	resul = tira_clase.consultaTiraCuenta(fecha, pventa, cuenta1+cuenta2, origen);
				break;
				case 4:
					// Consulta general por fecha, punto de venta y usuario
					String usuario = req.getParameter("usuario");
					System.out.println("usuario = " + usuario);
					url.append("&usuario="+usuario);
				 	resul = tira_clase.consultaTiraUsuario(fecha, pventa, usuario, origen);
				break;
				case 5:
					// Consulta general por fecha, punto de venta y hora de inicio y fin
					String horaini = req.getParameter("horaini")+":"+req.getParameter("minutoini");
					System.out.println("horaini = " + horaini);
					String horafin = req.getParameter("horafin")+":"+req.getParameter("minutofin");
					System.out.println("horafin = " + horafin);
					url.append("&horaini="+horaini+"&horafin="+horafin);
				 	resul = tira_clase.consultaTiraHora(fecha, pventa, horaini, horafin, origen);
				break;
				case 6:
					// Consulta general por fecha, punto de venta y importe de inicio y fin
					String importeini = req.getParameter("importeini");
					System.out.println("importeini = " + importeini);
					String importefin = req.getParameter("importefin");
					System.out.println("importefin = " + importefin);
					url.append("&importeini="+importeini+"&importefin="+importefin);
				 	resul = tira_clase.consultaTiraImporte(fecha, pventa, importeini, importefin, origen);
				break;
				case 7:
					// Consulta por identificador de ATS
					String idats = req.getParameter("cboIdAts");
					url.append("&idats="+idats);
				 	resul = tira_clase.consultaTiraIdAts(fecha, pventa, idats, origen);
				break;
				default:
					// En esta opcion nunca entrara.
					System.out.println("No es selecci�n valida.");
				break;
			}
			sesion.setAttribute("consultira",resul);
			resp.sendRedirect("/TiraAuditora/Tira/02ImpCon.jsp"+url);
		}
		if("imprimir".equals(accion))
		{
			int seleccion = Integer.parseInt(req.getParameter("seleccion"));
			HttpSession sesion = req.getSession();
			ArrayList total = (ArrayList)sesion.getAttribute("consultira");
			// Verifica los seleccionados de la pagina que ejecuto la impresion
			if(req.getParameter("pagina")!=null)
			{
			  int pag = Integer.parseInt(req.getParameter("pagina"));
			  ArrayList datpag = (ArrayList)total.get(pag);
			  for(int j=0;j<datpag.size();j++)
			  {
				  TiraValue tira = (TiraValue)datpag.get(j);
				  if(req.getParameter("sele"+j)!=null){
					  tira.setImpresion(1);
				  }
				  else{
					  tira.setImpresion(0);
				  }
			  }
			  datpag = null;
			}
			// Verifica que registros se seleccionaron o si no todos.
			ArrayList impresion = new ArrayList();
			ArrayList todos = new ArrayList();
			for(int i=0;i<total.size();i++)
			{
				ArrayList pag = (ArrayList)total.get(i);
				for(int j=0;j<pag.size();j++)
				{
					TiraValue reg = (TiraValue)pag.get(j);
					todos.add(reg);
					if(reg.getImpresion()==1)
					{
						impresion.add(reg);
					}
				}
			}
			if(impresion.isEmpty())
			{
				impresion = todos;
				todos = null;
			}
			System.out.println("Total a Imprimir: "+impresion.size());
			String dato1 = "";
			String dato2 = "";
			String encabeza = req.getParameter("pventa")+"-"+req.getParameter("nomsuc")+" "+req.getParameter("fecha");
			String origen = req.getParameter("origen");
			// Segun el caso de busqueda debo de leer los parametros para encabezado de reporte
			switch(seleccion)
			{
				case 0:
					dato1 = req.getParameter("pventa");
					dato2 = req.getParameter("fecha");
				break;
				case 1:
					dato1 = req.getParameter("toperacion");
				break;
				case 2:
					dato1 = req.getParameter("nooperacion");
				break;
				case 3:
					dato1 = req.getParameter("cuenta");
				break;
				case 4:
					dato1 = req.getParameter("usuario");
				break;
				case 5:
					dato1 = req.getParameter("horaini");
					dato2 = req.getParameter("horafin");
				break;
				case 6:
					dato1 = req.getParameter("importeini");
					dato2 = req.getParameter("importefin");
				break;
				case 7:
					dato1 = req.getParameter("idats");
				break;
				default:
					System.out.println("No es selecci�n valida.");
				break;
			}
			ByteArrayOutputStream baos = tira_clase.makePdf(seleccion,impresion,dato1,dato2, encabeza, origen);
			// setting some response headers
			resp.setHeader("Expires", "0");
			resp.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
			resp.setHeader("Pragma", "public");
			// setting the content type
			resp.setContentType("application/pdf");
			// the contentlength is needed for MSIE!!!
			resp.setContentLength(baos.size());
			// write ByteArrayOutputStream to the ServletOutputStream
			ServletOutputStream out = resp.getOutputStream();
			baos.writeTo(out);
			out.flush();
		}
		if("ajax".equals(accion)){
			String operacion = req.getParameter("operacion");

			if("getIdsAts".equalsIgnoreCase(operacion)){
				String idSuc = req.getParameter("idSuc");
				List<TiraValue> resul = null;
				resul = tira_clase.consultaClaveCustodio(idSuc);

				resp.setContentType("text/xml");
				resp.setHeader("Cache-Control", "no-cache");
				resp.getWriter().write("<response>");
				for(int i=0; i<resul.size();i++){
				    resp.getWriter().write("<item>");
				    resp.getWriter().write("<id>");
				    resp.getWriter().write(resul.get(i).getClaveCustodio());
				    resp.getWriter().write("</id>");
				    resp.getWriter().write("</item>");
				}
			    resp.getWriter().write("</response>");

			}

		}
	}
}