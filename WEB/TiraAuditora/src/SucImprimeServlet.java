/** Banco Santander Mexicano
*   Clase TiraServlet  Resuelve el flujo de las operaciones de Tira Auditora
*   @author Ing. David Aguilar G�mez / Angel Ramirez (modifico)
*   @version 1.0
*   fecha de creacion : 3 de Marzo del 2006
*   responsable : Eloisa Hernandez H
*   descripcion : resuelve la Impresi�n y creacion PDF de las Sucursales
*   modificacion :Angel Gabriel Ramirez Alva
*/
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.santander.sucursal.SucImpClase;

public class SucImprimeServlet extends HttpServlet
  {
    Properties propiedad;

	/**
	* @see javax.servlet.http.HttpServlet#void (javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	*/
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	  {
		doGeneral(req, resp);
	  }

	/**
	* @see javax.servlet.http.HttpServlet#void (javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	*/
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	  {
		doGeneral(req, resp);
	  }

	/**
	* @see javax.servlet.http.HttpServlet#void (javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	*/
	protected void doGeneral(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	  {
		// Aqui empieza la clase
  	    SucImpClase impclase = new SucImpClase();
	    String accion = req.getParameter("accion");
	    System.out.println("accion = " + accion);



		if(accion.equals("imprimir"))
		  {
		    System.out.println("Ingresa al metodo para imprimir Sucursales");
		    int seleccion = Integer.parseInt(req.getParameter("seleccion"));
                    System.out.println("Despues del selecci�n: "+ seleccion);
		    String valor = req.getParameter("lsDatos");
		    System.out.println("valor: "+valor);
		    Vector vPdf = new Vector();
		    /*****************************/
		    String limit = "|";
		    StringTokenizer	strToken_value= new StringTokenizer(valor, limit );
		    int iCont = 0;
		    while(strToken_value.hasMoreElements())
		      {
			iCont++;
			String tmpVar= (strToken_value.nextToken()).trim();
			vPdf.add(tmpVar);
			System.out.println(tmpVar);
		      }
		    System.out.println("Total a Imprimir: ");
		    String dato1 = "";
		    String dato2 = "";
		    //Se llama al metodo makePdf de la clase SucImpClase para creaci�n del archivo pdf
		    ByteArrayOutputStream baos = impclase.makePdf(propiedad, seleccion,vPdf,dato1,dato2);
		    // setting some response headers
		    resp.setHeader("Expires", "0");
		    resp.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		    resp.setHeader("Pragma", "public");
		    // setting the content type
		    resp.setContentType("application/pdf");
		    // the contentlength is needed for MSIE!!!
		    resp.setContentLength(baos.size());
		    // write ByteArrayOutputStream to the ServletOutputStream
		    ServletOutputStream out = resp.getOutputStream();
		    baos.writeTo(out);
		    out.flush();
		  }
	 }
  }